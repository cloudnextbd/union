<style type="text/css">
    thead tr th {
        color: #999 !important;
    }
</style>
<!-- Info Content-->
<div class="content_info txt-color">
    <div class="padding-top">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <table id="myTable" class="display nowrap table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>সদস্য নং </th>
                                <th>ছবি</th>
                                <th>নাম</th>
                                <th>ঠিকানা</th>
                                <th>NID</th>
                                <th>সেবার ধরন</th>
                                <th>Status 1</th>
                                <th>Status 2</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>সদস্য নং </th>
                                <th>ছবি</th>
                                <th>নাম</th>
                                <th>ঠিকানা</th>
                                <th>NID</th>
                                <th>সেবার ধরন</th>
                                <th>Application Status</th>
                                <th>service Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach($lists as $list) : ?>
                                <tr>
                                    <td><?=$list->id?></td>
                                    <td><a href="<?= site_url('home/view_member'); ?>"><img src="<?=base_url()?>assets/app_img/<?=$list->image?>"/></a></td>
                                    <td><?=$list->name?></td>
                                    <td><?=$list->house_name?></td>
                                    <td><?=$list->nid?></td>
                                    <td>nai</td>
                                    <td>
                                        <button type="button" class="btn btn-warning btn-xs btn-block">Pending</button>
                                        <button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
                                        <button type="button" class="btn btn-danger btn-xs btn-block">Refused</button>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs btn-block">Active</button>
                                        <button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="...">
                                            <button type="button" class="btn btn-xs btn-success" href="asdfas"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                            <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                            <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>