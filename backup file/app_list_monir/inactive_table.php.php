
<style type="text/css">
	thead tr th {
		color: #999 !important;
	}
</style>
<!-- Info Content-->
<div class="content_info txt-color">
	<div class="padding-top">
		<!-- Container Area - Boxes Services -->
		<div class="container">
			<div class="row">

				<div class="col-md-12">

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Add new
</button>
<br>
<br>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header label-success">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">

                    <form class="add-new-form form-horizontal" id="validation-form" method="get">
                        <!-- mem add -->
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">সদস্য নংঃ </label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-4" type="text" name="" id="email" value="ABC-123" placeholder="" readonly />
                                </div>
                            </div>
                        </div>

                        <!-- name -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">নামঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-8" type="text" name="" id="" placeholder="" />
                                </div>
                            </div>
                        </div>

                        <!-- gender -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">লিঙ্গঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="radio">
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> Male</span>
                                    </label>
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> Female</span>
                                    </label>
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> Others</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <!-- birth date -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="password2">জন্ম তারিখঃ:</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                <div class="input-group">
                                    <input class="form-control col-xs-12 col-sm-8 date-picker" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar bigger-110"></i>
                                    </span>
                                </div>  
                                </div>  
                            </div>
                        </div>

                        <!-- birth id no -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">জন্ম নিবন্ধন নংঃ</label>

                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-8" type="text" name="" id="" placeholder="" />
                                </div>
                            </div>
                        </div>

                        <!-- nid -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">জাতীয় পরিচয় পত্র নংঃ</label>

                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-8" type="text" name="" id="" placeholder="" />
                                </div>
                            </div>
                        </div>

                        <!-- phone -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">মোবাইল নংঃ</label>

                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-6" type="text" name="" id="" placeholder="" />
                                </div>
                            </div>
                        </div>

                        <div class="hr hr-dotted"></div>

                        <!-- state -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="platform">জেলাঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <select class="input-large" id="platform" name="platform">
                                        <option value="">চাঁদপুর</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <!-- sub state -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="platform">উপজেলাঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <select class="input-large" id="platform" name="platform">
                                        <option value="">চাঁদপুর সদর </option>
                                        <option value="">হাইমচর</option>
                                        <option value="">কচুয়া</option>
                                        <option value="">শাহরাস্তি</option>
                                        <option value="">চাঁদপুর সদর</option>
                                        <option value="">মতলব দক্ষিণ</option>
                                        <option value="">হাজীগঞ্জ</option>
                                        <option value="">মতলব উত্তর</option>
                                        <option value="">ফরিদগঞ্জ </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- union -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="platform">ইউনিয়নঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <select class="input-large" id="platform" name="platform">
                                        <option value="">বাগাদী</option>
                                        <option value="">বিষ্ণপুর</option>
                                        <option value="">আশিকাটি </option>
                                        <option value="">শাহ্‌ মাহমুদপুর</option>
                                        <option value="">কল্যাণপুর </option>

                                        <option value="">রামপুর</option>
                                        <option value="">মৈশাদী</option>
                                        <option value="">তরপুচন্ডী</option>
                                        <option value="">বাগাদী</option>
                                        <option value="">লক্ষীপুর মডেল</option>

                                        <option value="">হানারচর</option>
                                        <option value="">চান্দ্রা</option>
                                        <option value="">রাজরাজেশ্বর</option>
                                        <option value="">ইব্রাহীমপুর</option>
                                        <option value="">বালিয়া</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <!-- word no -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="platform">ওয়ার্ড নংঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <select class="input-large" id="platform" name="platform">
                                        <option >নির্বাচন করুন</option>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value=""></option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                        <option value="">9</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- village -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="platform">গ্রামের নামঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <select class="input-large" id="platform" name="platform">
                                        <option >নির্বাচন করুন</option>
                                        <option value="">রাড়িরচর ও চান্দের বাগ</option>
                                        <option value="">মনিহার</option>
                                        <option value="">দেবপুর</option>
                                        <option value="">চরবাকিলা ও ছোটসুন্দর</option>
                                        <option value="">আলগী</option>

                                        <option value="">কামরাঙ্গা</option>
                                        <option value="">বড়সুন্দর ও পাঁচ বাড়িয়া</option>
                                        <option value="">রামপুর</option>
                                        <option value="">সকদী পাঁচ গাঁও</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- house no -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">বাড়ির নামঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-4" type="text" name="" id="" placeholder="" />
                                </div>
                            </div>
                        </div>

                        <!-- holding no -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">হোল্ডিং নংঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-4" type="text" name="" id="" placeholder="" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="hr hr-dotted"></div>

                        <!-- profession -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">পেশাঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-4" type="text" name="" id="" placeholder="" />
                                </div>
                            </div>
                        </div>

                        <!-- physical problem -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">অসামর্থ্যতা/প্রতিবন্ধী প্রকৃতিঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="radio">
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> প্রযোজ্য নয়</span>
                                    </label>
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> দৃষ্টি প্রতিবন্ধী</span>
                                    </label>
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> শারীরিক প্রতিবন্ধী</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <!-- religion -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right">ধর্মঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="radio">
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> ইসলাম</span>
                                    </label>
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> হিন্দু</span>
                                    </label>
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> খ্রিষ্টান</span>
                                    </label>
                                    <label>
                                        <input name="form-field-radio" type="radio" class="ace">
                                        <span class="lbl"> বৌদ্ধ</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <!-- reference -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">সুপারিশ কারীর নাম(কমপক্ষে তিনজন)ঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-12" type="text" name="" id="" placeholder="" />
                                </div>
                            </div>
                        </div>

                        <!-- image -->
                        <div class="space-2"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">ছবিঃ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input class="col-xs-12 col-sm-12" type="file" name="" id="" />
                                </div>
                            </div>
                        </div>

                    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

					<table id="myTable" class="display nowrap table table-bordered table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>সদস্য নং </th>
								<th>ছবি</th>
								<th>নাম</th>
								<th>ঠিকানা</th>
								<th>NID</th>
								<th>সেবার ধরন</th>
								<th>Status 1</th>
								<th>Status 2</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>সদস্য নং </th>
								<th>ছবি</th>
								<th>নাম</th>
								<th>ঠিকানা</th>
								<th>NID</th>
								<th>সেবার ধরন</th>
								<th>Status 1</th>
								<th>Status 2</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							<tr>
                                <td>1</td>
                                <td><a href="<?=site_url('home/view_member');?>"><img src="http://localhost/union/assets/front/img/p1.jpg"/></a></td>
								<td>মোঃ মনসুর আহমেদ</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>১৯১৪০৯৫০৯৫৭৮৮</td>
								<td>ভিজিএফ</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td><img src="http://localhost/union/assets/front/img/p2.jpg"/></td>
								<td>হালিমা খাতুন</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>১৯১৪০৯৫০৯৫০৯৫</td>
								<td>ভিজিএফ</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td><img src="http://localhost/union/assets/front/img/p3.jpg"/></td>
								<td>জয়দল নেছা</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>১৯১৪০৯৫০৯৫৭৮৭</td>
								<td>ভিজিএফ</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							<tr>
								<td>4</td>
								<td><img src="http://localhost/union/assets/front/img/p4.jpg"/></td>
								<td>আমির হোসেন</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>১৯১৪০৯৫০৯৫৩৬৪</td>
								<td>ভিজিএফ</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td><img src="http://localhost/union/assets/front/img/p5.jpg"/></td>
								<td>লতিফা খাতুন</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>১৯১৪০৯৫০৯৪৯০২</td>
								<td>মাতৃত্বকালীন ভাতা </td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							<tr>
								<td>6</td>
								<td><img src="http://localhost/union/assets/front/img/p6.jpg"/></td>
								<td>বজলুর রহমান</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>০১৯১৪০৯৫০৯৪৫৮</td>
								<td>বয়স্ক ভাতা</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							<tr>
								<td>7</td>
								<td><img src="http://localhost/union/assets/front/img/p7.jpg"/></td>
								<td>হোসেনেয়ারা</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>১৯১৪০৯৫০৯৪৮৯৯</td>
								<td>বয়স্ক ভাতা</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							<tr>
								<td>8</td>
								<td><img src="http://localhost/union/assets/front/img/p8.jpg"/></td>
								<td>রাসিদা খাতুন</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>১৯১৪০৯৫০৯৭১৩০</td>
								<td>মাতৃত্বকালীন ভাতা </td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							<tr>
								<td>9</td>
								<td><img src="http://localhost/union/assets/front/img/p9.jpg"/></td>
								<td>নমিতা রানী</td>
								<td>ওয়াহেদপুর, বাগাদী ইউনিয়ন</td>
								<td>১৯১৪০৯৫০৯৭১৩১</td>
								<td>প্রতিবন্ধী ভাতা</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							
							<tr>
								<td>Jena Gaines</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Active</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Inactive</i></button>
								</td>
								<td>
									<button type="button" class="btn btn-success btn-xs btn-block">Pending...</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Approved</button>
									<button type="button" class="btn btn-success btn-xs btn-block">Refused</button>
								</td>
								<td>
									<div class="btn-group" role="group" aria-label="...">
									  <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
									  <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</div>
								</td>
							</tr>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


