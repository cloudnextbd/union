-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2017 at 02:40 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `union`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '60 digit',
  `token` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `level` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `IP` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `first_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `phn` varchar(30) CHARACTER SET utf8 NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'default.png',
  `joined_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `online_timestamp` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `token`, `level`, `status`, `IP`, `first_name`, `last_name`, `phn`, `address`, `avatar`, `joined_date`, `online_timestamp`) VALUES
(1, 'admin@test.com', '$2a$12$194Ikhvx5AZ9.HijXdjutu2DtnItiLCeo9X8jeEkn.uTh6VHN6j.S', 'be57f3349ef424357470eaa82eb39697', 2, 1, '', 'Super', 'Admin', '0123456789', 'Dhaka, Bangladesh', 'default.png', '2016-09-27 05:30:48', '1494245075');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
`id` int(11) NOT NULL,
  `short_name` varchar(30) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(7, '001', 'চাঁদপুর সদর', 0, '2017-05-06 10:12:54', 0, '0000-00-00 00:00:00', 0),
(8, '002', 'হাইমচর', 0, '2017-05-06 10:13:03', 0, '0000-00-00 00:00:00', 0),
(9, '003', 'কচুয়া', 0, '2017-05-06 10:13:08', 0, '0000-00-00 00:00:00', 1),
(10, '004', 'শাহরাস্তি', 0, '2017-05-06 10:13:17', 0, '0000-00-00 00:00:00', 0),
(11, '005', 'মতলব দক্ষিণ', 0, '2017-05-06 10:13:23', 0, '0000-00-00 00:00:00', 0),
(12, '006', 'হাজীগঞ্জ', 0, '2017-05-06 10:13:27', 0, '0000-00-00 00:00:00', 0),
(13, '007', 'মতলব উত্তর', 0, '2017-05-06 10:13:32', 0, '0000-00-00 00:00:00', 0),
(14, '008', 'ফরিদগঞ্জ', 0, '2017-05-06 10:13:46', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `front_set`
--

CREATE TABLE IF NOT EXISTS `front_set` (
`id` int(11) NOT NULL,
  `logo` varchar(128) NOT NULL,
  `add` text NOT NULL,
  `mail1` varchar(512) NOT NULL,
  `mail2` varchar(512) NOT NULL,
  `phn1` varchar(50) NOT NULL,
  `phn2` varchar(50) NOT NULL,
  `eme_phn` varchar(50) NOT NULL,
  `appoint_phn` varchar(50) NOT NULL,
  `sun_thu` varchar(256) NOT NULL,
  `fri` varchar(256) NOT NULL,
  `sat` varchar(256) NOT NULL,
  `fb` varchar(256) NOT NULL,
  `twt` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL,
  `copyright` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `front_set`
--

INSERT INTO `front_set` (`id`, `logo`, `add`, `mail1`, `mail2`, `phn1`, `phn2`, `eme_phn`, `appoint_phn`, `sun_thu`, `fri`, `sat`, `fb`, `twt`, `link`, `copyright`) VALUES
(1, '8891a2789a101d6c42d32d4e034ec215.png', '1234 Street Name, City Name', 'first_mail@example.com', 'second_mail@example.com', '(800) 123-4567', '(800) 987-4321', '(800) 999-9999', ' +1 874 801 8014', ' 8:30 am to 5:00 pm', 'Closed', '9:30 am to 1:00 pm', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com/', '© Copyright 2017. All Rights Reserved.');

-- --------------------------------------------------------

--
-- Table structure for table `member_personal`
--

CREATE TABLE IF NOT EXISTS `member_personal` (
`id` int(11) NOT NULL,
  `mem_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `birth_date` varchar(30) NOT NULL,
  `birth_id` varchar(30) NOT NULL,
  `nid` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `district` varchar(100) NOT NULL,
  `sub_district` varchar(100) NOT NULL,
  `union` varchar(100) NOT NULL,
  `word_no` varchar(30) NOT NULL,
  `village` varchar(100) NOT NULL,
  `house_name` varchar(100) NOT NULL,
  `holdings_no` varchar(30) NOT NULL,
  `education` varchar(100) NOT NULL,
  `profession` varchar(100) NOT NULL,
  `disability` varchar(30) NOT NULL,
  `religion` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_personal`
--

INSERT INTO `member_personal` (`id`, `mem_id`, `name`, `gender`, `birth_date`, `birth_id`, `nid`, `mobile`, `district`, `sub_district`, `union`, `word_no`, `village`, `house_name`, `holdings_no`, `education`, `profession`, `disability`, `religion`) VALUES
(1, 75, 'test name', 'male', '11-11-2017', '321', '123', '01672552966', 'borishal', 'vola', 'bagadi', '25', 'village name', 'molla bari', '25 no', 's.s.c', 'krishi', 'no', 'islam');

-- --------------------------------------------------------

--
-- Table structure for table `sub_district`
--

CREATE TABLE IF NOT EXISTS `sub_district` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_district`
--

INSERT INTO `sub_district` (`id`, `parent_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(12, '001', '01', 'বাগাদী', 0, '2017-05-06 10:39:07', 0, '0000-00-00 00:00:00', 1),
(13, '001', '02', 'বিষ্ণপুর', 0, '2017-05-06 10:19:33', 0, '0000-00-00 00:00:00', 0),
(14, '001', '03', 'আশিকাটি', 0, '2017-05-06 10:19:24', 0, '0000-00-00 00:00:00', 0),
(16, '002', '05', 'কল্যাণপুর', 0, '2017-05-06 10:20:02', 0, '0000-00-00 00:00:00', 0),
(17, '002', '04', 'শাহ্‌ মাহমুদপুর', 0, '2017-05-06 10:22:10', 0, '0000-00-00 00:00:00', 0),
(18, '002', '06', 'রামপুর', 0, '2017-05-06 10:22:26', 0, '0000-00-00 00:00:00', 0),
(19, '003', '07', 'মৈশাদী', 0, '2017-05-06 10:22:41', 0, '0000-00-00 00:00:00', 0),
(20, '003', '08', 'তরপুচন্ডী', 0, '2017-05-06 10:22:56', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `union`
--

CREATE TABLE IF NOT EXISTS `union` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `union`
--

INSERT INTO `union` (`id`, `parent_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(8, '01', '01', 'union 01', 0, '2017-05-06 11:58:03', 0, '0000-00-00 00:00:00', 1),
(9, '01', '02', 'union 02', 0, '2017-05-06 10:52:10', 0, '0000-00-00 00:00:00', 0),
(10, '02', '03', 'union 03', 0, '2017-05-06 10:52:19', 0, '0000-00-00 00:00:00', 0),
(11, '02', '04', 'union 04', 0, '2017-05-06 10:52:37', 0, '0000-00-00 00:00:00', 0),
(12, '03', '05', 'union 05', 0, '2017-05-06 10:52:49', 0, '0000-00-00 00:00:00', 0),
(13, '03', '06', 'union 06', 0, '2017-05-06 10:53:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `password` varchar(70) NOT NULL COMMENT '60 digit',
  `address` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `role` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `union` varchar(30) NOT NULL,
  `word` varchar(30) DEFAULT NULL,
  `token` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `online_timestamp` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `password`, `address`, `image`, `about`, `role`, `status`, `union`, `word`, `token`, `create_date`, `online_timestamp`) VALUES
(14, 'Mostafijur Rahman', '01672552966', '$2a$12$ImKGwSz3nrukXIDy31dNNuj3QDeC5eesaNyEsbvjeArzoohC.kYAq', 'test address', '87277b12b557a9f8eea1498e43d34e82.jpg', 'about user', 0, 1, '', 'selected', '', '2017-05-07 13:25:48', ''),
(15, 'Monir Ahmed', '01914088503', '$2a$12$9kBMxGmAYdgehE.rSpOg.eGFwlMn9.85Zcy8kDMikYRQiJTo7wrnm', 'Monir vai address', 'ba4d6da636a3d05ca247cb53396070d1.jpg', 'about monir vai', 1, 0, '', 'selected', '', '2017-05-07 13:27:15', ''),
(16, 'Mahmudul Hasan', '123456789', '$2a$12$XmtvaNdKivwflpCCfMlX3uhD3iOrP/FO4TI99tfYsDIJgSPnTcjka', 'Address of mahmudul', '53b202bb1cd6b9a22ee065feeae1a873.jpg', 'about mahmudul', 2, 1, '', 'selected', '', '2017-05-07 13:28:09', ''),
(17, 'aaaaaaaaaa', '123', '$2a$12$86Vrd.3QQ0aOYQqdMuX3ge2zSPyCCG.flz.Z8h/Ed7Ltf7dR9cE92', 'aaaaaaaaaaa', '8b93456f8e835e1d0851ab545a257b22.jpg', 'aaaaaaaaaa', 1, 1, '', 'selected', '', '2017-05-08 12:04:58', '');

-- --------------------------------------------------------

--
-- Table structure for table `village`
--

CREATE TABLE IF NOT EXISTS `village` (
`id` int(11) NOT NULL,
  `parent_word_id` varchar(30) NOT NULL,
  `parent_union_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `village`
--

INSERT INTO `village` (`id`, `parent_word_id`, `parent_union_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(5, '1', '02', '01', 'village 01', 0, '2017-05-06 11:56:37', 0, '0000-00-00 00:00:00'),
(6, '2', '04', '02', 'Village 02', 0, '2017-05-06 11:56:49', 0, '0000-00-00 00:00:00'),
(7, '2', '01', '03', 'village 03', 0, '2017-05-06 11:57:03', 0, '0000-00-00 00:00:00'),
(8, '2', '02', '04', 'village 04', 0, '2017-05-06 11:57:14', 0, '0000-00-00 00:00:00'),
(9, '1', '01', '05', 'village for union 01', 0, '2017-05-06 11:58:47', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `word`
--

CREATE TABLE IF NOT EXISTS `word` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `name` varchar(20) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `word`
--

INSERT INTO `word` (`id`, `parent_id`, `name`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(5, '01', '1', 0, '2017-05-06 11:16:35', 0, '0000-00-00 00:00:00'),
(6, '01', '2', 0, '2017-05-06 11:16:38', 0, '0000-00-00 00:00:00'),
(7, '01', '3', 0, '2017-05-06 11:16:41', 0, '0000-00-00 00:00:00'),
(8, '01', '4', 0, '2017-05-06 11:16:43', 0, '0000-00-00 00:00:00'),
(9, '01', '5', 0, '2017-05-06 11:16:46', 0, '0000-00-00 00:00:00'),
(10, '02', '1', 0, '2017-05-06 11:16:51', 0, '0000-00-00 00:00:00'),
(11, '02', '2', 0, '2017-05-06 11:16:55', 0, '0000-00-00 00:00:00'),
(12, '02', '3', 0, '2017-05-06 11:16:59', 0, '0000-00-00 00:00:00'),
(13, '02', '4', 0, '2017-05-06 11:17:02', 0, '0000-00-00 00:00:00'),
(14, '02', '5', 0, '2017-05-06 11:17:05', 0, '0000-00-00 00:00:00'),
(15, '03', '1', 0, '2017-05-06 11:17:13', 0, '0000-00-00 00:00:00'),
(16, '03', '2', 0, '2017-05-06 11:17:17', 0, '0000-00-00 00:00:00'),
(17, '03', '3', 0, '2017-05-06 11:17:22', 0, '0000-00-00 00:00:00'),
(18, '03', '4', 0, '2017-05-06 11:17:31', 0, '0000-00-00 00:00:00'),
(19, '03', '5', 0, '2017-05-06 11:17:36', 0, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_set`
--
ALTER TABLE `front_set`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_personal`
--
ALTER TABLE `member_personal`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_district`
--
ALTER TABLE `sub_district`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `union`
--
ALTER TABLE `union`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `village`
--
ALTER TABLE `village`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `word`
--
ALTER TABLE `word`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `front_set`
--
ALTER TABLE `front_set`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_personal`
--
ALTER TABLE `member_personal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sub_district`
--
ALTER TABLE `sub_district`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `union`
--
ALTER TABLE `union`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `village`
--
ALTER TABLE `village`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `word`
--
ALTER TABLE `word`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
