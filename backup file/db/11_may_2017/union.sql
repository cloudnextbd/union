-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2017 at 12:52 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `union`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '60 digit',
  `token` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `level` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `IP` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `first_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `phn` varchar(30) CHARACTER SET utf8 NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'default.png',
  `joined_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `online_timestamp` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `token`, `level`, `status`, `IP`, `first_name`, `last_name`, `phn`, `address`, `avatar`, `joined_date`, `online_timestamp`) VALUES
(1, 'admin@test.com', '$2a$12$194Ikhvx5AZ9.HijXdjutu2DtnItiLCeo9X8jeEkn.uTh6VHN6j.S', 'f16fba7fad19706240b4907fa9b70791', 2, 1, '', 'Super', 'Admin', '0123456789', 'Dhaka, Bangladesh', 'default.png', '2016-09-27 05:30:48', '1494400687');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
`id` int(11) NOT NULL,
  `short_name` varchar(30) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(7, '001', 'চাঁদপুর সদর', 0, '2017-05-06 10:12:54', 0, '0000-00-00 00:00:00', 0),
(8, '002', 'হাইমচর', 0, '2017-05-06 10:13:03', 0, '0000-00-00 00:00:00', 0),
(9, '003', 'কচুয়া', 0, '2017-05-06 10:13:08', 0, '0000-00-00 00:00:00', 1),
(10, '004', 'শাহরাস্তি', 0, '2017-05-06 10:13:17', 0, '0000-00-00 00:00:00', 0),
(11, '005', 'মতলব দক্ষিণ', 0, '2017-05-06 10:13:23', 0, '0000-00-00 00:00:00', 0),
(12, '006', 'হাজীগঞ্জ', 0, '2017-05-06 10:13:27', 0, '0000-00-00 00:00:00', 0),
(13, '007', 'মতলব উত্তর', 0, '2017-05-06 10:13:32', 0, '0000-00-00 00:00:00', 0),
(14, '008', 'ফরিদগঞ্জ', 0, '2017-05-06 10:13:46', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `front_set`
--

CREATE TABLE IF NOT EXISTS `front_set` (
`id` int(11) NOT NULL,
  `logo` varchar(128) NOT NULL,
  `add` text NOT NULL,
  `mail1` varchar(512) NOT NULL,
  `mail2` varchar(512) NOT NULL,
  `phn1` varchar(50) NOT NULL,
  `phn2` varchar(50) NOT NULL,
  `eme_phn` varchar(50) NOT NULL,
  `appoint_phn` varchar(50) NOT NULL,
  `sun_thu` varchar(256) NOT NULL,
  `fri` varchar(256) NOT NULL,
  `sat` varchar(256) NOT NULL,
  `fb` varchar(256) NOT NULL,
  `twt` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL,
  `copyright` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `front_set`
--

INSERT INTO `front_set` (`id`, `logo`, `add`, `mail1`, `mail2`, `phn1`, `phn2`, `eme_phn`, `appoint_phn`, `sun_thu`, `fri`, `sat`, `fb`, `twt`, `link`, `copyright`) VALUES
(1, '8891a2789a101d6c42d32d4e034ec215.png', '1234 Street Name, City Name', 'first_mail@example.com', 'second_mail@example.com', '(800) 123-4567', '(800) 987-4321', '(800) 999-9999', ' +1 874 801 8014', ' 8:30 am to 5:00 pm', 'Closed', '9:30 am to 1:00 pm', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com/', '© Copyright 2017. All Rights Reserved.');

-- --------------------------------------------------------

--
-- Table structure for table `sub_district`
--

CREATE TABLE IF NOT EXISTS `sub_district` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_district`
--

INSERT INTO `sub_district` (`id`, `parent_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(12, '001', '01', 'বাগাদী', 0, '2017-05-06 10:39:07', 0, '0000-00-00 00:00:00', 1),
(13, '001', '02', 'বিষ্ণপুর', 0, '2017-05-06 10:19:33', 0, '0000-00-00 00:00:00', 0),
(14, '001', '03', 'আশিকাটি', 0, '2017-05-06 10:19:24', 0, '0000-00-00 00:00:00', 0),
(16, '002', '05', 'কল্যাণপুর', 0, '2017-05-06 10:20:02', 0, '0000-00-00 00:00:00', 0),
(17, '002', '04', 'শাহ্‌ মাহমুদপুর', 0, '2017-05-06 10:22:10', 0, '0000-00-00 00:00:00', 0),
(18, '002', '06', 'রামপুর', 0, '2017-05-06 10:22:26', 0, '0000-00-00 00:00:00', 0),
(19, '003', '07', 'মৈশাদী', 0, '2017-05-06 10:22:41', 0, '0000-00-00 00:00:00', 0),
(20, '003', '08', 'তরপুচন্ডী', 0, '2017-05-06 10:22:56', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `temp_app_family`
--

CREATE TABLE IF NOT EXISTS `temp_app_family` (
`id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `father_name` varchar(100) NOT NULL,
  `father_nid` varchar(30) NOT NULL,
  `mother_name` varchar(100) NOT NULL,
  `mother_nid` varchar(30) NOT NULL,
  `marital_status` varchar(20) NOT NULL,
  `husband_name` varchar(100) NOT NULL,
  `husband_nid` varchar(30) NOT NULL,
  `wife_name` varchar(100) NOT NULL,
  `wife_nid` varchar(30) NOT NULL,
  `family_member` varchar(10) NOT NULL,
  `family_desc` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_app_family`
--

INSERT INTO `temp_app_family` (`id`, `parent_id`, `father_name`, `father_nid`, `mother_name`, `mother_nid`, `marital_status`, `husband_name`, `husband_nid`, `wife_name`, `wife_nid`, `family_member`, `family_desc`) VALUES
(1, 0, 'faather name', '123123', 'mother name ', '123123', 'yes', 'husband name', '123456789', 'wife name', '123465', '25', 'lorem iipsumm'),
(2, 0, '123', '123', '123', '123', 'no', '123', '123', '123', '12346512', '123', '1231'),
(3, 0, 'asd', 'fasdf', 'asdfasdf', 'asdf', 'yes', 'asd', 'asdf', 'asdf', 'asdf', 'asdf', 'asdfasd');

-- --------------------------------------------------------

--
-- Table structure for table `temp_app_finance`
--

CREATE TABLE IF NOT EXISTS `temp_app_finance` (
`id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `house_desc` text NOT NULL,
  `land` varchar(100) NOT NULL,
  `earn_person` varchar(20) NOT NULL,
  `earn_source` varchar(30) NOT NULL,
  `monthly_income` varchar(30) NOT NULL,
  `cause_poorness` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_app_finance`
--

INSERT INTO `temp_app_finance` (`id`, `parent_id`, `house_desc`, `land`, `earn_person`, `earn_source`, `monthly_income`, `cause_poorness`) VALUES
(1, 0, 'lorem ipsum', '2566', '25', '10', '25 ', 'caer'),
(2, 0, '123', '12312', '123', '123', '123', '123123'),
(3, 0, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdfasdf');

-- --------------------------------------------------------

--
-- Table structure for table `temp_app_interested_service`
--

CREATE TABLE IF NOT EXISTS `temp_app_interested_service` (
`id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `curr_service` varchar(100) NOT NULL,
  `inter_service` varchar(100) NOT NULL,
  `success_way` text NOT NULL,
  `inter_trainings` varchar(100) NOT NULL,
  `reference` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_app_interested_service`
--

INSERT INTO `temp_app_interested_service` (`id`, `parent_id`, `curr_service`, `inter_service`, `success_way`, `inter_trainings`, `reference`) VALUES
(1, 0, 'a', 'a', 'test way', 'test training', 'abcd, efghi'),
(2, 0, 'নির্বাচন করুন', 'নির্বাচন করুন', '', '', ''),
(3, 0, 'নির্বাচন করুন', 'নির্বাচন করুন', '', '', ''),
(4, 0, 'a', 'a', '123123', '1231', '23123'),
(5, 0, 'a', 'a', 'sdfa', 'sdfa', 'sdfasdf');

-- --------------------------------------------------------

--
-- Table structure for table `temp_app_personal`
--

CREATE TABLE IF NOT EXISTS `temp_app_personal` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `birth_date` varchar(30) NOT NULL,
  `birth_id` varchar(30) NOT NULL,
  `nid` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `district` varchar(100) NOT NULL,
  `sub_district` varchar(100) NOT NULL,
  `union` varchar(100) NOT NULL,
  `word_no` varchar(30) NOT NULL,
  `village` varchar(100) NOT NULL,
  `house_name` varchar(100) NOT NULL,
  `holdings_no` varchar(30) NOT NULL,
  `education` varchar(100) NOT NULL,
  `profession` varchar(100) NOT NULL,
  `disability` varchar(30) NOT NULL,
  `religion` varchar(30) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_app_personal`
--

INSERT INTO `temp_app_personal` (`id`, `name`, `gender`, `birth_date`, `birth_id`, `nid`, `mobile`, `district`, `sub_district`, `union`, `word_no`, `village`, `house_name`, `holdings_no`, `education`, `profession`, `disability`, `religion`, `create_by`, `create_date`) VALUES
(1, 'test name', 'male', '11-11-2017', '123', '123', '01672552966', 'borishal', 'vola', 'bagadi', '25', 'village name', 'molla bari', '25 no', 's.s.c', 'krishi', 'no', 'islam', 0, '2017-05-11 08:13:57'),
(2, 'Mostafijur Rahman', 'male', '11-05-2017', '11111123123', '123123123123', '01672552966', 'chand pur', 'chand pur shodor', 'bagadi', 'a', 'a', 'abcd', '40 no', 'a', 'abcd', 'no', 'islame', 0, '2017-05-11 06:49:29'),
(3, 'ashiquer rahman', 'male', '27-05-2017', '12312', '3123', '12312', 'chand pur', 'chand pur shodor', 'bagadi', 'a', 'a', '12312', '31231', 'a', '12312', 'no', 'islame', 0, '2017-05-11 06:49:29'),
(4, 'fasdfasdf', 'male', '23-05-2017', 'asdf', 'asdfasdf', 'asdf', 'chand pur', 'chand pur shodor', 'bagadi', 'a', 'a', 'asdf', 'asdf', 'a', 'asdf', 'no', 'islame', 0, '2017-05-11 06:49:29'),
(5, 'Mostafijur Rahman', 'male', '12-05-2017', '23423423', '4234234234', '01672552966', 'chand pur', 'chand pur shodor', 'bagadi', 'a', 'a', 'house name', '40 no', 'a', 'student', 'no', 'islame', 0, '2017-05-11 07:16:02'),
(6, 'Mostafijur Rahman', 'female', '26-05-2017', 'test', 'test', '01672552966', 'chand pur', 'chand pur shodor', 'bagadi', 'a', 'a', 'test', 'test', 'a', 'test', 'no', 'islame', 0, '2017-05-11 08:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `union`
--

CREATE TABLE IF NOT EXISTS `union` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `union`
--

INSERT INTO `union` (`id`, `parent_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(8, '01', '01', 'union 01', 0, '2017-05-06 11:58:03', 0, '0000-00-00 00:00:00', 1),
(9, '01', '02', 'union 02', 0, '2017-05-06 10:52:10', 0, '0000-00-00 00:00:00', 0),
(10, '02', '03', 'union 03', 0, '2017-05-06 10:52:19', 0, '0000-00-00 00:00:00', 0),
(11, '02', '04', 'union 04', 0, '2017-05-06 10:52:37', 0, '0000-00-00 00:00:00', 0),
(12, '03', '05', 'union 05', 0, '2017-05-06 10:52:49', 0, '0000-00-00 00:00:00', 0),
(13, '03', '06', 'union 06', 0, '2017-05-06 10:53:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `password` varchar(70) NOT NULL COMMENT '60 digit',
  `address` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `role` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `union` varchar(30) NOT NULL,
  `word` varchar(30) DEFAULT NULL,
  `token` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `online_timestamp` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `password`, `address`, `image`, `about`, `role`, `status`, `union`, `word`, `token`, `create_date`, `online_timestamp`) VALUES
(14, 'Mostafijur Rahman', '1', '1', 'test address', '87277b12b557a9f8eea1498e43d34e82.jpg', 'about user', 1, 1, '', 'selected', '87c580c8310730f3cdebf0b164b0c2df', '2017-05-11 10:44:36', '1494499476'),
(15, 'Monir Ahmed', '2', '2', 'Monir vai address', 'ba4d6da636a3d05ca247cb53396070d1.jpg', 'about monir vai', 1, 1, '', 'selected', '', '2017-05-10 07:10:49', ''),
(16, 'Mahmudul Hasan', '123456789', '$2a$12$XmtvaNdKivwflpCCfMlX3uhD3iOrP/FO4TI99tfYsDIJgSPnTcjka', 'Address of mahmudul', '53b202bb1cd6b9a22ee065feeae1a873.jpg', 'about mahmudul', 2, 1, '', 'selected', '', '2017-05-07 13:28:09', ''),
(17, 'aaaaaaaaaa', '123', '', 'aaaaaaaaaaa', '8b93456f8e835e1d0851ab545a257b22.jpg', 'aaaaaaaaaa', 1, 1, '', 'selected', '', '2017-05-10 05:39:26', '');

-- --------------------------------------------------------

--
-- Table structure for table `village`
--

CREATE TABLE IF NOT EXISTS `village` (
`id` int(11) NOT NULL,
  `parent_word_id` varchar(30) NOT NULL,
  `parent_union_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `village`
--

INSERT INTO `village` (`id`, `parent_word_id`, `parent_union_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(5, '1', '02', '01', 'village 01', 0, '2017-05-06 11:56:37', 0, '0000-00-00 00:00:00'),
(6, '2', '04', '02', 'Village 02', 0, '2017-05-06 11:56:49', 0, '0000-00-00 00:00:00'),
(7, '2', '01', '03', 'village 03', 0, '2017-05-06 11:57:03', 0, '0000-00-00 00:00:00'),
(8, '2', '02', '04', 'village 04', 0, '2017-05-06 11:57:14', 0, '0000-00-00 00:00:00'),
(9, '1', '01', '05', 'village for union 01', 0, '2017-05-06 11:58:47', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `word`
--

CREATE TABLE IF NOT EXISTS `word` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `name` varchar(20) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `word`
--

INSERT INTO `word` (`id`, `parent_id`, `name`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(5, '01', '1', 0, '2017-05-06 11:16:35', 0, '0000-00-00 00:00:00'),
(6, '01', '2', 0, '2017-05-06 11:16:38', 0, '0000-00-00 00:00:00'),
(7, '01', '3', 0, '2017-05-06 11:16:41', 0, '0000-00-00 00:00:00'),
(8, '01', '4', 0, '2017-05-06 11:16:43', 0, '0000-00-00 00:00:00'),
(9, '01', '5', 0, '2017-05-06 11:16:46', 0, '0000-00-00 00:00:00'),
(10, '02', '1', 0, '2017-05-06 11:16:51', 0, '0000-00-00 00:00:00'),
(11, '02', '2', 0, '2017-05-06 11:16:55', 0, '0000-00-00 00:00:00'),
(12, '02', '3', 0, '2017-05-06 11:16:59', 0, '0000-00-00 00:00:00'),
(13, '02', '4', 0, '2017-05-06 11:17:02', 0, '0000-00-00 00:00:00'),
(14, '02', '5', 0, '2017-05-06 11:17:05', 0, '0000-00-00 00:00:00'),
(15, '03', '1', 0, '2017-05-06 11:17:13', 0, '0000-00-00 00:00:00'),
(16, '03', '2', 0, '2017-05-06 11:17:17', 0, '0000-00-00 00:00:00'),
(17, '03', '3', 0, '2017-05-06 11:17:22', 0, '0000-00-00 00:00:00'),
(18, '03', '4', 0, '2017-05-06 11:17:31', 0, '0000-00-00 00:00:00'),
(19, '03', '5', 0, '2017-05-06 11:17:36', 0, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_set`
--
ALTER TABLE `front_set`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_district`
--
ALTER TABLE `sub_district`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_app_family`
--
ALTER TABLE `temp_app_family`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_app_finance`
--
ALTER TABLE `temp_app_finance`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_app_interested_service`
--
ALTER TABLE `temp_app_interested_service`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_app_personal`
--
ALTER TABLE `temp_app_personal`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `union`
--
ALTER TABLE `union`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `village`
--
ALTER TABLE `village`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `word`
--
ALTER TABLE `word`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `front_set`
--
ALTER TABLE `front_set`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sub_district`
--
ALTER TABLE `sub_district`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `temp_app_family`
--
ALTER TABLE `temp_app_family`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `temp_app_finance`
--
ALTER TABLE `temp_app_finance`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `temp_app_interested_service`
--
ALTER TABLE `temp_app_interested_service`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `temp_app_personal`
--
ALTER TABLE `temp_app_personal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `union`
--
ALTER TABLE `union`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `village`
--
ALTER TABLE `village`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `word`
--
ALTER TABLE `word`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
