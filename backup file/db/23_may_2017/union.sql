-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2017 at 04:24 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `union`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '60 digit',
  `token` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `level` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `IP` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `first_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `phn` varchar(30) CHARACTER SET utf8 NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'default.png',
  `joined_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `online_timestamp` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `token`, `level`, `status`, `IP`, `first_name`, `last_name`, `phn`, `address`, `avatar`, `joined_date`, `online_timestamp`) VALUES
(1, 'admin@test.com', '$2a$12$194Ikhvx5AZ9.HijXdjutu2DtnItiLCeo9X8jeEkn.uTh6VHN6j.S', '2e634df97acf270c9e0a46f0ac701b6c', 2, 1, '', 'Super', 'Admin', '0123456789', 'Dhaka, Bangladesh', 'default.png', '2016-09-27 05:30:48', '1495543868');

-- --------------------------------------------------------

--
-- Table structure for table `app_curr_service`
--

CREATE TABLE IF NOT EXISTS `app_curr_service` (
  `id` int(11) NOT NULL,
  `curr_service` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_curr_service`
--

INSERT INTO `app_curr_service` (`id`, `curr_service`) VALUES
(1, '4'),
(1, '5'),
(2, '4'),
(2, '5'),
(3, '5'),
(3, '7');

-- --------------------------------------------------------

--
-- Table structure for table `app_family`
--

CREATE TABLE IF NOT EXISTS `app_family` (
`id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `father_name` varchar(100) NOT NULL,
  `father_nid` varchar(30) NOT NULL,
  `mother_name` varchar(100) NOT NULL,
  `mother_nid` varchar(30) NOT NULL,
  `marital_status` varchar(20) NOT NULL,
  `husband_name` varchar(100) NOT NULL,
  `husband_nid` varchar(30) NOT NULL,
  `wife_name` varchar(100) NOT NULL,
  `wife_nid` varchar(30) NOT NULL,
  `family_member` varchar(10) NOT NULL,
  `family_desc` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_family`
--

INSERT INTO `app_family` (`id`, `parent_id`, `father_name`, `father_nid`, `mother_name`, `mother_nid`, `marital_status`, `husband_name`, `husband_nid`, `wife_name`, `wife_nid`, `family_member`, `family_desc`) VALUES
(1, 1, 'faather name', '13123', 'mother name', '23423', 'yes', 'husband name', '123', 'wife name', '123465', 'asdfasdf', 'asdfasdf'),
(2, 2, 'faather name', '123123', 'mother name', '123123', 'yes', 'husband name', '1324657989898', 'wife name', '123465234234', 'asd', 'asdfasdfasdf'),
(3, 3, 'faather name', 'asdf', 'mother name', 'asdf', 'no', 'husname', '123', '1231231', '123465', 'asd', 'asdfasdf');

-- --------------------------------------------------------

--
-- Table structure for table `app_finance`
--

CREATE TABLE IF NOT EXISTS `app_finance` (
`id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `house_desc` text NOT NULL,
  `house_image` varchar(255) NOT NULL,
  `land` varchar(100) NOT NULL,
  `earn_person` varchar(20) NOT NULL,
  `earn_source` varchar(30) NOT NULL,
  `monthly_income` varchar(30) NOT NULL,
  `cause_poorness` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_finance`
--

INSERT INTO `app_finance` (`id`, `parent_id`, `house_desc`, `house_image`, `land`, `earn_person`, `earn_source`, `monthly_income`, `cause_poorness`) VALUES
(1, 1, 'asdfasdfasdf', '5f9e03475d5bf114c5900b09b952a383.jpg', '2566', '123', '10', '25', 'way of success'),
(2, 2, 'house desc', 'ec1a7945376bb8b6087acbabc80342d6.jpg', '10', '5', '10', '100000', 'test'),
(3, 3, 'asdfasdfsadf', 'b275fb3e4010a41fff6c087bd02455b3.jpg', '2566', 'asdfsdf', 'asdfasd', 'fasdf', 'asdfasdf');

-- --------------------------------------------------------

--
-- Table structure for table `app_interested_service`
--

CREATE TABLE IF NOT EXISTS `app_interested_service` (
`id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `inter_service` int(11) NOT NULL,
  `success_way` text NOT NULL,
  `inter_trainings` varchar(100) NOT NULL,
  `sp_info` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_interested_service`
--

INSERT INTO `app_interested_service` (`id`, `parent_id`, `inter_service`, `success_way`, `inter_trainings`, `sp_info`) VALUES
(1, 1, 4, 'test', 'asdf', 'sadfasdf'),
(2, 2, 4, 'way of sucess', 'test training', 'test'),
(3, 3, 5, 'asdfasdf', 'asdf', 'asdfasdfasdf');

-- --------------------------------------------------------

--
-- Table structure for table `app_inter_service`
--

CREATE TABLE IF NOT EXISTS `app_inter_service` (
  `id` int(11) NOT NULL,
  `inter_service` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `app_personal`
--

CREATE TABLE IF NOT EXISTS `app_personal` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `birth_date` varchar(30) NOT NULL,
  `birth_id` varchar(30) NOT NULL,
  `nid` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `image` varchar(255) NOT NULL,
  `district` varchar(100) NOT NULL,
  `sub_district` varchar(100) NOT NULL,
  `union` varchar(100) NOT NULL,
  `word_no` varchar(30) NOT NULL,
  `village` varchar(100) NOT NULL,
  `house_name` varchar(100) NOT NULL,
  `holdings_no` varchar(30) NOT NULL,
  `education` varchar(100) NOT NULL,
  `profession` varchar(100) NOT NULL,
  `capability` varchar(100) NOT NULL,
  `disability` varchar(30) NOT NULL,
  `religion` varchar(30) NOT NULL,
  `app_status` int(1) NOT NULL COMMENT '0=refu, 1=pend, 2=appr',
  `status` int(1) NOT NULL COMMENT '1 = active, 0 = inactive ',
  `create_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_personal`
--

INSERT INTO `app_personal` (`id`, `name`, `gender`, `birth_date`, `birth_id`, `nid`, `mobile`, `image`, `district`, `sub_district`, `union`, `word_no`, `village`, `house_name`, `holdings_no`, `education`, `profession`, `capability`, `disability`, `religion`, `app_status`, `status`, `create_by`, `create_date`) VALUES
(1, 'ajijur rahman', 'male', '26-05-2017', '13246579879846545123', 'sadfa6sdf87a98sdf', '0167255826', 'a481fb3a14a4c2fdcb417e1924da9cae.jpg', 'chand pur', 'chand pur shodor', 'bagadi', 'a', 'v', 'house name', '40 a', 'b', 'student', 'complete_disable', 'no', 'islame', 2, 1, 0, '2017-05-23 14:03:10'),
(2, 'ajijur rahman', 'male', '26-05-2017', '14324657989633.3', '134657856asdf', '132465798', 'c38e19bead7198872e241c07996d6cbe.jpg', 'chand pur', 'chand pur shodor', 'bagadi', 'a', 'v', 'house name', '40 a', 'a', 'student', 'complete_disable', 'no', 'islame', 2, 1, 0, '2017-05-23 14:13:04'),
(3, 'test name', 'male', '25-05-2017', '23423423', '23423423423', '234234234', 'ff6e8093c93998093332926caa4af5bf.jpg', 'chand pur', 'chand pur shodor', 'bagadi', 'v', 'd', 'house name', '40 a', 'a', 'student', 'partial_disable', 'no', 'islame', 2, 1, 0, '2017-05-23 14:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `coord_comments`
--

CREATE TABLE IF NOT EXISTS `coord_comments` (
`pri_id` int(11) NOT NULL,
  `id` int(11) NOT NULL COMMENT 'app id (primary key of app. table)',
  `coor_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coord_comments`
--

INSERT INTO `coord_comments` (`pri_id`, `id`, `coor_id`, `comments`, `last_update`) VALUES
(1, 2, 0, 'this 2 id coor comment', '2017-05-23 14:13:04'),
(2, 3, 0, 'third comment', '2017-05-23 14:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
`id` int(11) NOT NULL,
  `short_name` varchar(30) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(7, '001', 'চাঁদপুর সদর', 0, '2017-05-06 10:12:54', 0, '0000-00-00 00:00:00', 0),
(8, '002', 'হাইমচর', 0, '2017-05-06 10:13:03', 0, '0000-00-00 00:00:00', 0),
(9, '003', 'কচুয়া', 0, '2017-05-06 10:13:08', 0, '0000-00-00 00:00:00', 1),
(10, '004', 'শাহরাস্তি', 0, '2017-05-06 10:13:17', 0, '0000-00-00 00:00:00', 0),
(11, '005', 'মতলব দক্ষিণ', 0, '2017-05-06 10:13:23', 0, '0000-00-00 00:00:00', 0),
(12, '006', 'হাজীগঞ্জ', 0, '2017-05-06 10:13:27', 0, '0000-00-00 00:00:00', 0),
(13, '007', 'মতলব উত্তর', 0, '2017-05-06 10:13:32', 0, '0000-00-00 00:00:00', 0),
(14, '008', 'ফরিদগঞ্জ', 0, '2017-05-06 10:13:46', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `front_set`
--

CREATE TABLE IF NOT EXISTS `front_set` (
`id` int(11) NOT NULL,
  `logo` varchar(128) NOT NULL,
  `add` text NOT NULL,
  `mail1` varchar(512) NOT NULL,
  `mail2` varchar(512) NOT NULL,
  `phn1` varchar(50) NOT NULL,
  `phn2` varchar(50) NOT NULL,
  `eme_phn` varchar(50) NOT NULL,
  `appoint_phn` varchar(50) NOT NULL,
  `sun_thu` varchar(256) NOT NULL,
  `fri` varchar(256) NOT NULL,
  `sat` varchar(256) NOT NULL,
  `fb` varchar(256) NOT NULL,
  `twt` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL,
  `copyright` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `front_set`
--

INSERT INTO `front_set` (`id`, `logo`, `add`, `mail1`, `mail2`, `phn1`, `phn2`, `eme_phn`, `appoint_phn`, `sun_thu`, `fri`, `sat`, `fb`, `twt`, `link`, `copyright`) VALUES
(1, '8891a2789a101d6c42d32d4e034ec215.png', '1234 Street Name, City Name', 'first_mail@example.com', 'second_mail@example.com', '(800) 123-4567', '(800) 987-4321', '(800) 999-9999', ' +1 874 801 8014', ' 8:30 am to 5:00 pm', 'Closed', '9:30 am to 1:00 pm', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com/', '© Copyright 2017. All Rights Reserved.');

-- --------------------------------------------------------

--
-- Table structure for table `ref_comments`
--

CREATE TABLE IF NOT EXISTS `ref_comments` (
`pri_id` int(11) NOT NULL,
  `id` varchar(30) NOT NULL COMMENT 'app id (primary key of app. table)',
  `ref_id` int(11) NOT NULL COMMENT 'supervisor id',
  `comments` text NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'last update time of comment'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ref_comments`
--

INSERT INTO `ref_comments` (`pri_id`, `id`, `ref_id`, `comments`, `last_update`) VALUES
(1, '1', 2, 'deya jai 1', '2017-05-23 13:59:13'),
(2, '2', 2, 'deya jai 1 asihk', '2017-05-23 13:59:40'),
(3, '2', 4, '', '2017-05-23 13:45:40'),
(4, '3', 2, '', '2017-05-23 13:47:30'),
(5, '3', 4, '', '2017-05-23 13:47:30');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `authority_id` int(11) NOT NULL COMMENT 'parent id (primary key of service_authority table)',
  `canvas` text NOT NULL,
  `definition` text NOT NULL,
  `status` int(1) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `authority_id`, `canvas`, `definition`, `status`, `create_by`, `create_date`) VALUES
(4, 'বয়স্ক ভাতা', 6, 'পৃথিবীর অন্যান্য উন্নয়নশীল রাষ্ট্রের ন্যায় বাংলাদেশ সরকারও সামাজিক নিরাপত্তা বেষ্টনী সুদৃঢ়করণের লক্ষ্যে দেশের দুঃস্থ, অবহেলিত, সুবিধাবঞ্চিত এবং অনগ্রসর মানুষের কল্যাণ ও উন্নয়নের ক্ষেত্রে সমাজকল্যাণ মন্ত্রণালয়ের মাধ্যমে ব্যাপক ও বহুমুখী কর্মসূচি বাস্তবায়ন করছে। সামাজিক নিরাপত্তা বিধানে সরকার তার সাংবিধানিক দায়িত্বের অংশ হিসেবে সমাজকল্যাণ মন্ত্রণালয়ের অধীন সমাজসেবা অধিদফতরের মাধ্যমে বয়স্কভাতা কর্মসূচি বাস্তবায়ন করছে।', 'বয়স্কে ব্যক্তি : বয়স্কভাতা কর্মসূচির আওতায় বয়স্ক ব্যক্তি বলতে পুরুষের ক্ষেত্রে ৬৫ বছর বা তদুর্ধ বয়সের এবং মহিলাদের ক্ষেত্রে ৬২ বা তদুর্ধ বয়সের কিংবা সরকার কর্তৃক সময় সময় নির্ধারিত বয়সের ব্যক্তিকে বুঝাবে।', 1, 0, '2017-05-21 14:15:38'),
(5, 'বিধবা ভাতা', 6, 'canvas', 'definition', 1, 0, '2017-05-22 13:27:18'),
(7, 'প্রতিবন্ধী  ভাতা', 6, 'canvas', 'definition', 1, 0, '2017-05-22 13:28:27'),
(9, 'ভিজি এফ', 6, 'sasdfasdfas', 'asdfasdf', 1, 0, '2017-05-22 13:29:59'),
(10, 'ভিজিডি', 6, 'asdf', 'asdf', 1, 0, '2017-05-22 13:30:27'),
(11, 'মাতৃত্বকালিন ভাতা', 6, 'asfd', 'asdf', 1, 0, '2017-05-22 13:30:58'),
(12, 'বিআরডিবি', 6, 'asdfasd', 'asdf', 1, 0, '2017-05-22 13:31:55');

-- --------------------------------------------------------

--
-- Table structure for table `services_disqualifi`
--

CREATE TABLE IF NOT EXISTS `services_disqualifi` (
  `id` int(11) NOT NULL COMMENT 'primary id of services table',
  `disqualifi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_disqualifi`
--

INSERT INTO `services_disqualifi` (`id`, `disqualifi`) VALUES
(3, 'asdfa'),
(4, 'সরকারি কর্মচারী পেনশনভোগী হলে।'),
(4, 'দুঃস্থ মহিলা হিসেবে ভিজিডি কার্ডধারী হলে।\r\n'),
(4, ' অন্য কোনোভাবে নিয়মিত সরকারী অনুদান/ভাতা প্রাপ্ত হলে।'),
(4, ' কোনো বেসরকারি সংস্থা/সমাজকল্যাণমূলক প্রতিষ্ঠান হতে নিয়মিতভাবে আর্থিক অনুদান/ভাতা প্রাপ্ত হলে।'),
(5, 'asdf'),
(6, 'asdf'),
(7, 'asdf'),
(8, 'asdf'),
(9, 'dfasd'),
(10, 'asdf'),
(11, 'asdf'),
(12, 'asdfasd');

-- --------------------------------------------------------

--
-- Table structure for table `services_goal`
--

CREATE TABLE IF NOT EXISTS `services_goal` (
  `id` int(11) NOT NULL COMMENT 'primary id of services table',
  `goal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_goal`
--

INSERT INTO `services_goal` (`id`, `goal`) VALUES
(3, 'asdfsd'),
(4, 'বয়স্ক জনগোষ্ঠীর আর্থ-সামাজিক উন্নয়ন ও সামাজিক নিরাপত্তা বিধান;'),
(4, 'পরিবার ও সমাজে তাঁদের মর্যাদা বৃদ্ধি;'),
(5, 'aasdf'),
(6, 'asdf'),
(7, 'asdf'),
(8, 'asdf'),
(9, 'asdf'),
(10, 'asdf'),
(11, 'asdf'),
(12, 'asdf');

-- --------------------------------------------------------

--
-- Table structure for table `services_purpose`
--

CREATE TABLE IF NOT EXISTS `services_purpose` (
  `id` int(11) NOT NULL COMMENT 'primary id of services table',
  `purpose` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_purpose`
--

INSERT INTO `services_purpose` (`id`, `purpose`) VALUES
(3, 'fasdfa'),
(4, 'আর্থিক অনুদানের মাধ্যমে তাঁদের মনোবল জোরদারকরণ;'),
(4, 'চিকিৎসা ও পুষ্টি সরবরাহ বৃদ্ধিতে সহায়তা করা।'),
(5, 'asdf'),
(6, 'asdf'),
(7, 'asdf'),
(8, 'asdf'),
(9, 'asdfa'),
(10, 'asdf'),
(11, 'asdf'),
(12, 'asdf');

-- --------------------------------------------------------

--
-- Table structure for table `services_qualifi`
--

CREATE TABLE IF NOT EXISTS `services_qualifi` (
  `id` int(11) NOT NULL COMMENT 'primary id of services table',
  `qualifi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_qualifi`
--

INSERT INTO `services_qualifi` (`id`, `qualifi`) VALUES
(3, 'asdf'),
(4, ' সংশ্লিষ্ট এলাকার স্থায়ী বাসিন্দা হতে হবে;'),
(4, 'জন্ম নিবন্ধন/জাতীয় পরিচিতি নম্বর থাকতে হবে;'),
(4, 'বয়স পুরুষের ক্ষেত্রে সর্বনিম্ন ৬৫ বছর এবং মহিলাদের ক্ষেত্রে সর্বনিম্ন ৬২ বছর হতে হবে। সরকার কর্তৃক সময় সময় নির্ধারিত বয়স বিবেচনায় নিতে হবে;'),
(4, 'প্রার্থীর বার্ষিক গড় আয় অনূর্ধ ১০,০০০ (দশ হাজার) টাকা হতে হবে'),
(4, 'বাছাই কমিটি কর্তৃক নির্বাচিত হতে হবে।'),
(4, 'জন্ম নিবন্ধন/জাতীয় পরিচিতি নম্বর থাকতে হবে; বিঃ দ্রঃ বয়স নির্ধারণের ক্ষেত্রে জাতীয় পরিচয়পত্র, জন্ম নিবন্ধন সনদ, এসএসসি/সমমান পরীক্ষার সনদপত্র বিবেচনা করতে হবে। এ ক্ষেত্রে কোন বিতর্ক দেখা দিলে সংশ্লিষ্ট কমিটির সিদ্ধান্ত চূড়ান্ত বলে বিবেচিত হবে।'),
(5, 'asdf'),
(6, 'asdfa'),
(7, 'asdf'),
(8, 'asdf'),
(9, 'sdfas'),
(10, 'asdf'),
(11, 'asdf'),
(12, 'asdf');

-- --------------------------------------------------------

--
-- Table structure for table `services_selection`
--

CREATE TABLE IF NOT EXISTS `services_selection` (
  `id` int(11) NOT NULL COMMENT 'primary id of services table',
  `selection` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_selection`
--

INSERT INTO `services_selection` (`id`, `selection`) VALUES
(3, 'asdfasd'),
(4, ' নাগরিকত্ব: প্রার্থীকে অবশ্যই বাংলাদেশের স্থায়ী নাগরিক হতে হবে।'),
(4, ' বয়স: সর্বোচ্চ বয়স্ক ব্যক্তিকে অগ্রাধিকার প্রদান করতে হবে।'),
(4, 'স্বাস্থ্যগত অবস্থা: যিনি শারীরিকভাবে অক্ষম অর্থাৎ সম্পূর্ণরূপে কর্মক্ষমতাহীন তাঁকে সর্বোচ্চ অগ্রাধিকার দিতে হবে।'),
(4, 'আর্থিক অবস্থার ক্ষেত্রে: নিঃস্ব, উদ্বাস্ত্ত ও ভূমিহীনকে ক্রমানুসারে অগ্রাধিকার দিতে হবে।'),
(4, ' সামাজিক অবস্থার ক্ষেত্রে: বিধবা, তালাকপ্রাপ্তা, বিপত্নীক, নিঃসন্তান, পরিবার থেকে বিচ্ছিন্ন ব্যক্তিদেরকে ক্রমানুসারে অগ্রাধিকার দিতে হবে।'),
(4, 'ভূমির মালিকানা: ভূমিহীন ব্যক্তিকে অগ্রাধিকার দিতে হবে। এক্ষেত্রে বসতবাড়ী ব্যতীত কোনো ব্যক্তির জমির পরিমাণ ০.৫ একর বা তার কম হলে তিনি ভূমিহীন বলে গণ্য হবেন।'),
(5, 'asdfasdf'),
(6, 'asdf'),
(7, 'asdf'),
(8, 'asdf'),
(9, 'fasdf'),
(10, 'fasdfasd'),
(11, 'asdf'),
(12, 'asdfasd');

-- --------------------------------------------------------

--
-- Table structure for table `service_authority`
--

CREATE TABLE IF NOT EXISTS `service_authority` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL COMMENT '1 = gov, 2 = non-gov, 3 = social,  4 = personal',
  `about` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `status` int(1) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_authority`
--

INSERT INTO `service_authority` (`id`, `name`, `logo`, `category`, `about`, `image`, `address`, `status`, `create_by`, `create_date`) VALUES
(6, 'সমাজকল্যাণ মন্ত্রণালয়', '21d4efa3123a291fcdfe23c675771d8b.png', '1', 'সমাজকল্যাণ মন্ত্রণালয় বাংলাদেশের পিছিয়েপরা এবং অনগ্রসর জনগোষ্ঠীর মানব সম্পদ উন্নয়ন, দারিদ্র্য বিমোচন, কল্যাণ, উন্নয়ন ও ক্ষমতায়ণ সাথে সংশ্লিষ্ট একটি অন্যতম গুরুত্বপূর্ণ মন্ত্রণালয়। একটি কল্যাণ রাষ্ট্র হিসেবে বাংলাদেশ পরিচয় করিয়ে দিতে সমাজকল্যাণ মন্ত্রণালয় বয়স্কভাতা, বিধবা ভাতা, প্রতিবন্ধী ভাতা, এসিডদগ্ধ ও প্রতিবন্ধী ব্যক্তিদের সহায়তা কার্যক্রম বাস্তবায়ন করছে।সমাজকল্যাণ মন্ত্রণালয় একইসাথে দেশব্যাপী গ্রামীণ এবং শহুরে উভয় এলাকায় সমাজের পিছিয়েপরা, অনগ্রসর অংশ, বেকার, ভূমিহীন, অনাথ, দুঃস্থ, ভবঘুরে, নিরাশ্রয়, সামাজিক, বুদ্ধিমত্তা এবং শারীরিক প্রতিবন্ধী ব্যক্তি, দরিদ্র, অসহায় রোগী, ঝুঁকিপূর্ণ শিশুদের কল্যাণ ও উন্নয়নে বহুমাত্রিক এবং নিবিড় কার্যক্রমও বাস্তবায়ন করছে।\r\n \r\nবাংলাদেশের সংবিধানের ১৫(ঘ) অনুচ্ছেদ অনুযায়ী, সমাজকল্যাণ মন্ত্রণালয় ও মন্ত্রণালয়ের সাথে সংযুক্ত দপ্তর ও সংস্থাসমূহের মাধ্যমে এ সকল কার্যক্রম বাস্তবায়ন করছে। কার্যক্রমসমূহ টেকসই উন্নয়ন লক্ষ্যমাত্রা (এসডিজি), ভিশন ২০২১ ও সপ্তম পঞ্চবার্ষিক পরিকল্পনায় বর্ণিত লক্ষমাত্রাসমূহ অর্জণের লক্ষ্যে গ্রহণ করা হয়েছে।\r\n \r\nকার্যক্রম বাস্তবায়নে অধিকতর গতিশীলতা ও স্বচ্ছতা আনয়নের লক্ষ্যে মন্ত্রণালয় দাতব্য পদ্ধতির পরিবর্তে গ্রহণ করেছে উন্নয়ন পদ্ধতি।পরিবার কেন্দ্রীক কর্মসূচি ও সঠিক কর্মপরিকল্পনা বাংলাদেশের মানুষের কাছে এ মন্ত্রণালয়কে করেছে জনপ্রিয়।', 'bc68b15476cadd2ed3a21172a4623cef.jpg', 'সমাজকল্যাণ মন্ত্রণালয়\r\nগণপ্রজাতন্ত্রী বাংলাদেশ সরকার\r\nভবন-৬, ৪র্থ তলা\r\nবাংলাদেশ সচিবালয়, ঢাকা\r\nঢাকা-১০০০\r\nবাংলাদেশ\r\nফোন: +৮৮০২ ৯৫৪০৪৫২\r\nফ্যাক্স: +৮৮০২ ৯৫৭৬৬৮০; +৮৮০২ ৯৫৭৬৬৮৫\r\nই-মেইল: sasadmin1@msw.gov.bd \r\nওয়েব: http://www.msw.gov.bd/', 1, 0, '2017-05-17 13:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `sub_district`
--

CREATE TABLE IF NOT EXISTS `sub_district` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_district`
--

INSERT INTO `sub_district` (`id`, `parent_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(12, '001', '01', 'বাগাদী', 0, '2017-05-06 10:39:07', 0, '0000-00-00 00:00:00', 1),
(13, '001', '02', 'বিষ্ণপুর', 0, '2017-05-06 10:19:33', 0, '0000-00-00 00:00:00', 0),
(14, '001', '03', 'আশিকাটি', 0, '2017-05-06 10:19:24', 0, '0000-00-00 00:00:00', 0),
(16, '002', '05', 'কল্যাণপুর', 0, '2017-05-06 10:20:02', 0, '0000-00-00 00:00:00', 0),
(17, '002', '04', 'শাহ্‌ মাহমুদপুর', 0, '2017-05-06 10:22:10', 0, '0000-00-00 00:00:00', 0),
(18, '002', '06', 'রামপুর', 0, '2017-05-06 10:22:26', 0, '0000-00-00 00:00:00', 0),
(19, '003', '07', 'মৈশাদী', 0, '2017-05-06 10:22:41', 0, '0000-00-00 00:00:00', 0),
(20, '003', '08', 'তরপুচন্ডী', 0, '2017-05-06 10:22:56', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `union`
--

CREATE TABLE IF NOT EXISTS `union` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `union`
--

INSERT INTO `union` (`id`, `parent_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`, `default`) VALUES
(8, '01', '01', 'union 01', 0, '2017-05-06 11:58:03', 0, '0000-00-00 00:00:00', 1),
(9, '01', '02', 'union 02', 0, '2017-05-06 10:52:10', 0, '0000-00-00 00:00:00', 0),
(10, '02', '03', 'union 03', 0, '2017-05-06 10:52:19', 0, '0000-00-00 00:00:00', 0),
(11, '02', '04', 'union 04', 0, '2017-05-06 10:52:37', 0, '0000-00-00 00:00:00', 0),
(12, '03', '05', 'union 05', 0, '2017-05-06 10:52:49', 0, '0000-00-00 00:00:00', 0),
(13, '03', '06', 'union 06', 0, '2017-05-06 10:53:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `password` varchar(70) NOT NULL COMMENT '60 digit',
  `address` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `role` int(1) NOT NULL COMMENT '0 = oper, 1 = supervi, 3 = Coordinat',
  `status` int(1) NOT NULL,
  `union` varchar(30) NOT NULL,
  `word` varchar(30) DEFAULT NULL,
  `token` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `online_timestamp` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `phone`, `password`, `address`, `image`, `about`, `role`, `status`, `union`, `word`, `token`, `create_date`, `online_timestamp`) VALUES
(1, 'Rana', 'Ahmed', '123456789', '693cfed9dd8adf7c63afbf53cf3a8043', 'test', '2badbc25bfb4d049d8a9f8cb1bdc4e0a.jpg', 'Health Worker', 0, 1, '', 'selected', '3750ab53dc1beed3bbd7686aa26a8a3e', '2017-05-23 14:20:49', '1495549249'),
(2, 'Ashiq', 'Khan', '12345678', '8843028fefce50a6de50acdf064ded27', '103, Kolabaddan, Dhaka', '39f1b516f1b497a6df09f4e98d370909.jpg', 'Member', 1, 1, '', 'selected', 'd82a0b67c06c77e760156cd00845cd84', '2017-05-23 13:57:36', '1495547856'),
(3, 'Monir', 'Ahmed', '12345678910', '61e0281e49f5a54bd73120f33d105e75', '105, Mirpur-1, Dhaka-1216', 'ea38d530be9ae07162d6f1b315626cd9.jpg', 'Upojela Chairman', 2, 1, '', 'selected', '8f594b08b12a1c5fd6f947c8ad39e9ec', '2017-05-23 14:17:27', '1495549047'),
(4, 'Goffur', 'Mia', '1122334455667788', '15f311fbd17d284b1a9bc06d892bc1b9', '103, new town, dhaka', 'fbae82dfa92c349f0f65fdcb5d2aa63f.jpg', 'Head Master', 1, 1, '', 'selected', '9c32f673f85ef210b473cc22d7b1b62a', '2017-05-23 12:36:42', '1495537155'),
(5, 'Alummuddin', 'Khan', '112233445566778899', 'f8519d6c8bf08a904c83d31978c6b6ef', '102, abcd, Dhaka', 'deb63987075e74b2867b1b583569c762.jpg', 'Member', 1, 1, '', 'selected', 'd6dcfe1f723cb60ae66f547a234da54e', '2017-05-23 12:36:52', '1495534125');

-- --------------------------------------------------------

--
-- Table structure for table `village`
--

CREATE TABLE IF NOT EXISTS `village` (
`id` int(11) NOT NULL,
  `parent_word_id` varchar(30) NOT NULL,
  `parent_union_id` varchar(30) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `village`
--

INSERT INTO `village` (`id`, `parent_word_id`, `parent_union_id`, `short_name`, `slug`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(5, '1', '02', '01', 'village 01', 0, '2017-05-06 11:56:37', 0, '0000-00-00 00:00:00'),
(6, '2', '04', '02', 'Village 02', 0, '2017-05-06 11:56:49', 0, '0000-00-00 00:00:00'),
(7, '2', '01', '03', 'village 03', 0, '2017-05-06 11:57:03', 0, '0000-00-00 00:00:00'),
(8, '2', '02', '04', 'village 04', 0, '2017-05-06 11:57:14', 0, '0000-00-00 00:00:00'),
(9, '1', '01', '05', 'village for union 01', 0, '2017-05-06 11:58:47', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `word`
--

CREATE TABLE IF NOT EXISTS `word` (
`id` int(11) NOT NULL,
  `parent_id` varchar(30) NOT NULL,
  `name` varchar(20) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `word`
--

INSERT INTO `word` (`id`, `parent_id`, `name`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(5, '01', '1', 0, '2017-05-06 11:16:35', 0, '0000-00-00 00:00:00'),
(6, '01', '2', 0, '2017-05-06 11:16:38', 0, '0000-00-00 00:00:00'),
(7, '01', '3', 0, '2017-05-06 11:16:41', 0, '0000-00-00 00:00:00'),
(8, '01', '4', 0, '2017-05-06 11:16:43', 0, '0000-00-00 00:00:00'),
(9, '01', '5', 0, '2017-05-06 11:16:46', 0, '0000-00-00 00:00:00'),
(10, '02', '1', 0, '2017-05-06 11:16:51', 0, '0000-00-00 00:00:00'),
(11, '02', '2', 0, '2017-05-06 11:16:55', 0, '0000-00-00 00:00:00'),
(12, '02', '3', 0, '2017-05-06 11:16:59', 0, '0000-00-00 00:00:00'),
(13, '02', '4', 0, '2017-05-06 11:17:02', 0, '0000-00-00 00:00:00'),
(14, '02', '5', 0, '2017-05-06 11:17:05', 0, '0000-00-00 00:00:00'),
(15, '03', '1', 0, '2017-05-06 11:17:13', 0, '0000-00-00 00:00:00'),
(16, '03', '2', 0, '2017-05-06 11:17:17', 0, '0000-00-00 00:00:00'),
(17, '03', '3', 0, '2017-05-06 11:17:22', 0, '0000-00-00 00:00:00'),
(18, '03', '4', 0, '2017-05-06 11:17:31', 0, '0000-00-00 00:00:00'),
(19, '03', '5', 0, '2017-05-06 11:17:36', 0, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_family`
--
ALTER TABLE `app_family`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_finance`
--
ALTER TABLE `app_finance`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_interested_service`
--
ALTER TABLE `app_interested_service`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_personal`
--
ALTER TABLE `app_personal`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coord_comments`
--
ALTER TABLE `coord_comments`
 ADD PRIMARY KEY (`pri_id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_set`
--
ALTER TABLE `front_set`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_comments`
--
ALTER TABLE `ref_comments`
 ADD PRIMARY KEY (`pri_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_authority`
--
ALTER TABLE `service_authority`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_district`
--
ALTER TABLE `sub_district`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `union`
--
ALTER TABLE `union`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `village`
--
ALTER TABLE `village`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `word`
--
ALTER TABLE `word`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_family`
--
ALTER TABLE `app_family`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_finance`
--
ALTER TABLE `app_finance`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_interested_service`
--
ALTER TABLE `app_interested_service`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_personal`
--
ALTER TABLE `app_personal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `coord_comments`
--
ALTER TABLE `coord_comments`
MODIFY `pri_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `front_set`
--
ALTER TABLE `front_set`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ref_comments`
--
ALTER TABLE `ref_comments`
MODIFY `pri_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `service_authority`
--
ALTER TABLE `service_authority`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sub_district`
--
ALTER TABLE `sub_district`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `union`
--
ALTER TABLE `union`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `village`
--
ALTER TABLE `village`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `word`
--
ALTER TABLE `word`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
