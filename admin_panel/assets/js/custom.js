// 
$(document).ready(function () {
    $('#selectall').click(function (event) {  //on click 
        if (this.checked) { // check select status
            $('.checkbox1').each(function () { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        } else {
            $('.checkbox1').each(function () { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
});


//this function use for delete specific row with table name and id 
//this function can delecte multiple table value with fixed id
//@author Mostafijur Rahman Rana
function dodelete(table, id, image) {

    if (confirm("Are you sure to delete? Click OK to confirm")) {

        for (i = 0; i < table.length; i++) {

            $.ajax({
                url: BASE_URL + "common/delete/" + table[i] + "/" + id,
                success: function (data) {

                    $('#row_' + id).remove();

                },
                error: function (data) {

                }
            });

        }
        if (image != '') {

            $.ajax({
                url: BASE_URL + "common/image_delete/" + image,
                success: function (data)
                {

                }
            });
        }
    }
}




//this function use for select all delete with multiple table and id with ajax loop system 
//and if have image there but have need to rename when it add in system
function deleteall(table, image)
{
    if (confirm("â€œAre you sure to delete? Click OK to confirmâ€"))
    {
        for (i = 0; i < table.length; i++)
        {

            $('input[type=checkbox]').each(function () {
                if (this.checked) {
                    var value = $(this).val();
                    var temp = value.split("-");
                    var id = temp[0];
                    var image = temp[1];
//                alert(id);
//                alert(image);

                    //delete multiple table data with id and table name
                    $.ajax(
                            {
                                url: BASE_URL + "common/delete/" + table[i] + "/" + id,
                                success: function (data)
                                {
                                    $('#row_' + id).remove();
                                },
                                error: function (data)
                                {

                                }
                            });
                    if (image != '')
                    {
                        $.ajax({
                            url: BASE_URL + "common/image_delete/" + image,
                            success: function (data)
                            {

                            }
                        });
                    }
                }
            });

        }
    }
}


function status_change(table, id, status)
{
    $.ajax({
        url: BASE_URL + "common/status_change/" + table + "/" + id + "/" + status,
        success: function (data) {
            if (data == 1) {
                status = 1;

                $('#status_' + id).html('<a href="#" class = "btn btn-success" onclick="status_change(table,' + id + ',' + status + ');">Enable</a>');
            } else
            {
                status = 0;
                $('#status_' + id).html('<a href="#" class = "btn btn-danger" onclick="status_change(table,' + id + ',' + status + ');">Disable</a>');
            }
        },
        error: function (data)
        {


        }
    });
}

function p_status_change(table, id, status)
{
    $.ajax({
        url: BASE_URL + "common/status_change/" + table + "/" + id + "/" + status,
        success: function (data) {
            if (data == 1) {
                status = 1;

                $('#status_' + id).html('<a href="#" class = "btn btn-success" onclick="p_status_change(table1,' + id + ',' + status + ');">Enable</a>');
            } else
            {

                status = 0;
                $('#status_' + id).html('<a href="#" class = "btn btn-danger" onclick="p_status_change(table1,' + id + ',' + status + ');">Disable</a>');
            }
        },
        error: function (data)
        {


        }
    });
}

function feature_change(f_table, id, status)
{
    if (status == 1) {
        if (!confirm("â€œAre you sure to delete This feature Product from feature list? Click OK to confirmâ€"))
        {
            return false;
        } else
        {
            var ref = 1;
        }
    }

    $.ajax({
        url: BASE_URL + "common/feature_change/" + f_table + "/" + id + "/" + status,
        success: function (data) {
            if (data == 1) {
                status = '1';
                $('#feature_' + id).html('<a href="#" class = "btn btn-warning" onclick="feature_change(f_table,' + id + ',' + status + ');">Yes</a>');
            } else
            {

                status = 0;
                $('#feature_' + id).html('<a href="#" class = "btn" onclick="feature_change(f_table,' + id + ',' + status + ');">No</i></a>');
            }

        },
        error: function (data)
        {
            location.reload();
        }
    });

}



function set_primary(product_id, clipart_id)
{
    $('.primary_set').attr('checked', false);
    $.ajax({
        url: BASE_URL + 'common/set_default_product/' + product_id + '/' + clipart_id,
        success: function (data)
        {

            $('#default_' + product_id).prop('checked', true);
//                        location.reload();
        }
    });
}

function refresh() {
    location.reload();
}

/**
 * 
 * @param {type} id
 * @returns {undefined}
 * this function use for change order status
 * @author Jahid Al Mamun <rjs.jahid11@gmai.com>
 */
function order_status(id)
{
    var check_value = $('#order_status_' + id).val();
    if (check_value == 'shipped')
    {
        var shipping_number = prompt('Please Enter Shipping Track Number!');

        if (shipping_number == null || shipping_number == '')
        {
            $('#order_status_' + id).val('');
            location.reload();
            return false;
        } else
        {
            $.ajax({
                url: BASE_URL + "common/order_shipping_track/" + id + "/" + shipping_number,
                success: function (data)
                {

                }
            });
        }

    }
    $.ajax({
        url: BASE_URL + "common/order_status/" + id + "/" + check_value,
        success: function (data)
        {
            if (check_value == 'received')
            {
                $('#order_status_' + id).css("color", "white");
                $('#order_status_' + id).css("background-color", "green");
            }
            if (check_value == 'in_preparation')
            {
                $('#order_status_' + id).css("color", "white");
                $('#order_status_' + id).css("background-color", "palevioletred");
            }
            if (check_value == 'shipped')
            {
                $('#order_status_' + id).css("color", "white");
                $('#order_status_' + id).css("background-color", "paleturquoise");
            }
            if (check_value == 'deleted')
            {
                $('#order_status_' + id).css("color", "white");
                $('#order_status_' + id).css("background-color", "red");
            }

        },
        error: function (data)
        {

        }
    });

}