<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin {
    
    // properties --------------------------------------------------------------
    var $info = array();
    var $loggedin = false;
    var $em = null; // user email
    var $tkn = null; // user token
    
    // -------------------------------------------------------------------------
    public function __construct() {

        // get user info from cookie ----------------------
        $CI = & get_instance();
        $config = $CI->config->item("cookieprefix");
        $this->em = $CI->input->cookie($config . "em", TRUE);
        $this->tkn = $CI->input->cookie($config . "tkn", TRUE);
        $user = null;

        // get user info from db by cookies info ----------------------
        if ($this->em && $this->tkn) {
            $user = $CI->db->select("id, email, level, status, first_name, last_name, avatar, online_timestamp")
                           ->where("email", $this->em)->where("token", $this->tkn)
                           ->get("admin");
        }

        // checking and matching user from db response by cookies info
        if ($user !== null) {

            if ($user->num_rows() == 0) {

                $this->loggedin = false;

            } else {

                $this->loggedin = true;

                $this->info = $user->row();

                //  update timestamp -------------------------------------------
                if ($this->info->online_timestamp < time() - 60 * 5) {

                    $this->update_online_timestamp($this->info->id);

                }

                // if inactive? -------------------------------------------------
                // if ($this->info->level == -1) {
                //     $CI->load->helper("cookie");
                //     $this->loggedin = false;
                //     $CI->session->set_flashdata("msg", lang("error_6"));
                //     delete_cookie($config . "un");
                //     delete_cookie($config . "tkn");
                //     redirect(site_url("login/inactive"));
                // }
                // ?
            }
        }
    }

    // -------------------------------------------------------------------------
    public function getPassword() {

        $CI = & get_instance();

        $user = $CI->db->select("password")->where("id", $this->info->id)->get("admin");

        $user = $user->row();

        return $user->password;

    }

    // -------------------------------------------------------------------------
    public function update_online_timestamp($userid) {

        $CI = & get_instance();

        $CI->db->where("id", $userid)->update("admin", array("online_timestamp" => time()));

    }

}

?>