<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$sub_title?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-users"></i><?=$sub_title?></a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <button class="btn btn-primary" onclick="window.history.back();" style='margin-right: 5px'><i class="fa fa-fw fa-arrow-left"></i> Go back</button>
                        <h3 class="box-title"> <?=$sub_title?></h3>
                    </div>
                    <?=form_open_multipart(site_url("services/add"), array("class" => "form-horizontal"))?>
                    <?php echo validation_errors(); ?>
                    <div class="box-body">
                        <?php 
                            if(form_error('ser_name')){
                                echo "<div class='form-group has-error'>";
                            }else{    
                                echo "<div class='form-group'>";
                            } 
                        ?>
                            <label class="col-sm-2 control-label"> Service Name <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" placeholder="service name" name="ser_name" value="<?=set_value('ser_name')?>" required>
                            </div>
                            <span class="help-block">
                                <?=form_error('ser_name')?>
                            </span>
                        </div>
                        <!-- service authority -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Service Authority<sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <select class="form-control" name="authority_id" required>
                                    <?php foreach($authoritys as $authority) : ?>
                                        <option value="<?=$authority->id?>"><?=$authority->ser_auth_name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <!-- canvas -->
                        <?php
                            if(form_error('canvas')){
                                echo "<div class='form-group has-error'>";
                            }else{    
                                echo "<div class='form-group'>";
                            } 
                        ?>
                            <label class="col-sm-2 control-label">Canvas <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <textarea  class="form-control" rows="15" name="canvas" placeholder="write here..." required><?=set_value('canvas')?></textarea>
                            </div>
                            <span class="help-block">
                                <?=form_error('canvas')?>
                            </span>
                        </div>
                        <!-- definition -->
                        <?php 
                            if(form_error('definition')){ 
                                echo "<div class='form-group has-error'>";
                            }else{    
                                echo "<div class='form-group'>";
                            } 
                        ?>
                            <label class="col-sm-2 control-label">Definition <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <textarea  class="form-control" rows="15" name="definition" placeholder="write here..." required><?=set_value('definition')?></textarea>
                            </div>
                            <span class="help-block">
                                <?=form_error('definition')?>
                            </span>
                        </div>
                        <!-- goal's -->
                        <hr />
                        <div>
                            <!--main field-->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Goal <sup style="color: red">*</sup></label>
                                <div class="col-sm-6">
                                    <textarea  class="form-control" rows="2" name="goal[]" placeholder="write here..." required></textarea>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" id="add_goal_btn" class="btn btn-info"><i class="fa fa-fw fa-plus-square"></i></button>
                                </div>
                            </div>
                            
                            <!--extra field-->
                            <div id="goal_fields"></div>
                        </div>
                        <!-- purpose -->
                        <hr />
                        <div>
                            <!--main field-->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Purpose <sup style="color: red">*</sup></label>
                                <div class="col-sm-6">
                                    <textarea  class="form-control" rows="2" name="purpose[]" placeholder="write here..." required></textarea>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" id="add_purpose_btn" class="btn btn-info"><i class="fa fa-fw fa-plus-square"></i></button>
                                </div>
                            </div>
                            
                            <!--extra field-->
                            <div id="purpose_fields"></div>
                        </div>
                        <!-- qualifications -->
                        <hr />
                        <div>
                            <!--main field-->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Qualifications <sup style="color: red">*</sup></label>
                                <div class="col-sm-6">
                                    <textarea  class="form-control" rows="2" name="qualifi[]" placeholder="write here..." required></textarea>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" id="add_qualifi_btn" class="btn btn-info"><i class="fa fa-fw fa-plus-square"></i></button>
                                </div>
                            </div>
                            
                            <!--extra field-->
                            <div id="qualifi_fields"></div>
                        </div>
                        <!-- disqualification -->
                        <hr />
                        <div>
                            <!--main field-->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Disqualification <sup style="color: red">*</sup></label>
                                <div class="col-sm-6">
                                    <textarea  class="form-control" rows="2" name="disqualifi[]" placeholder="write here..." required></textarea>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" id="add_disqualifi_btn" class="btn btn-info"><i class="fa fa-fw fa-plus-square"></i></button>
                                </div>
                            </div>
                            
                            <!--extra field-->
                            <div id="disqualifi_fields"></div>
                        </div>
                        <!-- selection process -->
                        <hr />
                        <div>
                            <!--main field-->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Selection Process <sup style="color: red">*</sup></label>
                                <div class="col-sm-6">
                                    <textarea  class="form-control" rows="2" name="selection[]" placeholder="write here..." required></textarea>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" id="add_selection_btn" class="btn btn-info"><i class="fa fa-fw fa-plus-square"></i></button>
                                </div>
                            </div>
                            
                            <!--extra field-->
                            <div id="selection_fields"></div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status <sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <select class="form-control" name="status">
                                    <option value="1" selected>Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-6">
                                <button type="reset" value="Reset" class="btn btn-danger">Reset</button>
                                <button type="submit" class="btn btn-success">Add</button>
                            </div>
                        </div>
                    </div>
                    <?=form_close()?>
                </div><!-- /.box -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div>