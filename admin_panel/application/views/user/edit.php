<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$sub_title?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-users"></i><?=$sub_title?></a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=$sub_title?></h3>
                    </div>
                    <?=form_open_multipart(site_url("user/edit"), array("class" => "form-horizontal"))?>
                    <?php echo validation_errors(); ?>
                    <div class="box-body">
                        <?php 
                            if(form_error('first_name')){ 
                                echo "<div class='form-group has-error' >";
                            }else{    
                                echo "<div class='form-group' >";
                            } 
                        ?>
                            <label for="First_name" class="col-sm-2 control-label">First Name <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <input class="form-control" id="first_name" type="text" placeholder="First name" name="first_name" value="<?=$info[0]->first_name?><?=set_value('first_name')?>" required>
                            </div>
                            <span class="help-block">
                                <?=form_error('first_name')?>
                            </span>
                        </div>
                        <?php 
                            if(form_error('last_name')){ 
                                echo "<div class='form-group has-error' >";
                            }else{    
                                echo "<div class='form-group' >";
                            } 
                        ?>
                            <label for="Last_name" class="col-sm-2 control-label">Last Name </label>
                            <div class="col-sm-6">
                                <input class="form-control" id="last_name" type="text" placeholder="Last name" name="last_name" value="<?=$info[0]->last_name?><?=set_value('last_name')?>" required>
                            </div>
                            <span class="help-block">
                                <?=form_error('last_name')?>
                            </span>
                        </div>
                        <?php 
                            if(form_error('address')){ 
                                echo "<div class='form-group has-error' >";
                            }else{    
                                echo "<div class='form-group' >";
                            } 
                        ?>
                            <label for="Address" class="col-sm-2 control-label">Address <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <textarea  class="form-control" id="address" rows="2" name="address" placeholder="Full address..." required><?=$info[0]->address?><?=set_value('address')?></textarea>
                            </div>
                            <span class="help-block">
                                <?=form_error('address')?>
                            </span>
                        </div>
                        <?php
                            if(form_error('about')){ 
                                echo "<div class='form-group has-error' >";
                            }else{    
                                echo "<div class='form-group' >";
                            } 
                        ?>
                            <label for="About" class="col-sm-2 control-label">About User <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <textarea  class="form-control" id="about" rows="4" name="about" placeholder="About something user..." required><?=$info[0]->about?><?=set_value('about')?></textarea>
                            </div>
                            <span class="help-block">
                                <?=form_error('about')?>
                            </span>
                        </div>
                        <!-- image -->
                        <div class="form-group">
                            <label for="Image" class="col-sm-2 control-label"> Image <sup style="color: red">*</sup></label>
                            <div class="col-sm-4">
                                <input type="file" name="image" id="myFile" onchange="imageshow(this);" />
                                <input type="hidden" name="img" value="<?=$info[0]->image?>" />
                                <p class="help-block">jpg or png image & maximum 1 MB</p>
                            </div>
                            <div class="col col-lg-4" >
                                <img id="previewImg1" width="80px" src="<?=base_url()?>assets/img/user/<?=$info[0]->image?>" />
                                <script>
                                    function imageshow(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();
                                            reader.onload = function(e) {
                                                $('#previewImg1')
                                                .attr('src', e.target.result)
                                                .width(130);
                                            }
                                            reader.readAsDataURL(input.files[0]);
                                        } else {
                                            var filename = "";
                                            filename = "file:\/\/" + input.value;
                                            document.form2.previewImg1.src = filename;
                                            document.form2.previewImg1.style.width = "80px";
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                        <hr>
                        <?php
                            if(form_error('status')){ 
                                echo "<div class='form-group has-error' >";
                            }else{    
                                echo "<div class='form-group' >";
                            } 
                        ?>
                            <label class="col-sm-2 control-label">Status <sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="status" name="status">
                                    <option value="1" <?php if($info[0]->status == 1) echo 'selected';?>>Active</option>
                                    <option value="0" <?php if($info[0]->status == 0) echo 'selected';?>>Inactive</option>
                                </select>
                                <span class="help-block">
                                    <?=form_error('status')?>
                                </span>
                            </div>
                        </div>
                        <?php
                            if(form_error('role')){ 
                                echo "<div class='form-group has-error' >";
                            }else{    
                                echo "<div class='form-group' >";
                            } 
                        ?>
                            <label for="Role" class="col-sm-2 control-label">Role <sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="role" name="role" required>
                                    <option value="0" <?php if($info[0]->role == 0) echo 'selected';?>>Operator</option>
                                    <option value="1" <?php if($info[0]->role == 1) echo 'selected';?>>Supervisor</option>
                                    <option value="2" <?php if($info[0]->role == 2) echo 'selected';?>>Coordinator</option>
                                </select>
                                <span class="help-block">
                                    <?=form_error('role')?>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Word"  class="col-sm-2 control-label">Word <sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="word" name="word">
                                    <option value="selected">system er selected union onujaie word ashbe</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-6">
                                <input type="hidden" name="id" value="<?=$info[0]->id?>" />
                                <button class="btn btn-primary" onclick="window.history.back();"><i class="fa fa-fw fa-arrow-left"></i> Go back</button>
                                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-check-square-o"></i> Update</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div><!-- /.box -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div>