<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$sub_title?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-map-marker"></i><?=$main_title?></a></li>
            <li class="active"><?=$sub_title?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">

                <!-- start data table -->
                <div style="clear:both;"></div>

                <br>
                <!-- Input addon -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=$sub_title?></h3>
                    </div>

                    <div class="box-body">
                        <?php
                            if (validation_errors()) {
                                echo '<div id="validation_errors" title="Error:" ';
                                echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
                                echo validation_errors();
                                echo '</ul></div>';
                                echo '</div>';
                            }
                        ?>

                        <div class="row">
                            <div class="col-lg-12">
                                <a class="btn btn-primary" data-toggle="modal" href="#add" title="Add" data-original-title="Add" ><i class="fa fa-plus"></i></a>
                                <a href="#" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                                <script>
                                    var table = ['word'];
                                    var image = '';
                                </script>
                                <a href="#" class="btn btn-danger" onclick="return deleteall(table,image);"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </div><br>
                        
                        <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
                            <thead>                         
                                <tr>
                                    <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                                    <th data-hide="phone"><i class="fa fa-fw fa-map-marker text-muted hidden-md hidden-sm hidden-xs"></i> Word Number</th>
                                    <th data-class="phone"><i class="fa fa-fw fa-arrow-up text-muted hidden-md hidden-sm hidden-xs"></i> Parent Union</th>
                                    <th data-hide="phone"><i class="fa fa-fw fa-pencil-square-o text-muted hidden-md hidden-sm hidden-xs"></i> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($words as $word): ?>
                                <tr id="row_<?=$word->id?>">
                                    <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?=$word->id?>" value="<?=$word->id?>" /></td>
                                    <td><?=$word->name?></td>
                                    <td><?=$word->parent_id?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a data-toggle="modal" href="#edit<?=$word->id?>" title="Edit" data-original-title="Edit">Edit</a>
                                                </li>
                                                <li>
                                                    <script>
                                                        var table = ["word"];
                                                        var image = "";
                                                    </script>
                                                    <a href="#" onclick="return dodelete(table,<?=$word->id?>,image);" title="Delete" >Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <!-- edit modal -->
                                <div class="modal fade" id="edit<?=$word->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Edit</h4>
                                        </div>
                                        <?=form_open(site_url("area/word_doedit"))?>
                                            <div class="modal-body">
                                                <!-- parent -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="slug"> Union Name</label>
                                                            <select class="form-control" name="parent_id">
                                                                <?php foreach($unions as $union) : ?>
                                                                    <option value="<?=$union->short_name?>" <?php if ($union->short_name == $word->parent_id) echo 'selected'; ?>><?=$union->slug?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- slug -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="slug"> Word Number</label>
                                                            <input class="form-control" name="name" type="text" value="<?=$word->name?>" placeholder="e.g: 1/2/3 etc..." required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="id" value="<?=$word->id?>">
                                                <button type="submit" name="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Update</button>
                                                <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                            </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                            </div>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>

                <script type="text/javascript">
                    $(function () {
                        $('#table-2').DataTable();
                    });
                </script>
                <!-- end data table -->

                <!-- add modal -->
                <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Add</h4>
                            </div>
                            <?=form_open(site_url("area/word_doadd"))?>
                                <div class="modal-body">
                                    <!-- parent -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="slug"> Union Name</label>
                                                <select class="form-control" name="parent_id">
                                                    <?php foreach($unions as $union) : ?>
                                                        <option value="<?=$union->short_name?>"><?=$union->slug?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- slug -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="slug"> Word Number</label>
                                                <input class="form-control" name="name" type="text" placeholder="e.g: 1/2/3 etc..." required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Add</button>
                                    <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                </div>
                            <?php echo form_close(); ?> 
                        </div>
                    </div>
                </div>

            </div>
            
            <!-- notice -->
            <?php $s = $this->session->flashdata('success');
            if (!empty($s)) { ?>
                <div id='msg' data-animation="drop" class="notify center top notify-dismissible notify-success">
                    <div class="message"><?=$this->session->flashdata('success')?></div>
                    <button type="button" class="close" data-close="notify" data-animation="drop" ;="">×</button>
                </div>
            <?php } ?>

            <?php $d = $this->session->flashdata('danger');
            if (!empty($d)) { ?>
                <div id='msg' data-animation="drop" class="notify center top notify-default notify-danger">
                    <div class="message"><?=$this->session->flashdata('danger')?></div>
                    <button type="button" class="close" data-close="notify" data-animation="drop" ;="">×</button>
                </div>
            <?php } ?>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

