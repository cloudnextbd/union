<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$sub_title?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-map-marker"></i><?=$main_title?></a></li>
            <li class="active"><?=$sub_title?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <!-- start data table -->
                <div style="clear:both;"></div>
                <br>
                <!-- Input addon -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=$sub_title?></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <a class="btn btn-primary" data-toggle="modal" href="#add" title="Add" data-original-title="Add" ><i class="fa fa-plus"></i></a>
                                <a href="#" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                                <script>
                                    var table = ['union'];
                                    var image = '';
                                </script>
                                <a href="#" class="btn btn-danger" onclick="return deleteall(table,image);"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </div><br>
                        <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
                            <thead>                         
                                <tr>
                                    <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                                    <th data-hide="phone"><i class="fa fa-fw fa-map-marker text-muted hidden-md hidden-sm hidden-xs"></i> Union Name</th>
                                    <th data-class="phone"><i class="fa fa-fw fa-ellipsis-h text-muted hidden-md hidden-sm hidden-xs"></i> Short Name</th>
                                    <th data-class="phone"><i class="fa fa-fw fa-arrow-up text-muted hidden-md hidden-sm hidden-xs"></i> Parent District</th>
                                    <th data-hide="phone"><i class="fa fa-fw fa-pencil-square-o text-muted hidden-md hidden-sm hidden-xs"></i> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($unions as $union): ?>
                                <tr id="row_<?=$union->id?>">
                                    <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?=$union->id?>" value="<?=$union->id?>" /></td>
                                    <td><?=$union->slug?></td>
                                    <td><?=$union->short_name?></td>
                                    <td><?=$union->parent_id?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a data-toggle="modal" href="#edit<?=$union->id?>" title="Edit" data-original-title="Edit">Edit</a>
                                                </li>
                                                <li>
                                                    <script>
                                                        var table = ["union"];
                                                        var image = "";
                                                    </script>
                                                    <a href="#" onclick="return dodelete(table,<?=$union->id?>,image);" title="Delete" >Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <!-- edit modal -->
                                <div class="modal fade" id="edit<?=$union->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Edit</h4>
                                        </div>
                                        <?=form_open(site_url("area/union_doedit"))?>
                                            <div class="modal-body">
                                                <!-- parent -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="slug"> Sub District Name</label>
                                                            <select class="form-control" name="parent_id">
                                                                <?php foreach($sub_districts as $sub_district) : ?>
                                                                    <option value="<?=$sub_district->short_name?>" <?php if ($sub_district->short_name == $union->parent_id) echo 'selected'; ?>><?=$sub_district->slug?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- slug -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="slug"> Union Name</label>
                                                            <input class="form-control" name="slug" type="text" value="<?=$union->slug?>" placeholder="e.g: Regional name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- short name -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="short_name"> Short Name</label>
                                                            <input class="form-control" name="short_name" type="text" value="<?=$union->short_name?>" placeholder="e.g: Short name like 'chd, bgd' etc.." required>
                                                       </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="id" value="<?=$union->id?>">
                                                <button type="submit" name="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Update</button>
                                                <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                            </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                            </div>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#table-2').DataTable();
                    });
                </script>
                <!-- end data table -->
                <!-- add modal -->
                <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Add</h4>
                            </div>
                            <?=form_open(site_url("area/union_doadd"))?>
                                <div class="modal-body">
                                    <!-- parent -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="slug"> Sub District Name</label>
                                                <select class="form-control" name="parent_id">
                                                    <?php foreach($sub_districts as $sub_district) : ?>
                                                        <option value="<?=$sub_district->short_name?>"><?=$sub_district->slug?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- slug -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="slug"> Union Name</label>
                                                <input class="form-control" name="slug" type="text" placeholder="e.g: Regional name" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- short name -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="short_name"> Short Name</label>
                                                <input class="form-control" name="short_name" type="text" placeholder="e.g: Short name like 'chd, bgd' etc.." required>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Add</button>
                                    <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                </div>
                            <?php echo form_close(); ?> 
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- notice -->
            <?php $s = $this->session->flashdata('success');
            if (!empty($s)) { ?>
                <div id='msg' data-animation="drop" class="notify center top notify-dismissible notify-success">
                    <div class="message"><?=$this->session->flashdata('success')?></div>
                    <button type="button" class="close" data-close="notify" data-animation="drop" ;="">×</button>
                </div>
            <?php } ?>
            <?php $d = $this->session->flashdata('danger');
            if (!empty($d)) { ?>
                <div id='msg' data-animation="drop" class="notify center top notify-default notify-danger">
                    <div class="message"><?=$this->session->flashdata('danger')?></div>
                    <button type="button" class="close" data-close="notify" data-animation="drop" ;="">×</button>
                </div>
            <?php } ?>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->