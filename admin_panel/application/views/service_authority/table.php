<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$sub_title?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-users"></i><?=$sub_title?></a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <!-- start data table -->
                <div style="clear:both;"></div><br>
                <!-- Input addon -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=$sub_title?></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <a class="btn btn-success" href="<?=base_url()?>service_authority/add"><i class="fa fa-user-plus"></i></a>
                                <a href="#" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                                <script>
                                    var table = ['service_authority'];
                                    var image = "assets/img/service_authority/<?=$list->image?>";
                                </script>
                                <a href="#" class="btn btn-danger" onclick="return deleteall(table,image);"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </div><br>
                        <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
                            <thead>                         
                                <tr>
                                    <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                                    <th data-hide="phone"><i class="fa fa-fw fa-bank text-muted hidden-md hidden-sm hidden-xs"></i> Authority Name</th>
                                    <th data-class="phone"><i class="fa fa-fw fa-image text-muted hidden-md hidden-sm hidden-xs"></i> Logo</th>
                                    <th data-hide="phone"><i class="fa fa-fw  fa-list-ul text-muted hidden-md hidden-sm hidden-xs"></i> Category</th>
                                    <th data-hide="phone"><i class="fa fa-fw fa-question text-muted hidden-md hidden-sm hidden-xs"></i> Status</th>
                                    <th data-hide="phone"><i class="fa fa-fw fa-pencil-square-o text-muted hidden-md hidden-sm hidden-xs"></i> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($lists as $list): ?>
                                <tr id="row_<?=$list->id?>">
                                    <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?=$list->id?>" value="<?=$list->id?>" /></td>
                                    <td><?=$list->ser_auth_name?></td>
                                    <td><img src="<?=base_url()?>assets/img/service_authority/<?=$list->logo?>" alt="<?=$list->ser_auth_name?>" /></td>
                                    <td>
                                    <?php if($list->category == 1) {
                                        echo "<span class='btn label-success'>Government</span>";
                                    } elseif($list->category == 2) {
                                        echo "<span class='btn label-warning'>Non-government</span>";
                                    } elseif($list->category == 3) {
                                        echo "<span class='btn label-primary'>Social</span>";
                                    } elseif($list->category == 4) {
                                        echo "<span class='btn label-info'>Personal</span>";
                                    } ?>
                                    </td>
                                    <td><?php if ($list->status == 1) {
                                        echo "<span id='status_" . $list->id . "'><a href='#' class = 'btn btn-success' onclick='p_status_change(table," . $list->id . "," . $list->status . ");'>Enable</a></span>";
                                    } else {
                                        echo "<span id='status_" . $list->id . "'><a href='#' class = 'btn btn-danger'  onclick='p_status_change(table," . $list->id . "," . $list->status . ");'>Disable</a></span>";
                                    } ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?=base_url()?>service_authority/edit/<?=$list->id?>" title="Edit">Edit</a>
                                                </li>
                                                <li>
                                                    <script>
                                                        var table = ["service_authority"];
                                                        var image = "assets/img/service_authority/<?=$list->image?>";
                                                    </script>
                                                    <a href="#" onclick="return dodelete(table,<?=$list->id?>,image);" title="Delete" >Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#table-2').DataTable();
                    });
                </script>
                <!-- end data table -->
            </div>
            <!-- notice -->
            <?php $s = $this->session->flashdata('success');
            if (!empty($s)) { ?>
                <div id='msg' data-animation="drop" class="notify center top notify-dismissible notify-success">
                    <div class="message"><?=$this->session->flashdata('success')?></div>
                    <button type="button" class="close" data-close="notify" data-animation="drop" ;="">×</button>
                </div>
            <?php } ?>
             <?php $d = $this->session->flashdata('danger');
            if (!empty($d)) { ?>
                <div id='msg' data-animation="drop" class="notify center top notify-default notify-danger">
                    <div class="message"><?=$this->session->flashdata('danger')?></div>
                    <button type="button" class="close" data-close="notify" data-animation="drop" ;="">×</button>
                </div>
            <?php } ?>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->