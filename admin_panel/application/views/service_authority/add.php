<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$sub_title?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-users"></i><?=$sub_title?></a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <button class="btn btn-primary" onclick="window.history.back();" style='margin-right: 5px'><i class="fa fa-fw fa-arrow-left"></i> Go back</button>
                        <h3 class="box-title"> <?=$sub_title?></h3>
                    </div>
                    <?=form_open_multipart(site_url("service_authority/add"), array("class" => "form-horizontal"))?>
                    <?php echo validation_errors(); ?>
                    <div class="box-body">
                        <?php 
                            if(form_error('ser_auth_name')){ 
                                echo "<div class='form-group has-error'>";
                            }else{    
                                echo "<div class='form-group'>";
                            } 
                        ?>
                            <label class="col-sm-2 control-label"> Authority Name <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" placeholder="authority name" name="ser_auth_name" value="<?=set_value('ser_auth_name')?>" required>
                            </div>
                            <span class="help-block">
                                <?=form_error('ser_auth_name')?>
                            </span>
                        </div>
                        <!-- logo -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label"> Logo <sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <input type="file" name="logo" onchange="imageshow(this);" required />
                                <p class="help-block">jpg or png image & maximum 1 MB</p> 
                            </div>
                            <div class="col col-lg-4" >
                                <img id="previewImg1" width="80px" src="<?=base_url()?>assets/img/no_image.png" />
                                <script>
                                    function imageshow(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();
                                            reader.onload = function(e) {
                                                $('#previewImg1')
                                                .attr('src', e.target.result)
                                                .width(80);
                                            }
                                            reader.readAsDataURL(input.files[0]);
                                        } else {
                                            var filename = "";
                                            filename = "file:\/\/" + input.value;
                                            document.form2.previewImg1.src = filename;
                                            document.form2.previewImg1.style.width = "80px";
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                        <!-- service category -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Category <sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <select class="form-control" name="category" required>
                                    <option value="1" selected>Government</option>
                                    <option value="2">Non-government</option>
                                    <option value="3">Social</option>
                                    <option value="4">Personal</option>
                                </select>
                            </div>
                        </div>
                        <!-- about authority -->
                        <?php
                            if(form_error('about')){ 
                                echo "<div class='form-group has-error'>";
                            }else{    
                                echo "<div class='form-group'>";
                            } 
                        ?>
                            <label class="col-sm-2 control-label">About Authority <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <textarea  class="form-control" rows="15" name="about" placeholder="write here..." required><?=set_value('about')?></textarea>
                            </div>
                            <span class="help-block">
                                <?=form_error('about')?>
                            </span>
                        </div>
                        <!-- image -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label"> Image <sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <input type="file" name="image" onchange="imageshow2(this);" required />
                                <p class="help-block">jpg or png image & maximum 1 MB</p> 
                            </div>
                            <div class="col col-lg-4" >
                                <img id="previewImg2" width="80px" src="<?=base_url()?>assets/img/no_image.png" />
                                <script>
                                    function imageshow2(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();
                                            reader.onload = function(e) {
                                                $('#previewImg2')
                                                .attr('src', e.target.result)
                                                .width(80);
                                            }
                                            reader.readAsDataURL(input.files[0]);
                                        } else {
                                            var filename = "";
                                            filename = "file:\/\/" + input.value;
                                            document.form2.previewImg2.src = filename;
                                            document.form2.previewImg2.style.width = "80px";
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                        <!-- address -->
                        <?php 
                            if(form_error('address')){ 
                                echo "<div class='form-group has-error'>";
                            }else{    
                                echo "<div class='form-group'>";
                            } 
                        ?>
                            <label class="col-sm-2 control-label">Address <sup style="color: red">*</sup></label>
                            <div class="col-sm-6">
                                <textarea  class="form-control" rows="6" name="address" placeholder="write here..." required><?=set_value('address')?></textarea>
                            </div>
                            <span class="help-block">
                                <?=form_error('address')?>
                            </span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status <sup style="color: red">*</sup></label>
                            <div class="col-sm-2">
                                <select class="form-control" name="status">
                                    <option value="1" selected>Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-6">
                                <button type="reset" value="Reset" class="btn btn-danger">Reset</button>
                                <button type="submit" class="btn btn-success">Add</button>
                            </div>
                        </div>
                    </div>
                    <?=form_close()?>
                </div><!-- /.box -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div>