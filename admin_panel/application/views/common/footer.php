    </div>
        <!-- ./wrapper -->
        <script src="<?=base_url()?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>

        
        <!-- DataTables -->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>


        <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>
        <script src="<?=base_url()?>assets/dist/js/demo.js"></script>


        
        <script src="<?=base_url()?>assets/js/noti_js/notify.js"></script>
        <script src="<?=base_url()?>assets/js/noti_js/prettify.js"></script>
        
        <script src="<?=base_url()?>assets/js/custom.js"></script>

        <!--rana script-->
        
        
        
        <script type="text/javascript">
            
            $("#pres_union").change(function() {
                $.ajax({
                    url: BASE_URL + "ajaxcalls/get_word_by_union_id/" + $(this).val(),
                    type: "GET",
                    success: function(data) {
                        alert(data);
                        //$("#pres_word").html(data);
                    }

                });
            });
            
        </script>
        
        
        

        
        
        
        
        
        
        
        
        
        
        <script>
            
            $(document).ready(function() {
                
                // notice
                setTimeout(function(){  
                    $("#msg").addClass("drop");
                }, 4000);
                
                //for service form ---------------------------------------------
                // for goal field ----------------------------------------------
                var max_fields      = 20; //maximum input boxes allowed
                var wrapper         = $("#goal_fields"); //Fields wrapper
                var add_button      = $("#add_goal_btn"); //Add button ID

                var x = 1; //initlal text box count
                $(add_button).click(function(e){ //on add input button click
                    e.preventDefault();
                    if(x < max_fields){ //max input box allowed
                        x++; //text box increment
                        $(wrapper).append('<div id="goal_field" class="form-group"><div class="col-sm-2"></div><div class="col-sm-6"><textarea  class="form-control" rows="2" name="goal[]" placeholder="write here..." required></textarea></div><div id="remove_btn" class="col-sm-4"><button type="button" id="remove_goal_btn" class="btn btn-danger"><i class="fa fa-fw fa-remove "></i></button></div></div>'); //add input box
                    }
                });

                $(wrapper).on("click","#remove_goal_btn", function(e){ //user click on remove text
                    e.preventDefault(); $('#remove_btn').parent('div#goal_field').remove(); x--;
                })
                
                // for purpose field -------------------------------------------
                var purpose_max_fields      = 20; 
                var purpose_wrapper         = $("#purpose_fields"); //Fields wrapper
                var purpose_add_button      = $("#add_purpose_btn"); //Add button ID

                var x = 1; //initlal text box count
                $(purpose_add_button).click(function(e){ //on add input button click
                    e.preventDefault();
                    if(x < purpose_max_fields){ //max input box allowed
                        x++; //text box increment
                        $(purpose_wrapper).append('<div id="purpose_field" class="form-group"><div class="col-sm-2"></div><div class="col-sm-6"><textarea  class="form-control" rows="2" name="purpose[]" placeholder="write here..." required></textarea></div><div id="purpose_remove_btn" class="col-sm-4"><button type="button" id="remove_purpose_btn" class="btn btn-danger"><i class="fa fa-fw fa-remove "></i></button></div></div>'); //add input box
                    }
                });

                $(purpose_wrapper).on("click","#remove_purpose_btn", function(e){ //user click on remove text
                    e.preventDefault(); $('#purpose_remove_btn').parent('div#purpose_field').remove(); x--;
                })
                
                // for Qualifications field ------------------------------------
                var qualifi_max_fields      = 20; 
                var qualifi_wrapper         = $("#qualifi_fields"); //Fields wrapper
                var qualifi_add_button      = $("#add_qualifi_btn"); //Add button ID

                var x = 1; //initlal text box count
                $(qualifi_add_button).click(function(e){ //on add input button click
                    e.preventDefault();
                    if(x < qualifi_max_fields){ //max input box allowed
                        x++; //text box increment
                        $(qualifi_wrapper).append('<div id="qualifi_field" class="form-group"><div class="col-sm-2"></div><div class="col-sm-6"><textarea  class="form-control" rows="2" name="qualifi[]" placeholder="write here..." required></textarea></div><div id="qualifi_remove_btn" class="col-sm-4"><button type="button" id="remove_qualifi_btn" class="btn btn-danger"><i class="fa fa-fw fa-remove "></i></button></div></div>'); //add input box
                    }
                });

                $(qualifi_wrapper).on("click","#remove_qualifi_btn", function(e){ //user click on remove text
                    e.preventDefault(); $('#qualifi_remove_btn').parent('div#qualifi_field').remove(); x--;
                })
                
                // for disqualification field ----------------------------------
                var disqualifi_max_fields      = 20; 
                var disqualifi_wrapper         = $("#disqualifi_fields"); //Fields wrapper
                var disqualifi_add_button      = $("#add_disqualifi_btn"); //Add button ID

                var x = 1; //initlal text box count
                $(disqualifi_add_button).click(function(e){ //on add input button click
                    e.preventDefault();
                    if(x < disqualifi_max_fields){ //max input box allowed
                        x++; //text box increment
                        $(disqualifi_wrapper).append('<div id="disqualifi_field" class="form-group"><div class="col-sm-2"></div><div class="col-sm-6"><textarea  class="form-control" rows="2" name="disqualifi[]" placeholder="write here..." required></textarea></div><div id="disqualifi_remove_btn" class="col-sm-4"><button type="button" id="remove_disqualifi_btn" class="btn btn-danger"><i class="fa fa-fw fa-remove "></i></button></div></div>'); //add input box
                    }
                });

                $(disqualifi_wrapper).on("click","#remove_disqualifi_btn", function(e){ //user click on remove text
                    e.preventDefault(); $('#disqualifi_remove_btn').parent('div#disqualifi_field').remove(); x--;
                })
                
                // selection process -------------------------------------------
                var selection_max_fields      = 20; 
                var selection_wrapper         = $("#selection_fields"); //Fields wrapper
                var selection_add_button      = $("#add_selection_btn"); //Add button ID

                var x = 1; //initlal text box count
                $(selection_add_button).click(function(e){ //on add input button click
                    e.preventDefault();
                    if(x < selection_max_fields){ //max input box allowed
                        x++; //text box increment
                        $(selection_wrapper).append('<div id="selection_field" class="form-group"><div class="col-sm-2"></div><div class="col-sm-6"><textarea  class="form-control" rows="2" name="selection[]" placeholder="write here..." required></textarea></div><div id="selection_remove_btn" class="col-sm-4"><button type="button" id="remove_selection_btn" class="btn btn-danger"><i class="fa fa-fw fa-remove "></i></button></div></div>'); //add input box
                    }
                });

                $(selection_wrapper).on("click","#remove_selection_btn", function(e){ //user click on remove text
                    e.preventDefault(); $('#selection_remove_btn').parent('div#selection_field').remove(); x--;
                })
                
            });
        </script>
       
    </body>
</html>
