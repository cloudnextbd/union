<!-- Left side column -->
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <!-- dashboard -->
            <li class="<?php if ($page == 'dashboard/dashboard') echo "active"; ?>">
                <a href="<?=site_url()?>" >
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- site user -->
            <li class="<?php if ($page == 'user/user' || $page == 'user/add' || $page == 'user/edit') echo "active"; ?>">
                <a href="<?=site_url("user")?>" >
                    <i class="fa fa-users"></i>
                    <span>Site User's</span>
                </a>
            </li>
            
            <!-- coverage area -->
            <li class="treeview <?php if ($page == 'area/district' || $page == 'area/sub_district' || $page == 'area/union' || $page == 'area/word' || $page == 'area/village')   echo "active"; ?>">
                <a href="#">
                    <i class="fa fa-map-marker"></i>
                    <span>Coverage Area</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($page == 'area/district') echo "active"; ?>">
                        <a href="<?= site_url("area/district") ?>"><i class="fa fa-arrow-right"></i> District</a>
                    </li>
                    <li class="<?php if ($page == 'area/sub_district') echo "active"; ?>">
                        <a href="<?= site_url("area/sub_district") ?>"><i class="fa fa-arrow-right"></i> Sub District</a>
                    </li >
                    <li class="<?php if ($page == 'area/union') echo "active"; ?>">
                        <a href="<?= site_url("area/union") ?>"><i class="fa fa-arrow-right"></i> Union</a>
                    </li>
                    <li class="<?php if ($page == 'area/word') echo "active"; ?>">
                        <a href="<?= site_url("area/word") ?>"><i class="fa fa-arrow-right"></i> Word</a>
                    </li>
                    <li class="<?php if ($page == 'area/village') echo "active"; ?>">
                        <a href="<?= site_url("area/village") ?>"><i class="fa fa-arrow-right"></i> Village</a>
                    </li>
                </ul>
            </li>
            
            <!-- service -->
            <li class="<?php if ($page == 'services/table' || $page == 'services/add' || $page == 'services/edit') echo "active"; ?>">
                <a href="<?=site_url("services")?>" >
                    <i class="fa fa-heart"></i>
                    <span>Service's</span>
                </a>
            </li>
            
            <!-- service authority -->
            <li class="<?php if ($page == 'service_authority/table' || $page == 'service_authority/add' || $page == 'service_authority/edit') echo "active"; ?>">
                <a href="<?=site_url("service_authority")?>" >
                    <i class="fa fa-suitcase"></i>
                    <span>Service Authority</span>
                </a>
            </li>
            
            <!-- file's import -->
            <li class="<?php if ($page == 'file/file') echo "active"; ?>">
                <a href="<?=site_url("files_operation")?>" >
                    <i class="fa fa-file"></i>
                    <span>File's Operation</span>
                </a>
            </li>
            
            <!-- common settings -->
            <li class="<?php if ($page == 'settings/common_settings') echo "active"; ?>" >
                <a href="<?php echo site_url("admin_panel/common_settings") ?>" >
                    <i class="fa fa-fw fa-gears"></i>
                    <span>Common Settings</span>
                </a>
            </li>
        </ul>
    </section>
</aside>