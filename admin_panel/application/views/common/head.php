<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?=$sub_title?> || Admin Panel</title>
        <link href="<?=base_url()?>assets/img/favicon.ico" rel="shortcut icon" type="image/png">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" href="<?=base_url()?>assets/img/favicon.ico" >
        <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css" >
        <link rel="stylesheet" href="<?=base_url()?>assets/extra/font-awesome.min.css" >
        <link rel="stylesheet" href="<?=base_url()?>assets/extra/ionicons.min.css" >

       <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.css">

        <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css" >
        <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/skins/_all-skins.min.css" >
        
        
        <link rel="stylesheet" href="<?=base_url()?>assets/css/noti_css/notify.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/css/noti_css/prettify.css">

        <link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css" >

        <script src="<?=base_url()?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!--[if lt IE 9]>
        <script src="<?=base_url()?>assets/extra/html5shiv.min.js"></script>
        <script src="<?=base_url()?>assets/extra/respond.min.js"></script>
        <![endif]-->

        <script>
            var BASE_URL = '<?=base_url()?>';
        </script>
    </head>
    <body class="sidebar-mini skin-blue fixed">
        <div class="wrapper">
            <!-- header  -->
            <header class="main-header">
                <!-- Logo -->
                <a href="<?=site_url("")?>" class="logo">
                    <span class="logo-mini"><b>Admin Panel</b></span>
                    <span class="logo-lg"><b>Admin Panel</b></span>
                </a>
                <!-- header navbar -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- user account -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?=base_url()?>assets/img/profile/<?php echo $this->admin->info->avatar ?>" class="user-image" alt="<?php echo $this->admin->info->avatar ?>">
                                    <span class="hidden-xs"><?php echo $this->admin->info->first_name . ' ' . $this->admin->info->last_name ?> &nbsp; As &nbsp;
                                        <?php if($this->admin->info->level == 0){
                                            echo '<span class="label label-primary">Manager</span>';
                                        }elseif($this->admin->info->level == 1){
                                            echo '<span class="label label-warning">Admin</span>';
                                        }elseif($this->admin->info->level == 2){
                                            echo '<span class="label label-success">Super Admin</span>';
                                        } ?>
                                    </span>        
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?=base_url()?>assets/img/profile/<?php echo $this->admin->info->avatar ?>" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo $this->admin->info->first_name . ' ' . $this->admin->info->last_name ?>
                                            <b><?php echo $this->admin->info->email ?></b>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="<?=site_url("dashboard/logout/" . $this->security->get_csrf_hash())?>" class="btn btn-default btn-flat">Logout</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            