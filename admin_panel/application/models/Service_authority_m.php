<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Service_authority_m extends CI_Model {

    function __construct() {
        
        parent :: __construct();
        
        $this->table = 'service_authority';
        
    }

    public function add($logo, $image) {
        
        $value = array(

            'ser_auth_name'=> $this->filter->nohtml($this->input->post('ser_auth_name')),
            'logo'=> $logo,
            'category'=> $this->filter->nohtml($this->input->post('category')),
            'about'=> $this->filter->nohtml($this->input->post('about')),
            'image'=> $image,
            'address'=> $this->filter->nohtml($this->input->post('address')),
            'status'=> $this->filter->nohtml($this->input->post('status'))
            
        );
        
        $result = $this->data->save($this->table, $value);

        if($result){
            return TRUE;
        }  else {
            return FALSE; 
        }
        
    }
    
    public function edit($id, $logo = NULL, $image = NULL) {

        $value = array(

            'ser_auth_name'=> $this->filter->nohtml($this->input->post('ser_auth_name')),
            'logo'=> $logo,
            'category'=> $this->filter->nohtml($this->input->post('category')),
            'about'=> $this->filter->nohtml($this->input->post('about')),
            'image'=> $image,
            'address'=> $this->filter->nohtml($this->input->post('address')),
            'status'=> $this->filter->nohtml($this->input->post('status'))
            
        );

        $result = $this->data->update($this->table, $id, $value);

        if($result){
            return TRUE;
        }  else {
            return FALSE; 
        }
    }

}