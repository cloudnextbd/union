<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services_m extends CI_Model {

    function __construct() {
        
        parent :: __construct();
        
        $this->table = 'services';
        $this->service_authority_table = 'service_authority';
        
        $this->goal_table = 'services_goal';
        $this->purpose_table = 'services_purpose';
        $this->qualifi_table = 'services_qualifi';
        $this->disqualifi_table = 'services_disqualifi';
        $this->selection_table = 'services_selection';
        
    }

    public function add() {
        
        $value = array(

            'ser_name'=> $this->filter->nohtml($this->input->post('ser_name')),
            'authority_id'=> $this->filter->nohtml($this->input->post('authority_id')),
            'canvas'=> $this->filter->nohtml($this->input->post('canvas')),
            'definition'=> $this->filter->nohtml($this->input->post('definition')),
            'status'=> $this->filter->nohtml($this->input->post('status'))
            
        );
        
        $last_id = $this->data->save($this->table, $value);

        if($last_id){
            
            // goal data save 
            $goals = $this->input->post('goal');
            $this->data->delete($this->goal_table, $last_id); // delete exits same parent id
            foreach ($goals as $goal) {
                $value = array(
                    'id' => $last_id,
                    'goal' => $goal
                );
                $this->data->save($this->goal_table, $value);
            }

            // purpose data save
            $purposes = $this->input->post('purpose');
            $this->data->delete($this->purpose_table, $last_id); // delete exits same parent id
            foreach ($purposes as $purpose) {
                $value = array(
                    'id' => $last_id,
                    'purpose' => $purpose
                );
                $this->data->save($this->purpose_table, $value);
            }
            
            // qualifi data save
            $qualifis = $this->input->post('qualifi');
            $this->data->delete($this->qualifi_table, $last_id); // delete exits same parent id
            foreach ($qualifis as $qualifi) {
                $value = array(
                    'id' => $last_id,
                    'qualifi' => $qualifi
                );
                $this->data->save($this->qualifi_table, $value);
            }
            
            // disqualifi data save
            $disqualifis = $this->input->post('disqualifi');
            $this->data->delete($this->disqualifi_table, $last_id); // delete exits same parent id
            foreach ($disqualifis as $disqualifi) {
                $value = array(
                    'id' => $last_id,
                    'disqualifi' => $disqualifi
                );
                $this->data->save($this->disqualifi_table, $value);
            }
            
            // selection data save
            $selections = $this->input->post('selection');
            $this->data->delete($this->selection_table, $last_id); // delete exits same parent id
            foreach ($selections as $selection) {
                $value = array(
                    'id' => $last_id,
                    'selection' => $selection
                );
                $this->data->save($this->selection_table, $value);
            }
            
            
            return TRUE; 
            
        }  else {
            return FALSE; 
        }
        
    }
    
    public function edit($id) {

        $value = array(

            'ser_name'=> $this->filter->nohtml($this->input->post('ser_name')),
            'authority_id'=> $this->filter->nohtml($this->input->post('authority_id')),
            'canvas'=> $this->filter->nohtml($this->input->post('canvas')),
            'definition'=> $this->filter->nohtml($this->input->post('definition')),
            'status'=> $this->filter->nohtml($this->input->post('status'))
            
        );

        $result = $this->data->update($this->table, $id, $value);

        if($result){
            return TRUE;
        }  else {
            return FALSE; 
        }
    }
    
    
    public function get_sepcific_with_parent() {
        
        $this->db->select('services.id, services.ser_name, service_authority.ser_auth_name, service_authority.logo, service_authority.category, services.status');
        $this->db->from('services');
        $this->db->join('service_authority', 'service_authority.id = services.authority_id', 'left');
        $query = $this->db->get();
        return $query->result();
        
    }
        
    

}