<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class user_m extends CI_Model {

    function __construct() {

        $this->table = 'user';

        parent :: __construct();

    }

    public function add($image) {

        //$raw_pass = $this->input->post('password');       
        //$php_pass = new PasswordHash(12, false);
        //$pass = $php_pass->HashPassword($raw_pass);
        
        $pass = md5(sha1($this->input->post('password')));

        $value = array(

            'first_name'=> $this->filter->nohtml($this->input->post('first_name')),
            'last_name'=> $this->filter->nohtml($this->input->post('last_name')),
            'address'=> $this->filter->nohtml($this->input->post('address')),
            'about'=> $this->filter->nohtml($this->input->post('about')),
            'image'=> $image,
            'status'=> $this->filter->nohtml($this->input->post('status')),
            'role'=> $this->filter->nohtml($this->input->post('role')),
            'union'=> $this->filter->nohtml($this->input->post('union')),
            'word'=> $this->filter->nohtml($this->input->post('word')),
            'phone'=> $this->filter->nohtml($this->input->post('phone')),
            'password'=> $pass

        );
        
        $result = $this->data->save($this->table, $value);

        if($result){

            return TRUE;

        }  else {

            return FALSE; 

        }
        
    }
    
    public function edit($id, $image = NULL) {

        $value = array(

            'first_name'=> $this->filter->nohtml($this->input->post('first_name')),
            'last_name'=> $this->filter->nohtml($this->input->post('last_name')),
            'about'=> $this->filter->nohtml($this->input->post('about')),
            'image'=> $image,
            'status'=> $this->filter->nohtml($this->input->post('status')),
            'role'=> $this->filter->nohtml($this->input->post('role')),
            'word'=> $this->filter->nohtml($this->input->post('word'))
                
        );

        $result = $this->data->update($this->table, $id, $value);

        if($result){

            return TRUE;

        }  else {

            return FALSE; 

        }

    }



    // public function is_unique_without_this_row($table, $field, $row_id,  $data){

    //     return $this->db->like("$field", $data)->where_not_in('id', $row_id)->get("$table");

    //  }


}