<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Union extends CI_Model {

    function __construct() {

        $this->table = 'union';

        parent :: __construct();
        
    }

    public function check_duplicate() {

        $f2 = 'short_name';
            
        $d2 = $this->filter->nohtml($this->input->post('short_name'));

        $s = $this->db->select('id')->where("$f2", $d2)->get($this->table);
        
        if ($s->num_rows() > 0) {
            return false;
        } else {
            return true;
        }

    }

    public function add() {
        
        $value = array(

            'parent_id'=> $this->filter->nohtml($this->input->post('parent_id')),
            'slug'=> $this->filter->nohtml($this->input->post('slug')),
            'short_name'=> $this->filter->nohtml($this->input->post('short_name'))
            
        );
        
        $result = $this->data->save($this->table, $value);

         if($result){

            return TRUE;

         }  else {

            return FALSE; 

         }
        
    }
    
    public function edit($id) {

        $value = array(

            'parent_id'=> $this->filter->nohtml($this->input->post('parent_id')),
            'slug'=> $this->filter->nohtml($this->input->post('slug')),
            'short_name'=> $this->filter->nohtml($this->input->post('short_name'))
            
        );

        $result = $this->data->update($this->table, $id, $value);

        if($result){

            return TRUE;

        }  else {

            return FALSE; 

        }
    }


}
