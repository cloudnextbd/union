<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->user_table = 'user';
        

        $this->load->model("login_m");
        $this->load->model("admin_m");

        if ($this->admin->loggedin) {

            redirect(site_url('dashboard'));

        }

    }

    // *** index ---------------------------------------------------------------
    public function index() {

        $data['title'] = "Login Form";
        $this->load->view('login/login', $data);

    }

    // *** login check ---------------------------------------------------------
    public function logincheck() {

        if(!$_POST){

            redirect(site_url('login'));

        }else{

            // retrieve log info from cookie ------------
            $config = $this->config->item("cookieprefix");
            if ($this->admin->loggedin) {
                redirect('dashboard');
            }

            // get info ------------
            $email = $this->input->post("email", true);
            $pass = $this->filter->nohtml($this->input->post("pass", true));
            $rem = $this->input->post("rem", true);

            // blank check ------------
            if (empty($email) || empty($pass)) {
                $this->session->set_flashdata("danger", 'Empty Email or Passwod!');
            }

            // is email exist ------------
            $login = $this->login_m->get_user_by_email($email);
            if ($login->num_rows() == 0) {
                $this->session->set_flashdata("danger", ' Incorrect Email or Password!');
            }

            // pass matching ------------
            $r = $login->row();
            $id = $r->id;
            $phpass = new PasswordHash(12, false);
            if (!$phpass->CheckPassword($pass, $r->password)) {
                $this->session->set_flashdata("danger", ' Incorrect Email or Password!');
            }

            // generate a token and update it ------------
            $token = rand(1, 100000) . $email;
            $token = md5(sha1($token));
            $this->login_m->update_user_token($id, $token);


            // if remenber create cookies ------------
            if ($rem == 1) {
                $ttl = 3600 * 12;
            } else {
                $ttl = 3600 * 12;
            }
            setcookie($config . "em", $email, time() + $ttl, "/");
            setcookie($config . "tkn", $token, time() + $ttl, "/");
            redirect(site_url('dashboard'));

        }

        
    }
    
    // *** inactive ------------------------------------------------------------
    public function inactive() {

        echo "Your are inactive user";
        
    }

    


}