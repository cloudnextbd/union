<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Area extends CI_Controller {

    function __construct() {

        parent :: __construct();

        $this->load->model('data');

        $this->district_table = 'district';
        $this->load->model('district');
        
        $this->sub_district_table = 'sub_district';
        $this->load->model('sub_district');

        $this->union_table = 'union';
        $this->load->model('union');

        $this->word_table = 'word';
        $this->load->model('word');

        $this->village_table = 'village';
        $this->load->model('village');

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if (!$this->admin->loggedin) {
            redirect(site_url('login'));
        }

    }

    public function index() {


        redirect(site_url('area/district'));

    }
    
    // ##############################################################################################

    public function district() {

        $data['districts'] = $this->data->getall($this->district_table);

        $data['main_title'] = "Coverage Area";

        $data['sub_title'] = "Districts";

        $data['page'] = "area/district";

        $this->load->view('common/template', $data);

    }

    // district do add
    public function district_doadd() {

        if($_POST){

            // check duplicate
            $result = $this->district->check_duplicate();
            
            if($result){

                $id = $this->district->add();

                if ($id) {
                    
                    $this->session->set_flashdata('success', 'Added Successfully!');
                    redirect(site_url('area/district'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Added!');
                    redirect(site_url('area/district'));

                }
                

            }  else {

                $this->session->set_flashdata('danger', 'This data already exist!');
                redirect(site_url('area/district'));

            }

        } else {

            redirect(site_url('area/district'));

        }
        
    }


    // do edit
    public function district_doedit() {

        if($_POST){

            // check duplicate
            $result = $this->district->check_duplicate();
            
            if($result){

                $id = $this->filter->nohtml($this->input->post('id'));

                $done = $this->district->edit($id);

                if ($done) {
                    
                    $this->session->set_flashdata('success', 'Update Successfully!');
                    redirect(site_url('area/district'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Update!');
                    redirect(site_url('area/district'));

                }
                

            }  else {

                $this->session->set_flashdata('danger', 'This data already exist!');
                redirect(site_url('area/district'));

            }

        } else {

            redirect(site_url('area/district'));

        }

    }


    // ##############################################################################################

    public function sub_district() {

        $data['sub_districts'] = $this->data->getall($this->sub_district_table);

        $data['districts'] = $this->data->getall($this->district_table);
        

        $data['main_title'] = "Coverage Area";

        $data['sub_title'] = "Sub District";

        $data['page'] = "area/sub_district";

        $this->load->view('common/template', $data);

    }

    public function sub_district_doadd() {

        if($_POST){

            // check duplicate
            $result = $this->sub_district->check_duplicate();
            
            if($result){

                $id = $this->sub_district->add();

                if ($id) {
                    
                    $this->session->set_flashdata('success', 'Added Successfully!');
                    redirect(site_url('area/sub_district'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Added!');
                    redirect(site_url('area/sub_district'));

                }
                

            }  else {

                $this->session->set_flashdata('danger', 'This data already exist!');
                redirect(site_url('area/sub_district'));

            }

        } else {

            redirect(site_url('area/sub_district'));

        }
        
    }


    public function sub_district_doedit() {

        if($_POST){

            // check duplicate
            $result = $this->sub_district->check_duplicate();
            
            if($result){

                $id = $this->filter->nohtml($this->input->post('id'));

                $done = $this->sub_district->edit($id);

                if ($done) {
                    
                    $this->session->set_flashdata('success', 'Update Successfully!');
                    redirect(site_url('area/sub_district'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Update!');
                    redirect(site_url('area/sub_district'));

                }
                

            }  else {

                $this->session->set_flashdata('danger', 'This data already exist!');
                redirect(site_url('area/sub_district'));

            }

        } else {

            redirect(site_url('area/sub_district'));

        }

    }


    // ##############################################################################################

    public function union() {

        $data['unions'] = $this->data->getall($this->union_table);

        $data['sub_districts'] = $this->data->getall($this->sub_district_table);

        $data['main_title'] = "Coverage Area";

        $data['sub_title'] = "Union";

        $data['page'] = "area/union";

        $this->load->view('common/template', $data);

    }

    public function union_doadd() {

        if($_POST){

            // check duplicate
            $result = $this->union->check_duplicate();
            
            if($result){

                $id = $this->union->add();

                if ($id) {
                    
                    $this->session->set_flashdata('success', 'Added Successfully!');
                    redirect(site_url('area/union'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Added!');
                    redirect(site_url('area/union'));

                }
                

            }  else {

                $this->session->set_flashdata('danger', 'This data already exist!');
                redirect(site_url('area/union'));

            }

        } else {

            redirect(site_url('area/union'));

        }
        
    }


    public function union_doedit() {

        if($_POST){

            // check duplicate
            $result = $this->union->check_duplicate();
            
            if($result){

                $id = $this->filter->nohtml($this->input->post('id'));

                $done = $this->union->edit($id);

                if ($done) {
                    
                    $this->session->set_flashdata('success', 'Update Successfully!');
                    redirect(site_url('area/union'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Update!');
                    redirect(site_url('area/union'));

                }
                

            }  else {

                $this->session->set_flashdata('danger', 'This data already exist!');
                redirect(site_url('area/union'));

            }

        } else {

            redirect(site_url('area/union'));

        }

    }


    // ##############################################################################################

    public function word() {

        $data['words'] = $this->data->getall($this->word_table);

        $data['unions'] = $this->data->getall($this->union_table);

        $data['main_title'] = "Coverage Area";

        $data['sub_title'] = "Word";

        $data['page'] = "area/word";

        $this->load->view('common/template', $data);

    }

    public function word_doadd() {

        if($_POST){

            
            $id = $this->word->add();

            if ($id) {
                
                $this->session->set_flashdata('success', 'Added Successfully!');
                redirect(site_url('area/word'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Added!');
                redirect(site_url('area/word'));

            }
                

        } else {

            redirect(site_url('area/word'));

        }
        
    }


    public function word_doedit() {

        if($_POST){

            $id = $this->filter->nohtml($this->input->post('id'));

            $done = $this->word->edit($id);

            if ($done) {
                
                $this->session->set_flashdata('success', 'Update Successfully!');
                redirect(site_url('area/word'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Update!');
                redirect(site_url('area/word'));

            }
                

        } else {

            redirect(site_url('area/word'));

        }

    }


    // ##############################################################################################

    public function village() {

        $data['villages'] = $this->data->getall($this->village_table);

        $data['unions'] = $this->data->getall($this->union_table);

        $data['words'] = $this->data->getall($this->word_table);

        $data['main_title'] = "Coverage Area";

        $data['sub_title'] = "Village";

        $data['page'] = "area/village";

        $this->load->view('common/template', $data);

    }

    public function village_doadd() {

        if($_POST){

            $id = $this->village->add();

            if ($id) {
                
                $this->session->set_flashdata('success', 'Added Successfully!');
                redirect(site_url('area/village'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Added!');
                redirect(site_url('area/village'));

            }
                
        } else {

            redirect(site_url('area/village'));

        }
        
    }


    public function village_doedit() {

        if($_POST){

            $id = $this->filter->nohtml($this->input->post('id'));

            $done = $this->village->edit($id);

            if ($done) {
                
                $this->session->set_flashdata('success', 'Update Successfully!');
                redirect(site_url('area/village'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Update!');
                redirect(site_url('area/village'));

            }
                
        } else {

            redirect(site_url('area/village'));

        }
    }




}