<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Service_authority extends CI_Controller {

    function __construct() {

        parent :: __construct();

        $this->service_authority_table = 'service_authority';
        $this->service_authority_path = './assets/img/service_authority/';
        $this->load->model('service_authority_m');
        
        $this->load->model('data');
        
        if (!$this->admin->loggedin) {
            redirect(site_url('login'));
        }

    }

    
    public function index() {

        $data['lists'] = $this->data->getall($this->service_authority_table);

        $data['sub_title'] = "Service Authority";

        $data['page'] = "service_authority/table";

        $this->load->view('common/template', $data);

    }

    public function add() {
        
        
        if(!$_POST){
            
            $data['main_title'] = "Service Authority";

            $data['sub_title'] = "Add Service Authority";

            $data['page'] = "service_authority/add";

            $this->load->view('common/template', $data);
            
        }else{
            
            $this->load->library('form_validation');

            $this->form_validation->set_rules('ser_auth_name', 'Name', 'required');
            
            $this->form_validation->set_rules('about', 'About', 'required');

            $this->form_validation->set_rules('address', 'Address', 'required');
            

            if ($this->form_validation->run() == FALSE){
                

                $data['main_title'] = "Service Authority";

                $data['sub_title'] = "Add Service Authority";

                $data['page'] = "service_authority/add";

                $this->load->view('common/template', $data);
                

            } else {
                
                // logo
                if ($_FILES['logo']['name'] != '') {

                    $image_upload = $this->do_upload('logo', $this->service_authority_path);

                    if ($image_upload == FALSE) {

                        $this->session->set_flashdata('danger', 'Image Upload Failed');
                        redirect(site_url('service_authority'));

                    } else {

                        $logo = $image_upload["file_name"];

                    }

                }
                
                // autho image
                if ($_FILES['image']['name'] != '') {

                    $image_upload = $this->do_upload('image', $this->service_authority_path);

                    if ($image_upload == FALSE) {

                        $this->session->set_flashdata('danger', 'Image Upload Failed');
                        redirect(site_url('service_authority'));

                    } else {

                        $image = $image_upload["file_name"];

                    }

                }

                $id = $this->service_authority_m->add($logo, $image);

                if ($id) {
                    
                    $this->session->set_flashdata('success', 'Added Successfully!');
                    redirect(site_url('service_authority'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Added!');
                    redirect(site_url('service_authority'));

                }

            }
            
        }
        
    }
    
    // edit
    public function edit($id = null) {

        if(!$_POST){

            $data['info'] = $this->data->getone($this->service_authority_table, $id);

            $data['main_title'] = "Service Authority";

            $data['sub_title'] = "Add Service Authority";

            $data['page'] = "service_authority/edit";

            $this->load->view('common/template', $data);

        } else {

            
            $this->id = $this->filter->nohtml($this->input->post('id'));
            $this->session->set_flashdata('id', $this->id);

            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('ser_auth_name', 'Name', 'required');
            
            $this->form_validation->set_rules('about', 'About', 'required');

            $this->form_validation->set_rules('address', 'Address', 'required');
            
            if ($this->form_validation->run() == FALSE){

                if(!isset($id)){
                    redirect(site_url('user'));
                }

                $id = $this->session->flashdata('id');

                $data['info'] = $this->data->getone($this->service_authority_table, $id);

                $data['main_title'] = "Service Authority";

                $data['sub_title'] = "Add Service Authority";

                $data['page'] = "service_authority/edit";

                $this->load->view('common/template', $data);

            } else {

                $id = $this->filter->nohtml($this->input->post('id'));
                
                // logo
                if ($_FILES['logo']['name'] != '') {

                    $del_image = $this->filter->nohtml($this->input->post('logo_img'));

                    $filename = $this->service_authority_path . $del_image;

                    if (file_exists($filename)) {
                    
                        unlink($filename);

                    }

                    $image_upload = $this->do_upload('logo', $this->service_authority_path);

                    if ($image_upload == FALSE) {

                        $this->session->set_flashdata('danger', 'Image Upload Failed!');
                        redirect(site_url('service_authority'));

                    } else {

                        $image = $image_upload["file_name"];

                    }

                } else {

                    $logo = $this->filter->nohtml($this->input->post('logo_img'));

                }
                
                // image
                if ($_FILES['image']['name'] != '') {

                    $del_image = $this->filter->nohtml($this->input->post('img'));

                    $filename = $this->service_authority_path . $del_image;

                    if (file_exists($filename)) {
                    
                        unlink($filename);

                    }

                    $image_upload = $this->do_upload('image', $this->service_authority_path);

                    if ($image_upload == FALSE) {

                        $this->session->set_flashdata('danger', 'Image Upload Failed!');
                        redirect(site_url('service_authority'));

                    } else {

                        $image = $image_upload["file_name"];

                    }

                } else {

                    $image = $this->filter->nohtml($this->input->post('img'));

                }
                
                
                if ($this->service_authority_m->edit($id, $logo, $image)) {

                    $this->session->set_flashdata('success', 'Update Successfully.');
                    redirect(site_url('service_authority'));
                
                } else {

                    $this->session->set_flashdata('danger', 'Not Update!');
                    redirect(site_url('service_authority'));

                }

            }

        }
        
    }
    

    // default upload of this class
    function do_upload($field_name , $path) {

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024';
        $config['xss_clean'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field_name)) {
            return false;
        } else {
            return $this->upload->data();
        }
    }
      
    
}