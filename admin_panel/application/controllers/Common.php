<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Common extends CI_Controller {

    function __construct() {

        parent :: __construct();
        $root = base_url();
        $this->load->model('data');

    }

    // delete DT
    function delete($table, $id) {
        if ($this->data->delete($table, $id)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // kono kichu default korar jonno  DT
    public function default_change($table, $id, $url) {
        $value = array(
            'default' => 0
        );
        $statuss = $this->data->updateall($table, $value);
        $value = array(
            'default' => 1
        );
        $statuss = $this->data->update($table, $id, $value);
        redirect("$url");
    }

    // image delete  DT
    public function image_delete($root, $path, $name) {

        $filename = $root . "/" . $path . "/" . $name;
        if (file_exists($filename)) {
            unlink($filename);
            return TRUE;
        } else {
            return FALSE;
        }
    }









    public function edit($table, $id) {
        
    }

    public function status_change($table, $id, $status) {
        if ($status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $value = array(
            'status' => $status
        );
        $statuss = $this->data->update($table, $id, $value);
        if ($statuss) {
            echo $status;
        } else {
            echo $status;
        }
    }

    public function feature_change($table, $id, $status) {
        if ($status == '1') {
            $status = '0';
        } else {
            $status = '1';
        }
        $value = array(
            'feature_product' => $status
        );
        $statuss = $this->data->update($table, $id, $value);
        if ($statuss) {
            echo $status;
        } else {
            echo $status;
        }
    }

    

    /**
     * This method use for get country state with country code
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $country_id
     */
    public function country_state($country_id) {
        $state = $this->data->get_region($country_id);
        $result = '';
        foreach ($state as $state) {
            $result .="<option value='" . $state->id . "'>" . $state->name . "</option>";
        }
        echo $result;
    }

    /**
     * This method use for get country state with country code
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $country_id
     */
    public function country_city($country_id) {
        $city = $this->data->get_city($country_id);
        $result = '';
        foreach ($city as $city) {
            $result .="<option value='" . $city->id . "'>" . $city->name . "</option>";
        }
        echo $result;
    }
    
    public function region_city($region_id)
{
        $city = $this->data->get_region_city($region_id);
        $result = '';
        foreach ($city as $city) {
            $result .="<option value='" . $city->id . "'>" . $city->name . "</option>";
        }
        echo $result;
}



    public function get_country_code($country) {
        $country = $this->data->getone('country', $country);
        echo $country[0]->country_code;
    }

    /**
     * THIS FUNCTION use for add product in cart with coffing_cart library
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     */
    public function add_to_cart() {
        $this->load->library('coffing_cart');
        $this->coffing_cart->addtocart($_POST);
    }

    /**
     * this method use for get total cart item quantity and use by call ajax method from site.js
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * 
     */
    public function total_cart_quantity() {
        echo $toal_item = $this->cart->total_items();
    }

    /**
     * This method use for remove single cart item with ajax call method
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     * @param type $rowid
     */
    public function remove_cart_item($rowid = NULL) {
        $data = array(
            'rowid' => $rowid,
            'qty' => 0
        );

        if ($this->cart->update($data)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function cart_update($rowid, $id, $qty) {
        $data = array(
            'rowid' => $rowid,
            'qty' => $qty
        );
        if ($this->cart->update($data)) {
            echo "1";
        } else {
            echo "0";
        }
    }


    /**
     * This method use for coupon code apply with get coupon value
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     * @param type $code
     */
    public function coupon_apply($code) {
        $coupon = $this->data->coupon_info($code);
        if ($coupon) {
            $data = array(
                'coupon_name' => $coupon[0]->name,
                'coupon_code' => $coupon[0]->code,
                'coupon_type' => $coupon[0]->type,
                'coupon_discount' => $coupon[0]->discount,
                'coupon_shipping' => $coupon[0]->shipping,
            );
            $this->session->set_userdata($data);
            echo "1";
        } else {
            $data = array(
                'coupon_name' => '',
                'coupon_code' => '',
                'coupon_type' => '',
                'coupon_discount' => '',
                'coupon_shipping' => '',
            );
            $this->session->set_userdata($data);
            echo "0";
        }
    }

    /**
     * 
     * @param type $id
     * @param type $check_value
     * this function use for change order status from backend
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     */
    public function order_status($id = NULL, $check_value = NULL) {
        $update_value = array(
            'status' => $check_value
        );
        if ($this->data->update('orders', $id, $update_value)) {
            echo "1";
        } else {
            echo "0";
        }
    }

    /**
     * 
     * @param type $id
     * @param type $shipping_number
     * this method use for update shipping track number with shipping date
     * @author Jahid al mamun <rjs.jahid11@gmai.com>
     */
    public function order_shipping_track($id = NULL, $shipping_number = NULL) {
        $date = date('y-m-d');
        $update_value = array(
            'shipping_track_number' => $shipping_number,
            'shipped_on' => $date
        );
        if ($this->data->update('orders', $id, $update_value)) {
            echo "1";
        } else {
            echo "0";
        }
    }
    /**
     * this method use for check customer login or not
     * @author jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function customer_login_check()
    {
        if ($this->auth->is_customer()) {
            echo 1;
        }else
        {
            echo 0;
        }
    }
    /**
     * 
     * @param type $id
     * @param type $name
     * this method use to set back url when one user want to product rating with login or register
     * @author jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function rating_back($id,$name)
    {
        $name = urldecode($name);
        $this->session->set_userdata("back_url","products/details/$id/$name");
        echo 1;
    }
    
    
}
