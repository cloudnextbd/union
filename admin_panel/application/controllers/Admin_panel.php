<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_panel extends CI_Controller {

    function __construct() {

        parent :: __construct();

        $this->load->model('data');


        // $this->slider_table = 'slider';
        // $this->slider_path = './assets/img/slider/';
        // $this->load->model('slider');

        // $this->package_table = 'package';
        // $this->package_path = './assets/img/package/';
        // $this->load->model('package');

        // $this->coverage_area_table = 'coverage_area';
        // $this->coverage_area_path = './assets/img/area/';
        // $this->load->model('coverage_area');

        // $this->faqs_table = 'faqs';
        // $this->load->model('faqs');

        // $this->com_settings_table = 'common_settings';
        // $this->com_settings_path = './assets/img/logo/';
        // $this->load->model('front_set');
        

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

        $this->output->set_header('Pragma: no-cache');

        if (!$this->admin->loggedin) {

            redirect(site_url('login'));

        }

    }

    public function index() {


        $data['title'] = "Dashboard";

        $data['page'] = "back/dashboard/dashboard";

        $this->load->view('back/common/template', $data);

    }
    
    // ##############################################################################################
    //  faqs section
    // ##############################################################################################

    public function district() {

        $data['lists'] = $this->data->getall($this->faqs_table);
        
        $data['title'] = "Faqs";

        $data['page'] = "back/faqs/faqs";

        $this->load->view('back/common/template', $data);
    }

    public function add_faqs() {
        if($_POST){

            $id = $this->faqs->add();
            if ($id) {
                
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect(site_url('admin_panel/faqs'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Added Successfully');
                redirect(site_url('admin_panel/faqs'));

            }

        } else {

            redirect(site_url('admin_panel/faqs'));

        }
    }

    public function edit_faqs($id) {
        
        if(empty($id)){

            redirect(site_url('admin_panel/faqs'));

        }

        $data['list'] = $this->data->getone($this->faqs_table, $id);
        
        $data['title'] = "Edit Faqs";

        $data['page'] = "back/faqs/edit_faqs";

        $this->load->view('back/common/template', $data);

    }

    public function do_edit_faqs() {

        if($_POST){

            $id = $this->input->post('id');

            if ($this->faqs->edit($id)) {

                $this->session->set_flashdata('success', 'Update Successfully.');
                redirect('admin_panel/faqs');

            }

        } else {
            redirect('admin_panel/faqs', 'refresh');
        }

    }

    public function delete_faqs($id) {

        $result = $this->delete($this->faqs_table, $id);

        if($result){

            $this->session->set_flashdata('success', 'Delete Successfully.');
            redirect('admin_panel/faqs');

        } else{

            $this->session->set_flashdata('danger', 'Not Delete Successfully.');
            redirect('admin_panel/faqs');

        }

    }
    
    
    
    
    

    // ##############################################################################################
    //  slider section
    // ##############################################################################################

    public function slider() {

        $data['lists'] = $this->data->getall($this->slider_table);
        
        $data['title'] = "Slider";

        $data['page'] = "back/slider/slider";

        $this->load->view('back/common/template', $data);
    }

    public function add_slider() {

        if($_POST){

            if ($_FILES['image']['name'] != '') {

                $image_upload = $this->do_upload('image', $this->slider_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');

                    redirect(site_url('admin_panel/slider'));

                } else {

                    $image = $image_upload["file_name"];

                }

            }
            $id = $this->slider->add($image);
            if ($id) {
                
                $this->session->set_flashdata('success', 'Added Successfully.');

                redirect(site_url('admin_panel/slider'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Added Successfully');

                redirect(site_url('admin_panel/slider'));

            }

        } else {

            redirect(site_url('admin_panel/slider'));

        }
    }

    public function edit_slider($id) {
        
        if(empty($id)){

            redirect(site_url('admin_panel/slider'));

        }

        $data['list'] = $this->data->getone($this->slider_table, $id);
        
        $data['title'] = "Edit Slider";

        $data['page'] = "back/slider/edit_slider";

        $this->load->view('back/common/template', $data);

    }

    public function do_edit_slider() {

        if($_POST){

            $id = $this->input->post('id');

            if ($_FILES['image']['name'] != '') {

                $del_image = $this->input->post('img');
                $filename = $this->slider_path . $del_image;
                if (file_exists($filename)) {
                    unlink($filename);
                }

                $image_upload = $this->do_upload('image', $this->slider_path);

                if ($image_upload == FALSE) {
                    $this->session->set_flashdata('danger', 'Image Upload Failed!');
                    redirect(site_url('admin_panel/slider'));
                } else {
                    $image = $image_upload["file_name"];
                }
            } else {
                $image = $this->input->post('img');
            }

            if ($this->slider->edit($id, $image)) {

                $this->session->set_flashdata('success', 'Update Successfully.');
                redirect('admin_panel/slider');

            }

        } else {
            redirect('admin_panel/slider', 'refresh');
        }

    }

    public function delete_slider($id, $image) {

        $result = $this->delete($this->slider_table, $id, $image, $this->slider_path);

        if($result){

            $this->session->set_flashdata('success', 'Delete Successfully.');
            redirect('admin_panel/slider');

        } else{

            $this->session->set_flashdata('danger', 'Not Delete Successfully.');
            redirect('admin_panel/slider');

        }

    }


    // ##############################################################################################
    //  package section
    // ##############################################################################################

    public function package() {

        $data['lists'] = $this->data->getall($this->package_table);
        $data['areas'] = $this->data->getall($this->coverage_area_table);
        
        $data['title'] = "Our Package";

        $data['page'] = "back/package/package";

        $this->load->view('back/common/template', $data);
    }

    public function add_package() {

        if($_POST){

            // banner image
            if ($_FILES['image']['name'] != '') {

                $image_upload = $this->do_upload('image', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image = $image_upload["file_name"];

                }

            }

            // image 2
            if ($_FILES['image2']['name'] != '') {

                $image_upload = $this->do_upload('image2', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image2 = $image_upload["file_name"];

                }

            }

            // image 3
            if ($_FILES['image3']['name'] != '') {

                $image_upload = $this->do_upload('image3', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image3 = $image_upload["file_name"];

                }

            }

            // image 4
            if ($_FILES['image4']['name'] != '') {

                $image_upload = $this->do_upload('image4', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image4 = $image_upload["file_name"];

                }

            }

            // image 5
            if ($_FILES['image5']['name'] != '') {

                $image_upload = $this->do_upload('image5', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image5 = $image_upload["file_name"];

                }

            }

            // image 6
            if ($_FILES['image6']['name'] != '') {

                $image_upload = $this->do_upload('image6', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image6 = $image_upload["file_name"];

                }

            }

            // image 7
            if ($_FILES['image7']['name'] != '') {

                $image_upload = $this->do_upload('image7', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image7 = $image_upload["file_name"];

                }

            }

            // image 8
            if ($_FILES['image8']['name'] != '') {

                $image_upload = $this->do_upload('image8', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image8 = $image_upload["file_name"];

                }

            }

            // image 9
            if ($_FILES['image9']['name'] != '') {

                $image_upload = $this->do_upload('image9', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image9 = $image_upload["file_name"];

                }

            }

            // image 10
            if ($_FILES['image10']['name'] != '') {

                $image_upload = $this->do_upload('image10', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');
                    redirect(site_url('admin_panel/package'));

                } else {

                    $image10 = $image_upload["file_name"];

                }

            }

            $id = $this->package->add($image, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10);
            
            if ($id) {
                
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect(site_url('admin_panel/package'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Added Successfully');
                redirect(site_url('admin_panel/package'));

            }

        } else {

            redirect(site_url('admin_panel/package'));

        }
    }

    public function edit_package($id) {
        
        if(empty($id)){

            redirect(site_url('admin_panel/package'));

        }

        $data['list'] = $this->data->getone($this->package_table, $id);

        $data['areas'] = $this->data->getall($this->coverage_area_table);
        
        $data['title'] = "Edit Package";

        $data['page'] = "back/package/edit_package";

        $this->load->view('back/common/template', $data);

    }

    public function do_edit_package() {

        if($_POST){

            $id = $this->input->post('id');

            // banner image
            if ($_FILES['image']['name'] != '') {

                $del_image = $this->input->post('img');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image = $image_upload["file_name"];

                }

            } else {

                $image = $this->input->post('img');

            }

            // image2
            if ($_FILES['image2']['name'] != '') {

                $del_image = $this->input->post('img2');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image2', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image2 = $image_upload["file_name"];

                }

            } else {

                $image2 = $this->input->post('img2');

            }

            // image3
            if ($_FILES['image3']['name'] != '') {

                $del_image = $this->input->post('img3');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image3', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image3 = $image_upload["file_name"];

                }

            } else {

                $image3 = $this->input->post('img3');

            }

            // image4
            if ($_FILES['image4']['name'] != '') {

                $del_image = $this->input->post('img4');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image4', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image4 = $image_upload["file_name"];

                }

            } else {

                $image4 = $this->input->post('img4');

            }

            // image5
            if ($_FILES['image5']['name'] != '') {

                $del_image = $this->input->post('img5');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image5', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image5 = $image_upload["file_name"];

                }

            } else {

                $image5 = $this->input->post('img5');

            }

            // image6
            if ($_FILES['image6']['name'] != '') {

                $del_image = $this->input->post('img6');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image6', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image6 = $image_upload["file_name"];

                }

            } else {

                $image6 = $this->input->post('img6');

            }

            // image7
            if ($_FILES['image7']['name'] != '') {

                $del_image = $this->input->post('img7');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image7', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image7 = $image_upload["file_name"];

                }

            } else {

                $image7 = $this->input->post('img7');

            }

            // image8
            if ($_FILES['image8']['name'] != '') {

                $del_image = $this->input->post('img8');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image8', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image8 = $image_upload["file_name"];

                }

            } else {

                $image8 = $this->input->post('img8');

            }

            // image9
            if ($_FILES['image9']['name'] != '') {

                $del_image = $this->input->post('img9');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image9', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image9 = $image_upload["file_name"];

                }

            } else {

                $image9 = $this->input->post('img9');

            }

            // image10
            if ($_FILES['image10']['name'] != '') {

                $del_image = $this->input->post('img10');

                $filename = $this->package_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image10', $this->package_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/package'));

                } else {

                    $image10 = $image_upload["file_name"];

                }

            } else {

                $image10 = $this->input->post('img10');

            }

            if ($this->package->edit($id, $image, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10)) {

                $this->session->set_flashdata('success', 'Update Successfully.');

                redirect('admin_panel/package');

            }

        } else {

            redirect('admin_panel/package', 'refresh');

        }

    }

    public function delete_package($id, $image, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10) {

        $result = $this->delete_10($this->package_table, $id, $image, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10, $this->package_path);

        if($result){

            $this->session->set_flashdata('success', 'Delete Successfully.');
            redirect('admin_panel/package');

        } else{

            $this->session->set_flashdata('danger', 'Not Delete Successfully.');
            redirect('admin_panel/package');

        }

    }


    // ##############################################################################################
    //  coverage area section
    // ##############################################################################################

    public function coverage_area() {

        $data['lists'] = $this->data->getall($this->coverage_area_table);
        
        $data['title'] = "Coverage Area";

        $data['page'] = "back/coverage_area/coverage_area";

        $this->load->view('back/common/template', $data);

    }


    public function add_coverage_area() {

        if($_POST){

            if ($_FILES['image']['name'] != '') {

                $image_upload = $this->do_upload('image', $this->coverage_area_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed');

                    redirect(site_url('admin_panel/coverage_area'));

                } else {

                    $image = $image_upload["file_name"];

                }

            }

            $id = $this->coverage_area->add($image);

            if ($id) {
                
                $this->session->set_flashdata('success', 'Added Successfully.');

                redirect(site_url('admin_panel/coverage_area'));

            } else {
                
                $this->session->set_flashdata('danger', 'Not Added Successfully');

                redirect(site_url('admin_panel/coverage_area'));

            }

        } else {

            redirect(site_url('admin_panel/coverage_area'));

        }

    }


    public function edit_coverage_area($id) {
        
        if(empty($id)){

            redirect(site_url('admin_panel/coverage_area'));

        }

        $data['list'] = $this->data->getone($this->coverage_area_table, $id);
        
        $data['title'] = "Edit Coverage Area";

        $data['page'] = "back/coverage_area/edit_coverage_area";

        $this->load->view('back/common/template', $data);

    }


    public function do_edit_coverage_area() {

        if($_POST){

            $id = $this->input->post('id');

            if ($_FILES['image']['name'] != '') {

                $del_image = $this->input->post('img');

                $filename = $this->coverage_area_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);

                }

                $image_upload = $this->do_upload('image', $this->coverage_area_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/coverage_area'));

                } else {

                    $image = $image_upload["file_name"];

                }

            } else {

                $image = $this->input->post('img');

            }

            if ($this->coverage_area->edit($id, $image)) {

                $this->session->set_flashdata('success', 'Update Successfully.');

                redirect('admin_panel/coverage_area');

            }

        } else {

            redirect('admin_panel/coverage_area', 'refresh');

        }

    }


    public function delete_coverage_area($id) {

        $result = $this->delete($this->coverage_area_table, $id, $image, $this->coverage_area_path);

        if($result){

            $this->session->set_flashdata('success', 'Delete Successfully.');

            redirect('admin_panel/coverage_area');

        } else{

            $this->session->set_flashdata('danger', 'Not Delete Successfully.');

            redirect('admin_panel/coverage_area');

        }

    }





    // ##############################################################################################
    //  Common settings section
    // ##############################################################################################

    public function common_settings() {

        $data['list'] = $this->data->getall($this->com_settings_table);
        
        $data['title'] = "Common Settings";

        $data['page'] = "back/settings/common_settings";

        $this->load->view('back/common/template', $data);
    }

    
    public function do_edit_common_settings() {

        if($_POST){

            $id = $this->input->post('id');

            if ($_FILES['image']['name'] != '') {

                $del_image = $this->input->post('img');

                $filename = $this->com_settings_path . $del_image;

                if (file_exists($filename)) {

                    unlink($filename);
                }

                $image_upload = $this->do_upload('image', $this->com_settings_path);

                if ($image_upload == FALSE) {

                    $this->session->set_flashdata('danger', 'Image Upload Failed!');

                    redirect(site_url('admin_panel/common_settings'));

                } else {

                    $image = $image_upload["file_name"];

                }

            } else {

                $image = $this->input->post('img');

            }

            if ($this->common_settings->edit($id, $image)) {

                $this->session->set_flashdata('success', 'Update Successfully.');

                redirect('admin_panel/common_settings');

            }

        } else {

            redirect('admin_panel/common_settings', 'refresh');

        }

    }

    public function do_upload($field_name , $path) {

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '5120';
        $config['xss_clean'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field_name)) {
            return false;
        } else {
            return $this->upload->data();
        }
    }

    public function delete($table, $id, $image = NULL, $path = NULL) {

        if($image != NULL){

            $this->data->delete($table, $id);
            $filename = $path . $image;

            if (file_exists($filename)) {
                unlink($filename);
                return TRUE;
            } else {
                return FALSE;
            }

        } else {

            if ($this->data->delete($table, $id)) {
                return TRUE;
            } else {
                return FALSE;
            }

        }
        
    }

    public function delete_10($table, $id, $image = NULL, $image2 = NULL, $image3 = NULL, $image4 = NULL, $image5 = NULL, $image6 = NULL, $image7 = NULL, $image8 = NULL, $image9 = NULL, $image10 = NULL,  $path = NULL) {

        if($image != NULL){

            $this->data->delete($table, $id);

            $filename = $path . $image;
            $filename2 = $path . $image2;
            $filename3 = $path . $image3;
            $filename4 = $path . $image4;
            $filename5 = $path . $image5;
            $filename6 = $path . $image6;
            $filename7 = $path . $image7;
            $filename8 = $path . $image8;
            $filename9 = $path . $image9;
            $filename10 = $path . $image10;

            if (file_exists($filename)) {

                unlink($filename);
                unlink($filename2);
                unlink($filename3);
                unlink($filename4);
                unlink($filename5);
                unlink($filename6);
                unlink($filename7);
                unlink($filename8);
                unlink($filename9);
                unlink($filename10);

                return TRUE;

            } else {

                return FALSE;
                
            }

        } else {

            if ($this->data->delete($table, $id)) {

                return TRUE;

            } else {

                return FALSE;

            }

        }
        
    }


    public function logout($hash) {

        $config = $this->config->item("cookieprefix");

        $this->load->helper("cookie");
        if ($hash != $this->security->get_csrf_hash()) {

            $data['title'] = "Error";

            $data['page'] = "back/error/admin_error";

            $this->session->set_flashdata('danger', 'CSRF and Cookie problem');

            $this->load->view('back/common/template', $data);
        }
        delete_cookie($config . "un");
        delete_cookie($config . "tkn");
        $this->session->sess_destroy();
        redirect('login');
    }




}