<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {

        parent :: __construct();

        $this->load->model('data');

        $this->user_table = 'user';
        $this->load->model('user_m');
        $this->user_path = './assets/img/user/';
        
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if (!$this->admin->loggedin) {
            redirect(site_url('login'));
        }

    }

    
    // ##############################################################################################

    public function index() {

        $data['users'] = $this->data->getall($this->user_table);

        $data['sub_title'] = "User";

        $data['page'] = "user/user";

        $this->load->view('common/template', $data);

    }

    public function add() {

        if($_POST){

            $this->load->library('form_validation');

            $this->form_validation->set_rules('first_name', 'First_name', 'required');
            
            $this->form_validation->set_rules('address', 'Address', 'required');

            $this->form_validation->set_rules('about', 'About', 'required');

            $this->form_validation->set_rules('about', 'About', 'required');

            $this->form_validation->set_rules('status', 'Status', 'required');

            $this->form_validation->set_rules('role', 'Role', 'required');

            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|is_unique[user.phone]');

            $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');

            $this->form_validation->set_rules('con_password', 'Confirm password', 'required|matches[password]');


            if ($this->form_validation->run() == FALSE){

                $data['main_title'] = "User";

                $data['sub_title'] = "Add User";

                $data['page'] = "user/add";

                $this->load->view('common/template', $data);

            } else {
                
                if ($_FILES['image']['name'] != '') {

                    $image_upload = $this->do_upload('image', $this->user_path);

                    if ($image_upload == FALSE) {

                        $this->session->set_flashdata('danger', 'Image Upload Failed');
                        redirect(site_url('user/add_user'));

                    } else {

                        $image = $image_upload["file_name"];

                    }

                }

                $id = $this->user_m->add($image);

                if ($id) {
                    
                    $this->session->set_flashdata('success', 'Added Successfully!');
                    redirect(site_url('user'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Added!');
                    redirect(site_url('user'));

                }

            }


        } else {

            $data['main_title'] = "User";

            $data['sub_title'] = "Add User";

            $data['page'] = "user/add";

            $this->load->view('common/template', $data);

        }

    }
    
    // edit
    public function edit($id = null) {

        if(!$_POST){

            $data['info'] = $this->data->getone($this->user_table, $id);

            $data['main_title'] = "User";

            $data['sub_title'] = "Edit User";

            $data['page'] = "user/edit";

            $this->load->view('common/template', $data);

        } else {

            $this->id = $this->filter->nohtml($this->input->post('id'));

            $this->session->set_flashdata('id', $this->id);

            $this->load->library('form_validation');

            $this->form_validation->set_rules('first_name', 'First Name', 'required');

            $this->form_validation->set_rules('address', 'Address', 'required');

            $this->form_validation->set_rules('about', 'About', 'required');

            $this->form_validation->set_rules('status', 'Status', 'required');

            $this->form_validation->set_rules('role', 'Role', 'required');

            if ($this->form_validation->run() == FALSE){

                if(!isset($id)){
                    redirect(site_url('user'));
                }

                $id = $this->session->flashdata('id');

                $data['info'] = $this->data->getone($this->user_table, $id);

                $data['main_title'] = "User";

                $data['sub_title'] = "Edit User";

                $data['page'] = "user/edit";

                $this->load->view('common/template', $data);

            } else {

                $id = $this->filter->nohtml($this->input->post('id'));

                if ($_FILES['image']['name'] != '') {

                    $del_image = $this->filter->nohtml($this->input->post('img'));

                    $filename = $this->user_path . $del_image;

                    if (file_exists($filename)) {
                    
                        unlink($filename);

                    }

                    $image_upload = $this->do_upload('image', $this->user_path);

                    if ($image_upload == FALSE) {

                        $this->session->set_flashdata('danger', 'Image Upload Failed!');
                        redirect(site_url('user'));

                    } else {

                        $image = $image_upload["file_name"];

                    }

                } else {

                    $image = $this->filter->nohtml($this->input->post('img'));

                }

                if ($this->user_m->edit($id, $image)) {

                    $this->session->set_flashdata('success', 'Update Successfully.');
                    redirect(site_url('user'));
                
                } else {

                    $this->session->set_flashdata('danger', 'Not Update!');
                    redirect(site_url('user'));

                }

            }

        }
        
    }

    // default upload of this class
    function do_upload($field_name , $path) {

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024';
        $config['xss_clean'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field_name)) {
            return false;
        } else {
            return $this->upload->data();
        }
    }


}