<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {

        parent :: __construct();

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

        $this->output->set_header('Pragma: no-cache');

        if (!$this->admin->loggedin) {

            redirect(site_url('login'));

        }

    }

    public function index() {


        $data['title'] = "Dashboard";

        $data['page'] = "dashboard/dashboard";

        $this->load->view('common/template', $data);

    }

    // logout ------------------------------------------------------------
    public function logout($hash) {

        $config = $this->config->item("cookieprefix");

        $this->load->helper("cookie");
        if ($hash != $this->security->get_csrf_hash()) {

            $data['title'] = "Error";

            $data['page'] = "error/admin_error";

            $this->session->set_flashdata('danger', 'CSRF and Cookie problem');

            $this->load->view('common/template', $data);
        }
        delete_cookie($config . "un");
        delete_cookie($config . "tkn");
        $this->session->sess_destroy();
        redirect('login');
    }

}