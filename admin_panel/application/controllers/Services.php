<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {

    function __construct() {

        parent :: __construct();
        
        $this->service_authority_table = 'service_authority';

        $this->services_table = 'services';
        $this->load->model('services_m');
        
        $this->load->model('data');
        
        if (!$this->admin->loggedin) {
            redirect(site_url('login'));
        }

    }

    
    public function index() {

        $data['lists'] = $this->services_m->get_sepcific_with_parent();
        
        $data['sub_title'] = "Services";

        $data['page'] = "services/table";
        
        $this->load->view('common/template', $data);

    }

    public function add() {
        
        
        if(!$_POST){
            
            $data['authoritys'] = $this->data->getall($this->service_authority_table);
            
            $data['main_title'] = "Services";

            $data['sub_title'] = "Add Services";

            $data['page'] = "services/add";

            $this->load->view('common/template', $data);
            
        }else{
            
            $this->load->library('form_validation');

            $this->form_validation->set_rules('ser_name', 'Name', 'required');
            
            $this->form_validation->set_rules('canvas', 'Canvas', 'required');

            $this->form_validation->set_rules('definition', 'Definition', 'required');
            

            if ($this->form_validation->run() == FALSE){
                

                $data['authoritys'] = $this->data->getall($this->service_authority_table);
            
                $data['main_title'] = "Services";

                $data['sub_title'] = "Add Services";

                $data['page'] = "services/add";

                $this->load->view('common/template', $data);
                

            } else {
                
                $id = $this->services_m->add();

                if ($id) {
                    
                    $this->session->set_flashdata('success', 'Added Successfully!');
                    redirect(site_url('services'));

                } else {
                    
                    $this->session->set_flashdata('danger', 'Not Added!');
                    redirect(site_url('services'));

                }

            }
            
        }
        
    }
    
    // edit
    public function edit($id = null) {

        if(!$_POST){
            
            $data['authoritys'] = $this->data->getall($this->service_authority_table);

            $data['info'] = $this->data->getone($this->services_table, $id);

            $data['main_title'] = "Service Authority";

            $data['sub_title'] = "Add Service Authority";

            $data['page'] = "services/edit";

            $this->load->view('common/template', $data);

        } else {

            
            $this->id = $this->filter->nohtml($this->input->post('id'));
            $this->session->set_flashdata('id', $this->id);

            
            $this->load->library('form_validation');

            $this->form_validation->set_rules('ser_name', 'Name', 'required');
            
            $this->form_validation->set_rules('canvas', 'Canvas', 'required');

            $this->form_validation->set_rules('definition', 'Definition', 'required');
            
            if ($this->form_validation->run() == FALSE){

                if(!isset($id)){
                    redirect(site_url('user'));
                }

                $id = $this->session->flashdata('id');

                $data['authoritys'] = $this->data->getall($this->service_authority_table);

                $data['info'] = $this->data->getone($this->services_table, $id);

                $data['main_title'] = "Service Authority";

                $data['sub_title'] = "Add Service Authority";

                $data['page'] = "services/edit";

                $this->load->view('common/template', $data);

            } else {

                $id = $this->filter->nohtml($this->input->post('id'));
                
                if ($this->services_m->edit($id)) {

                    $this->session->set_flashdata('success', 'Update Successfully.');
                    redirect(site_url('services'));
                
                } else {

                    $this->session->set_flashdata('danger', 'Not Update!');
                    redirect(site_url('services'));

                }

            }

        }
        
    }
    

    // default upload of this class
    function do_upload($field_name , $path) {

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024';
        $config['xss_clean'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field_name)) {
            return false;
        } else {
            return $this->upload->data();
        }
    }
      
    
}