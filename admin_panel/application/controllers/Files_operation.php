<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Files_operation extends CI_Controller {

    function __construct() {

        parent :: __construct();

        $this->load->model('data');

        $this->app_per_t = 'app_personal';
        $this->app_family_t = 'app_family';
        $this->app_curr_service_t = 'app_curr_service';
        $this->coord_comments_t = 'coord_comments';
        $this->ref_comments_t = 'ref_comments';
        
        $this->app_interested_service_t = 'app_interested_service';
        $this->app_inter_service_t = 'app_inter_service';
        
        
        if (!$this->admin->loggedin) {
            redirect(site_url('login'));
        }

    }

    public function index() {
        
        $data['main_title'] = "File's Import";

        $data['sub_title'] = "file import sub";

        $data['page'] = "file/file";

        $this->load->view('common/template', $data);
        

    }
    
    public function csv_import() {

//        echo '<pre>';
//        print_r($_FILES);
//        exit();
    
        if($_POST){
            
            // file empty check
            if($_FILES["file"]["size"] > 0){
                
                // get file name 
                $filename = $_FILES["file"]["tmp_name"];
                
                // open file and define reading mode
                $file = fopen($filename, "r");
                
                // get data from CSV and write to data base
                while (($csv_data = fgetcsv($file, 10000, ",")) !== FALSE) {
                    
                    $value1 = array(

                        'name'=> $csv_data[1],
                        'birth_date'=> $csv_data[5],
                        'nid'=> $csv_data[7],
                        'village'=> $csv_data[8],
                        'word_no'=> $csv_data[9],
                        'religion'=> $csv_data[10],
                        'profession'=> $csv_data[11],
                        'gender'=> $csv_data[12],
                        'mobile'=> $csv_data[13],
                        
                        'app_status'=> '2', // 0 = refuesd, 1 = pending, 2 = approved
                        'status'=> '1', // 1 = active
                        
                    );
                    
                    $last_id = $this->data->save($this->app_per_t, $value1);
                    
                    if($last_id){
                        
                        $value2 = array(
                            
                            'parent_id'=> $last_id,
                            'mother_name'=> $csv_data[2],
                            'father_name'=> $csv_data[3],
                            'husband_name'=> $csv_data[4]
                        
                        );
                        
                       $this->data->save($this->app_family_t, $value2);
                       
                       
                       // curr service
                       $value3 = array(
                            
                            'id'=> $last_id,
                            'curr_service'=> '4' // 4 = boishko , 7 = protibondhi, 5 = widow
                               
                        );
                        
                       $this->data->save($this->app_curr_service_t, $value3);
                       
                       
                       // coord comments
                       $value4 = array(
                            
                            'id'=> $last_id,
                            'coor_id'=> '0', // 0 = current cordinator id
                            'comments'=> 'Allowed',
                               
                        );
                        
                       $this->data->save($this->coord_comments_t, $value4);
                       
                       
                       // ref comments 
                       $value5 = array(
                            
                            'id'=> $last_id,
                            'ref_id'=> '2', // 0 = 
                            'comments'=> 'May be allowed',
                               
                        );
                        
                       $this->data->save($this->ref_comments_t, $value5);
                       
                       
                       // details about interseted service
                       $value6 = array(
                            
                            'parent_id'=> $last_id,
                            'success_way'=> 'testing pupose text(success)!', // 0 =
                            'inter_trainings'=> 'testing pupose text(training)!',
                            'sp_info'=> 'testing pupose text(special info)!',
                               
                        );
                        
                       $inter_ser_id = $this->data->save($this->app_interested_service_t, $value6);
                       
                       
                       // short code of interested id
                       $value7 = array(
                            
                            'id'=> $inter_ser_id,
                            'inter_service'=> '28' // 27 = bostro prodan // 28 = jakat prodan
                           
                        );
                        
                        $this->data->save($this->app_inter_service_t, $value7);
                     
                    }
                    
                    
                    
                    
                    
                }
                
                // file close
                fclose($file);
                
                $this->session->set_flashdata('success', 'CSV File has been successfully Imported!');
                redirect(site_url('files_operation'));
                
                
            } else {
                
                $this->session->set_flashdata('danger', 'No CSV file Found!');
                redirect(site_url('files_operation'));
                
            }   
            
        }else{

            redirect(site_url('files_operation'));

        }
    }
    
    
    
    
    
    public function csv_import_reson() {

//        echo '<pre>';
//        print_r($_FILES);
//        exit();
    
        if($_POST){
            
            // file empty check
            if($_FILES["file"]["size"] > 0){
                
                // get file name 
                $filename = $_FILES["file"]["tmp_name"];
                
                // open file and define reading mode
                $file = fopen($filename, "r");
                
                // get data from CSV and write to data base
                while (($csv_data = fgetcsv($file, 10000, ",")) !== FALSE) {
                    
                
                    
                    
                    
                    
                    $value1 = array(

                        'name'=> $csv_data[1],
                        'birth_date'=> $csv_data[5],
                        'nid'=> $csv_data[7],
                        'village'=> $csv_data[8],
                        'word_no'=> $csv_data[9],
                        'religion'=> $csv_data[10],
                        'profession'=> $csv_data[11],
                        'gender'=> $csv_data[12],
                        'mobile'=> $csv_data[13],
                        
                        'app_status'=> '1', // 1 = pending
                        'status'=> '0', // 1 = active
                        
                    );
                    
                    $last_id = $this->data->save($this->app_per_t, $value1);
                    
                    if($last_id){
                        
                        $value2 = array(
                            
                            'parent_id'=> $last_id,
                            'mother_name'=> $csv_data[2],
                            'father_name'=> $csv_data[3],
                            'husband_name'=> $csv_data[4]
                        
                        );
                        
                       $this->data->save($this->app_family_t, $value2);
                       
                       
                       // curr service
                       $value3 = array(
                            
                            'id'=> $last_id,
                            'curr_service'=> '5' // 4 = boishko , 7 = protibondhi, 5 = widow
                               
                        );
                        
                       $this->data->save($this->app_curr_service_t, $value3);
                       
                       
                       // coord comments
                       $value4 = array(
                            
                            'id'=> $last_id,
                            'coor_id'=> '0', // 0 = current cordinator id
                               
                        );
                        
                       $this->data->save($this->coord_comments_t, $value4);
                       
                       
                       // ref comments 
                       $value5 = array(
                            
                            'id'=> $last_id,
                            'ref_id'=> '2', // 0 = 
                               
                        );
                        
                       $this->data->save($this->ref_comments_t, $value5);
                       
                       
                       // details about interseted service
                       $value6 = array(
                            
                            'parent_id'=> $last_id,
                            'success_way'=> 'testing pupose text(success)!', // 0 =
                            'inter_trainings'=> 'testing pupose text(training)!',
                            'sp_info'=> 'testing pupose text(special info)!',
                               
                        );
                        
                       $inter_ser_id = $this->data->save($this->app_interested_service_t, $value6);
                       
                       
                       // short code of interested id
                       $value7 = array(
                            
                            'id'=> $inter_ser_id,
                            'inter_service'=> '28' // 27 = bostro prodan // 28 = jakat prodan
                           
                        );
                        
                        $this->data->save($this->app_inter_service_t, $value7);
                     
                    }
                    
                    
                    
                    
                    
                }
                
                // file close
                fclose($file);
                
                $this->session->set_flashdata('success', 'CSV File has been successfully Imported!');
                redirect(site_url('files_operation'));
                
                
            } else {
                
                $this->session->set_flashdata('danger', 'No CSV file Found!');
                redirect(site_url('files_operation'));
                
            }   
            
        }else{

            redirect(site_url('files_operation'));

        }
    }


}