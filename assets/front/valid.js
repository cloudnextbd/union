// check birth id
function birth_id_check(birth_id) {

   if (birth_id.length > 0) {
        $.ajax({
            url: global_base_url + "new_form/birth_id_check",
            type: "get",
            data: {
                "birth_id": birth_id
            },
            success: function (msg) {
                $('#birth_result').html(msg);
            }
        });
    } else {
        $('#birth_result').html('');
    }
}

// check nid
function nid_check(nid) {

   if (nid.length > 0) {
        $.ajax({
            url: global_base_url + "new_form/nid_check",
            type: "get",
            data: {
                "nid": nid
            },
            success: function (msg) {
                $('#nid_result').html(msg);
            }
        });
    } else {
        $('#nid_result').html('');
    }
}

$(document).ready(function () {

    $('#birth_id').change(function () {
        check_birth_id();
    });

    $('#nid').change(function () {
        check_nid();
    });

});


//--------------------------------------------------------------------------------


 
// this function start ---------------------------------------------------------
// get_auth_by_cat_id (create service controller) 
$("#cat").change(function () {
    
    // when category dropdown is changed do empty all right side field
    $('#mem_image').attr("src", global_base_url + 'assets/app_img/default.png');
    $('#mem_check').empty();
    $('#name').empty();
    $('#village').empty();
    $('#current_service').empty();
    $('#ser_id').val('');
    $('#isqualified').empty();
    // disabled form btn
    $("#submit-btn").attr("disabled","disabled");
    $('#mem_id').val('');
    
    

    var cat_id = this.value;

    $('#auth_r2').empty();
    $('#service').empty();
    $('#auth_r2').append("<option>লোড হচ্ছে...</option>");

    $.ajax({
        // http post method
        type: "get",

        // targeted URL
        url: global_base_url + "create_service/get_auth_by_cat_id",

        // fired data type
        data: {
            "cat_id": cat_id
        },

        // content type
        contentType: "application/json;charset=utf-8",
        dataType: 'json',

        // if controller successfully response with expected data 
        success: function (response_data) {

            // $('#test_data').html(response_data);

           $('#auth_r2').empty();
           $('#auth_r2').append("<option>নির্বাচন করুন</option>");
            $.each(response_data, function(key) {
                $('#auth_r2').append('<option value="' + response_data[key].id + '">' + response_data[key].ser_auth_name + '</option>');
            });

        },

        // if controller unsuccessfully respons
        error: function () {
           $('#error').html('<span style="color:red"s>get_auth_by_cat_id_error!</span>');
        }

    });

});

// this function end -------------------------------------------------------



// start -----------------------------------------------------
// get_ser_name_by_ser_auth_id (create service controller) 
$("#auth_r2").change(function () {
    
    // when author dropdown is changed do empty all right side field
    $('#mem_image').attr("src", global_base_url + 'assets/app_img/default.png');
    $('#mem_check').empty();
    $('#name').empty();
    $('#village').empty();
    $('#current_service').empty();
    $('#ser_id').val('');
    $('#isqualified').empty();
    // disabled form btn
    $("#submit-btn").attr("disabled","disabled");
    $('#mem_id').val('');
    
    
    var ser_auth_id = this.value;

    $('#service').empty();
    $('#service').append("<option>লোড হচ্ছে...</option>");

    $.ajax({
        // http post method
        type: "get",

        // targeted URL
        url: global_base_url + "create_service/get_ser_name_by_ser_auth_id",

        // fired data type
        data: {
            "ser_auth_id": ser_auth_id
        },

        // content type
        contentType: "application/json;charset=utf-8",
        dataType: 'json',

        // if controller successfully response with expected data 
        success: function (response_data2) {

             //$('#test_data').html(response_data);

           $('#service').empty();
           $('#service').append("<option>নির্বাচন করুন</option>");
            $.each(response_data2, function(key) {
                $('#service').append('<option value="' + response_data2[key].id + '">' + response_data2[key].ser_name + '</option>');
            });

        },

        // if controller unsuccessfully respons
        error: function () {
           $('#error').html('<span style="color:red">get_ser_name_by_ser_auth_id_error!</span>');
        }

    });

});

// function end -------------------------------------------------------


// start -----------------------------------------------------
// get_mem_info_by_mem_id (create service controller) 
function get_mem_info(mem_id) {

    if(mem_id.length > 0){

        $.ajax({

            url: global_base_url + "create_service/get_mem_info_by_mem_id",

            type: "get",

            data: {
                "mem_id": mem_id
            },

            // if controller successfully response with expected data 
            success: function (response_info) {

                if(response_info){

                    // when id found do empty output field
                    $('#mem_check').empty();
                    $('#name').empty();
                    $('#village').empty();
                    $('#current_service').empty();
                    $('#ser_id').val('');

                    // json parse and put to html field
                    var obj = JSON.parse(response_info);

                    $('#mem_image').attr("src", global_base_url + 'assets/app_img/' + obj.image);
                    $("#name").html(obj.name);
                    $("#village").html(obj.village);
                    $('#current_service').html(obj.ser_name);
                    $('#ser_id').val(obj.id);

                    // qualified compare start ---------------------------------
                    var cat = $('#cat').val();
                    var service = $('#service').val();
                    var ser_id = $('#ser_id').val();
                    
                    if(service){
                        
                        // when left side service is selected
                        if(cat == 1){
                            // gov
                            if(service === ser_id){

                                $('#isqualified').empty();
                                $("#isqualified").append('<span style="color: green; font-weight:bold">তিনি এই সেবাটি পাওয়ার যোগ্য</span>');
                                $("#submit-btn").removeAttr("disabled","disabled");

                            } else {

                                $('#isqualified').empty();
                                $("#isqualified").append('<span style="color: red; font-weight:bold">তিনি এই সেবাটি পাওয়ার যোগ্য নয়</span>');
                                $("#submit-btn").attr("disabled","disabled");

                            }
                        
                        } else {
                            // others
                            $('#isqualified').empty();
                            $("#isqualified").append('<span style="color: green; font-weight:bold">তিনি এই সেবাটি পাওয়ার যোগ্য</span>');
                            $("#submit-btn").removeAttr("disabled","disabled");

                        }
                        
                    } else {
                        
                        // when left side service is not selected
                        $('#isqualified').empty();
                        $("#isqualified").append('<span style="color: orange; font-weight:bold">আপনি কোনো সেবা বাছাই করেননি </span>');
                        $("#submit-btn").attr("disabled","disabled");
                        
                    }
                    // qualified compare end -----------------------------------

                } else {

                    // when id not found and do empty output field
                    $('#mem_image').attr("src", global_base_url + 'assets/app_img/default.png');
                    $('#mem_check').empty();
                    $('#name').empty();
                    $('#village').empty();
                    $('#current_service').empty();
                    $('#ser_id').val('');
                    $('#isqualified').empty();
                    
                    // disabled form btn
                    $("#submit-btn").attr("disabled","disabled");
                    
                    // id not found msg
                    $('#mem_check').html('<span style="color:red; font-weight: bold">এই নম্বর দিয়ে কোন সদস্য পাওয়া যায়নি!</span>');

                }

            },

            // if controller unsuccessfully respons
            error: function () {

               $('#error').html('<span style="color:red">get_mem_info_by_mem_id_error!</span>');

            }

        });

    } else {
        
        // when member id input field is empty
        $('#mem_image').attr("src", global_base_url + 'assets/app_img/default.png');
        $('#name').empty();
        $('#village').empty();
        $('#current_service').empty();
        $('#ser_id').val('');
        $('#mem_check').empty();
        
        // isqulified empty
        $('#isqualified').empty();
        
        // disabled form btn
        $("#submit-btn").attr("disabled","disabled");
        
    }

}
// function end -------------------------------------------------------



// if left side service (drop down) changed then isqualified result remodifie
$("#service").change(function () {
    
    // when left side service drop down is changed then do all right side field empty
//    $('#mem_image').attr("src", global_base_url + 'assets/app_img/default.png');
//    $('#name').empty();
//    $('#village').empty();
//    $('#current_service').empty();
//    $('#ser_id').val('');
//    $('#mem_check').empty();
    // isqulified empty
    $('#isqualified').empty();
    // disabled form btn
    $("#submit-btn").attr("disabled","disabled");
    

    // qualified compare ----------------------
    var cat = $('#cat').val();
    var service = $('#service').val();
    var ser_id = $('#ser_id').val();
    
    // when right side member is not define (service id is not define)
    if(!ser_id){

        $('#isqualified').empty();
        $("#submit-btn").attr("disabled","disabled");
        
    }else{
        
        if(cat == 1){
            
            // gov
            if(service === ser_id){

                $('#isqualified').empty();
                $("#isqualified").append('<span style="color: green; font-weight:bold">তিনি এই সেবাটি পাওয়ার যোগ্য</span>');
                $("#submit-btn").removeAttr("disabled","disabled");

            } else {

                $('#isqualified').empty();
                $("#isqualified").append('<span style="color: red; font-weight:bold">তিনি এই সেবাটি পাওয়ার যোগ্য নয়</span>');
                $("#submit-btn").attr("disabled","disabled");
                
            }

        } else {
            
            // others
            $('#isqualified').empty();
            $("#isqualified").append('<span style="color: green; font-weight:bold">তিনি এই সেবাটি পাওয়ার যোগ্য</span>');
            $("#submit-btn").removeAttr("disabled","disabled");
            
        }

    }


});