<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Personal_serv_autho {

    public function __construct() {

        $CI = & get_instance();
        
        $r = $CI->db->select("ser_auth_name, id")
                    ->from("service_authority")
                    ->where('category','4')
                    ->get();
        
        $this->menu = $r->result();
        
    }  
}

?>