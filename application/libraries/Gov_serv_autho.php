<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gov_serv_autho {

    public function __construct() {

        $CI = & get_instance();
        
        $r = $CI->db->select("ser_auth_name, id")
                    ->from("service_authority")
                    ->where('category','1')
                    ->get();
        
        $this->menu = $r->result();
    }  
}

?>