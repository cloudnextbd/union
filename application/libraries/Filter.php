<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

  
class Filter {
    
    // form filter -------------------------------------------------------------
    public function nohtml($data) {
        $data = trim($data);
        $data = strip_tags($data);
        $data = htmlspecialchars($data, ENT_QUOTES);
        return $data;
    }
}