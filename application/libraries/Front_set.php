<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Front_set {

    public function __construct() {

        $CI = & get_instance();
        
        $result = $CI->db->select('*')->where('id', 1)->get("front_set");

        if ($result->num_rows() == 0) {

            die('Your are missing the site database row!');

        } else {

            $this->info = $result->row();
            
        }

    }

}

?>