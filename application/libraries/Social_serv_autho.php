<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Social_serv_autho {

    public function __construct() {

        $CI = & get_instance();
        
        $r = $CI->db->select("ser_auth_name, id")
                    ->from("service_authority")
                    ->where('category','3')
                    ->get();
        
        $this->menu = $r->result();
        
    }  
}

?>