<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User {

    
    // properties --------------------------------------------------------------
    var $info = array();
    var $loggedin = false;
    var $phone = null; // user email
    var $token = null; // user token
 
    // construct method 
    public function __construct() {

        // get user info from browser cookie ----------------------
        $CI = & get_instance();
        $config = $CI->config->item("cookieprefix");
        $this->phone = $CI->input->cookie($config . "phone", TRUE);
        $this->token = $CI->input->cookie($config . "token", TRUE);
        $user = null;

        // get user info from db by cookies info ----------------------
        if ($this->phone && $this->token) {
            $user = $CI->db->select("id, first_name, last_name, phone, address, image, about, role, status, union, word, online_timestamp")
                           ->where("phone", $this->phone)->where("token", $this->token)
                           ->get("user");
        }

        // checking and matching user from db response by cookies info
        if ($user !== null) {

            if ($user->num_rows() == 0) {

                $this->loggedin = false;

            } else {

                $this->loggedin = true;

                $this->info = $user->row();

                //  update timestamp 
                if ($this->info->online_timestamp < time() - 60 * 5) {

                    $this->update_online_timestamp($this->info->id);

                }

                // if inactive user then
                if ($this->info->status == 0) {
                    $CI->load->helper("cookie");
                    $this->loggedin = false;
                    $CI->session->set_flashdata("msg", 'This user is inactive');
                    delete_cookie($config . "phone");
                    delete_cookie($config . "token");
                    redirect(site_url());
                }
            }
        }
    }

    // -------------------------------------------------------------------------
    // public function getPassword() {

    //     $CI = & get_instance();

    //     $user = $CI->db->select("password")->where("id", $this->info->id)->get("admin");

    //     $user = $user->row();

    //     return $user->password;

    // }

    // -------------------------------------------------------------------------
    public function update_online_timestamp($userid) {

        $CI = & get_instance();

        $CI->db->where("id", $userid)->update("user", array("online_timestamp" => time()));

    }

}

?>