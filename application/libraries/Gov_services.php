<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gov_services {

    public function __construct() {

        $CI = & get_instance();
        
        $r = $CI->db->select("services.ser_name, services.id")
                    ->from("services")
                    ->join('service_authority', 'service_authority.id = services.authority_id', 'left')
                    ->where('service_authority.category','1')
                    ->get();
        
        $this->menu = $r->result();
        
    }  
}

?>