<div class="content_info">
    <div class="padding-top">
        
        <!-- Container Area -->
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <h3>নির্বাচন করুন</h3>
                    <div class="form-group">
                        <a href="<?=base_url('district/coming_soon');?>" class="btn btn-primary">হাইমচর</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('district/coming_soon');?>" class="btn btn-primary">কচুয়া</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('district/coming_soon');?>" class="btn btn-primary">শাহরাস্তি</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/chandpur_sadar');?>" class="btn btn-primary">চাঁদপুর সদর</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <h3>নির্বাচন করুন</h3>
                    <div class="form-group">
                        <a href="<?=base_url('district/coming_soon');?>" class="btn btn-primary">মতলব দক্ষিণ</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('district/coming_soon');?>" class="btn btn-primary">হাজীগঞ্জ</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('district/coming_soon');?>" class="btn btn-primary">মতলব উত্তর</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('district/coming_soon');?>" class="btn btn-primary">ফরিদগঞ্জ</a>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <!-- End Container Area -->
        
    </div>
</div>