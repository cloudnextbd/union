

                <!-- Info Content - Section Title-->
                <div class="content_info">
                    <!-- Info Section title-->
                    <div class="section-title-01 section-title-01-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <h3>Page Full Widht</h3>
                                    <h5>FEATURES SECTION</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Info Section Title-->

                    <!-- breadcrumbs-->
                    <div class="breadcrumbs breadcrumbs-sections">
                        <ul>
                            <li class="breadcrumbs-home">
                                <a href="#" title="Back To Home">
                                  <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">Features</a>
                            </li>
                            <li>
                                /
                            </li>
                            <li>
                                Page Full Widht
                            </li>
                        </ul>
                    </div>
                    <!-- End breadcrumbs-->
                </div>
                <!-- End Info Content - Section Title-->

                <!-- End content info - Page Full Widht-->
                <div class="content_info">
                    <div class="paddings">
                        <div class="container">
                          <h3>tempor sit amet, ante. Donec eu libero</h3>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>

                            <div class="row">
                                <div class="col-md-4">
                                     <img class="img-responsive" src="img/gallery/1.jpg" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h3>orbi tristique orper pharetra</h3>
                                     <p>Pellentesque habitant morbi tristique orper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidu senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. </p>
                                     <p>Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
                                </div>
                            </div>               

                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>

                            <div class="row">
                                <div class="col-md-8">
                                    <h3>Mauris placerat eleifend leo</h3>
                                    <p>Pellentesque habitant morbi tristique orper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidu senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. </p>
                                    <p>Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
                                </div>
                                <div class="col-md-4">                       
                                    <img class="img-responsive" src="img/gallery/2.jpg" alt="">
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                <!-- End content info - Page Full Widht --> 
            </div>
            <!-- End content-central-->

            <!-- Sponsors Container-->  
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                       <!-- Sponsors Zone-->     
                        <ul class="owl-carousel carousel-sponsors tooltip-hover" id="carousel-sponsors">
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/1.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/2.png" alt="Image"></a>
                            </li>
                             <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/3.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/4.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/5.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/6.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/7.png" alt="Image"></a>
                            </li>
                             <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/8.png" alt="Image"></a>
                            </li>                                       
                        </ul> 
                        <!-- End Sponsors Zone-->    
                    </div>                    
                </div>
            </div>
            <!-- End Sponsors Container-->  
      
            