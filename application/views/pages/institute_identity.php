<!-- Info Content - Section Title-->
                <div class="content_info">
                    <!-- Info Section title-->
                    <div class="section-title-01 section-title-01-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Accounts Area</h3>
                                    <h5>Our Portfolio</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Info Section Title-->

                    <!-- breadcrumbs-->
                    <div class="breadcrumbs breadcrumbs-sections">
                        <ul>
                            <li class="breadcrumbs-home">
                                <a href="#" title="Back To Home">
                                  <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">Portfolio</a>
                            </li>
                            <li>
                                /
                            </li>
                            <li>
                                Accounts
                            </li>
                        </ul>
                    </div>
                    <!-- End breadcrumbs-->
                </div>
                <!-- End Info Content - Section Title-->

                <!-- Info Content -->
                <div class="content_info">
                    <div class="paddings">
                        <!-- Container Area - Boxes Services -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3>Savings account</h3>

                                    <p class="lead color-skin">Available well with your advisor the group that owns your account as this can change the characteristics and conditions thereof.</p>

                                    <p>Subject to conditions and eligibility. These insurances are endless incidents. While the cardholder meets the requirements of each insurance you will be covered as often as necessary and will be compensated equally</p>

                                     <h3>Features Account:</h3>

                                     <p>It is an account to manage your salary and help you organize your money, let him have a monthly savings while enjoying many other benefits:</p>

                                     <ul class="list-styles">
                                        <li><i class="fa fa-check"></i> <a href="#"> No management fee or handling.</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> It allows you to separate your savings silver in the same account, without needing an additional product.</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> 5 free withdrawals per month at ATMs, multifunctional ATMs and bank offices. From the sixth retirement payment corresponding to the channel use1 be made.</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> Unlimited free consultations through our Amiga, Website, Mobile Banking and Mobile Application Line (downloading our app for smartphones).</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> Free notifications.</a></li>
                                    </ul>

                                    <h3>More Features</h3>

                                    <p>From 2015 monthly withdrawals from savings accounts to $ 9,898,000 and accounts pensioner to $ 1,159,000 provided their monthly pension does not exceed this amount are exempt from 4 per thousand, if the account is marked as exempt from tax.</p>

                                    <p>Pay your everyday purchases at merchants with their debit card for my salary to free one of two insurance that protect you and your family.</p>

                                    <ul class="list-styles">
                                        <li><i class="fa fa-check"></i> <a href="#"> For security, you will receive a text message to the phone and / or email that tells about transactions equal to or greater than $ 50,000 for retirement amounts, deposits and $ 100,000 to 200,000 for purchases.</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> Free all transfers between the pocket and the saving account.</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">Set the savings from $ 5,000 monthly or fortnightly on the "pocket saving" Salary Account My friend; in the time you need will be available.</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> Chip debit card has to perform all their transactions safely.</a></li>
                                    </ul>
                                </div>       

                                <!--Aside - mini and full boxes -->
                                <aside class="col-md-3">


                                </aside>
                                <!-- End Aside - mini and full boxes -->           
                            </div>
                        </div>
                        <!-- End Container Area - Boxes Services -->
                    </div>
                </div>
                <!-- End Info Content-->
<div class="content_info">
                    <!-- title-vertical-line-->
                    <div class="title-vertical-line">
                        <h2><span>Last</span> News</h2>
                        <p class="lead">Keep informed and updated on all news related to your bank.</p>
                    </div>
                    <!-- End title-vertical-line-->

                    <!-- Container Blog Post -->
                    <div class="container">
                        <!--Blog Post Items-->
                        <div class="row paddings">
                            <!--Col Item Blog Post-->
                            <div class="col-md-4">
                                <!--Item Blog Post-->
                                <div class="item-blog-post">
                                    <!--Head Blog Post-->
                                    <div class="head-item-blog-post">
                                        <i class="fa fa-calculator"></i>
                                        <h3>Protection With you</h3>
                                    </div>
                                    <!--End Head Blog Post-->  

                                    <!--Img Blog Post-->
                                    <div class="img-item-blog-post">
                                        <img src="<?php echo base_url(); ?>assets/front/img/blog-img/thumbs/1.jpg" alt="">
                                        <div class="post-meta">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-user"></i>
                                                    <a href="#">Iwthemes</a>
                                                </li>

                                                <li>
                                                    <i class="fa fa-clock-o"></i>
                                                    <span>April 23, 2015</span>
                                                </li>

                                                <li>
                                                    <i class="fa fa-eye"></i>
                                                    <span>234 Views</span>
                                                </li>
                                            </ul>                      
                                        </div>
                                    </div>
                                    <!--End Img Blog Post-->  

                                    <!--Ingo Blog Post-->
                                    <div class="info-item-blog-post">
                                        <p>Find all the support and information they need to make all decisions about saving for your future.</p>
                                        <a href="#"><i class="fa fa-plus-circle"></i> View more</a>
                                    </div>
                                    <!--End Ingo Blog Post-->  
                                </div>
                                <!--End Item Blog Post-->  
                            </div>
                            <!--End Col Item Blog Post-->

                            <!--Col Item Blog Post-->
                            <div class="col-md-4">
                                <!--Item Blog Post-->
                                <div class="item-blog-post">
                                    <!--Head Blog Post-->
                                    <div class="head-item-blog-post">
                                        <i class="fa fa-database"></i>
                                        <h3>For your future</h3>
                                    </div>
                                    <!--End Head Blog Post-->  

                                    <!--Img Blog Post-->
                                    <div class="img-item-blog-post">
                                        <img src="<?php echo base_url(); ?>assets/front/img/blog-img/thumbs/2.jpg" alt="">
                                        <div class="post-meta">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-user"></i>
                                                    <a href="#">Iwthemes</a>
                                                </li>

                                                <li>
                                                    <i class="fa fa-clock-o"></i>
                                                    <span>April 23, 2015</span>
                                                </li>

                                                <li>
                                                    <i class="fa fa-eye"></i>
                                                    <span>234 Views</span>
                                                </li>
                                            </ul>                      
                                        </div>
                                    </div>
                                    <!--End Img Blog Post-->  

                                    <!--Ingo Blog Post-->
                                    <div class="info-item-blog-post">
                                        <p>Meet here all our range of products and services, rules of our products and everything related to your savings in pension.</p>
                                        <a href="#"><i class="fa fa-plus-circle"></i> View more</a>
                                    </div>
                                    <!--End Ingo Blog Post-->  
                                </div>
                                <!--End Item Blog Post-->  
                            </div>
                            <!--End Col Item Blog Post-->

                            <!--Col Item Blog Post-->
                            <div class="col-md-4">
                                <!--Item Blog Post-->
                                <div class="item-blog-post">
                                    <!--Head Blog Post-->
                                    <div class="head-item-blog-post">
                                        <i class="fa fa-cubes"></i>
                                        <h3>Zone Saver</h3>
                                    </div>
                                    <!--End Head Blog Post-->  

                                    <!--Img Blog Post-->
                                    <div class="img-item-blog-post">
                                        <img src="<?php echo base_url(); ?>assets/front/img/blog-img/thumbs/3.jpg" alt="">
                                        <div class="post-meta">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-user"></i>
                                                    <a href="#">Iwthemes</a>
                                                </li>

                                                <li>
                                                    <i class="fa fa-clock-o"></i>
                                                    <span>April 23, 2015</span>
                                                </li>

                                                <li>
                                                    <i class="fa fa-eye"></i>
                                                    <span>234 Views</span>
                                                </li>
                                            </ul>                      
                                        </div>
                                    </div>
                                    <!--End Img Blog Post-->  

                                    <!--Ingo Blog Post-->
                                    <div class="info-item-blog-post">
                                        <p>Accompany relevant share you mean, renewed and information of interest to learn to save you and your projects come true.</p>
                                        <a href="#"><i class="fa fa-plus-circle"></i> View more</a>
                                    </div>
                                    <!--End Ingo Blog Post-->  
                                </div>
                                <!--End Item Blog Post-->  
                            </div>
                            <!--End Col Item Blog Post-->
                        </div>
                        <!--End Blog Post Items-->
                    </div>
                    <!-- End Container Blog Post -->
                </div>
                <!-- Info Content - Resalt Info-->
                <div class="content_info">
                    <!-- Info Resalt-->
                    <div class="skin_base color-white paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12 title-resalt">
                                    <h2>Please, Help us</h2>
                                    <p class="lead">See how Client has transformed clinical trials quickly and securely using our platform</p>
                                </div>               
                            </div>
                        </div>
                    </div>
                    <!-- End Info Resalt-->
                </div>
                <!-- End Info Content - Resalt Info-->      