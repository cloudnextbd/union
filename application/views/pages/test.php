
                <!-- Info Content - Section Title-->
                <div class="content_info">
                    <!-- Info Section title-->
                    <div class="section-title-01 section-title-01-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Our History</h3>
                                    <h5>Great Company</h5>
                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Info Section Title-->

                    <!-- breadcrumbs-->
                    <div class="breadcrumbs breadcrumbs-sections">
                        <ul>
                            <li class="breadcrumbs-home">
                                <a href="#" title="Back To Home">
                                  <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">Company</a>
                            </li>
                            <li>
                                /
                            </li>
                            <li>
                               History
                            </li>
                        </ul>
                    </div>
                    <!-- End breadcrumbs-->
                </div>
                <!-- End Info Content - Section Title-->

                <!-- Info Content  1980-->
                <div class="content_info">
                    <!-- title-vertical-line-->
                    <div class="title-vertical-line">
                        <h2>Year <span>1980</span></h2>
                        <p class="lead">Inauguration of our Bank.</p>
                    </div>
                    <!-- End title-vertical-line-->

                    <div class="paddings">
                        <!-- Container Area - Boxes Services -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="img/gallery/1.jpg" alt="" class="img-responsive">
                                </div>       

                                <div class="col-md-7">
                                    <div class="title-subtitle">
                                        <h5>Company Value</h5>
                                        <h3>Who Are You</h3>
                                        <p class="lead">Coop Bank is a company of the envato Foundation through its banking activities to contribute in overcoming the structural causes of poverty in Australia.</p>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>Our Mission</h5>
                                            <p>Lorem iur adipiscing elit. Ut vehicula dapibus augue nec maximustiam eleifend magna erat, quis vestibulum lacus mattis sit ametec pellentesque lorem sapien.</p>
                                        </div>

                                        <div class="col-md-6">
                                            <h5>Responsibilty</h5>
                                            <p>Lorem iur adipiscing elit. Ut vehicula dapibus augue nec maximustiam eleifend magna erat, quis vestibulum lacus mattis sit ametec pellentesque lorem sapien.</p>
                                        </div>
                                    </div>
                                </div>             
                            </div>   
                        </div>
                    </div>
                    <!-- End Container Area - Boxes Services -->
                </div>
                <!-- End Info Content  1980-->

                <!-- Info Content -->
                <div class="parallax-window" data-parallax="scroll" data-image-src="img/parallax-img/parallax-01.jpg">
                   <!-- Content Parallax-->
                    <div class="opacy_bg_02">
                        <div class="container">
                            <div class="row padding-bottom">
                                <!-- title-vertical-line-->
                                <div class="title-vertical-line padding-bottom">
                                    <h2>Year <span>1990</span></h2>
                                    <p class="lead">Inauguration of our Bank.</p>
                                </div>
                                <!-- End title-vertical-line-->

                                <div class="col-md-9">
                                    <div class="title-subtitle">
                                        <h5>Company Value</h5>
                                        <h3>Who Are You</h3>
                                        <p class="lead">Coop Bank is a company of the envato Foundation through its banking activities to contribute in overcoming the structural causes of poverty in Australia.</p>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>Our Mission</h5>
                                            <p>Lorem iur adipiscing elit. Ut vehicula dapibus augue nec maximustiam eleifend magna erat, quis vestibulum lacus mattis sit ametec pellentesque lorem sapien.</p>
                                        </div>

                                        <div class="col-md-6">
                                            <h5>Responsibilty</h5>
                                            <p>Lorem iur adipiscing elit. Ut vehicula dapibus augue nec maximustiam eleifend magna erat, quis vestibulum lacus mattis sit ametec pellentesque lorem sapien.</p>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div> 
                    </div>  
                    <!-- End Content Parallax--> 
                </div>
                <!-- End Info Content -->

                <!--Info Content-->
                <div class="content_info">
                    <!-- Parallax Background -->
                    <div class="borders world-bg"></div>
                    <!-- Parallax Background -->

                    <!-- title-vertical-line-->
                    <div class="title-vertical-line">
                        <h2>Year <span>2000</span></h2>
                        <p class="lead">Inauguration of our Bank.</p>
                    </div>
                    <!-- End title-vertical-line-->

                    <div class="paddings">
                        <!-- Container Area - Boxes Services -->
                        <div class="container">
                            <div class="row">
                              <div class="col-md-7">
                                    <div class="title-subtitle">
                                        <h5>Company Value</h5>
                                        <h3>Who Are You</h3>
                                        <p class="lead">Coop Bank is a company of the envato Foundation through its banking activities to contribute in overcoming the structural causes of poverty in Australia.</p>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>Our Mission</h5>
                                            <p>Lorem iur adipiscing elit. Ut vehicula dapibus augue nec maximustiam eleifend magna erat, quis vestibulum lacus mattis sit ametec pellentesque lorem sapien.</p>
                                        </div>

                                        <div class="col-md-6">
                                            <h5>Responsibilty</h5>
                                            <p>Lorem iur adipiscing elit. Ut vehicula dapibus augue nec maximustiam eleifend magna erat, quis vestibulum lacus mattis sit ametec pellentesque lorem sapien.</p>
                                        </div>
                                    </div>
                                </div>    

                                <div class="col-md-5">
                                    <img src="img/gallery/2.jpg" alt="" class="img-responsive">
                                </div>   
                            </div>   
                        </div>
                    </div>
                    <!-- End Container Area - Boxes Services -->
                </div>
                <!-- End Info Content -->

                <div class="bg-dark color-white">
                    <!-- Container Area - Boxes Services -->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- title-vertical-line-->
                                <div class="title-vertical-line padding-bottom">
                                    <h2>Year <span>2015</span></h2>
                                    <p class="lead">Inauguration of our Bank.</p>
                                </div>
                                <!-- End title-vertical-line-->
                            </div>
                        </div>

                        <div class="row padding-bottom">
                            <div class="col-md-5">
                                <img src="img/gallery/3.jpg" alt="" class="img-responsive">
                            </div>       

                            <div class="col-md-7">
                                <div class="title-subtitle">
                                    <h5>Company Value</h5>
                                    <h3>Who Are You</h3>
                                    <p class="lead">Coop Bank is a company of the envato Foundation through its banking activities to contribute in overcoming the structural causes of poverty in Australia.</p>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>Our Mission</h5>
                                        <p>Lorem iur adipiscing elit. Ut vehicula dapibus augue nec maximustiam eleifend magna erat, quis vestibulum lacus mattis sit ametec pellentesque lorem sapien.</p>
                                    </div>

                                    <div class="col-md-6">
                                        <h5>Responsibilty</h5>
                                        <p>Lorem iur adipiscing elit. Ut vehicula dapibus augue nec maximustiam eleifend magna erat, quis vestibulum lacus mattis sit ametec pellentesque lorem sapien.</p>
                                    </div>
                                </div>
                            </div>             
                        </div>  
                    </div>
                    <!-- End Container Area - Boxes Services -->
                </div>
            </div>
            <!-- End content-central-->

            <!-- Sponsors Container-->  
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                       <!-- Sponsors Zone-->     
                        <ul class="owl-carousel carousel-sponsors tooltip-hover" id="carousel-sponsors">
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/1.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/2.png" alt="Image"></a>
                            </li>
                             <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/3.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/4.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/5.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/6.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/7.png" alt="Image"></a>
                            </li>
                             <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/8.png" alt="Image"></a>
                            </li>                                       
                        </ul> 
                        <!-- End Sponsors Zone-->    
                    </div>                    
                </div>
            </div>
            <!-- End Sponsors Container-->  
      
            <!-- footer-->
            <footer id="footer">
                <!-- Services-lines Items Container -->
                <div class="container">
                    <div class="row">
                        <!-- Services-lines Items -->
                        <div class="col-md-12 services-lines-container">
                            <ul class="services-lines">
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-credit-card"></i>
                                        <h5>Your Payments</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                 <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-industry"></i>
                                        <h5>District Tax Payment</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-university"></i>
                                        <h5>Paying Taxes - CoopBank</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                            </ul>

                            <ul class="services-lines no-margin">
                                <li>
                                    <div class="item-service-line">
                                       <i class="fa fa-balance-scale"></i>
                                        <h5>Payment of contributions</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                 <li>
                                    <div class="item-service-line">
                                       <i class="fa fa-cc"></i>
                                        <h5>Settlement and severance pay.</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-line-chart"></i>
                                        <h5>Workers pay contributions</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                            </ul>
                        </div> 
                        <!-- End Services-lines Items --> 
                    </div>
                </div>
                <!-- Services-lines Items Container -->

        