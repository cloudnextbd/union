

                <!-- Info Content - Section Title-->
                <div class="content_info">
                    <!-- Info Section title-->
                    <div class="section-title-01 section-title-01-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <h3>Background Sections</h3>
                                    <h5>FEATURES SECTION</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Info Section Title-->

                    <!-- breadcrumbs-->
                    <div class="breadcrumbs breadcrumbs-sections">
                        <ul>
                            <li class="breadcrumbs-home">
                                <a href="#" title="Back To Home">
                                  <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">Features</a>
                            </li>
                            <li>
                                /
                            </li>
                            <li>
                                Background Sections
                            </li>
                        </ul>
                    </div>
                    <!-- End breadcrumbs-->
                </div>
                <!-- End Info Content - Section Title-->

                <!-- End content info - White Section-->
                <div class="content_info">
                    <!-- Info Resalt-->
                    <div class="paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>White Section</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>                    
                            </div>
                        </div>
                    </div>
                    <!-- End Info Resalt-->
                </div>   
                <!-- End content info - White Section--> 

                <!-- End content info - Grey Section-->
                <div class="content_info">
                    <!-- Info Resalt-->
                    <div class="content_resalt paddings borders">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Grey Section</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>                    
                            </div>
                        </div>
                    </div>
                    <!-- End Info Resalt-->
                </div>   
                <!-- End content info - Grey Section--> 

                <!-- End content info - Dark Section-->
                <div class="content_info">
                    <!-- Info Resalt-->
                    <div class="bg-dark color-white paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Dark Section</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>                    
                            </div>
                        </div>
                    </div>
                    <!-- End Info Resalt-->
                </div>   
                <!-- End content info - Dark Section--> 

                <!-- End content info - Parallax Section 01 -->
                <div class="parallax-window" data-parallax="scroll" data-image-src="img/parallax-img/parallax-01.jpg">
                   <!-- Content Parallax-->
                    <div class="opacy_bg_01 paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Section with parallax 01</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>  
                            </div>
                        </div>
                    </div>  
                    <!-- End Content Parallax--> 
                </div>
                <!-- End content info - Parallax Section 01 --> 

                <!-- End content info - Parallax Section 02 -->
                <div class="parallax-window" data-parallax="scroll" data-image-src="img/parallax-img/parallax-02.jpg">
                   <!-- Content Parallax-->
                    <div class="opacy_bg_02 paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Section with parallax 02</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>  
                            </div>
                        </div>
                    </div>  
                    <!-- End Content Parallax--> 
                </div>
                <!-- End content info - Parallax Section 02 -->  

                <!-- End content info - Parallax Section 03 -->
                <div class="parallax-window" data-parallax="scroll" data-image-src="img/parallax-img/parallax-04.jpg">
                   <!-- Content Parallax-->
                    <div class="opacy_bg_03 paddings color-white">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Section with parallax 03</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>  
                            </div>
                        </div>
                    </div>  
                    <!-- End Content Parallax--> 
                </div>
                <!-- End content info - Parallax Section 03 -->  

                <!-- End content info - Parallax Section 04 -->
                <div class="parallax-window" data-parallax="scroll" data-image-src="img/parallax-img/parallax-03.jpg">
                   <!-- Content Parallax-->
                    <div class="opacy_bg_04 paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Section with parallax 04</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>  
                            </div>
                        </div>
                    </div>  
                    <!-- End Content Parallax--> 
                </div>
                <!-- End content info - Parallax Section 04 -->   

                <!-- End content info - Skin Base Section-->
                <div class="content_info">
                    <!-- Info Resalt-->
                    <div class="skin_base color-white paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Skin Base Section</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>                    
                            </div>
                        </div>
                    </div>
                    <!-- End Info Resalt-->
                </div>   
                <!-- End content info - Skin Base Section--> 
        
                <!-- End content info - Video Section -->
                <div class="content_info overflow-hidde">
                    <!-- Vide Background -->
                    <video class="bg_video" preload="auto" autoplay="autoplay" loop muted  poster='img/parallax-img/parallax-04.jpg' data-setup="{}">
                        <source src="img/video/video.mp4" type="video/mp4">
                        <source src="img/video/video.webm" type="video/webm">
                    </video>
                    <!-- Vide Background -->

                    <!-- Content Video-->
                    <div class="opacy_bg_02 paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>div with Section background</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                        </div> 
                    </div>  
                    <!-- End Content Video--> 
                </div>   
                <!-- End content info - Video Section --> 
            </div>
            <!-- End content-central-->

            <!-- Sponsors Container-->  
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                       <!-- Sponsors Zone-->     
                        <ul class="owl-carousel carousel-sponsors tooltip-hover" id="carousel-sponsors">
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/1.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/2.png" alt="Image"></a>
                            </li>
                             <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/3.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/4.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/5.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/6.png" alt="Image"></a>
                            </li>
                            <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/7.png" alt="Image"></a>
                            </li>
                             <li data-toggle="tooltip" title data-original-title="Name Sponsor">
                                <a href="#"  class="tooltip_hover" title="Name Sponsor"><img src="img/sponsors/8.png" alt="Image"></a>
                            </li>                                       
                        </ul> 
                        <!-- End Sponsors Zone-->    
                    </div>                    
                </div>
            </div>
            <!-- End Sponsors Container-->  
      
            <!-- footer-->
            <footer id="footer">
                <!-- Services-lines Items Container -->
                <div class="container">
                    <div class="row">
                        <!-- Services-lines Items -->
                        <div class="col-md-12 services-lines-container">
                            <ul class="services-lines">
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-credit-card"></i>
                                        <h5>Your Payments</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                 <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-industry"></i>
                                        <h5>District Tax Payment</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-university"></i>
                                        <h5>Paying Taxes - CoopBank</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                            </ul>

                            <ul class="services-lines no-margin">
                                <li>
                                    <div class="item-service-line">
                                       <i class="fa fa-balance-scale"></i>
                                        <h5>Payment of contributions</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                 <li>
                                    <div class="item-service-line">
                                       <i class="fa fa-cc"></i>
                                        <h5>Settlement and severance pay.</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-line-chart"></i>
                                        <h5>Workers pay contributions</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                            </ul>
                        </div> 
                        <!-- End Services-lines Items --> 
                    </div>
                </div>
                <!-- Services-lines Items Container -->

              