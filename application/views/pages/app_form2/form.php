<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>

            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
</div>
<!-- Info Content -->
<div class="content_info">
    <div class="paddings">
        <!-- container area -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="fuelux-wizard-container">
                                    <!--  form header -->
                                    <div>
                                        <ul class="steps">
                                            <li data-step="1" class="active">
                                                <span class="step">১</span>
                                                <span class="title">ব্যক্তিগত পরিচিতি</span>
                                            </li>
                                            <li data-step="2">
                                                <span class="step">২</span>
                                                <span class="title">পারিবারিক পরিচিতি</span>
                                            </li>
                                            <li data-step="3">
                                                <span class="step">৩</span>
                                                <span class="title">অর্থনৈতিক অবস্থা</span>
                                            </li>
                                            <li data-step="4">
                                                <span class="step">৪</span>
                                                <span class="title">যে সেবার জন্য আগ্রহী</span>
                                            </li>
                                            <li data-step="5">
                                                <span class="step">৫</span>
                                                <span class="title">জমা দিন</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr />
                                    <div class="step-content pos-rel">
                                        <?= form_open_multipart(site_url("new_form/do_add"), array("class" => "form-horizontal")) ?>
                                        <!-- step 01 -->
                                        <div class="step-pane active" data-step="1">
                                            <!-- name -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">নামঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-md-6" type="text" name="name" id="name" maxlength="100" placeholder="বিঃদ্রঃ ভোটার আইডি কার্ডে যে নাম দেয়া আছে" required />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- gender -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right">লিঙ্গঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-md-9">
                                                    <div class="radio">
                                                        <label>
                                                            <input class="ace" type="radio" name="gender" value="male" checked>
                                                            <span class="lbl"> পুরুষ </span> 
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="gender" value="female">
                                                            <span class="lbl"> মহিলা </span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="gender" value="other">
                                                            <span class="lbl"> অনান্য </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- birth date -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Birth_date">জন্ম তারিখঃ:  <sup style="color: red">*</sup> </label>
                                                <div class="col-xs-12 col-md-9">
                                                    <div class="clearfix">
                                                        <div class="input-group">
                                                            <input class="form-control col-xs-12 col-md-4 date-picker" id="id-date-picker-1" data-date-format="dd-mm-yyyy" name="birth_date" placeholder="dd-mm-yyyy" required/>
                                                        </div>  
                                                    </div>  
                                                </div>
                                            </div>
                                            <!-- birth id -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Birth_id">জন্ম নিবন্ধন নংঃ </label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-md-6" type="text" name="birth_id" onkeyup="birth_id_check(this.value)" id="birth_id" />
                                                    </div>
                                                    <div id="birth_result" class='help-block'></div>
                                                </div>
                                            </div>
                                            <!-- nid -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Nid">জাতীয় পরিচয় পত্র নংঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-md-6" type="text" name="nid" id="nid" onkeyup="nid_check(this.value)" required/>
                                                    </div>
                                                    <div id="nid_result" class='help-block'></div>
                                                </div>
                                            </div>
                                            <!-- phone -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Mobile">মোবাইল নংঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-md-6" type="text" name="mobile" id="mobile" placeholder="+880 167-2552-966" required/>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- image -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Image">ছবিঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-md-4">
                                                    <div class="clearfix">
                                                        <input type="file" name="image" id="myFile" onchange="imageshow(this);" required />
                                                        <p class="help-block">jpg or png image & maximum 1 MB</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-5">
                                                    <img id="previewImg1" width="50px" src="<?=base_url()?>assets/app_img/default.png" />
                                                    <script>
                                                        function imageshow(input) {
                                                            if (input.files && input.files[0]) {
                                                                var reader = new FileReader();
                                                                reader.onload = function(e) {
                                                                    $('#previewImg1')
                                                                    .attr('src', e.target.result)
                                                                    .width(130);
                                                                }
                                                                reader.readAsDataURL(input.files[0]);
                                                            } else {
                                                                var filename = "";
                                                                filename = "file:\/\/" + input.value;
                                                                document.form2.previewImg1.src = filename;
                                                                document.form2.previewImg1.style.width = "50px";
                                                            }
                                                        }
                                                    </script>
                                                </div>
                                            </div>
                                            <div class="hr hr-dotted"></div>
                                            <!--  state -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">জেলাঃ </label>
                                                <div class="col-xs-12 col-md-6">
                                                    <label class="control-label" style="color: #777" >চাঁদপুর</label>
                                                    <input type="hidden" name="district" id="district" value="chand pur" required/>
                                                </div>
                                            </div>
                                            <!-- sub state -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="platform">উপজেলাঃ </label>
                                                <div class="col-xs-12 col-md-6">
                                                    <label class="control-label" style="color: #777" >চাঁদপুর সদর</label>
                                                    <input type="hidden" name="sub_district" id="sub_district" value="chand pur shodor" required/>
                                                </div>
                                            </div>
                                            <!-- union -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="platform">ইউনিয়নঃ </label>
                                                <div class="col-xs-12 col-md-6">
                                                    <label class="control-label" style="color: #777" >বাগাদী</label>
                                                    <input type="hidden" name="union" id="union" value="bagadi" required/>
                                                </div>
                                            </div>
                                            <!-- word no -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Word_no">ওয়ার্ড নংঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <select class="input-large" id="word_no" name="word_no">
                                                            <option selected >নির্বাচন করুন</option>
                                                            <option value="a">1</option>
                                                            <option value="v">2</option>
                                                            <option value="c">3</option>
                                                            <option value="d">4</option>
                                                            <option value="d">5</option>
                                                            <option value="d">6</option>
                                                            <option value="d">7</option>
                                                            <option value="d">8</option>
                                                            <option value="d">9</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- village -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="platform">গ্রামের নামঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <select class="input-large" id="village" name="village">
                                                            <option selected>নির্বাচন করুন</option>
                                                            <option value="a">রাড়িরচর ও চান্দের বাগ</option>
                                                            <option value="v">মনিহার</option>
                                                            <option value="c">দেবপুর</option>
                                                            <option value="d">চরবাকিলা ও ছোটসুন্দর</option>
                                                            <option value="d">আলগী</option>
                                                            <option value="d">কামরাঙ্গা</option>
                                                            <option value="d">বড়সুন্দর ও পাঁচ বাড়িয়া</option>
                                                            <option value="d">রামপুর</option>
                                                            <option value="d">সকদী পাঁচ গাঁও</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- house name -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">বাড়ির নামঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="house_name" id="house_name" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- holding no -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">হোল্ডিং নংঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="holdings_no" id="holdings_no" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hr hr-dotted"></div>
                                            <!-- education -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">শিক্ষাগত যোগ্যতাঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <select class="input-large" id="education" name="education">
                                                            <option selected>নির্বাচন করুন</option>
                                                            <option value="a">ডিগ্রী </option>
                                                            <option value="b">এইচ এস সি</option>
                                                            <option value="c">এস এস সি </option>
                                                            <option value="d">৮ম শ্রেণী </option>
                                                            <option value="d">৫ম শ্রেণী </option>
                                                            <option value="d">অক্ষর জ্ঞান</option>
                                                            <option value="d">নিরক্ষর </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- profession -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">পেশাঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-6" type="text" name="profession" id="profession" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- capability -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">স্বক্ষমতা <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <select class="input-large" id="capability" name="capability">
                                                            <option selected>নির্বাচন করুন</option>
                                                            <option value="complete_disable" >সম্পূর্ণ কর্মক্ষমতাহীন </option>
                                                            <option value="partial_disable" >আংশিক কর্মক্ষম </option>
                                                            <option value="handicaped" > প্রতিবন্ধী </option>
                                                            <option value="ill" >অসুস্থ</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- physical problem -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right">অসামর্থ্যতা/প্রতিবন্ধী প্রকৃতিঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="radio">
                                                        <label>
                                                            <input class="ace" type="checkbox" name="disability" value="no" checked>
                                                            <span class="lbl"> প্রযোজ্য নয়</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="checkbox" name="disability" value="eye_dis">
                                                            <span class="lbl"> দৃষ্টি প্রতিবন্ধী</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="checkbox" name="disability" value="body_dis">
                                                            <span class="lbl"> শারীরিক প্রতিবন্ধী</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- religion -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right">ধর্মঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="radio">
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="islame" checked>
                                                            <span class="lbl"> ইসলাম</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="hidnu">
                                                            <span class="lbl"> হিন্দু</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="christians">
                                                            <span class="lbl"> খ্রিষ্টান</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="buddha">
                                                            <span class="lbl"> বৌদ্ধ</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="other">
                                                            <span class="lbl"> অনান্য </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /step 1 -->
                                        <!-- step 2 -->
                                        <div class="step-pane" data-step="2">
                                            <!-- father name -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">পিতার নামঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-6" type="text" name="father_name" id="father_name" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- father nid -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Father_nid">জাতীয় পরিচয় পত্র নংঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-8" type="text" name="father_nid" id="father_nid" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- mother name -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">মাতার নামঃ  <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-6" type="text" name="mother_name" id="mother_name" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- mother nid -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">জাতীয় পরিচয় পত্র নংঃ  <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-8" type="text" name="mother_nid" id="mother_nid" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- marital_status -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right">বৈবাহিক অবস্থাঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="radio">
                                                        <label>
                                                            <input class="ace" type="radio" name="marital_status" value="yes" >
                                                            <span class="lbl"> হ্যা</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="marital_status" value="no" >
                                                            <span class="lbl"> না</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="marital_status" value="widow" >
                                                            <span class="lbl"> বিধবা</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="marital_status" value="bipotnik" >
                                                            <span class="lbl"> বিপত্নীক </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- husband name -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Husband_name">স্বামীর নামঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-6" type="text" name="husband_name" id="husband_name" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- husband nid -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Husband_nid">জাতীয় পরিচয় পত্র নংঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-8" type="text" name="husband_nid" id="husband_nid" placeholder="husband_nid_no" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- wife name -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">স্ত্রীর নামঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-6" type="text" name="wife_name" id="wife_name" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- wife nid -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Wife_nid">জাতীয় পরিচয় পত্র নংঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-8" type="text" name="wife_nid" id="wife_nid" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- family member  -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Family_member">পরিবারের সদস্য সংখাঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-3" type="text" name="family_member" id="family_member" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- family description  -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Family_desc">পরিবারের সদস্যদের বর্ননাঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <textarea class="col-xs-12 col-sm-8 input-xlarge" name="family_desc" id="family_desc" rows="7" placeholder=""></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /step 2 -->
                                        <!-- step 3 -->
                                        <div class="step-pane" data-step="3">
                                            <!-- house definition -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="House_desc">বাসস্থানের অবস্থার বিবরণঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <textarea class="col-xs-12 col-sm-8 input-xlarge" name="house_desc" id="house_desc" rows="7" placeholder="" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- image -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="House_image">বাসস্থানের ছবিঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-md-4">
                                                    <div class="clearfix">
                                                        <input type="file" name="house_image" id="myFile" onchange="imageshow2(this);" required />
                                                        <p class="help-block">jpg or png image & maximum 1 MB</p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-5">
                                                    <img id="previewImg2" width="50px" src="<?=base_url()?>assets/app_img/default.png" />
                                                    <script>
                                                        function imageshow2(input) {
                                                            if (input.files && input.files[0]) {
                                                                var reader = new FileReader();
                                                                reader.onload = function(e) {
                                                                    $('#previewImg2')
                                                                    .attr('src', e.target.result)
                                                                    .width(130);
                                                                }
                                                                reader.readAsDataURL(input.files[0]);
                                                            } else {
                                                                var filename = "";
                                                                filename = "file:\/\/" + input.value;
                                                                document.form2.previewImg2.src = filename;
                                                                document.form2.previewImg2.style.width = "50px";
                                                            }
                                                        }
                                                    </script>
                                                </div>
                                            </div>
                                            <!-- land -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Land">জমির পরিমানঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="land" id="land"  placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- number of earning person -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Earn_person">উপার্জনক্ষম সদস্য সংখাঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="earn_person" id="earn_person"  placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- earning source -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Earn_source">আয়ের উৎসঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="earn_source" id="earn_source"  placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- monthly income -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Monthly_income">মাসিক আয়ঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="monthly_income" id="monthly_income"  placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- cause of poorness -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Cause_poorness">দারিদ্র্যের কারনঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <textarea class="col-xs-12 col-sm-8 input-xlarge" name="cause_poorness" id="cause_poorness" rows="3" placeholder="" ></textarea>
                                                    </div>
                                                </div>
                                            </div>                      
                                        </div>
                                        <!-- step 4 -->
                                        <div class="step-pane" data-step="4">
                                            <!-- current service -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Curr_service">বর্তমানে কোন ধরনের সেবা পাচ্ছেনঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <select multiple="multiple"  name="curr_service[]" class="select2" style="width: auto" data-placeholder="Click to Choose...">
                                                            <?php foreach($services as $service) : ?>
                                                                <option value="<?=$service->id?>"><?=$service->ser_name?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- interested service -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Inter_service">কোন ধরনের সেবার জন্য আগ্রহীঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <select class="input-large" id="inter_service" name="inter_service">
                                                            <?php foreach($services as $service) : ?>
                                                                <option value="<?=$service->id?>"><?=$service->ser_name?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- way of success -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Success_way">দারিদ্রতা থেকে উত্তরনের উপায়ঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <textarea class="col-xs-12 col-sm-8 input-xlarge" name="success_way" id="success_way" rows="2" placeholder="" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- interested training -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Inter_trainings">কি ধরনের প্রশিক্ষণের জন্য আগ্রহিঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="inter_trainings" id="inter_trainings"  placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!--new ref-->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Reference">সুপারিশ কারীর নামঃ (কমপক্ষে তিনজন)</label>
                                                <div class="col-xs-12 col-md-9">
                                                    <div class="clearfix">
                                                        <select multiple="multiple"  name="reference[]" class="select2" style="width: auto" data-placeholder="Click to Choose...">
                                                            <?php foreach($refs as $ref) : ?>
                                                                <option value="<?=$ref->id?>"><?=$ref->first_name .' '. $ref->last_name?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- special info -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="Success_way">বিশেষায়িত তথ্য <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <textarea class="col-xs-12 col-sm-8 input-xlarge" name="sp_info" id="sp_info" rows="8" placeholder="" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /step 4 -->
                                        <!-- step 5 -->
                                        <div class="step-pane" data-step="5">
                                            <div class="form-group">
                                                <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                                    <label>
                                                        <input name="agree" id="agree" type="checkbox" class="ace">
                                                        <span class="lbl"> আমি সজ্ঞানে সকল তথ্য সঠিক বলিয়া নিশ্চিত করিলাম।</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="alert alert-success">
                                                    <strong>
                                                        <i class="ace-icon fa fa-check"></i>
                                                        আপনাকে ধন্যবাদ। আপনার আবেদন পত্রটি সফল ভাবে সম্পূর্ণ হইল। আমাদের অবিরত চেষ্টা আপনার কল্যাণের জন্য।
                                                    </strong>
                                                </div>
                                            </div>
                                            <button class="btn btn-success" type="submit" >submit</button>
                                        </div>
                                    <?=form_close()?>
                                    </div>
                                </div>
                                <hr />
                                <div class="wizard-actions">
                                    <button class="btn btn-prev">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        পূর্ববর্তী
                                    </button>

                                    <button class="btn btn-success btn-next" data-last="Finish">
                                        পরবর্তী
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>
                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>