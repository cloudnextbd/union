<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>

            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
</div>
<!-- Info Content  - Principal Info-->
<div class="content_info">
    <div class="paddings">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="fuelux-wizard-container">
                                    <!--  form header -->
                                    <div>
                                        <ul class="steps">
                                            <li data-step="1">
                                                <span class="step">১</span>
                                                <span class="title">ব্যক্তিগত পরিচিতি</span>
                                            </li>
                                            <li data-step="2">
                                                <span class="step">২</span>
                                                <span class="title">পারিবারিক পরিচিতি</span>
                                            </li>
                                            <li data-step="3">
                                                <span class="step">৩</span>
                                                <span class="title">অর্থনৈতিক অবস্থা</span>
                                            </li>
                                            <li data-step="4">
                                                <span class="step">৪</span>
                                                <span class="title">যে সেবার জন্য আগ্রহী</span>
                                            </li>
                                            <li data-step="5" class="active">
                                                <span class="step">৫</span>
                                                <span class="title">জমা দিন</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr />
                                    <div class="step-content pos-rel">
                                        <?= form_open(site_url(""), array("class" => "form-horizontal")) ?>
                                        <div class="step-pane active" data-step="5">
                                            <?php
                                                if (validation_errors()) {
                                                    echo '<div id="validation_errors" title="Error:" ';
                                                    echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
                                                    echo validation_errors();
                                                    echo '</ul></div>';
                                                    echo '</div>';
                                                }
                                            ?>
                                            
                                            <?php $dm = $this->session->flashdata('danger');
                                            if (!empty($dn)) { ?>
                                                <!-- notice -->
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <h5><i class="icon fa fa-check"></i><?php echo $this->session->flashdata('danger') ?></h5>
                                                </div>
                                            <?php } ?>
                                                
                                        </div>
                                        <?=form_close()?>
                                    </div>
                                </div>
                                <hr />
                                <div class="wizard-actions">
                                    <a href="<?=site_url('new_form')?>">
                                        <button class="btn btn-success">
                                            <i class="ace-icon fa fa-undo fa-arrow-left"></i>
                                            পুনরায় পূরণ করুন 
                                        </button>
                                    </a>
                                    <a href="<?=site_url()?>">
                                        <button class="btn btn-warning">
                                            হোম
                                            <i class="ace-icon fa fa-home icon-on-right"></i>
                                        </button>
                                    </a>
                                </div>
                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>