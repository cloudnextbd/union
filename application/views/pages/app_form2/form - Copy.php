<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>

            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
    <!-- End breadcrumbs-->
</div>

<!-- Info Content  - Principal Info-->
<div class="content_info">
    <div class="paddings">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>

                        <div class="widget-body">

                            <div class="widget-main">
                                <div id="fuelux-wizard-container">

                                    <!-- --------------------- form header --------------------- -->
                                    <div>
                                        <ul class="steps">
                                            <li data-step="1" class="active">
                                                <span class="step">১</span>
                                                <span class="title">ব্যক্তিগত পরিচিতি</span>
                                            </li>
                                            <li data-step="2">
                                                <span class="step">২</span>
                                                <span class="title">পারিবারিক পরিচিতি</span>
                                            </li>
                                            <li data-step="3">
                                                <span class="step">৩</span>
                                                <span class="title">অর্থনৈতিক অবস্থা</span>
                                            </li>
                                            <li data-step="4">
                                                <span class="step">৪</span>
                                                <span class="title">যে সেবার জন্য আগ্রহী</span>
                                            </li>
                                            <li data-step="6">
                                                <span class="step">৭</span>
                                                <span class="title">ফলাফল</span>
                                            </li>
                                        </ul>
                                    </div>

                                    <hr />

                                    <div class="step-content pos-rel">

                                        <!-- step 01 -->
                                        <div class="step-pane active" data-step="1">

                                            <form class="form-horizontal" method="get">
                                                
                                                <!-- name -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">নামঃ <sup style="color: red">*</sup></label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="name" id="name" placeholder="বিঃদ্রঃ ভোটার আইডি কার্ডে যে নাম দেয়া আছে" required />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- gender -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right">লিঙ্গঃ <sup style="color: red">*</sup></label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="radio">
                                                            <label>
                                                                <input class="ace" type="radio" name="gender" value="male" checked>
                                                                <span class="lbl"> পুরুষ </span> 
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="gender" value="female">
                                                                <span class="lbl"> মহিলা </span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="gender" value="other">
                                                                <span class="lbl"> অনান্য </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- birth date -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="password2">জন্ম তারিখঃ:  <sup style="color: red">*</sup> </label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <div class="input-group">
                                                                <input class="form-control col-xs-12 col-md-4 date-picker" id="id-date-picker-1" data-date-format="dd-mm-yyyy" name="birth_date" placeholder="dd-mm-yyyy" required/>
                                                            </div>  
                                                        </div>  
                                                    </div>
                                                </div>
                                                
                                                <!-- birth id no -->
                                                <div class="space-2"></div>
                                                <?php if (form_error('birth_id')) {
                                                        echo "<div class='form-group has-error'>";
                                                    } else {
                                                        echo "<div class='form-group'>";
                                                    }
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">জন্ম নিবন্ধন নংঃ </label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="birth_id" id="birth_id" onkeyup="check_birth_id(this.value)" value="<?php echo set_value('birth_id'); ?>" />
                                                        </div>
                                                        <div id="birth_result" class='help-block'></div>
                                                        <?php if (form_error('birth_id')) {
                                                            echo "<div class='help-block col-xs-12 col-sm-reset inline'> error feedback text here! </div>";
                                                        } ?>
                                                    </div>
                                                </div>
                                        
                                                <!-- nid -->
                                                <div class="space-2"></div>
                                                <?php if (form_error('nid')) {
                                                        echo "<div class='form-group has-error'>";
                                                    } else {
                                                        echo "<div class='form-group'>";
                                                    }
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">জাতীয় পরিচয় পত্র নংঃ <sup style="color: red">*</sup></label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="nid" id="nid_id" onkeyup="check_nid(this.value)" value="<?php echo set_value('nid'); ?>" required/>
                                                        </div>
                                                        <div id="nid_result" class='help-block'></div>
                                                        <?php if (form_error('nid')) {
                                                            echo "<div class='help-block col-xs-12 col-sm-reset inline'> error feedback text here! </div>";
                                                        } ?>
                                                    </div>
                                                </div>
                                    
                                                <!-- phone -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">মোবাইল নংঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="mobile" id="mobile" placeholder="+880 167-2552-966" required/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="hr hr-dotted"></div>
                                                <!--  state -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">জেলাঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <label class="control-label" style="color: #777" >চাঁদপুর</label>
                                                        <input type="hidden" name="district" id="district" value="chand pur" required/>
                                                    </div>
                                                </div>
                                                <!-- sub state -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="platform">উপজেলাঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <label class="control-label" style="color: #777" >চাঁদপুর সদর</label>
                                                        <input type="hidden" name="sub_district" id="sub_district" value="chand pur shodor" required/>
                                                    </div>
                                                </div>
                                                <!-- union -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="platform">ইউনিয়নঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <label class="control-label" style="color: #777" >বাগাদী</label>
                                                        <input type="hidden" name="union" id="union" value="bagadi" required/>
                                                    </div>
                                                </div>
                                                <!-- word no -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="platform">ওয়ার্ড নংঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <select class="input-large" id="word_no" name="word_no">
                                                                <option selected >নির্বাচন করুন</option>
                                                                <option value="a">1</option>
                                                                <option value="v">2</option>
                                                                <option value="c">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                                <option value="">6</option>
                                                                <option value="">7</option>
                                                                <option value="">8</option>
                                                                <option value="">9</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- village -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="platform">গ্রামের নামঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <select class="input-large" id="village" name="village">
                                                                <option selected>নির্বাচন করুন</option>
                                                                <option value="a">রাড়িরচর ও চান্দের বাগ</option>
                                                                <option value="v">মনিহার</option>
                                                                <option value="c">দেবপুর</option>
                                                                <option value="">চরবাকিলা ও ছোটসুন্দর</option>
                                                                <option value="">আলগী</option>
                                                                <option value="">কামরাঙ্গা</option>
                                                                <option value="">বড়সুন্দর ও পাঁচ বাড়িয়া</option>
                                                                <option value="">রামপুর</option>
                                                                <option value="">সকদী পাঁচ গাঁও</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- house name -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">বাড়ির নামঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-4" type="text" name="house_name" id="house_name" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- holding no -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">হোল্ডিং নংঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-4" type="text" name="holdings_no" id="holdings_no" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="hr hr-dotted"></div>
                                                <!-- education -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">শিক্ষাগত যোগ্যতাঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <select class="input-large" id="education" name="education">
                                                                <option selected>নির্বাচন করুন</option>
                                                                <option value="a">ডিগ্রী </option>
                                                                <option value="b">এইচ এস সি</option>
                                                                <option value="c">এস এস সি </option>
                                                                <option value="">৮ম শ্রেণী </option>
                                                                <option value="">৫ম শ্রেণী </option>
                                                                <option value="">অক্ষর জ্ঞান</option>
                                                                <option value="">নিরক্ষর </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- profession -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">পেশাঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-6" type="text" name="profession" id="profession" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- physical problem -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right">অসামর্থ্যতা/প্রতিবন্ধী প্রকৃতিঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="radio">
                                                            <label>
                                                                <input class="ace" type="checkbox" name="disabilities" value="no" checked>
                                                                <span class="lbl"> প্রযোজ্য নয়</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="checkbox" name="disabilities" value="eye_dis">
                                                                <span class="lbl"> দৃষ্টি প্রতিবন্ধী</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="checkbox" name="disabilities" value="body_dis">
                                                                <span class="lbl"> শারীরিক প্রতিবন্ধী</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- religion -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right">ধর্মঃ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="radio">
                                                            <label>
                                                                <input class="ace" type="radio" name="religion" value="islame" checked>
                                                                <span class="lbl"> ইসলাম</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="religion" value="hidnu">
                                                                <span class="lbl"> হিন্দু</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="religion" value="christians">
                                                                <span class="lbl"> খ্রিষ্টান</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="religion" value="buddha">
                                                                <span class="lbl"> বৌদ্ধ</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="religion" value="other">
                                                                <span class="lbl"> অনান্য </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div><!-- step 1/end -->

                                        <!-- step 2 -->
                                        <div class="step-pane" data-step="2">

                                            <form class="form-horizontal" id="validation-form" method="get">

                                                <!-- father name -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">পিতার নামঃ</label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-6" type="text" name="father_name" id="father_name" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- father nid -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">জাতীয় পরিচয় পত্র নংঃ</label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-8" type="text" name="father_nid_no" id="father_nid_no" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- mother name -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">মাতার নামঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-6" type="text" name="mother_name" id="mother_name" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- mother nid -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">জাতীয় পরিচয় পত্র নংঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-8" type="text" name="mother_nid_no" id="mother_nid_no" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- marital_status -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right">বৈবাহিক অবস্থাঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="radio">
                                                            <label>
                                                                <input class="ace" type="radio" name="marital_status" value="yes" >
                                                                <span class="lbl">হ্যা</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="marital_status" value="no" >
                                                                <span class="lbl">না</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="marital_status" value="widow" >
                                                                <span class="lbl">বিধবা</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="marital_status" value="bipotnik" >
                                                                <span class="lbl">বিপত্নীক </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- husband name -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">স্বামীর নামঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-6" type="text" name="husband_name" id="husband_name" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- husband nid -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">জাতীয় পরিচয় পত্র নংঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-8" type="text" name="" id="husband_nid_no" placeholder="husband_nid_no" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- wife name -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">স্ত্রীর নামঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-6" type="text" name="wife_name" id="wife_name" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- wife nid -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">জাতীয় পরিচয় পত্র নংঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-8" type="text" name="wife_nid_no" id="wife_nid_no" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- family member  -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">পরিবারের সদস্য সংখাঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-3" type="text" name="family_member" id="family_member" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- family description  -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">পরিবারের সদস্যদের বর্ননাঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <textarea class="col-xs-12 col-sm-8 input-xlarge" name="family_desc" id="family_desc" rows="7" placeholder=""></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>


                                        </div>

                                        <!-- step 3 -->
                                        <div class="step-pane" data-step="3">

                                            <form class="form-horizontal" id="validation-form" method="get">

                                                <!-- house definition -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">বাসস্থানের অবস্থার বিবরণঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <textarea class="col-xs-12 col-sm-8 input-xlarge" name="comment" id="comment" rows="7" placeholder="" ></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- image -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">বাসস্থানের ছবিঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-12" type="file" name="" id="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- land -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">জমির পরিমানঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-4" type="text" name="" id=""  placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- number of earning person -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">উপার্জনক্ষম সদস্য সংখাঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-4" type="text" name="" id=""  placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- earning source -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">আয়ের উৎসঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-4" type="text" name="" id=""  placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- monthly income -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">মাসিক আয়ঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-4" type="text" name="" id=""  placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- cause of poorness -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">দারিদ্র্যের কারনঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <textarea class="col-xs-12 col-sm-8 input-xlarge" name="comment" id="comment" rows="3" placeholder="" ></textarea>
                                                        </div>
                                                    </div>
                                                </div>                      

                                            </form>

                                        </div>

                                        <!-- step 4 active-->
                                        <div class="step-pane" data-step="4">

                                            <form class="form-horizontal" id="validation-form" method="get">

                                                <!-- present service -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="platform">বর্তমানে কোন ধরনের সেবা পাচ্ছেনঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <select class="input-large" id="platform" name="platform">
                                                                <option >নির্বাচন করুন</option>
                                                                <option value="">ভিজিএফ</option>
                                                                <option value="">ভিজিডি</option>
                                                                <option value="">মাতৃত্বকালীন ভাতা</option>
                                                                <option value="">মুক্তিযোদ্ধা ভাতা</option>
                                                                <option value="">প্রতিবন্ধী ভাতা</option>
                                                                <option value="">বিধবা ভাতা</option>
                                                                <option value="">ববয়স্ক ভাতা</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- interested service -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="platform">কোন ধরনের সেবার জন্য আগ্রহীঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <select class="input-large" id="platform" name="platform">
                                                                <option >নির্বাচন করুন</option>
                                                                <option value="">ভিজিএফ</option>
                                                                <option value="">ভিজিডি</option>
                                                                <option value="">মাতৃত্বকালীন ভাতা</option>
                                                                <option value="">মুক্তিযোদ্ধা ভাতা</option>
                                                                <option value="">প্রতিবন্ধী ভাতা</option>
                                                                <option value="">বিধবা ভাতা</option>
                                                                <option value="">ববয়স্ক ভাতা</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div> 

                                                <!-- way of success -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">দারিদ্রতা থেকে উত্তরনের উপায়ঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <textarea class="col-xs-12 col-sm-8 input-xlarge" name="comment" id="comment" rows="2" placeholder="" ></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- interested training -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">কি ধরনের প্রশিক্ষণের জন্য আগ্রহিঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-4" type="text" name="" id=""  placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- reference -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">সুপারিশ কারীর নামঃ (কমপক্ষে তিনজন) </label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-12" type="text" name="" id="" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>               

                                            </form>

                                        </div>

                                        <!-- step 5 -->
                                        <div class="step-pane" data-step="5">

                                            <form class="form-horizontal" id="validation-form" method="get">
                                                <!-- image -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">ছবিঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-12" type="file" name="image" id="image" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>

                                        <!-- step 5 -->
                                        <div class="step-pane" data-step="6">

                                            <form class="form-horizontal" id="validation-form" method="get">

                                                <div class="form-group">
                                                    <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                                        <label>
                                                            <input name="agree" id="agree" type="checkbox" class="ace">
                                                            <span class="lbl"> আমি সজ্ঞানে সকল তথ্য সঠিক বলিয়া নিশ্চিত করিলাম।</span>
                                                        </label>
                                                    </div>
                                                </div>

                                            </form>



                                        </div>

                                        <!-- step 6 -->
                                        <div class="step-pane" data-step="7">

                                            <div>
                                                <div class="alert alert-success">
                                                    <strong>
                                                        <i class="ace-icon fa fa-check"></i>
                                                        আপনাকে ধন্যবাদ। আপনার আবেদন পত্রটি সফল ভাবে সম্পূর্ণ হইল। আমাদের অবিরত চেষ্টা আপনার কল্যাণের জন্য।
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <hr />
                                <div class="wizard-actions">
                                    <button class="btn btn-prev">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        পূর্ববর্তী
                                    </button>

                                    <button class="btn btn-success btn-next" data-last="Finish">
                                        পরবর্তী
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>


                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->

                    </div>
                </div>
            </div>
        </div>
    </div>