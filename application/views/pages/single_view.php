
<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- Info Section title-->
    <div class="section-title-01 section-title-01-small">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h3>User Area.</h3>
                    <h5>Profile User</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- End Info Section Title-->

    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                  <i class="fa fa-home"></i>
              </a>
          </li>
          <li>
            <a href="#">Our Services</a>
        </li>
        <li>
            <li>
                /
            </li>
            <li>
                User Area
            </li>
        </ul>
    </div>
    <!-- End breadcrumbs-->
</div>
<!-- End Info Content - Section Title-->

<!-- Info Content - User Area-->
<div class="content_info">
    <div class="paddings">  
        <div class="container">
            <div class="row user-area">
                <!-- Left User Area-->
                <div class="col-md-4">
                    <div class="item-team">
                        <h4>জনাব মোঃ মনসুর আহমেদ</h4>
                        <span class="country"><img src="<?=base_url()?>assets/front/poor.jpeg" alt=""> চরবাকিলা ও ছোটসুন্দর</span>

                    </div>
                </div>
                <!-- End Left User Area-->

                <!-- Content Tabs Section-->
                <div class="col-md-8 item-team">
                    <!-- Nav tabs -->

                    <!-- End Nav tabs -->

                    <!-- tab-content-->

                    <!-- tab-Item-1-->
                    <div id="tab1">

                        <h4>ব্যক্তিগত পরিচিতি</h4>
                        <br />

                        <div class="profile-user-info profile-user-info-striped">

                            <div class="profile-info-row">
                                <div class="profile-info-name" style="width: 130px">সদস্য নংঃ</div>
                                <div class="profile-info-value">
                                    <span class="editable" id="username">0123</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name">নামঃ</div>
                                <div class="profile-info-value">
                                    <span class="editable" id="username">মোঃ মনসুর আহমেদ</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name"> লিঙ্গঃ </div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">পুরুষ</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name"> জন্ম তারিখঃ </div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">১/১/১৯৪৫</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name">জন্ম নিবন্ধন নংঃ</div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">১০৯৮৭৬২৫৬৪৭০৯৮৬</span>
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name"> জাতীয় পরিচয় পত্র নংঃ</div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">৬৫৭০৯৩৮৭৫৩১২১৯৫৬</span>
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name"> মোবাইল নংঃ</div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">০১৭১০০০০০০০</span>
                                </div>
                            </div>


                            <div class="profile-info-row">
                                <div class="profile-info-name"> জেলাঃ</div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">চাঁদপুর</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name"> উপজেলাঃ</div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">চাঁদপুর</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name"> ইউনিয়নঃ</div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">বাগাদী</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name"> ওয়ার্ড নংঃ</div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">৫</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name"> বাড়ির নামঃ </div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">বেপারি বাড়ি</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name">হোল্ডিং নংঃ  </div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">৯৮৬</span>
                                </div>
                            </div>



                            <div class="profile-info-row">
                                <div class="profile-info-name">পেশাঃ</div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">কৃষি</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name">অসামর্থ্যতা/প্রতিবন্ধী প্রকৃতিঃ </div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">প্রযোজ্য নয়</span>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name">ধর্মঃ </div>

                                <div class="profile-info-value">
                                    <span class="editable" id="age">ইসলাম</span>
                                </div>
                            </div>



                        </div>

                    </div>
                    <!-- tab-Item-1-->




                </div>
                <!-- End tab-content -->
            </div>
            <!-- End Tabs section-->
        </div>
    </div>
</div>
</div>
<!-- End Info Content - User Area-->
</div>
<!-- End content-central-->
