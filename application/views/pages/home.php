<?php
//echo '<br>';
//echo '<pre>';
//print_r($this->user);
//echo '<br>';
?>
<!-- Info Content-->
<div class="content_info">
    <div class="padding-top">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">

                <div class="col-md-9 blog-post-section blog-home">
                    <!--Item Blog Post-->
                    <div class="item-blog-post">
                        <!--Head Blog Post-->
                        <div class="head-item-blog-post">
                            <h3>বাগাদী ইউনিয়নের মমিনপুর গ্রামে এসডি এফ মাধ্যমে ছয় জন গরীব মহিলা কে নগদ টাকা প্রদান</h3>
                        </div>
                        <!--End Head Blog Post-->  
                        <!--Img Blog Post-->
                        <div class="img-item-blog-post home-post">
                            <img src="<?php echo base_url(); ?>assets/front/img/blog-img/1.jpg" alt="">
                            <div class="post-meta">
                                <ul>
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <a href="#">Iwthemes</a>
                                    </li>

                                    <li>
                                        <i class="fa fa-clock-o"></i>
                                        <span>April 23, 2015</span>
                                    </li>

                                    <li>
                                        <i class="fa fa-eye"></i>
                                        <span>234 Views</span>
                                    </li>
                                </ul>                      
                            </div>
                        </div>
                        <!--End Img Blog Post-->  

                        <!--Ingo Blog Post-->
                        <div class="info-item-blog-post">
                            <p >
                                <a href="<?php echo base_url(); ?>home/single"><i class="fa fa-plus-circle"></i> View more</a></p>
                        </div>
                        <!--End Ingo Blog Post-->  
                    </div>
                    <!--End Item Blog Post-->

                    <!--Item Blog Post-->
                    <div class="item-blog-post">
                        <!--Head Blog Post-->
                        <div class="head-item-blog-post">

                            <h3>শতভাগ মিড ডে মিল বাস্তবায়নের কর্মসূচি</h3>
                        </div>
                        <!--End Head Blog Post-->  

                        <!--Img Blog Post-->
                        <div class="img-item-blog-post home-post">
                            <img src="<?php echo base_url(); ?>assets/front/img/blog-img/2.jpg" alt="">
                            <div class="post-meta">
                                <ul>
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <a href="#">Iwthemes</a>
                                    </li>

                                    <li>
                                        <i class="fa fa-clock-o"></i>
                                        <span>April 23, 2015</span>
                                    </li>

                                    <li>
                                        <i class="fa fa-eye"></i>
                                        <span>234 Views</span>
                                    </li>
                                </ul>                      
                            </div>
                        </div>
                        <!--End Img Blog Post-->  

                        <!--Ingo Blog Post-->
                        <div class="info-item-blog-post">
                            <p>বাগাদী ইউনিয়নের চেয়ারম্যান বেলায়েত হোসেন গাজী বিল্লালের ব্যক্তিগত উদ্যেগে শতভাগ মিড ডে মিল

                                বাস্তবায়নের কর্মসূচি হিসেবে এগারটি প্রাথমিক বিদ্যালয়ে টিফিন বক্সে বিতরণ অনুষ্ঠানে প্রধান অতিথি হিসেবে

                                উপস্থিত ছিলেন চট্রগ্রাম বিভাগের স্থানীয় সরকার বিভাগের পরিচালক অতিরিক্ত সচিব দীপক চক্রবর্তী, বিশেষ

                                অতিথি জেলা প্রশাসক আব্দুস সবুর মন্ডল, উপজেলা নির্বাহী কর্মকর্তা উদয়ন দেওয়ান, জেলা প্রাথমিক শিক্ষা

                                অফিসার সহ অন্যান্য অতিথিবৃন্দ।

                                <a href="<?php echo base_url(); ?>home/single"><i class="fa fa-plus-circle"></i> View more</a></p>
                        </div>
                        <!--End Ingo Blog Post-->  
                    </div>
                    <!--End Item Blog Post-->  

                    <!--Item Blog Post-->
                    <div class="item-blog-post">
                        <!--Head Blog Post-->
                        <div class="head-item-blog-post">

                            <h3>বাগাদী ইউনিয়নের চৌরাস্তা বাজারে শীতার্ত মানুষের মাঝে কম্বল বিতরণ</h3>
                        </div>
                        <!--End Head Blog Post-->  

                        <!--Img Blog Post-->
                        <div class="img-item-blog-post home-post">
                            <img src="<?php echo base_url(); ?>assets/front/img/blog-img/3.jpg" alt="">
                            <div class="post-meta">
                                <ul>
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <a href="#">Iwthemes</a>
                                    </li>

                                    <li>
                                        <i class="fa fa-clock-o"></i>
                                        <span>April 23, 2015</span>
                                    </li>

                                    <li>
                                        <i class="fa fa-eye"></i>
                                        <span>234 Views</span>
                                    </li>
                                </ul>                      
                            </div>
                        </div>
                        <!--End Img Blog Post-->  

                        <!--Ingo Blog Post-->
                        <div class="info-item-blog-post">
                            <p>মাননীয়া প্রধানমন্ত্রী শেখ হাসিনার পক্ষে চাঁদপুর সদর উপজেলার কৃতি সন্তান মাটি ও মানুষের নেতা বাংলাদেশ

                                আওয়ামী লীগের ত্রাণ ও সমাজকল্যাণ সম্পাদক বাবু সুজিত রায় নন্দীর উদ্যোগে চাঁদপুর সদর উপজেলার বাগাদী

                                ইউনিয়নের চৌরাস্তা বাজারে শীতার্ত মানুষের মাঝে কম্বল বিতরণ শীতার্ত মানুষের মাঝে কম্বল বিতরণ 

                                <a href="<?php echo base_url(); ?>home/single"><i class="fa fa-plus-circle"></i> View more</a></p>
                        </div>
                        <!--End Ingo Blog Post-->  
                    </div>
                    <!--End Item Blog Post-->
                    <div class="row">
                        <div class="col-md-6">
                            <!-- Widget List -->
                            <div class="item-blog-post">
                                <div class="head-item-blog-post">
                                    <h3 style="padding: 0px; color:#fff;">সরকারী সেবাসমূহ</h3>
                                </div>
                                <ul class="list-styles">
                                    <li><i class="fa fa-check"></i> <a href="#">ত্রাণ ও পূনর্বাসন</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">ভিজিএফ</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">ভিজিডি</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">মাতৃত্বকালীন ভাতা</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">মুক্তিযোদ্ধা ভাতা</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">প্রতিবন্ধী ভাতা</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">বিধবা ভাতা</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">বয়স্ক ভাতা</a></li>
                                </ul>
                            </div>
                            <!-- End Widget List -->
                        </div>
                        <div class="col-md-6">
                            <!-- Widget List -->
                            <div class="item-blog-post">
                                <div class="head-item-blog-post">
                                    <h3 style="padding: 0px; color:#fff;">সরকারী সেবাসমূহ</h3>
                                </div>
                                <ul class="list-styles">
                                    <li><i class="fa fa-check"></i> <a href="#">ত্রাণ ও পূনর্বাসন</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">ভিজিএফ</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">ভিজিডি</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">মাতৃত্বকালীন ভাতা</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">মুক্তিযোদ্ধা ভাতা</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">প্রতিবন্ধী ভাতা</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">বিধবা ভাতা</a></li>
                                    <li><i class="fa fa-check"></i> <a href="#">বয়স্ক ভাতা</a></li>
                                </ul>
                            </div>
                            <!-- End Widget List -->
                        </div>
                    </div>
                </div>
                <!-- Sidebars-->
                <div class="col-md-3">
                    
                    <!-- Widget List -->
                    <div class="item-blog-post">
                        <div class="head-item-blog-post">
                            <h3 style="padding: 0px; color:#fff;">স্বাগত বক্তব্য </h3>
                        </div><br>
                        <!--person-->
                        <div style="text-align: center">
                            <img src="<?=base_url()?>assets/img/bag_up_chrmn.jpg" width="100px" height="100px" alt="" /> 
                            <div>
                                <h4 style="margin-bottom: -18px;">শফিক দেওয়ান</h4>
                                <span>উপজেলা চেয়ারম্যান </span>
                            </div>
                        </div><hr />
                        <!--person-->
                        <div style="text-align: center">
                            <img src="<?=base_url()?>assets/img/chnd_sadar_uno.jpg" width="100px" height="100px" alt="" /> 
                            <div>
                                <h4 style="margin-bottom: -18px;">উদয়ন দেওয়ান</h4>
                                <span>ইউ.এন.ও </span>
                            </div>
                        </div><hr />
                        
                        <!--person-->
                        <div style="text-align: center">
                            <img src="<?=base_url()?>assets/img/bag_chrmn.jpg" width="100px" height="100px" alt="" /> 
                            <div>
                                <h4 style="margin-bottom: -18px;">মোঃ বেলায়েত হোসেন</h4>
                                <span>বাগাদী ইউনিয়ন</span>
                            </div>
                        </div>
                    </div><br>
                    <!-- End Widget List -->
                    
                    <!-- Widget List -->
                    <div class="item-blog-post">
                        <div class="head-item-blog-post">
                            <h3 style="padding: 0px; color:#fff;">সরকারী সেবাসমূহ</h3>
                        </div>
                        <ul class="list-styles">
                            <li><i class="fa fa-check"></i> <a href="#">ত্রাণ ও পূনর্বাসন</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">ভিজিএফ</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">ভিজিডি</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">মাতৃত্বকালীন ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">মুক্তিযোদ্ধা ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">প্রতিবন্ধী ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">বিধবা ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">বয়স্ক ভাতা</a></li>
                        </ul>
                    </div><br>
                    
                    <!-- Widget Text -->
                    <aside class="widget">
                        <h4>সুখবর...</h4>
                        <p>কৃষক ভাইদের জন্য সুখবরকৃষকরা সরাসরি সরকারি গুদামে প্রতি কেজি ধান ২৩ টাকা দরে বিক্রয় করে ন্যায্য মূল্য প্রাপ্তির সুযোগ গ্রহন করুন। আগ্রহী কৃষকগণকে মৈশাদী্ ইউনিয়ন কৃষি কর্মকর্তা গণের সাথে জরুরী ভিত্তিতে যোগাযোগ করুন।মোঃ আব্দুর রশিদ ।</p>
                    </aside>
                </div>
                <!-- End  Sidebars-->


                <!-- Content Tabs Section-->

                <!-- End Col boxes-services -->

                <!-- Content Tabs Section-->

                <!-- End Tabs section-->
            </div>
        </div>
        <!-- End Container Area - Boxes Services -->
    </div>
</div>
<!-- End Info Content-->

<!-- End Info Content Blog Post-->
<div class="content_info">
    <!-- Info Resalt-->
    <div class="content_resalt padding-bottom borders">
        <!-- title-vertical-line-->
        <div class="title-vertical-line">
            <h2><span>আসুন এদের</span>  পাশে দাড়াই</h2>
            <p class="lead">আমাদের একটু সহযোগিতায় পাল্টে দিতে পারে ওদের জীবন </p>
        </div><br>
        <!-- End title-vertical-line-->

        <!-- Container Blog Post -->
        <div id="main">

            <section class="causes-style-2 margin-none">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="outer">
                                <div class="box">
                                    <div class="frame"><a href="#"><img src="<?= base_url() ?>assets/front/img/d1.jpg" alt="img"></a></div>
                                    <div class="text-box">
                                        <h4><a href="#">আসুন একজন হত দরিদ্রের পাশে

                                                দাড়াই</a></h4>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="outer">
                                <div class="box">
                                    <div class="frame"><a href="#"><img src="<?= base_url() ?>assets/front/img/d2.jpg" alt="img"></a></div>
                                    <div class="text-box">
                                        <h4><a href="#">আসুন একজন প্রতিবন্ধির পাশে

                                                দাড়াই</a></h4>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="outer">
                                <div class="box">
                                    <div class="frame"><a href="#"><img src="<?= base_url() ?>assets/front/img/d3.jpg" alt="img"></a></div>
                                    <div class="text-box">
                                        <h4><a href="#">আসুন  উন্নয়নমূলক

                                                কর্মকাণ্ডের পাশে দাড়াই।</a></h4>

                                    </div>
                                </div>
                            </div>

                        </div>



                    </div>

                </div>
            </section>

        </div>
        <!-- End Container Blog Post -->
    </div>
    <!-- End Info Resalt-->
</div>
<!-- End Info Content Blog Post-->

<!-- Info Content Process-->
<div class="content_info">
    <!-- title-vertical-line-->
    <div class="title-vertical-line">
        <h2><span>ডাটাবেজ তৈরির </span> ধাপ সমূহ</h2>
        <p class="lead">সকলের অংশগ্রহন ও সহযোগিতায় সুবিধা বঞ্চিতদের একটি নিখুত ও স্বচ্ছ তালিকা তৈরির প্রচেষ্টা</p>
    </div>
    <!-- End title-vertical-line-->

    <!-- Info Resalt-->
    <div class="paddings">
        <!-- Container Area - services-process -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- services-process -->
                    <div class="services-process">
                        <!-- item-service-process -->
                        <div class="item-service-process color-bg-1">
                            <div class="head-service-process">
                                <i class="fa fa-cubes"></i>
                                <h3>Apply</h3>
                            </div>
                            <div class="divisor-service-process">
                                <span class="circle-top">1</span>
                                <span class="circle"></span>
                            </div>
                            <div class="info-service-process">
                                <h3>Apply by Beneficiary people</h3>
                                <p>Your peace of mind is priceless, we offer a broad portfolio of solutions to ensure what you love most.</p>
                            </div>
                        </div>
                        <!-- End item-service-process -->

                        <!-- item-service-process -->
                        <div class="item-service-process color-bg-2">
                            <div class="head-service-process">
                                <i class="fa fa-diamond"></i>
                                <h3>Review</h3>
                            </div>
                            <div class="divisor-service-process">
                                <span class="circle-top">2</span>
                                <span class="circle"></span>
                            </div>
                            <div class="info-service-process">
                                <h3>Review & sugetion by Assigned person</h3>
                                <p>We have created alliances with recognized entities that contribute to improving quality of your life.</p>
                            </div>
                        </div>
                        <!-- End item-service-process -->

                        <!-- item-service-process -->
                        <div class="item-service-process color-bg-3">
                            <div class="head-service-process">
                                <i class="fa fa-bicycle"></i>
                                <h3>Approval</h3>
                            </div>
                            <div class="divisor-service-process">
                                <span class="circle-top">3</span>
                                <span class="circle"></span>
                            </div>
                            <div class="info-service-process">
                                <h3>Approved by Depatmental Officer</h3>
                                <p>Our programs with social sense, everyday comprehensively improve the quality of life of our members.</p>
                            </div>
                        </div>
                        <!-- End item-service-process -->

                        <!-- item-service-process -->
                        <div class="item-service-process color-bg-4">
                            <div class="head-service-process">
                                <i class="fa fa-hotel"></i>
                                <h3>Finalize</h3>
                            </div>
                            <div class="divisor-service-process">
                                <span class="circle-top">4</span>
                                <span class="circle"></span>
                            </div>
                            <div class="info-service-process">
                                <h3>Finalize By UNO, Chandpur Sadar</h3>
                                <p>We offer the best alternatives for recreation, relaxation and adventure to share with family and friends.</p>
                            </div>
                        </div>
                        <!-- End item-service-process -->
                    </div>
                    <!-- End services-process-->
                </div>
            </div>
        </div>
        <!-- End Container Area - services-process -->
    </div>
    <!-- End Info Resalt-->
</div>
<!-- End Info Content Process-->
</div>
<!-- End content-central-->