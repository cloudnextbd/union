
<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- Info Section title-->
    <div class="section-title-01 section-title-01-small">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h3>Blog Single Post.</h3>
                    <h5>COOPBANK LAST NEWS</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- End Info Section Title-->

    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>
                /
            </li>
            <li>
                Blog Single Post
            </li>
        </ul>
    </div>
    <!-- End breadcrumbs-->
</div>
<!-- End Info Content - Section Title-->

<!-- Info Content - News Items-->
<div class="content_info">
    <div class="paddings">
        <!-- events Container-->  
        <div class="container">
            <div class="row">
                <div class="col-md-9 blog-post-section">
                    <!--Item Blog Post-->
                    <div class="item-blog-post">
                        <!--Head Blog Post-->
                        <div class="head-item-blog-post">
                            <i class="fa fa-calculator"></i>
                            <h3>Protection With you</h3>
                        </div>
                        <!--End Head Blog Post-->  

                        <!--Img Blog Post-->
                        <div class="img-item-blog-post">
                            <img src="<?php echo base_url(); ?>assets/front/img/blog-img/1.jpg" alt="">
                            <div class="post-meta">
                                <ul>
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <a href="#">Iwthemes</a>
                                    </li>

                                    <li>
                                        <i class="fa fa-clock-o"></i>
                                        <span>April 23, 2015</span>
                                    </li>

                                    <li>
                                        <i class="fa fa-eye"></i>
                                        <span>234 Views</span>
                                    </li>
                                </ul>                      
                            </div>
                        </div>
                        <!--End Img Blog Post-->  

                        <!--Ingo Blog Post-->
                        <div class="info-item-blog-post">
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>

                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>

                            <h4>Recent Links</h4>

                            <ul class="list-styles">
                                <li><i class="fa fa-check"></i> <a href="#">Corporate Web CoopBank</a></li>
                                <li><i class="fa fa-check"></i> <a href="#">CoopBank Innovation Center</a></li>
                                <li><i class="fa fa-check"></i> <a href="#">Corporate Responsibility</a></li>
                                <li><i class="fa fa-check"></i> <a href="#">Information of interest</a></li>
                            </ul>

                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
                        </div>
                        <!--End Ingo Blog Post-->  
                    </div>
                    <!--End Item Blog Post-->

                    <h4><i class="fa fa-pencil"></i>New Comment</h4>

                    <form action="#" class="form-theme post-comment">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Your name *</label>
                                    <input type="text" required="required" value="" placeholder="Please Enter Your Name" class="form-control" name="name">
                                </div>
                                <div class="col-md-6">
                                    <label>Your email address *</label>
                                    <input type="email" required="required" value="" placeholder="Please Enter Your Email" class="form-control" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Comment *</label>
                                    <textarea placeholder="Please Enter Your Comment" class="form-control" name="comment" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Post Comment" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                    <div class="user_comments">
                        <ul>
                            <li>
                                <i class="fa fa-user"></i>
                                <div class="comments">
                                    <i class="fa fal fa-caret-left"></i>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-user"></i>
                                <div class="comments">
                                    <i class="fa fal fa-caret-left"></i>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-user"></i>
                                <div class="comments">
                                    <i class="fa fal fa-caret-left"></i>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3">
                    <!-- Widget List -->
                    <div class="item-blog-post">
                        <div class="head-item-blog-post">
                            <h3 style="padding: 0px; color:#fff;">সরকারী সেবাসমূহ</h3>
                        </div>
                        <ul class="list-styles">
                            <li><i class="fa fa-check"></i> <a href="#">ত্রাণ ও পূনর্বাসন</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">ভিজিএফ</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">ভিজিডি</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">মাতৃত্বকালীন ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">মুক্তিযোদ্ধা ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">প্রতিবন্ধী ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">বিধবা ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">বয়স্ক ভাতা</a></li>
                        </ul>
                    </div>
                    <!-- End Widget List -->
                    <br>
                    <!-- Widget Text -->
                    <aside class="widget">
                        <h4>সুখবর...</h4>
                        <p>কৃষক ভাইদের জন্য সুখবরকৃষকরা সরাসরি সরকারি গুদামে প্রতি কেজি ধান ২৩ টাকা দরে বিক্রয় করে ন্যায্য মূল্য প্রাপ্তির সুযোগ গ্রহন করুন। আগ্রহী কৃষকগণকে মৈশাদী্ ইউনিয়ন কৃষি কর্মকর্তা গণের সাথে জরুরী ভিত্তিতে যোগাযোগ করুন।মোঃ আব্দুর রশিদ ।</p>
                    </aside>
                    <!-- End Widget Text -->
                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id))
                                return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-page" data-href="https://www.facebook.com/cloudnextbd" data-tabs="timeline" data-width="270" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/cloudnextbd" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/cloudnextbd">Cloud Next Generation LTD.</a></blockquote></div>
                </div>
                <!-- End  Sidebars-->
                
            </div>
        </div>
        <!-- End events Container--> 
    </div>
</div>
<!-- End Info Content - News Items-->