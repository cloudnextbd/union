<!-- Info Content - Section Title-->
                <div class="content_info">
                    <!-- Info Section title-->
                    <div class="section-title-01 section-title-01-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <h3>Gallery.</h3>
                                    <h5>Four COLUMNS</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Info Section Title-->

                    <!-- breadcrumbs-->
                    <div class="breadcrumbs breadcrumbs-sections">
                        <ul>
                            <li class="breadcrumbs-home">
                                <a href="#" title="Back To Home">
                                  <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">Company</a>
                            </li>
                            <li>
                                /
                            </li>
                            <li>
                                <a href="#">Gallery</a>
                            </li>
                            <li>
                                /
                            </li>
                            <li>
                                Four Columns
                            </li>
                        </ul>
                    </div>
                    <!-- End breadcrumbs-->
                </div>
                <!-- End Info Content - Section Title-->

                <!-- Info Content - Gallery Items-->
                <div class="content_info">
                    <div class="paddings">  
                        <div class="container">
                            
                            <div class="portfolioFilter">
                                <a href="#" data-filter="*" class="current">Show All</a>
                                <a href="#education" data-filter=".education">Education</a>
                                <a href="#events" data-filter=".events">Events</a>
                                <a href="#donations" data-filter=".donations">Donations</a>
                                <a href="#meetings" data-filter=".meetings">Meetings</a>
                            </div>

                            <div class="portfolioContainer">
                                <div class="col-md-3 education">
                                    <!-- Item Work -->
                                    <div class="item-gallery">
                                       <div class="head-gallery">
                                            <h4>Gallery March, 2015</h4>
                                       </div>
                                       <div class="hover">
                                            <img src="<?=base_url()?>assets/front/img/gallery/1.jpg" alt="Image"/>                               
                                             <a href="<?=base_url()?>assets/front/img/gallery/1.jpg" rel="gallery_group" class="fancybox" title="Image"><div class="overlay"></div></a>
                                        </div>                                   
                                        <div class="info-gallery">
                                            <p>The establishment of our agency, with two professional insurance.</p>
                                            <i class="fa fa-picture-o"></i>
                                        </div>  
                                    </div>  
                                    <!-- Item Work -->
                                </div>

                                <div class="col-md-3 events">
                                  <!-- Item Work -->
                                  <div class="item-gallery">
                                     <div class="head-gallery">
                                          <h4>Gallery June, 2015</h4>
                                     </div>
                                     <div class="hover">
                                          <img src="<?=base_url()?>assets/front/img/gallery/2.jpg" alt="Image"/>                               
                                           <a href="<?=base_url()?>assets/front/img/gallery/2.jpg" rel="gallery_group" class="fancybox" title="Image"><div class="overlay"></div></a>
                                      </div>                                   
                                      <div class="info-gallery">
                                          <p>Three new insurance agents in our team, 2.500 customers!</p>
                                          <i class="fa fa-picture-o"></i>
                                      </div>  
                                  </div>  
                                  <!-- Item Work -->
                                </div>

                                <div class="col-md-3 education">
                                  <!-- Item Work -->
                                  <div class="item-gallery">
                                     <div class="head-gallery">
                                          <h4>Gallery February, 2015</h4>
                                     </div>
                                     <div class="hover">
                                          <img src="<?=base_url()?>assets/front/img/gallery/3.jpg" alt="Image"/>                               
                                           <a href="<?=base_url()?>assets/front/img/gallery/3.jpg" rel="gallery_group" class="fancybox" title="Image"><div class="overlay"></div></a>
                                      </div>                                   
                                      <div class="info-gallery">
                                          <p>Moving to a new office in Boston. More than 5.000 customers.</p>
                                          <i class="fa fa-picture-o"></i>
                                      </div>  
                                  </div>  
                                  <!-- Item Work -->
                                </div>

                                <div class="col-md-3 meetings">
                                  <!-- Item Work -->
                                  <div class="item-gallery">
                                     <div class="head-gallery">
                                          <h4>Gallery March, 2015</h4>
                                     </div>
                                     <div class="hover">
                                          <img src="<?=base_url()?>assets/front/img/gallery/4.jpg" alt="Image"/>                               
                                           <a href="<?=base_url()?>assets/front/img/gallery/4.jpg" rel="gallery_group" class="fancybox" title="Image"><div class="overlay"></div></a>
                                      </div>                                   
                                      <div class="info-gallery">
                                          <p>The establishment of our agency, with two professional insurance.</p>
                                          <i class="fa fa-picture-o"></i>
                                      </div>  
                                  </div>  
                                  <!-- Item Work -->
                                </div>

                                <div class="col-md-3 donations">
                                  <!-- Item Work -->
                                  <div class="item-gallery">
                                     <div class="head-gallery">
                                          <h4>Gallery March, 2015</h4>
                                     </div>
                                     <div class="hover">
                                          <img src="<?=base_url()?>assets/front/img/gallery/5.jpg" alt="Image"/>                               
                                           <a href="<?=base_url()?>assets/front/img/gallery/5.jpg" rel="gallery_group" class="fancybox" title="Image"><div class="overlay"></div></a>
                                      </div>                                   
                                      <div class="info-gallery">
                                          <p>The establishment of our agency, with two insurance agents.</p>
                                          <i class="fa fa-picture-o"></i>
                                      </div>  
                                  </div>  
                                  <!-- Item Work -->
                                </div>

                                <div class="col-md-3 meetings">
                                  <!-- Item Work -->
                                  <div class="item-gallery">
                                     <div class="head-gallery">
                                          <h4>Gallery March, 2015</h4>
                                     </div>
                                     <div class="hover">
                                          <img src="<?=base_url()?>assets/front/img/gallery/6.jpg" alt="Image"/>                               
                                           <a href="<?=base_url()?>assets/front/img/gallery/6.jpg" rel="gallery_group" class="fancybox" title="Image"><div class="overlay"></div></a>
                                      </div>                                   
                                      <div class="info-gallery">
                                          <p>The establishment of our agency, with two insurance agents.</p>
                                          <i class="fa fa-picture-o"></i>
                                      </div>  
                                  </div>  
                                  <!-- Item Work -->
                                </div>

                                <div class="col-md-3 education">
                                  <!-- Item Work -->
                                  <div class="item-gallery">
                                     <div class="head-gallery">
                                          <h4>Gallery February, 2015</h4>
                                     </div>
                                     <div class="hover">
                                          <img src="<?=base_url()?>assets/front/img/gallery/7.jpg" alt="Image"/>                               
                                           <a href="<?=base_url()?>assets/front/img/gallery/7.jpg" rel="gallery_group" class="fancybox" title="Image"><div class="overlay"></div></a>
                                      </div>                                   
                                      <div class="info-gallery">
                                          <p>Moving to a new office in Boston. More than 5.000 customers.</p>
                                          <i class="fa fa-picture-o"></i>
                                      </div>  
                                  </div>  
                                  <!-- Item Work -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Info Content - Gallery Items-->
            </div>
            <!-- End content-central-->
  