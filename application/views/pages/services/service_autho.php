<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- Info Section title-->
    <div class="section-title-01 section-title-01-small">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3><?= $info[0]->ser_auth_name ?></h3>
                    <h5>বাস্তবায়নকারী মন্ত্রণালয়/দপ্তর: 
                        সমাজকল্যাণ মন্ত্রণালয়, সমাজসেবা অধিদফতর</h5>
                    <h5>কার্যক্রম শুরুর বছর: 
                        ১৯৯৭-৯৮ অর্থবছর</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- End Info Section Title-->
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="3" title="Back To Home"><i class="fa fa-home"></i></a>
            </li>
            <li>
                <a href="#">সেবাসমূহ</a>
            </li>
            <li>
                /
            </li>
            <li>
                <?= $info[0]->ser_auth_name ?>
            </li>
        </ul>
    </div>
    <!-- End breadcrumbs-->
</div>
<!-- End Info Content - Section Title-->
<!-- Info Content -->
<div class="content_info">
    <div class="paddings">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h3> পটভূমি:</h3>
                    <p class="lead color-skin"><?= $info[0]->about ?></p>

                    <h3>ঠিকানা:</h3>
                    <p class="lead color-skin"><?= $info[0]->address ?></p>
                </div> 
                <!-- Right sidebar -->
                <div class="col-md-3">
                    <!-- Widget List -->
                    <div class="item-blog-post">
                        <div class="head-item-blog-post">
                            <h3 style="padding: 0px; color:#fff;">সরকারী সেবাসমূহ</h3>
                        </div>
                        <ul class="list-styles">
                            <li><i class="fa fa-check"></i> <a href="#">ত্রাণ ও পূনর্বাসন</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">ভিজিএফ</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">ভিজিডি</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">মাতৃত্বকালীন ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">মুক্তিযোদ্ধা ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">প্রতিবন্ধী ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">বিধবা ভাতা</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">বয়স্ক ভাতা</a></li>
                        </ul>
                    </div>
                    <!-- End Widget List -->
                    <br>
                    <!-- Widget Text -->
                    <aside class="widget">
                        <h4>সুখবর...</h4>
                        <p>কৃষক ভাইদের জন্য সুখবরকৃষকরা সরাসরি সরকারি গুদামে প্রতি কেজি ধান ২৩ টাকা দরে বিক্রয় করে ন্যায্য মূল্য প্রাপ্তির সুযোগ গ্রহন করুন। আগ্রহী কৃষকগণকে মৈশাদী্ ইউনিয়ন কৃষি কর্মকর্তা গণের সাথে জরুরী ভিত্তিতে যোগাযোগ করুন।মোঃ আব্দুর রশিদ ।</p>
                    </aside>
                    <!-- End Widget Text -->
                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id))
                                return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <div class="fb-page" data-href="https://www.facebook.com/cloudnextbd" data-tabs="timeline" data-width="270" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/cloudnextbd" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/cloudnextbd">Cloud Next Generation LTD.</a></blockquote></div>
                </div>
                <!-- End  Sidebars -->
            </div>
        </div>
        <!-- End Container Area - Boxes Services -->
    </div>
</div>
<!-- End Info Content-->
<!-- Info Content - Resalt Info-->
<div class="content_info">
    <!-- Info Resalt-->
    <div class="skin_base color-white paddings">
        <div class="container wow fadeInUp">
            <div class="row">
                <div class="col-md-12 title-resalt">
                    <h2>Please, Help us</h2>
                    <p class="lead">See how Client has transformed clinical trials quickly and securely using our platform</p>
                </div>               
            </div>
        </div>
    </div>
    <!-- End Info Resalt-->
</div>
<!-- End Info Content - Resalt Info-->    