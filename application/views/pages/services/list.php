<style type="text/css">
    thead tr th {
        color: #999 !important;
    }
</style>
<!-- Info Content-->
<div class="content_info txt-color">
    <div class="padding-top">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <table id="myTable" class="display nowrap table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Sl. </th>
                                <th>Image </th>
                                <th>Mem. ID </th>
                                
                                <th>Name </th>
                                <th>Address </th>
                                <th>NID </th>
                                
                                <th>Serv. type</th>
                                <th>App. Status </th>
                                <th>Serv. Status </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Sl. </th>
                                <th>Image </th>
                                <th>Mem. ID </th>
                                
                                <th>Name </th>
                                <th>Address </th>
                                <th>NID </th>
                                
                                <th>Serv. type</th>
                                <th>App. Status</th>
                                <th>Serv. Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach($lists as $list) : ?>
                                <tr>
                                    <td>nai</td>
                                    <td><img src="<?=base_url()?>assets/app_img/<?=$list->image?>"/></td>
                                    <td><?=$list->id?></td>
                                    
                                    <td><?=$list->name?></td>
                                    <td><?=$list->house_name?></td>
                                    <td><?=$list->nid?></td>
                                    
                                    <td>nai</td>
                                    <td>
                                        <?php if($list->app_status == 0) {
                                            echo "<button type='button' class='btn btn-danger btn-xs btn-block'>Refused</button>";
                                        } elseif($list->app_status == 1) {
                                            echo "<button type='button' class='btn btn-warning btn-xs btn-block'>Pending</button>";
                                        } elseif($list->app_status == 2) {
                                            echo "<button type='button' class='btn btn-success btn-xs btn-block'>Approved</button>";
                                        } ?>
                                    </td>
                                    <td>
                                        <?php if($list->status == 0) {
                                            echo "<button type='button' class='btn btn-danger btn-xs btn-block'>Inactive</button>";
                                        } elseif($list->status == 1) {
                                            echo "<button type='button' class='btn btn-success btn-xs btn-block'>Active</button>";
                                        } ?>
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="...">
                                            <a href="<?=base_url()?>app_list/details/<?=$list->id?>">
                                                <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                            </a>
                                            <?php if ($list->app_status == 1) {
                                                echo "<a href=''>
                                                        <button type='button' class='btn btn-xs btn-warning'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button>
                                                    </a>";
                                            } ?>
                                            <?php if ($this->user->info->role == 2) {
                                                echo "<a href=''>
                                                        <button type='button' class='btn btn-xs btn-danger'><i class='fa fa-trash' aria-hidden='true'></i></button>
                                                    </a>";
                                            } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>