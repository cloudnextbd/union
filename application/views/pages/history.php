<style type="text/css">
  .rows{
    display: block;
    clear: both;
    min-height: 200px;
    margin-top: 60px;
    margin-bottom: 0px;
  }
  .team-box {
	  /*border-radius: 15px;*/
  }
  .team-box ul {
    background-color: #4aa5d3 !important;
}
  .team-box .rounds {
    display: block;
    width: 130px;
    height: 130px;
    border-radius: 100%;
    border: 3px solid #40a1d2;
    margin: -70px auto 15px;
    overflow: hidden;
  }
.product-sec h4 {
    text-align: center;
    color: #0088cc;
    font-size: 15px;
}
.product-item {
    width: 160px;
    background: #e5e5e5;
    float: left;
    margin: 5px;
    padding: 2px;
    border-radius: 4px;
}
.product-item:hover{
    box-shadow: 2px 2px 3px #ccc;
}
.product-item img{
  width: 100%;
}
.clients img{
    width: 164px;
    float: left;
    margin: 5px;
}

</style>
        <div id="main">
          <section class="generic-heading-3">
            <section class="about-page">
              <section class="about-section-1">
                <div class="container">
                  <div class="row-fluid">
                    <div class="span3">
                      <h2 class="title-opt">Concern Overview</h2>
                      <div class="about-box-1">
                        <nav class="nav-sidebar">
                          <ul class="nav tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab">About Company</a></li>
                            <li class=""><a href="#tab2" data-toggle="tab">Core values</a></li>
                            <li class=""><a href="#tab3" data-toggle="tab">Organogram</a></li>
                            <li class=""><a href="#tab4" data-toggle="tab">Product</a></li>   
                            <li class=""><a href="#tab5" data-toggle="tab">Client</a></li>                            
                          </ul>
                        </nav>
                      </div>
                    </div>
                    <div class="span6">
                      <div class="">
                        <!-- tab content -->
                        <div class="tab-content">
                          <div class="tab-pane active text-style" id="tab1">
                            <h2 class="title-opt">About Company</h2>
                            <div class="about-box-1">
                            <p>To manufacture products comparable to international standards, to be customer-focused and globally competitive through better quality, latest technology and continuous innovation.<br><br>
                            To be recognized by our customers as the best strategic partner in our product that fully participate a meaningful role on sustainable development in national economy.</p>
                           <hr>
                           <iframe width="100%" height="350" src="https://www.youtube.com/embed/eeik7MuQ930" frameborder="0" allowfullscreen></iframe>
 
                         </div>
                         </div>
                         <div class="tab-pane text-style" id="tab2">
                          <h2 class="title-opt">Core Values:</h2>
                          <div class="about-box-1">
                          <img class="img img-responsive" src="<?php echo base_url(); ?>assets/front/images/cv.jpg">
                          <br>
                          <br>
                          <p style="text-align: justify">Our Companies’ Core Values of Leadership, Integrity, Respect, Innovation and Continuous Improvement are at the heart of everything we do. These Core Values are a source of inspiration as we lead the way to a brighter future for our company and all who depend on it.  They support Masafi Groups' purpose: Making lives better one call at a time, by connecting people to solutions.
                          <br><strong>Leadership   </strong><br>
                          <span style="color: blue">We develop leaders... </span> <br>

                          We foster trust and collaboration to develop leaders focused on sustainable, superior performance.  We set positive examples and invest in others so that they can follow the guidelines of leadership.
                          <br>
                          <br><strong>Integrity</strong><br>
                          <span style="color: blue">We do the right thing… </span> <br>
                          We conduct our business in accordance with the highest standards of professional behavior and ethics.  We are transparent, honest and ethical in all our interactions with employees, clients, consumers, vendors and the public.
                          <br>
                          <br>

                          <strong>Respect</strong><br>
                          <span style="color: blue">We treat others as we expect to be treated… </span> <br>
                          We embrace each individual’s unique talents and honor diverse life and work styles.  We operate in a spirit of cooperation and value human dignity.
                          <br>
                          <br>


                          <strong>Innovation</strong><br>
                          <span style="color: blue">We anticipate change and shape it to fit our purposes… </span> <br>
                          We acknowledge the weaknesses within our industry and create ethical, forward thinking solutions to overcome them.  We identify, develop and deploy leading edge technology, employee development programs and process improvement tools.
                          <br>
                          <br>


                          <strong>Continuous Improvement</strong><br>
                          <span style="color: blue">We are a learning organization…</span> <br>
                          We measure, monitor, analyze and improve productivity, processes, tasks and ourselves to satisfy clients and stakeholders.  We work with enthusiasm and intellect, and are driven to surpass what has already been achieved.  We are not afraid to stand alone, especially when it is the right thing to do.
                          <br>
                          <br>
                          </p>
                           
                          </div>
                          </div>
                          <div class="tab-pane text-style" id="tab3">
                          <h2 class="title-opt">Organogram</h2>
                          <div class="about-box-1 product-sec">
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Name & Designation</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Name & Designation</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Name & Designation</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Name & Designation</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Name & Designation</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Name & Designation</h4>
                                </div>
                              </div>
                          </div>



                            <div class="tab-pane text-style" id="tab4">
                              <h2 class="title-opt">Our Product</h2>
                              <div class="about-box-1 product-sec">
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Product Name</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Product Name</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Product Name</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Product Name</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Product Name</h4>
                                </div>
                                <div class="product-item">
                                  <img src="http://placehold.it/150x150" class="img-rounded">
                                  <h4>Product Name</h4>
                                </div>
                              </div>
                            </div>

                            <div class="tab-pane text-style" id="tab5">
                              <h2 class="title-opt">Clients</h2>
                              <div class="about-box-1 clients">
                                 <img src="http://placehold.it/150x150" class="img-rounded">
                                 <img src="http://placehold.it/150x150" class="img-rounded">
                                 <img src="http://placehold.it/150x150" class="img-rounded">
                                 <img src="http://placehold.it/150x150" class="img-rounded">
                              </div>
                            </div>
                            </div>
                          </div>
                          </div>
                          <div class="span3">
                            <h2 class="title-opt">Contact Details</h2>
                            <div class="about-box-1">
                            <img class="img img-responsive" src="<?php echo base_url(); ?>assets/front/images/sis/sirin.png">
                              <p><strong>Address:</strong> Gulshan, Dhaka-1212</p>
                              <p><strong>City:</strong> Dhaka</p>
                              <p><strong>Country:</strong> Bangladesh</p>
                              <p><strong>Phone:</strong> 01717231363</p>
                              <p><strong>Email:</strong> masafi@masafigroup.com</p>
                              <p><strong>Web:</strong> masafigroup.com</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>

                </section>

              </div>
