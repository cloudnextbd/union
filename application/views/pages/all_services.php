<!-- Info Content - Section Title-->
                <div class="content_info">
                    <!-- Info Section title-->
                    <div class="section-title-01 section-title-01-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>বয়স্ক ভাতা</h3>
                                    <h5>বাস্তবায়নকারী মন্ত্রণালয়/দপ্তর: 
সমাজকল্যাণ মন্ত্রণালয়, সমাজসেবা অধিদফতর</h5>
<h5>কার্যক্রম শুরুর বছর: 
১৯৯৭-৯৮ অর্থবছর</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Info Section Title-->

                    <!-- breadcrumbs-->
                    <div class="breadcrumbs breadcrumbs-sections">
                        <ul>
                            <li class="breadcrumbs-home">
                                <a href="#" title="Back To Home">
                                  <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">সেবাসমূহ</a>
                            </li>
                            <li>
                                /
                            </li>
                            <li>
                                বয়স্ক ভাতা
                            </li>
                        </ul>
                    </div>
                    <!-- End breadcrumbs-->
                </div>
                <!-- End Info Content - Section Title-->

                <!-- Info Content -->
                <div class="content_info">
                    <div class="paddings">
                        <!-- Container Area - Boxes Services -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9">
								
								 <h3> পটভূমি:</h3>

                                    <p class="lead color-skin">পৃথিবীর অন্যান্য উন্নয়নশীল রাষ্ট্রের ন্যায় বাংলাদেশ সরকারও সামাজিক নিরাপত্তা বেষ্টনী সুদৃঢ়করণের
লক্ষ্যে দেশের দুঃস্থ, অবহেলিত, সুবিধাবঞ্চিত এবং অনগ্রসর মানুষের কল্যাণ ও উন্নয়নের ক্ষেত্রে
সমাজকল্যাণ মন্ত্রণালয়ের মাধ্যমে ব্যাপক ও বহুমুখী কর্মসূচি বাস্তবায়ন করছে। সামাজিক নিরাপত্তা
বিধানে সরকার তার সাংবিধানিক দায়িত্বের অংশ হিসেবে সমাজকল্যাণ মন্ত্রণালয়ের অধীন সমাজসেবা
অধিদফতরের মাধ্যমে বয়স্কভাতা কর্মসূচি বাস্তবায়ন করছে। 
</p>
								
								<h3>সংজ্ঞা:</h3>

                                    <p class="lead color-skin">বয়স্কে ব্যক্তি : বয়স্কভাতা কর্মসূচির আওতায় বয়স্ক ব্যক্তি বলতে পুরুষের ক্ষেত্রে ৬৫ বছর বা তদুর্ধ
বয়সের এবং মহিলাদের ক্ষেত্রে ৬২ বা তদুর্ধ বয়সের কিংবা সরকার কর্তৃক সময় সময় নির্ধারিত বয়সের
ব্যক্তিকে বুঝাবে। </p>
								
								<h3>Savings account</h3>

                                    <p class="lead color-skin">Available well with your advisor the group that owns your account as this can change the characteristics and conditions thereof.</p>
								
								
                                     <h3>লক্ষ্য ও উদ্দেশ্য:</h3>

                                     
                                     <ul class="list-styles">
                                        <li><i class="fa fa-check"></i> <a href="#"> বয়স্ক জনগোষ্ঠীর আর্থ-সামাজিক উন্নয়ন ও সামাজিক নিরাপত্তা বিধান;</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> পরিবার ও সমাজে তাঁদের মর্যাদা বৃদ্ধি;</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">  আর্থিক অনুদানের মাধ্যমে তাঁদের মনোবল জোরদারকরণ;</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> চিকিৎসা ও পুষ্টি সরবরাহ বৃদ্ধিতে সহায়তা করা।.</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> Free notifications.</a></li>
                                    </ul>

<h3>প্রার্থী নির্বাচনের মানদন্ড:</h3>

                                    <ul class="list-styles">
                                        <li><i class="fa fa-check"></i> <a href="#">নাগরিকত্ব: প্রার্থীকে অবশ্যই বাংলাদেশের স্থায়ী নাগরিক হতে হবে।</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"></a>বয়স: সর্বোচ্চ বয়স্ক ব্যক্তিকে অগ্রাধিকার প্রদান করতে হবে।</li>
                                        <li><i class="fa fa-check"></i> <a href="#">স্বাস্থ্যগত অবস্থা: যিনি শারীরিকভাবে অক্ষম অর্থাৎ সম্পূর্ণরূপে কর্মক্ষমতাহীন তাঁকে সর্বোচ্চ অগ্রাধিকার দিতে হবে। </a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">আর্থিক অবস্থার ক্ষেত্রে: নিঃস্ব, উদ্বাস্ত্ত ও ভূমিহীনকে ক্রমানুসারে অগ্রাধিকার দিতে হবে।</a></li>
										<li><i class="fa fa-check"></i> <a href="#">সামাজিক অবস্থার ক্ষেত্রে: বিধবা, তালাকপ্রাপ্তা, বিপত্নীক, নিঃসন্তান, পরিবার থেকে বিচ্ছিন্ন ব্যক্তিদেরকে ক্রমানুসারে অগ্রাধিকার দিতে হবে।</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">ভূমির মালিকানা: ভূমিহীন ব্যক্তিকে অগ্রাধিকার দিতে হবে। এক্ষেত্রে বসতবাড়ী ব্যতীত কোনো ব্যক্তির জমির পরিমাণ ০.৫ একর বা তার কম হলে তিনি ভূমিহীন বলে গণ্য হবেন।</a></li>
                                    </ul>

                                    <h3>ভাতা প্রাপ্তির যোগ্যতা ও শর্তাবলী</h3>

                                    <ul class="list-styles">
                                        <li><i class="fa fa-check"></i> <a href="#">সংশ্লিষ্ট এলাকার স্থায়ী বাসিন্দা হতে হবে;</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">জন্ম নিবন্ধন/জাতীয় পরিচিতি নম্বর থাকতে হবে;</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">বয়স পুরুষের ক্ষেত্রে সর্বনিম্ন ৬৫ বছর এবং মহিলাদের ক্ষেত্রে সর্বনিম্ন ৬২ বছর  হতে হবে।
সরকার কর্তৃক সময় সময় নির্ধারিত বয়স বিবেচনায় নিতে হবে; </a></li>
                                        <li><i class="fa fa-check"></i> <a href="#"> প্রার্থীর বার্ষিক গড় আয় অনূর্ধ ১০,০০০ (দশ হাজার) টাকা হতে হবে </a></li>
										<li><i class="fa fa-check"></i> <a href="#">বাছাই কমিটি কর্তৃক নির্বাচিত হতে হবে।</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">জন্ম নিবন্ধন/জাতীয় পরিচিতি নম্বর থাকতে হবে; বিঃ দ্রঃ বয়স নির্ধারণের ক্ষেত্রে জাতীয় পরিচয়পত্র, জন্ম নিবন্ধন সনদ, এসএসসি/সমমান পরীক্ষার সনদপত্র বিবেচনা করতে হবে। এ ক্ষেত্রে কোন বিতর্ক দেখা দিলে সংশ্লিষ্ট কমিটির সিদ্ধান্ত চূড়ান্ত বলে বিবেচিত হবে।</a></li>
                                    </ul>
									
									

									<h3>ভাতা প্রাপ্তির অযোগ্যতা</h3>

                                    <ul class="list-styles">
                                        <li><i class="fa fa-check"></i> <a href="#">সরকারি কর্মচারী পেনশনভোগী হলে;</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">দুঃস্থ মহিলা হিসেবে ভিজিডি কার্ডধারী হলে;</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">অন্য কোনোভাবে নিয়মিত সরকারী অনুদান/ভাতা প্রাপ্ত হলে;</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">কোনো বেসরকারি সংস্থা/সমাজকল্যাণমূলক প্রতিষ্ঠান হতে নিয়মিতভাবে আর্থিক অনুদান/ভাতা প্রাপ্ত হলে।</a></li>
										
                                    </ul>
									
                                </div>       

                                <!--Aside - mini and full boxes -->
                                <aside class="col-md-3">


                                </aside>
                                <!-- End Aside - mini and full boxes -->           
                            </div>
                        </div>
                        <!-- End Container Area - Boxes Services -->
                    </div>
                </div>
                <!-- End Info Content-->

                <!-- Info Content - Resalt Info-->
                <div class="content_info">
                    <!-- Info Resalt-->
                    <div class="skin_base color-white paddings">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12 title-resalt">
                                    <h2>Please, Help us</h2>
                                    <p class="lead">See how Client has transformed clinical trials quickly and securely using our platform</p>
                                </div>               
                            </div>
                        </div>
                    </div>
                    <!-- End Info Resalt-->
                </div>
                <!-- End Info Content - Resalt Info-->      
				