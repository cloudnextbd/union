<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>

            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
</div>
<!-- Info Content -->
<div class="content_info">
    <div class="paddings">
        <!-- container area -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">বিশেষ সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>
                        <div class="widget-body">
                            <div class="col-md-7">
                                <div class="widget-main">
                                    <div id="fuelux-wizard-container">
                                        <div class="step-content pos-rel">
                                            <?= form_open(site_url("create_service/add"), array("class" => "form-horizontal")) ?>
                                                <!--category-->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">ক্যাটাগরি <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <select class="input-large" id="cat" name="category" required>
                                                                <option value="0" selected>নির্বাচন করুন</option>
                                                                <option value="1" >সরকারী</option>
                                                                <option value="2" >বেসরকারী</option>
                                                                <option value="3" >সামাজিক</option>
                                                                <option value="4" >ব্যাক্তিগত </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--authority-->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">সেবার কর্তৃপক্ষ <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <select class="input-large" id="auth_r2" name="authority_id" required>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--service-->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">সেবার নাম <sup style="color:red;">*</sup></label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <select class="input-large" id="service" name="ser_id">

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- provider person -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">সেবা প্রদানকারী ব্যক্তির নাম  <sup style="color: red">*</sup></label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="provider_name" maxlength="256" required />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- date -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Birth_date">সেবা প্রদানের তারিখ <sup style="color: red">*</sup> </label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <div class="input-group">
                                                                <input class="form-control col-xs-12 col-md-4 date-picker" id="id-date-picker-1" data-date-format="dd-mm-yyyy" name="date" placeholder="dd-mm-yyyy" required/>
                                                            </div>  
                                                        </div>  
                                                    </div>
                                                </div>
                                                <!-- member id -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">সেবা গ্রহীতার সদস্য নং <sup style="color: red">*</sup></label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" id="mem_id" onkeyup="get_mem_info(this.value)" name="app_id" placeholder="উদা: ১২৩৪৫৬৭৮৯" required />
                                                        </div>
                                                        <div id="mem_check" class='help-block col-xs-12 col-sm-reset inline'></div>
                                                    </div>
                                                </div>
                                                <!-- note -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">নোট  <sup style="color: red">*</sup></label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <textarea class="input-xlarge" name="note" rows="3" placeholder="e.g: write here..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <!-- submit -->
                                                <div class="form-group">
                                                    <div class="col-xs-12 col-md-3 no-padding-right"></div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <button id="submit-btn" class="btn btn-success" type="submit" disabled>submit</button>
                                                        </div>
                                                        <div id="error" class='help-block'></div>
                                                    </div>
                                                </div>
                                            <?=form_close()?>
                                        </div>
                                    </div>
                                </div><!-- /.widget-main -->
                            </div>
                            
                            <!-- expected member -->
                            <div class="col-md-5">
                                <?=form_open(site_url(),array("class" => "form-horizontal")) ?>
                                <!--image-->
                                <div class="form-group">
                                    <div class="col-xs-12 col-md-12">
                                        <img id="mem_image" src="<?=base_url()?>assets/app_img/default.png" />
                                    </div>
                                </div>
                                <!--  name -->
                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">নামঃ </label>
                                    <div class="col-xs-12 col-md-6">
                                        <label class="control-label" id="name" style="color: #777" ></label>
                                    </div>
                                </div>
                                <!--  village -->
                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">গ্রামের নামঃ </label>
                                    <div class="col-xs-12 col-md-6">
                                        <label class="control-label" id="village" style="color: #777" ></label>
                                    </div>
                                </div>
                                <!--  present service -->
                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">বর্তমানে যে সেবা পাচ্ছেঃ </label>
                                    <div class="col-xs-12 col-md-6">
                                        <label class="control-label" id="current_service" style="color: #777" > </label>
                                        <input type="hidden" id="ser_id" value=""/>
                                    </div>
                                </div>
                                <!--  status -->
                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="">সেবাটি পাওয়ার যোগ্যতাঃ  </label>
                                    <div class="col-xs-12 col-md-6">
                                        <label class="control-label" id="isqualified" style="color: #777" ></label>
                                    </div>
                                </div>
                                
                                <?=form_close()?>
                            </div>
                        </div><!-- /.widget-body -->
                    </div>
                </div>
                
            </div>
        </div>
    </div>