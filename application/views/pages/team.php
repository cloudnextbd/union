<div class="content_info">
                    <div class="section-title-01 section-title-01-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Team</h3>
                                    <h5>Our Portfolio</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Parallax Background -->
                    <div class="borders world-bg"></div>
                    <!-- Parallax Background -->

                    <!-- title-vertical-line-->
                    <div class="title-vertical-line">
                        <h2><span>Team</span> Members</h2>
                        <p class="lead">We have created alliances with recognized entities that contribute to improving quality of your life.</p>
                    </div>
                    <!-- End title-vertical-line-->

                    <!-- Content Parallax-->
                    <section class="paddings">
                        <div class="container">
                            <div class="row">
                                <!-- Item Team Member--> 
                                <div class="col-md-3">
                                    <div class="item-team">
                                        <a href="<?php echo base_url(); ?>assets/front/img/team/1.jpg" class="fancybox"><img src="<?php echo base_url(); ?>assets/front/img/team/1.jpg" alt=""></a>
                                        <h4>Jeniffer</h4>
                                        <span class="country"><img src="<?php echo base_url(); ?>assets/front/img/country/london.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><i class="fa fa-envelope"></i> <a href="#">jm@iwthemes.com</a></li>
                                            <li><i class="fa fa-headphones"></i> <a href="#">+56 3456298</a></li>
                                            <li><i class="fa fa-facebook"></i> <a href="#">@jeniffer</a></li>
                                            <li><i class="fa fa-linkedin"></i> <a href="#">jenifer</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Item Team Member-->  

                                <!-- Item Team Member--> 
                                <div class="col-md-3">
                                    <div class="item-team">
                                        <img src="<?php echo base_url(); ?>assets/front/img/team/2.jpg" alt="">
                                        <h4>Federick</h4>
                                        <span class="country"><img src="<?php echo base_url(); ?>assets/front/img/country/au.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><i class="fa fa-envelope"></i> <a href="#">fg@iwthemes.com</a></li>
                                            <li><i class="fa fa-headphones"></i> <a href="#">+56 3456298</a></li>
                                            <li><i class="fa fa-facebook"></i> <a href="#">@jeniffer</a></li>
                                            <li><i class="fa fa-linkedin"></i> <a href="#">jenifer</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Item Team Member-->  

                                <!-- Item Team Member--> 
                                <div class="col-md-3">
                                    <div class="item-team">
                                        <img src="<?php echo base_url(); ?>assets/front/img/team/3.jpg" alt="">
                                        <h4>Maria</h4>
                                        <span class="country"><img src="<?php echo base_url(); ?>assets/front/img/country/us.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><i class="fa fa-envelope"></i> <a href="#">ms@iwthemes.com</a></li>
                                            <li><i class="fa fa-headphones"></i> <a href="#">+56 3456298</a></li>
                                            <li><i class="fa fa-facebook"></i> <a href="#">@jeniffer</a></li>
                                            <li><i class="fa fa-linkedin"></i> <a href="#">jenifer</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Item Team Member-->  

                                <!-- Item Team Member--> 
                                <div class="col-md-3">
                                    <div class="item-team">
                                        <img src="<?php echo base_url(); ?>assets/front/img/team/4.jpg" alt="">
                                        <h4>John</h4>
                                        <span class="country"><img src="<?php echo base_url(); ?>assets/front/img/country/london.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><i class="fa fa-envelope"></i> <a href="#">jr@iwthemes.com</a></li>
                                            <li><i class="fa fa-headphones"></i> <a href="#">+56 3456298</a></li>
                                            <li><i class="fa fa-facebook"></i> <a href="#">@jeniffer</a></li>
                                            <li><i class="fa fa-linkedin"></i> <a href="#">jenifer</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Item Team Member-->  
                            </div>
<br>
<br>

                            <div class="row">
                                <!-- Item Team Member--> 
                                <div class="col-md-3">
                                    <div class="item-team">
                                        <a href="<?php echo base_url(); ?>assets/front/img/team/1.jpg" class="fancybox"><img src="<?php echo base_url(); ?>assets/front/img/team/1.jpg" alt=""></a>
                                        <h4>Jeniffer</h4>
                                        <span class="country"><img src="<?php echo base_url(); ?>assets/front/img/country/london.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><i class="fa fa-envelope"></i> <a href="#">jm@iwthemes.com</a></li>
                                            <li><i class="fa fa-headphones"></i> <a href="#">+56 3456298</a></li>
                                            <li><i class="fa fa-facebook"></i> <a href="#">@jeniffer</a></li>
                                            <li><i class="fa fa-linkedin"></i> <a href="#">jenifer</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Item Team Member-->  

                                <!-- Item Team Member--> 
                                <div class="col-md-3">
                                    <div class="item-team">
                                        <img src="<?php echo base_url(); ?>assets/front/img/team/2.jpg" alt="">
                                        <h4>Federick</h4>
                                        <span class="country"><img src="<?php echo base_url(); ?>assets/front/img/country/au.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><i class="fa fa-envelope"></i> <a href="#">fg@iwthemes.com</a></li>
                                            <li><i class="fa fa-headphones"></i> <a href="#">+56 3456298</a></li>
                                            <li><i class="fa fa-facebook"></i> <a href="#">@jeniffer</a></li>
                                            <li><i class="fa fa-linkedin"></i> <a href="#">jenifer</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Item Team Member-->  

                                <!-- Item Team Member--> 
                                <div class="col-md-3">
                                    <div class="item-team">
                                        <img src="<?php echo base_url(); ?>assets/front/img/team/3.jpg" alt="">
                                        <h4>Maria</h4>
                                        <span class="country"><img src="<?php echo base_url(); ?>assets/front/img/country/us.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><i class="fa fa-envelope"></i> <a href="#">ms@iwthemes.com</a></li>
                                            <li><i class="fa fa-headphones"></i> <a href="#">+56 3456298</a></li>
                                            <li><i class="fa fa-facebook"></i> <a href="#">@jeniffer</a></li>
                                            <li><i class="fa fa-linkedin"></i> <a href="#">jenifer</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Item Team Member-->  

                                <!-- Item Team Member--> 
                                <div class="col-md-3">
                                    <div class="item-team">
                                        <img src="<?php echo base_url(); ?>assets/front/img/team/4.jpg" alt="">
                                        <h4>John</h4>
                                        <span class="country"><img src="<?php echo base_url(); ?>assets/front/img/country/london.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><i class="fa fa-envelope"></i> <a href="#">jr@iwthemes.com</a></li>
                                            <li><i class="fa fa-headphones"></i> <a href="#">+56 3456298</a></li>
                                            <li><i class="fa fa-facebook"></i> <a href="#">@jeniffer</a></li>
                                            <li><i class="fa fa-linkedin"></i> <a href="#">jenifer</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Item Team Member-->  
                            </div>
                        </div> 
                    </section>  
                    <!-- End Content Parallax--> 
                </div>