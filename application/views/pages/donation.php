<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- Info Section title-->
    <div class="section-title-01 section-title-01-small">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h3>Page Full Widht</h3>
                    <h5>FEATURES SECTION</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- End Info Section Title-->

    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>
                /
            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
    <!-- End breadcrumbs-->
</div>
<!-- End Info Content - Section Title-->


<div id="main">

    <section class="causes-style-2 margin-none">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="outer">
                        <div class="box">
                            <div class="frame"><a href="#"><img src="<?= base_url() ?>assets/front/img/d1.jpg" alt="img"></a></div>
                            <div class="text-box">
                                <h4><a href="#">আসুন একজন হত দরিদ্রের পাশে

দাড়াই</a></h4>

                            </div>
                        </div>
                    </div>

                </div>
                 <div class="col-md-4 col-sm-6">
                    <div class="outer">
                        <div class="box">
                            <div class="frame"><a href="#"><img src="<?= base_url() ?>assets/front/img/d2.jpg" alt="img"></a></div>
                            <div class="text-box">
                                <h4><a href="#">আসুন একজন প্রতিবন্ধির পাশে

দাড়াই</a></h4>

                            </div>
                        </div>
                    </div>

                </div>
                 <div class="col-md-4 col-sm-6">
                    <div class="outer">
                        <div class="box">
                            <div class="frame"><a href="#"><img src="<?= base_url() ?>assets/front/img/d3.jpg" alt="img"></a></div>
                            <div class="text-box">
                                <h4><a href="#">আসুন  উন্নয়নমূলক

কর্মকাণ্ডের পাশে দাড়াই।</a></h4>

                            </div>
                        </div>
                    </div>

                </div>
                
                 
               
            </div>
            <div class="pagination-box">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li> <a href="#" aria-label="Previous"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li> <a href="#" aria-label="Next"> <i class="fa fa-angle-right" aria-hidden="true"></i> </a> </li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>

</div>