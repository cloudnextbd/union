<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- Info Section title-->
    <div class="section-title-01 section-title-01-small">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h3>User Area</h3>
                    <h5>Profile User</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- End Info Section Title-->
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Our Services</a>
            </li>
            <li>
            <li>
                /
            </li>
            <li>
                User Area
            </li>
        </ul>
    </div>
    <!-- End breadcrumbs-->
</div>
<!-- End Info Content - Section Title-->
<!-- Info Content - User Area-->
<div class="content_info">
    <div class="paddings">  
        <div class="container">
            <div class="row user-area">
                <!-- Left User Area-->
                <div class="col-md-3">
                    <div class="item-team">
                        <h4><?=$info[0]->name?></h4>
                        <span class="country"><img src="<?=base_url()?>assets/app_img/<?=$info[0]->image?>"  style="width:auto; height:100px"  alt="<?=$info[0]->name?>"></span>
                    </div>
                    <div class="panel panel-default">
                        <!-- List group -->
                        <ul class="list-group">
                            <li class="list-group-item"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">ব্যক্তিগত পরিচিতি</a></li>
                            <li class="list-group-item"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">পারিবারিক পরিচিতি</a></li>
                            <li class="list-group-item"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">অর্থনৈতিক অবস্থা</a></li>
                            <li class="list-group-item"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">যে সেবার জন্য আগ্রহী</a></li>
                            <li class="list-group-item"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">প্রত্যায়ন</a></li>
                            
                            <?php if(isset($this->user->info->role) &&  !$this->user->info->role ==  0) : ?>
                                <li class="list-group-item"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">সুপারিশ সমূহ ও পদক্ষেপ গ্রহন</a></li>
                            <?php endif; ?>
                            <li class="list-group-item"><a href="#tab7" aria-controls="tab7" role="tab" data-toggle="tab">Service History</a></li>   
                        </ul>
                    </div>
                </div>
                <!-- End Left User Area-->
                <!-- Content Tabs Section-->
                <div class="col-md-9 item-team">
                    <!-- tab-content-->
                    <div class="tab-content">
                        
                        <!-- personal tab 1-->
                        <div role="tabpanel" class="tab-pane fade in active" id="tab1">
                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">ব্যক্তিগত পরিচিতি</h4>
                            <br />
                            <div class="profile-user-info profile-user-info-striped">
                                <div class="profile-info-row">
                                    <div class="profile-info-name" style="width: 130px">সদস্য নংঃ</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->id?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">নামঃ</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->name?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> লিঙ্গঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->gender?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> জন্ম তারিখঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->birth_date?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">জন্ম নিবন্ধন নংঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->birth_id?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> জাতীয় পরিচয় পত্র নংঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->nid?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> মোবাইল নংঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->mobile?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> জেলাঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->district?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> উপজেলাঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->sub_district?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> ইউনিয়নঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->union?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> ওয়ার্ড নংঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->word_no?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> বাড়ির নামঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->house_name?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">হোল্ডিং নংঃ  </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->holdings_no?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">পেশাঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->profession?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">স্বক্ষমতা</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="">
                                            <?php if($info[0]->capability ==  'complete_disable'){ 
                                                echo "সম্পূর্ণ কর্মক্ষমতাহীন";
                                            } elseif($info[0]->capability ==  'partial_disable') {
                                                echo "আংশিক কর্মক্ষম";
                                            } elseif($info[0]->capability ==  'handicaped') {
                                                echo "প্রতিবন্ধী";
                                            } elseif($info[0]->capability ==  'ill') {
                                                echo "অসুস্থ";
                                            } ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">অসামর্থ্যতা/প্রতিবন্ধী প্রকৃতিঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->disability?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">ধর্মঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->religion?></span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <!-- family tab 2-->
                        <div role="tabpanel" class="tab-pane fade in " id="tab2">

                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">পারিবারিক পরিচিতি</h4>
                            <br />

                            <div class="profile-user-info profile-user-info-striped">

                                <div class="profile-info-row">
                                    <div class="profile-info-name" style="width: 130px">পিতার নামঃ </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->father_name?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name">জাতীয় পরিচয় পত্র নংঃ </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->father_nid?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> মাতার নামঃ  </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->mother_nid?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> জাতীয় পরিচিত পত্র নংঃ  </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->mother_nid?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name">বৈবাহিক অবস্থাঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->marital_status?></span>
                                    </div>
                                </div>
                                
                                
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> স্বামীর নাম </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->husband_name?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> স্বামীর পরিচিত পত্র নংঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->husband_nid?></span>
                                    </div>
                                </div>
                                
                                
                                
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> স্ত্রীর নামঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->wife_name?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> জাতীয় পরিচিত পত্র নংঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->wife_nid?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> পরিবারের সদস্য সংখাঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->family_member?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> পরিবারের বর্ণনা</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->family_desc?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <!-- finance tab 3-->
                        <div role="tabpanel" class="tab-pane fade in " id="tab3">

                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">অর্থনৈতিক অবস্থা</h4>
                            <br />

                            <div class="profile-user-info profile-user-info-striped">

                                <div class="profile-info-row">
                                    <div class="profile-info-name" style="width: 130px">বাসস্থানের অবস্থার বিবরণঃ </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->house_desc?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">ছবিঃ</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id=""><img src="<?=base_url()?>assets/app_img/<?=$info[0]->house_image?>" alt="<?=$info[0]->house_image?>"></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> জমির পরিমানঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->land?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> উপার্জনক্ষম সদস্য সংখাঃ  </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->earn_person?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">আয়ের উৎসঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->earn_source?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> মাসিক আয়ঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->monthly_income?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> দারিদ্র্যের কারনঃ</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->cause_poorness?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- service tab 4-->
                        <div role="tabpanel" class="tab-pane fade in" id="tab4">

                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">যে সেবার জন্য আগ্রহী</h4>
                            <br />

                            <div class="profile-user-info profile-user-info-striped">

                                <div class="profile-info-row">
                                    <div class="profile-info-name" style="width: 280px">বর্তমানে কোন ধরনের সেবা পাচ্ছেনঃ</div>
                                    <div class="profile-info-value">
                                        <?php foreach($curr_services as $curr_service) : ?>
                                        <span class="editable"><?=$curr_service->ser_name?></span>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name">কোন ধরনের সেবার জন্য আগ্রহীঃ</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$inter_serv[0]->ser_name?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> দারিদ্রতা থেকে উত্তরনের উপায়ঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->success_way?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> কি ধরনের প্রশিক্ষণের জন্য আগ্রহিঃ </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->inter_trainings?></span>
                                    </div>
                                </div>
                                
                                <div class="profile-info-row">
                                    <div class="profile-info-name">সুপারিশ কারীর নাম(কমপক্ষে তিনজন)ঃ </div>

                                    <div class="profile-info-value">
                                        <?php foreach($ref_names as $ref_name) : ?>
                                        <span class="editable"><?=$ref_name->first_name .' '. $ref_name->last_name?></span>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                
                                <div class="profile-info-row">
                                    <div class="profile-info-name">বিশেষায়িত তথ্য </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id=""><?=$info[0]->sp_info?></span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        
                        <!-- tab-Item-5-->
                        <div role="tabpanel" class="tab-pane fade in" id="tab5">

                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">প্রত্যায়ন</h4>
                            <br />

                            <div class="profile-user-info profile-user-info-striped">

                                <div class="profile-info-row">
                                    <div class="profile-info-name" style="width: 130px">.....</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username">আমি সজ্ঞানে সকল তথ্য সঠিক বলিয়া নিশ্চিত করিলাম।....</span>
                                    </div>
                                </div>




                            </div>

                        </div>
                        <!-- tab-Item-5-->

                        <?php if(isset($this->user->info->role) &&  !$this->user->info->role ==  0) : ?>
                        <!-- tab-Item-6-->
                        <div role="tabpanel" class="tab-pane fade in" id="tab6">

                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">সুপারিশ সমূহ ও পদক্ষেপ গ্রহন</h4>
                            <br />

                            <?php if(isset($this->user->info->role) &&  $this->user->info->role ==  1) : ?>

                                <?php if(empty($ref_comnt[0]->comments)) : ?>
                                    <!--supervisor_comment_update_view-->
                                    <?=form_open(site_url('app_list/ref_comments'))?>
                                        <div class="form-group">
                                            <label for="Comments" class="col-md-2 control-label">আপনার মন্তব্য</label>
                                            <div class="col-md-10">
                                                <textarea  class="form-control" id="comments" rows="6" name="comments" placeholder="write anything here..." required></textarea>
                                            </div>
                                        </div> <br />
                                        <div class="form-group">
                                            <div class="col-md-2 control-label"></div>
                                            <div class="col-md-10">
                                                <input type="hidden" name="id" value="<?=$info[0]->id?>" />
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    <?=form_close()?>
                                <?php endif; ?>

                                <?php if(!empty($ref_comnt[0]->comments)) : ?>
                                    <!--supervisor_only_comment_view-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">আপনার মন্তব্য</label>
                                        <div class="col-md-10">
                                            <textarea  class="form-control" rows="6" readonly><?=$ref_comnt[0]->comments?></textarea>
                                        </div>
                                    </div> <br />
                                <?php endif; ?>

                            <?php endif; ?>

                            <?php if(isset($this->user->info->role) &&  $this->user->info->role ==  2) : ?>
                                <!--co-ordinator-->
                                <div class="profile-user-info profile-user-info-striped">
                                    <?php foreach($comnts as $comnt) : ?>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="width: 230px"><?=$comnt->first_name .' '. $comnt->last_name?></div>
                                        <div class="profile-info-value">
                                            <span class="editable" id="">
                                                <?php if(empty($comnt->comments)) : ?>
                                                    তিনি মন্তব্য করেননি 
                                                <?php else: ?>
                                                    <?=$comnt->comments?>
                                                <?php endif; ?>
                                            <br></span>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                                <h5>উক্ত ব্যক্তিবর্গের সুপারিশ সাপেক্ষে এই ব্যক্তিকে সেবা প্রদানের জন্য তালিকাভুক্ত করা হইল <br>- সমাজসেবা অফিসার
                                    চাঁদপুর সদর</h5>

                                <?php if(empty($coor_comnt[0]->comments)) : ?>
                                    <?=form_open(site_url('app_list/coor_action'))?>
                                        <div class="form-group">
                                            <label for="Comments" class="col-md-2 control-label">আপনার মন্তব্য</label>
                                            <div class="col-md-10">
                                                <textarea  class="form-control" id="comments" rows="6" name="comments" placeholder="write anything here..." required></textarea>
                                            </div>
                                        </div> <br />
                                        <!--supervisor_comment_update_view-->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Application status</label>
                                            <div class="col-md-9">
                                                <input type="radio" name="app_status" value="2" checked> Approved<br>
                                                <input type="radio" name="app_status" value="1"> Pending<br>
                                                <input type="radio" name="app_status" value="3"> Need to re-edit<br>
                                                <input type="radio" name="app_status" value="0"> Refuesd
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Service status</label>
                                            <div class="col-md-9">
                                                <input type="radio" name="status" value="1" checked> Active<br>
                                                <input type="radio" name="status" value="0"> Inactive<br>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="<?=$info[0]->id?>" />
                                        <input type="hidden" name="coor_id" value="<?=$this->user->info->id?>" />
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    <?=form_close()?>
                                <?php endif; ?>

                                <?php if(!empty($coor_comnt[0]->comments)) : ?>
                                    <!--supervisor_only_comment_view-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">আপনার মন্তব্য</label>
                                        <div class="col-md-10">
                                            <textarea  class="form-control" rows="6" readonly><?=$coor_comnt[0]->comments?></textarea>
                                        </div>
                                    </div> <br />
                                <?php endif; ?>

                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                        
                        
                        <!-- finance tab 7-->
                        <div role="tabpane7" class="tab-pane fade in" id="tab7">
                            
                            <table id="myTable" class="display nowrap table table-bordered table-striped" cellspacing="0" width="100%">
                                
                                <thead>
                                    <tr>
                                        <th style="color: #547EA8;">নং</th>
                                        <th style="color: #547EA8;">তারিখ</th>
                                        <th style="color: #547EA8;">সেবার নাম</th>
                                        <th style="color: #547EA8;">নোট</th>
                                        <th style="color: #547EA8;">কর্তৃপক্ষ</th>
                                        <th style="color: #547EA8;">প্রদানকারীর নাম</th>
                                    </tr>
                                </thead>
<!--                                <tfoot>
                                    <tr>
                                        <th>নং</th>
                                        <th>তারিখ</th>
                                        <th>সেবার নাম</th>
                                        <th>নোট</th>
                                        <th>কর্তৃপক্ষ</th>
                                        <th>প্রদানকারীর নাম</th>
                                    </tr>
                                </tfoot>-->
                                <tbody>
                                    <?php if(empty($historys)){ ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php } else { ?>
                                    
                                        <?php $x = 0; foreach($historys as $hist) : $x++; ?>
                                        <tr>
                                            <td><?=$x?></td>
                                            <td><?=$hist->date?></td>
                                            <td><?=$hist->ser_name?></td>
                                            <td><?=$hist->note?></td>
                                            <td><?=$hist->ser_auth_name?></td>
                                            <td><?=$hist->provider_name?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                    <!-- End tab-content -->
                </div>
                <!-- End Tabs section-->
            </div>
        </div>
    </div>
</div>
<!-- End Info Content - User Area-->
</div>
<!-- End content-central-->