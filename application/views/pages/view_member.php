
                <!-- Info Content - Section Title-->
                <div class="content_info">
                    <!-- Info Section title-->
                    <div class="section-title-01 section-title-01-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <h3>User Area.</h3>
                                    <h5>Profile User</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Info Section Title-->

                    <!-- breadcrumbs-->
                    <div class="breadcrumbs breadcrumbs-sections">
                        <ul>
                            <li class="breadcrumbs-home">
                                <a href="#" title="Back To Home">
                                  <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">Our Services</a>
                            </li>
                            <li>
                            <li>
                                /
                            </li>
                            <li>
                                User Area
                            </li>
                        </ul>
                    </div>
                    <!-- End breadcrumbs-->
                </div>
                <!-- End Info Content - Section Title-->

                <!-- Info Content - User Area-->
                <div class="content_info">
                    <div class="paddings">  
                        <div class="container">
                            <div class="row user-area">
                                <!-- Left User Area-->
                                <div class="col-md-4">
                                    <div class="item-team">
                                        <h4>জনাব মোঃ মনসুর আহমেদ</h4>
                                        <span class="country"><img src="<?=base_url()?>assets/front/poor.jpeg" alt=""> চরবাকিলা ও ছোটসুন্দর</span>
                                        
                                    </div>

                                    <div class="panel panel-default">

                                        <!-- List group -->
                                      <ul class="list-group">
                                        <li class="list-group-item"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">ব্যক্তিগত পরিচিতি</a></li>
                                        <li class="list-group-item"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">পারিবারিক পরিচিতি</a></li>
                                        <li class="list-group-item"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">অর্থনৈতিক অবস্থা</a></li>
                                        <li class="list-group-item"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">যে সেবার জন্য আগ্রহী</a></li>
                                        <li class="list-group-item"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">প্রত্যায়ন</a></li>
                                        <li class="list-group-item"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">সুপারিশ সমূহ ও পদক্ষেপ গ্রহন</a></li>
                                      </ul>
                                    </div>
                                </div>
                                <!-- End Left User Area-->

                                <!-- Content Tabs Section-->
                                <div class="col-md-8 item-team">
                                    <!-- Nav tabs -->
                                    
                                    <!-- End Nav tabs -->

                                    <!-- tab-content-->
                                    <div class="tab-content">


                                        <!-- tab-Item-1-->
                                      <div role="tabpanel" class="tab-pane fade in active" id="tab1">

                                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">ব্যক্তিগত পরিচিতি</h4>
                                            <br />

                                            <div class="profile-user-info profile-user-info-striped">

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name" style="width: 130px">সদস্য নংঃ</div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">0123</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">নামঃ</div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">মোঃ মনসুর আহমেদ</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> লিঙ্গঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">পুরুষ</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> জন্ম তারিখঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">১/১/১৯৪৫</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">জন্ম নিবন্ধন নংঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">১০৯৮৭৬২৫৬৪৭০৯৮৬</span>
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> জাতীয় পরিচয় পত্র নংঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">৬৫৭০৯৩৮৭৫৩১২১৯৫৬</span>
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> মোবাইল নংঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">০১৭১০০০০০০০</span>
                                                    </div>
                                                </div>
												
												
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> জেলাঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">চাঁদপুর</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> উপজেলাঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">চাঁদপুর</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> ইউনিয়নঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">বাগাদী</span>
                                                    </div>
                                                </div>
												
						<div class="profile-info-row">
                                                    <div class="profile-info-name"> ওয়ার্ড নংঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">৫</span>
                                                    </div>
                                                </div>
												
						<div class="profile-info-row">
                                                    <div class="profile-info-name"> বাড়ির নামঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">বেপারি বাড়ি</span>
                                                    </div>
                                                </div>
												
						<div class="profile-info-row">
                                                    <div class="profile-info-name">হোল্ডিং নংঃ  </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">৯৮৬</span>
                                                    </div>
                                                </div>
												
					
												
						<div class="profile-info-row">
                                                    <div class="profile-info-name">পেশাঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">কৃষি</span>
                                                    </div>
                                                </div>
												
						<div class="profile-info-row">
                                                    <div class="profile-info-name">অসামর্থ্যতা/প্রতিবন্ধী প্রকৃতিঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">প্রযোজ্য নয়</span>
                                                    </div>
                                                </div>
												
						<div class="profile-info-row">
                                                    <div class="profile-info-name">ধর্মঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">ইসলাম</span>
                                                    </div>
                                                </div>
												
						<div class="profile-info-row">
                                                    <div class="profile-info-name">সুপারিশ কারীর নাম(কমপক্ষে তিনজন)ঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">চেয়ারম্যান, ইউপি সচিব, ইউপি সদস্য, পরিবার কল্যান পরিদর্শক, ব্র্যাক</span>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                        <!-- tab-Item-1-->



                                        <!-- tab-Item-2-->
                                         <div role="tabpanel" class="tab-pane fade in " id="tab2">

                                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">পারিবারিক পরিচিতি</h4>
                                            <br />

                                            <div class="profile-user-info profile-user-info-striped">

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name" style="width: 130px">পিতার নামঃ </div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">মালু মিয়া</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">জাতীয় পরিচয় পত্র নংঃ </div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">১৯১৪০৯৫০৪৯০৩৫</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> মাতার নামঃ  </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">আম্বিয়া খাতুন</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> জাতীয় পরিচিত পত্র নংঃ  </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">১৯১৪০৯৫০৯৪৯০৩</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">বৈবাহিক অবস্থাঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">বিবাহিত</span>
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> স্ত্রীর নামঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">বিউটি আক্তার</span>
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> জাতীয় পরিচিত পত্র নংঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">১৩১২২৩১১৭৫৪৬৬</span>
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> পরিবারের সদস্য সংখাঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">৫</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> সদস্যদের নামঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">বয়সঃ </span>
                                                    </div>
                                                </div>

                                                


                                            </div>

                                        </div>
                                        <!-- tab-Item-2-->

                                        <!-- tab-Item-3-->
                                        <div role="tabpanel" class="tab-pane fade in " id="tab3">

                                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">অর্থনৈতিক অবস্থা</h4>
                                            <br />

                                            <div class="profile-user-info profile-user-info-striped">

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name" style="width: 130px">বাসস্থানের অবস্থার বিবরণঃ </div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">দুই শতাংশ জমির উপর টিনের ঘর</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">ছবিঃ</div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">Name here</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> জমির পরিমানঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">৬ শতাংশ</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> উপার্জনক্ষম সদস্য সংখাঃ  </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">১</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">আয়ের উৎসঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">রিকশা</span>
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> মাসিক আয়ঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">৪০০০ টাকা</span>
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> দারিদ্র্যের কারনঃ</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">.........</span>
                                                    </div>
                                                </div>
                                


                                            </div>

                                        </div>
                                        <!-- tab-Item-3-->
                                        
                                        <!-- tab-Item-4-->
                                        <div role="tabpanel" class="tab-pane fade in" id="tab4">

                                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">যে সেবার জন্য আগ্রহী</h4>
                                            <br />

                                            <div class="profile-user-info profile-user-info-striped">

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name" style="width: 280px">বর্তমানে কোন ধরনের সেবা পাচ্ছেনঃ</div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">......</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">কোন ধরনের সেবার জন্য আগ্রহীঃ</div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">বয়স্ক ভাতা</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> দারিদ্রতা থেকে উত্তরনের উপায়ঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">..........</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> কি ধরনের প্রশিক্ষণের জন্য আগ্রহিঃ </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">..........</span>
                                                    </div>
                                                </div>

                              


                                            </div>

                                        </div>
                                        <!-- tab-Item-4-->
                                        
                                        <!-- tab-Item-5-->
                                        <div role="tabpanel" class="tab-pane fade in" id="tab5">

                                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">প্রত্যায়ন</h4>
                                            <br />

                                            <div class="profile-user-info profile-user-info-striped">

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name" style="width: 130px">.....</div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">আমি সজ্ঞানে সকল তথ্য সঠিক বলিয়া নিশ্চিত করিলাম।....</span>
                                                    </div>
                                                </div>

                          


                                            </div>

                                        </div>
                                        <!-- tab-Item-5-->
                                        
                                        <!-- tab-Item-6-->
                                        <div role="tabpanel" class="tab-pane fade in" id="tab6">

                                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">সুপারিশ সমূহ ও পদক্ষেপ গ্রহন</h4>
                                            <br />

                                            <div class="profile-user-info profile-user-info-striped">

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name" style="width: 230px">_ইউপি চেয়ারম্যান।</div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">আবেদনকারীকে আমি ব্যক্তিগত ভাবে চিনি। তাকে উক্ত ভাতার জন্য তালিকা ভুক্ত করা যাইতে পারে।<br></span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">_ ইউপি সদস্য, ৩নং ওয়ার্ড।</div>
                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username">আবেদনকারী আমার ওয়ার্ডের বাসিন্দা। তার পরিবারে বর্তমানে উপার্জনক্ষম কেউ নেই। তার জন্য আমি উক্ত সেবার সুপারিশ করিলাম।<br></span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> _ সাহেদা বেগম
এনজিও কর্মী
ব্রাক </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">আবেদনকারীকে আমি চিনি। উনার আর্থিক অবস্থা খুবই করুন। উনার জন্য সুপারিশ করিলাম।<br></span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> _ সেলিম পাটোয়ারী
প্রধান শিক্ষক,  উচ্চ বিদ্যালয় </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">আবেদনকারী আমার প্রতিবেশী এবং  আমি তাকে ব্যক্তিগত ভাবে চিনি। উনি শারীরিকভাবে অক্ষম অর্থাৎ সম্পূর্ণরুপে কর্মক্ষমতাহীন এবং ভূমিহীন । তিনি এই সেবা পাওয়ার জন্য উপযুক্ত। তাকে এই সেবা প্রদানের জন্য আমি সুপারিশ করিলাম।</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">_রুবি আক্তার
স্বাস্থ্যকর্মী</div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="age">আবেদনকারীকে আমি ব্যক্তিগত ভাবে চিনি। উনি আর্থিক ভাবে নিঃস্ব এবং ভূমিহীন । তাকে এই সেবা প্রদানের জন্য আমি সুপারিশ করিলাম।</span>
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
<h5>উক্ত ব্যক্তিবর্গের সুপারিশ সাপেক্ষে এই ব্যক্তিকে সেবা প্রদানের জন্য তালিকাভুক্ত করা হইল <br>- সমাজসেবা অফিসার
চাঁদপুর সদর</h5>
                                                <button type="button" class="btn btn-primary">Approved</button><button type="button" class="btn btn-danger">Denied</button>
                                        </div>
                                        <!-- tab-Item-6-->



                                    </div>
                                    <!-- End tab-content -->
                                </div>
                                <!-- End Tabs section-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Info Content - User Area-->
            </div>
            <!-- End content-central-->
