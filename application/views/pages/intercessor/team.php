<div class="content_info">
    <div class="section-title-01 section-title-01-small">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>সুপারিশকারী</h3>
                    <h5>The Team</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- Parallax Background -->
    <div class="borders world-bg"></div>
    <!-- Parallax Background -->

    <!-- title-vertical-line-->
    <div class="title-vertical-line">
        <h2><span>সুপারিশকারী</span> Members</h2>
        <p class="lead">We have created alliances with recognized entities that contribute to improving quality of your life.</p>
    </div>
    <!-- End title-vertical-line-->

    <!-- Content Parallax-->
    <section class="paddings">
        <div class="container">
            <div class="row">
                <?php foreach($lists as $list) : ?>
                    <!-- Item Team Member--> 
                    <div class="col-md-3">
                        <div class="item-team">
                            <a href="<?=base_url()?>admin_panel/assets/img/user/<?=$list->image?>" class="fancybox"><img src="<?=base_url()?>/admin_panel/assets/img/user/<?=$list->image?>" alt=""></a>
                            <h4><?=$list->first_name . ' ' . $list->last_name?></h4>
                            
                            <span class="country"> <?=$list->about?></span>
                            <ul class="list-styles">
                                <li><i class="fa fa-home"></i><?=$list->address?></li>
                                <li><i class="fa fa-phone"></i>+৮৮০ ১৭১৫ ৩২৫ ৬৫</li>
                            </ul>
                            
                        </div>
                    </div>
                    <!-- End Item Team Member-->
                <?php endforeach; ?>
            </div>
        </div>
    </section>     
    
    <section class="paddings">    
        <div class="container">
            <div class="row">
                
                <div class="col-md-3">
                    <div class="item-team">
                        <a href="<?=base_url()?>admin_panel/assets/img/user/opr_1.jpg" class="fancybox"><img src="<?=base_url()?>admin_panel/assets/img/user/opr_1.jpg" alt=""></a>
                        <h4>জসিম উদ্দিন</h4>
                        <span class="country">অপারেটর</span>
                        <ul class="list-styles">
                            <li><i class="fa fa-home"></i>হাজরা,চাঁদপুর সদর</li>
                            <li><i class="fa fa-phone"></i>+৮৮০ ১৭১৫ ৩২৫ ৬৫</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="item-team">
                        <a href="<?=base_url()?>admin_panel/assets/img/user/sup_5.jpg" class="fancybox"><img src="<?=base_url()?>admin_panel/assets/img/user/sup_5.jpg" alt=""></a>
                        <h4>সাইফুল ইসলাম</h4>
                        <span class="country"> হেড মাস্টার </span>
                        <ul class="list-styles">
                            <li><i class="fa fa-home"></i>সাহেব বাজার, চাঁদপুর সদর</li>
                            <li><i class="fa fa-phone"></i>+৮৮০ ১৭১৫ ৩২৫ ৬৫</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="item-team">
                        <a href="<?=base_url()?>admin_panel/assets/img/user/sup_3.jpg" class="fancybox"><img src="<?=base_url()?>admin_panel/assets/img/user/sup_3.jpg" alt=""></a>
                        <h4>আক্তার প্রধান</h4>
                        <span class="country"> হেড মাস্টার </span>
                        <ul class="list-styles">
                            <li><i class="fa fa-home"></i>পশ্চিম সকদী, চাঁদপুর সদর</li>
                            <li><i class="fa fa-phone"></i>+৮৮০ ১৭১৫ ৩২৫ ৬৫</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="item-team">
                        <a href="<?=base_url()?>admin_panel/assets/img/user/sup_4.jpg" class="fancybox"><img src="<?=base_url()?>admin_panel/assets/img/user/sup_4.jpg" alt=""></a>
                        <h4>ইমরান হোসেন</h4>
                        <span class="country"> হেড মাস্টার </span>
                        <ul class="list-styles">
                            <li><i class="fa fa-home"></i>চাঁদপুর মৌজা,চাঁদপুর সদর</li>
                            <li><i class="fa fa-phone"></i>+৮৮০ ১৭১৫ ৩২৫ ৬৫</li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>    
    </section> 
    <!-- End Content Parallax-->
</div>