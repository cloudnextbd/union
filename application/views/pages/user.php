
                <!-- Info Content - Section Title-->
                <div class="content_info">
                    <!-- Info Section title-->
                    <div class="section-title-01 section-title-01-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <h3>User Area.</h3>
                                    <h5>Profile User</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Info Section Title-->

                    <!-- breadcrumbs-->
                    <div class="breadcrumbs breadcrumbs-sections">
                        <ul>
                            <li class="breadcrumbs-home">
                                <a href="#" title="Back To Home">
                                  <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">Our Services</a>
                            </li>
                            <li>
                            <li>
                                /
                            </li>
                            <li>
                                User Area
                            </li>
                        </ul>
                    </div>
                    <!-- End breadcrumbs-->
                </div>
                <!-- End Info Content - Section Title-->

                <!-- Info Content - User Area-->
                <div class="content_info">
                    <div class="paddings">  
                        <div class="container">
                            <div class="row user-area">
                                <!-- Left User Area-->
                                <div class="col-md-4">
                                    <div class="item-team">
                                        <img src="img/team/2.jpg" alt="">
                                        <h4>Federick Gordon</h4>
                                        <span class="country"><img src="img/country/au.png" alt=""> London, UK</span>
                                        <ul class="list-styles">
                                            <li><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></li>
                                        </ul>
                                    </div>

                                    <div class="panel panel-default">
                                      <!-- List group -->
                                      <ul class="list-group">
                                        <li class="list-group-item"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">ব্যক্তিগত পরিচিতি</a></li>
                                        <li class="list-group-item"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">পারিবারিক পরিচিতি</a></li>
                                        <li class="list-group-item"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">অর্থনৈতিক অবস্থা</a></li>
                                        <li class="list-group-item"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">যে সেবার জন্য আগ্রহী</a></li>
                                        <li class="list-group-item"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">প্রত্যায়ন</a></li>
                                        <li class="list-group-item"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">অন্যান্য</a></li>
                                      </ul>
                                    </div>
                                </div>
                                <!-- End Left User Area-->

                                <!-- Content Tabs Section-->
                                <div class="col-md-8 item-team">
                                    <!-- Nav tabs -->
                                    
                                    <!-- End Nav tabs -->

                                    <!-- tab-content-->
                                    <div class="tab-content">
                                        <!-- tab-Item-1-->
                                        <div role="tabpanel" class="tab-pane fade in active " id="tab1">
                                            <h4 style="text-align: center; background-color:#a4c639; margin-top: -25px; color: white">Federick Gordon</h4><br>
                                            <form action="#" class="form-theme">
                                                <label>First Name</label>
                                                <input type="number" placeholder="Federick" class="input">

                                                <label>Last Name</label>
                                                <input type="number" placeholder="Gordon" class="input">

                                                <label>Mobile Number</label>
                                                <input type="number" placeholder="+1 6358734" class="input">

                                                <label>Interests</label>
                                                <input type="number" placeholder="Credits, accounts, etc.." class="input">

                                                <label>Ocupation</label>
                                                <input type="number" placeholder="Web Developer" class="input">

                                                <label>About</label>
                                                <textarea placeholder="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas"></textarea>

                                                <label>Web Site Url</label>
                                                <input type="number" placeholder="http://www.iwthemes.com" class="input">

                                                <input type="submit" class="btn" value="Save Changes">
                                            </form>
                                        </div>
                                        <!-- tab-Item-1-->

                                        <!-- tab-Item-2-->
                                        <div role="tabpanel" class="tab-pane fade in" id="tab2">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
                                            <div class="fcorn-shortcodes">
                                                <form class="file-input">
                                                    <label for="file99">
                                                        <!-- You can use this `onchange="this.parentNode.parentNode.nextElementSibling.value = this.value"` 
                                                        in input for file uploader to work -->
                                                        <span class="btn left"><input type="file" id="file99">Choose File</span>
                                                      </label>
                                                      <input type="text" placeholder="No file choosen" class="file-replacer left" readonly="">
                                                </form>
                                            </div>
                                        </div>
                                        <!-- tab-Item-2-->

                                        <!-- tab-Item-3-->
                                        <div role="tabpanel" class="tab-pane fade in" id="tab3">
                                            <form action="#" class="form-theme">
                                                <label>Current Password</label>
                                                <input type="number" placeholder="Federick" class="input">

                                                <label>New Password</label>
                                                <input type="number" placeholder="Gordon" class="input">

                                                <label>Re-type New Password</label>
                                                <input type="number" placeholder="+1 6358734" class="input">

                                                <input type="submit" class="btn" value="Save Changes">
                                            </form>
                                        </div>
                                        <!-- tab-Item-3-->
                                    </div>
                                    <!-- End tab-content -->
                                </div>
                                <!-- End Tabs section-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Info Content - User Area-->
            </div>
            <!-- End content-central-->
