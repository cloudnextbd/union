<div id="main">
 
<section class="post-news-row blog-post causes-style-2">
<div class="container">
<div class="row">
<div class="col-md-9 col-sm-8">
<div class="post-box">
<div class="frame"> <img src="<?=base_url()?>assets/front/donation/causes-detail-img.jpg" alt="img"> <strong class="sticky">Sticky Post</strong> </div>
<div class="donation-row-outer">
<div class="donation-row">
<div class="col-md-12">
<div class="donation-progress">
<div class="progressbars" progress="52%"></div>
<strong class="title">Complete</strong> </div>
</div>
<div class="col-md-3">
<div class="progress-box"> <sup>$</sup> <strong class="number">49,610</strong> <span>Pledged</span> </div>
</div>
<div class="col-md-3">
<div class="progress-box"> <sup>$</sup> <strong class="number">85,790</strong> <span>Needed</span> </div>
</div>
<div class="col-md-3">
<div class="progress-box"> <strong class="number">208</strong> <span>Donators</span> </div>
</div>
<div class="col-md-3">
<div class="progress-box"> <strong class="number">34</strong> <span>Days To Go</span> </div>
</div>
<div class="btn-row"><a class="btn-style-1" href="#">Donate Now</a></div>
</div>
</div>
<div class="text-box">
<h3><a href="#">Remove Unhealthy Growth from Environment</a></h3>
<div class="tags-row"> <a href="#" class="link">Eco,</a> <a href="#" class="link">Energy,</a> <a href="#" class="link">Green,</a> <a href="#" class="link">Solar,</a> <a href="#" class="link">Eco Friendly</a> </div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusant doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>
<div class="blockquote-outer">
<blockquote> <i class="fa fa-quote-left" aria-hidden="true"></i> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beata. </blockquote>
</div>
<p>Spgnissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia.vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores .</p>
<h3>Donors List</h3>
<div class="donors-list">
<ul>
<li> <span class="number">1</span> <strong class="title">John Doe (Manager, Ecoist)</strong> <strong class="amount">$570 USD <span>Donate</span></strong> </li>
<li> <span class="number">2</span> <strong class="title">William</strong> <strong class="amount">$570 USD <span>Donate</span></strong> </li>
<li> <span class="number">3</span> <strong class="title">Roy Miller and Family</strong> <strong class="amount">$570 USD <span>Donate</span></strong> </li>
<li> <span class="number">4</span> <strong class="title">Eddie Ferbes (Manager, Company)</strong> <strong class="amount">$570 USD <span>Donate</span></strong> </li>
<li> <span class="number">5</span> <strong class="title">Allan Parker</strong> <strong class="amount">$570 USD <span>Donate</span></strong> </li>
</ul>
</div>
<div class="list-box">
<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et raesentium voluptatum deleniti atque corrupti quos dolores et quas similique sunt in culpa qui officia.</p>
<ul>
<li>Donec semper augue id pharetra feugiat.</li>
<li>Duis at mi mattis risus lacinia viverra ut ac sapien.</li>
<li>Morbi sed nulla et risus porttitor egestas id eget nibh.</li>
</ul>
</div>
<div class="share-post"> <strong class="title">Share Post</strong>
<ul>
<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
<li><a href="#" class="bullet">...</a></li>
</ul>
</div>
<div class="comment-box">
<div class="heading-left">
<h2>Leave a Comment</h2>
</div>
<form action="#">
<div class="row">
<div class="col-md-4">
<input placeholder="Name" required type="text">
</div>
<div class="col-md-4">
<input placeholder="Email" required type="text">
</div>
<div class="col-md-4">
<input placeholder="Subject" required type="text">
</div>
<div class="col-md-12">
<textarea cols="10" rows="10" placeholder="Comments" required></textarea>
</div>
<div class="col-md-12">
<input value="Submit Comment" type="submit">
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="col-md-3 col-sm-4">
 
<aside>
<div class="sidebar">
<div class="sidebar-box">
<h3>Search</h3>
<form action="#">
<input type="text" placeholder="Enter Key words" required>
<button type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
</form>
</div>
<div class="sidebar-box">
<h3>Text Widget</h3>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.</p>
</div>
<div class="sidebar-box">
<h3>Recent Projects</h3>
<div class="box">
<div id="home-banner" class="owl-carousel owl-theme">
<div class="item">
<div class="frame"><img src="<?=base_url()?>assets/front/donation/recent-project-img.jpg" alt="img"></div>
<a href="#" class="title">Help Young Planting</a> </div>
<div class="item">
<div class="frame"><img src="<?=base_url()?>assets/front/donation/recent-project-img.jpg" alt="img"></div>
<a href="#" class="title">Help Young Planting</a> </div>
</div>
</div>
</div>
<div class="sidebar-box">
<h3>Make Donation</h3>
<div class="donation-form">
<form action="#">
<input placeholder="Name" required type="text">
<input placeholder="Email / Contact" required type="text">
<div class="selector">
<select class="full-width">
<option value="1">Select Project </option>
<option value="2">Campaign</option>
<option value="3">Cause</option>
<option value="4">Eco</option>
</select>
</div>
<div class="selector">
<select class="full-width">
<option value="1">Select Amount </option>
<option value="2">Campaign</option>
<option value="3">Cause</option>
<option value="4">Eco</option>
</select>
</div>
<input value="Donate Now" type="submit">
</form>
</div>
</div>
<div class="sidebar-box">
<h3>Recent Posts</h3>
<div class="recent-post">
<ul>
<li>
<div class="thumb"><img src="images/blog-recent-post-img-1.jpg" alt="img"></div>
<div class="text-col"> <a href="#">Making Sustainable
Agriculture</a> <span><i class="fa fa-calendar" aria-hidden="true"></i>22 Oct, 2016</span> </div>
</li>
<li>
<div class="thumb"><img src="images/blog-recent-post-img-2.jpg" alt="img"></div>
<div class="text-col"> <a href="#">Animals Migration to
Save their Lives</a> <span><i class="fa fa-calendar" aria-hidden="true"></i>18 Sep, 2016</span> </div>
</li>
<li>
<div class="thumb"><img src="images/blog-recent-post-img-3.jpg" alt="img"></div>
<div class="text-col"> <a href="#">Wind Turbines and
Solar Panels Energy</a> <span><i class="fa fa-calendar" aria-hidden="true"></i>30 Aug, 2016</span> </div>
</li>
<li>
<div class="thumb"><img src="images/blog-recent-post-img-4.jpg" alt="img"></div>
<div class="text-col"> <a href="#">Animals Migration to
Save their Lives</a> <span><i class="fa fa-calendar" aria-hidden="true"></i>18 Aug, 2016</span> </div>
</li>
</ul>
</div>
</div>
<div class="sidebar-box">
<h3>Popular Posts</h3>
<div class="popular-post">
<ul>
<li> <a href="#">Making Sustainable Agriculture</a> <span><i class="fa fa-calendar" aria-hidden="true"></i>22 Aug, 2016</span> <span><i class="fa fa-comments-o" aria-hidden="true"></i>207</span> </li>
<li> <a href="#">Animals Migration to Save their Lives</a> <span><i class="fa fa-calendar" aria-hidden="true"></i>18 Aug, 2016</span> <span><i class="fa fa-comments-o" aria-hidden="true"></i>207</span> </li>
<li> <a href="#">Wind Turbines and Solar Panels </a> <span><i class="fa fa-calendar" aria-hidden="true"></i>30 Sep, 2016</span> <span><i class="fa fa-comments-o" aria-hidden="true"></i>207</span> </li>
<li> <a href="#">Animals Migration Save their Plants</a> <span><i class="fa fa-calendar" aria-hidden="true"></i>18 Aug, 2016</span> <span><i class="fa fa-comments-o" aria-hidden="true"></i>207</span> </li>
</ul>
</div>
</div>
<div class="sidebar-box">
<h3>Archives</h3>
<div class="archives-widget">
<ul>
<li><a href="#">March<span>2016</span></a></li>
<li><a href="#">February<span>2016</span></a></li>
<li><a href="#">January<span>2016</span></a></li>
<li><a href="#">December<span>2015</span></a></li>
<li><a href="#">November<span>2015</span></a></li>
<li><a href="#">October<span>2015</span></a></li>
</ul>
</div>
</div>
<div class="sidebar-box">
<h3>Tags</h3>
<div class="tags"> <a href="#">Recycling</a> <a href="#">Forest</a> <a href="#">Trees</a> <a href="#">Solor Panels</a> <a href="#">Save Ozone</a> <a href="#">Wind Energy</a> <a href="#">Projects</a> <a href="#">Water Refining</a> <a href="#">Planting</a> </div>
</div>
</div>
</aside>
 
</div>
</div>
</div>
</section>
 
</div>