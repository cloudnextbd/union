<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>

            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
</div>
<div class="content_info">
    <div class="paddings">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="fuelux-wizard-container">
                                    <!-- form header -->
                                    <div>
                                        <ul class="steps">
                                            <li data-step="1">
                                                <span class="step">১</span>
                                                <span class="title">ব্যক্তিগত পরিচিতি</span>
                                            </li>
                                            <li data-step="2">
                                                <span class="step">২</span>
                                                <span class="title">পারিবারিক পরিচিতি</span>
                                            </li>
                                            <li data-step="3">
                                                <span class="step">৩</span>
                                                <span class="title">অর্থনৈতিক অবস্থা</span>
                                            </li>
                                            <li data-step="4" class="active">
                                                <span class="step">৪</span>
                                                <span class="title">যে সেবার জন্য আগ্রহী</span>
                                            </li>
                                            <li data-step="5">
                                                <span class="step">৫</span>
                                                <span class="title">আবেদনকারীর ছবি</span>
                                            </li>
                                            <li data-step="6">
                                                <span class="step">৬</span>
                                                <span class="title">প্রত্যায়ন</span>
                                            </li>
                                            <li data-step="6">
                                                <span class="step">৭</span>
                                                <span class="title">ফলাফল</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr />
                                    <div class="step-content pos-rel">
                                        <!-- step 01 -->
                                        <div data-step="1" class="complete"></div>
                                        <!-- step 2 -->
                                        <div data-step="2" class="complete"></div>
                                        <!-- step 3 --> 
                                        <div data-step="3" class="complete"></div>
                                        <!-- step 4 active-->
                                        <div data-step="4" class="active">
                                            <?=form_open(site_url("service/interested_service"), array("class" => "form-horizontal"))?>
                                                <!-- current service -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('curr_service')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Curr_service">বর্তমানে কোন ধরনের সেবা পাচ্ছেনঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="col-md-4 clearfix">
                                                            <select class="input-large" id="curr_service" name="curr_service">
                                                                <option >নির্বাচন করুন</option>
                                                                <option value="a">ভিজিএফ</option>
                                                                <option value="b">ভিজিডি</option>
                                                                <option value="c">মাতৃত্বকালীন ভাতা</option>
                                                                <option value="d">মুক্তিযোদ্ধা ভাতা</option>
                                                                <option value="e">প্রতিবন্ধী ভাতা</option>
                                                                <option value="f">বিধবা ভাতা</option>
                                                                <option value="g">ববয়স্ক ভাতা</option>

                                                            </select>
                                                        </div>
                                                        <span class="col-md-6 help-block">
                                                            <?=form_error('curr_service')?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- interested service -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('inter_service')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Inter_service">কোন ধরনের সেবার জন্য আগ্রহীঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="col-md-4 clearfix">
                                                            <select class="input-large" id="inter_service" name="inter_service">
                                                                <option >নির্বাচন করুন</option>
                                                                <option value="a">ভিজিএফ</option>
                                                                <option value="b">ভিজিডি</option>
                                                                <option value="c">মাতৃত্বকালীন ভাতা</option>
                                                                <option value="d">মুক্তিযোদ্ধা ভাতা</option>
                                                                <option value="e">প্রতিবন্ধী ভাতা</option>
                                                                <option value="f">বিধবা ভাতা</option>
                                                                <option value="g">ববয়স্ক ভাতা</option>
                                                            </select>
                                                        </div>
                                                        <span class="col-md-6 help-block">
                                                            <?=form_error('inter_service')?>
                                                        </span>
                                                    </div>
                                                </div> 
                                                <!-- way of success -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('success_way')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Success_way">দারিদ্রতা থেকে উত্তরনের উপায়ঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <textarea class="col-xs-12 col-md-6 input-xlarge" name="success_way" id="success_way" rows="2" placeholder="" ></textarea>
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('success_way')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- interested training -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('inter_trainingss')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Inter_trainings">কি ধরনের প্রশিক্ষণের জন্য আগ্রহিঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-4" type="text" name="inter_trainings" id="inter_trainings"  placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('inter_trainings')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- reference -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('reference')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Reference">সুপারিশ কারীর নামঃ (কমপক্ষে তিনজন) </label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="reference" id="reference" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('reference')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>               
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="wizard-actions">
                                    <button class="btn btn-prev">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        পূর্ববর্তী
                                    </button>
                                    <button class="btn btn-success" type="submit">
                                        পরবর্তী
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>
                            <?=form_close()?>
                        </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
                </div>
            </div>
        </div>
    </div>
</div>