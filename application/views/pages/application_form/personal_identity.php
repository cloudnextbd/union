<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
</div>
<!-- Info Content  - Principal Info-->
<div class="content_info">
    <div class="paddings">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="fuelux-wizard-container">.
                                    <?=validation_errors()?>
                                    <!-- form header -->
                                    <div>
                                        <ul class="steps">
                                            <li data-step="1" class="active">
                                                <span class="step">১</span>
                                                <span class="title">ব্যক্তিগত পরিচিতি</span>
                                            </li>

                                            <li data-step="2">
                                                <span class="step">২</span>
                                                <span class="title">পারিবারিক পরিচিতি</span>
                                            </li>

                                            <li data-step="3">
                                                <span class="step">৩</span>
                                                <span class="title">অর্থনৈতিক অবস্থা</span>
                                            </li>

                                            <li data-step="4">
                                                <span class="step">৪</span>
                                                <span class="title">যে সেবার জন্য আগ্রহী</span>
                                            </li>

                                            <li data-step="5">
                                                <span class="step">৫</span>
                                                <span class="title">আবেদনকারীর ছবি</span>
                                            </li>

                                            <li data-step="6">
                                                <span class="step">৬</span>
                                                <span class="title">প্রত্যায়ন</span>
                                            </li>

                                            <li data-step="6">
                                                <span class="step">৭</span>
                                                <span class="title">ফলাফল</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr />
                                    <!-- form body -->
                                    <div class="step-content pos-rel">
                                        <!-- step 01 -->
                                        <div class="step-pane active" data-step="1">
                                            <?= form_open(site_url("service/personal_identity"), array("class" => "form-horizontal")) ?>
                                            <!-- name -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('name')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Name">নামঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-md-6" type="text" name="name" id="name" placeholder="বিঃদ্রঃ ভোটার আইডি কার্ডে যে নাম দেয়া আছে" required />
                                                        <span class="col-md-6 help-block">
                                                            <?=form_error('name')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- gender -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('gender')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Gender">লিঙ্গঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-md-3">
                                                    <div class="radio">
                                                        <label>
                                                            <input class="ace" type="radio" name="gender" value="male" checked>
                                                            <span class="lbl"> পুরুষ </span> 
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="gender" value="female">
                                                            <span class="lbl"> মহিলা </span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="gender" value="other">
                                                            <span class="lbl"> অনান্য </span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <span class="col-md-6 help-block">
                                                    <?=form_error('gender')?>
                                                </span>
                                            </div>
                                            <!-- birth date -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('birth_date')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Birth_date">জন্ম তারিখঃ:  <sup style="color: red">*</sup> </label>
                                                <div class="col-xs-12 col-md-9">
                                                    <div class="clearfix">
                                                        <div class="input-group">
                                                            <input class="form-control col-xs-12 col-md-4 date-picker" id="id-date-picker-1" data-date-format="dd-mm-yyyy" name="birth_date" placeholder="dd-mm-yyyy" required/>
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('birth_date')?>
                                                            </span>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <!-- birth id no -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('birth_id')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Birth_id">জন্ম নিবন্ধন নংঃ </label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-md-6" type="text" name="birth_id" id="birth_id" onkeyup="//check_birth_id(this.value)" value="" required/>
                                                        <span id="birth_result" class="col-md-6 help-block">
                                                            <?=form_error('birth_id')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- nid -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('nid')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Nid">জাতীয় পরিচয় পত্র নংঃ <sup style="color: red">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-md-6" type="text" name="nid" id="nid" onkeyup="//check_nid(this.value)" value="" required/>
                                                        <span id="nid_result" class="col-md-6 help-block">
                                                            <?=form_error('nid')?>
                                                        </span>
                                                    </div>
                                               </div>
                                            </div>
                                            <!-- phone -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('mobile')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Mobile">মোবাইল নংঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-md-6" type="text" name="mobile" id="mobile" placeholder="+880 167-2552-966" required/>
                                                        <span class="col-md-6 help-block">
                                                            <?=form_error('mobile')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hr hr-dotted"></div>
                                            <!--  state -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="District">জেলাঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-9">
                                                    <div class="clearfix">
                                                        <label class="control-label" style="color: #777" >চাঁদপুর</label>
                                                        <input type="hidden" name="district" id="district" value="chand pur" required/>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- sub state -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Sub_district">উপজেলাঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <label class="control-label" style="color: #777" >চাঁদপুর সদর</label>
                                                    <input type="hidden" name="sub_district" id="sub_district" value="chand pur shodor" required/>
                                                </div>
                                            </div>
                                            <!-- union -->
                                            <div class="space-2"></div>
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Union">ইউনিয়নঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <label class="control-label" style="color: #777" >বাগাদী</label>
                                                    <input type="hidden" name="union" id="union" value="bagadi" required/>
                                                </div>
                                            </div>
                                            <!-- word no -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('word_no')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                   echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Word_no">ওয়ার্ড নংঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-9">
                                                    <div class="clearfix">
                                                        <select class="col-md-3 input-large" id="word_no" name="word_no" required>
                                                            <option selected >নির্বাচন করুন</option>
                                                            <option value="a">1</option>
                                                            <option value="v">2</option>
                                                            <option value="c">3</option>
                                                            <option value="">4</option>
                                                            <option value="">5</option>
                                                            <option value="">6</option>
                                                            <option value="">7</option>
                                                            <option value="">8</option>
                                                            <option value="">9</option>
                                                        </select>
                                                        <span class="col-md-9 help-block">
                                                            <?=form_error('word_no')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- village -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('village')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Village">গ্রামের নামঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <select class="col-md-3 input-large" id="village" name="village" required>
                                                            <option selected>নির্বাচন করুন</option>
                                                            <option value="a">রাড়িরচর ও চান্দের বাগ</option>
                                                            <option value="v">মনিহার</option>
                                                            <option value="c">দেবপুর</option>
                                                            <option value="">চরবাকিলা ও ছোটসুন্দর</option>
                                                            <option value="">আলগী</option>
                                                            <option value="">কামরাঙ্গা</option>
                                                            <option value="">বড়সুন্দর ও পাঁচ বাড়িয়া</option>
                                                            <option value="">রামপুর</option>
                                                            <option value="">সকদী পাঁচ গাঁও</option>
                                                        </select>
                                                        <span class="col-md-9 help-block">
                                                            <?=form_error('village')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- house name -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('house_name')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="House_name">বাড়ির নামঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="house_name" id="house_name" placeholder="" required/>
                                                        <span class="col-md-6 help-block">
                                                            <?=form_error('house_name')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- holding no -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('holdings_no')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Holdings_no">হোল্ডিং নংঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-4" type="text" name="holdings_no" id="holdings_no" placeholder="" required/>
                                                        <span class="col-md-6 help-block">
                                                            <?=form_error('holdings_no')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hr hr-dotted"></div>
                                            <!-- education -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('education')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Education">শিক্ষাগত যোগ্যতাঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <select class="col-md-3 input-large" id="education" name="education" required>
                                                            <option selected>নির্বাচন করুন</option>
                                                            <option value="a">ডিগ্রী </option>
                                                            <option value="b">এইচ এস সি</option>
                                                            <option value="c">এস এস সি </option>
                                                            <option value="">৮ম শ্রেণী </option>
                                                            <option value="">৫ম শ্রেণী </option>
                                                            <option value="">অক্ষর জ্ঞান</option>
                                                            <option value="">নিরক্ষর </option>
                                                        </select>
                                                        <span class="col-md-9 help-block">
                                                            <?=form_error('education')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- profession -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('profession')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Profession">পেশাঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="clearfix">
                                                        <input class="col-xs-12 col-sm-6" type="text" name="profession" id="profession" placeholder="" required/>
                                                        <span class="col-md-6 help-block">
                                                            <?=form_error('profession')?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- disability -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('disability')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Disability">অসামর্থ্যতা/প্রতিবন্ধী প্রকৃতিঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="col-md-9 radio">
                                                        <label>
                                                            <input class="ace" type="checkbox" name="disability" value="no" checked>
                                                            <span class="lbl"> প্রযোজ্য নয়</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="checkbox" name="disability" value="eye_dis">
                                                            <span class="lbl"> দৃষ্টি প্রতিবন্ধী</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="checkbox" name="disability" value="body_dis">
                                                            <span class="lbl"> শারীরিক প্রতিবন্ধী</span>
                                                        </label>
                                                    </div>
                                                    <span class="col-md-3 help-block">
                                                        <?=form_error('disability')?>
                                                    </span>
                                                </div>
                                            </div>
                                            <!-- religion -->
                                            <div class="space-2"></div>
                                            <?php 
                                                if(form_error('religion')){ 
                                                    echo "<div class='form-group has-error'>";
                                                }else{    
                                                    echo "<div class='form-group'>";
                                                } 
                                            ?>
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Religion">ধর্মঃ <sup style="color:red;">*</sup></label>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="col-md-9 radio">
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="islame" checked>
                                                            <span class="lbl"> ইসলাম</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="hidnu">
                                                            <span class="lbl"> হিন্দু</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="christians">
                                                            <span class="lbl"> খ্রিষ্টান</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="buddha">
                                                            <span class="lbl"> বৌদ্ধ</span>
                                                        </label>
                                                        <label>
                                                            <input class="ace" type="radio" name="religion" value="other">
                                                            <span class="lbl"> অনান্য </span>
                                                        </label>
                                                    </div>
                                                    <span class="col-md-3 help-block">
                                                        <?=form_error('religion')?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div><!-- step 1/end -->
                                    </div>
                                </div>
                                <hr />
                                <div class="wizard-actions">
                                    <button class="btn btn-prev">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        পূর্ববর্তী
                                    </button>
                                    <button class="btn btn-success" type="submit">
                                        পরবর্তী
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>
                                <?= form_close() ?>
                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>