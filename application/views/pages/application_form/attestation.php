<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>

            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
    <!-- End breadcrumbs-->
</div>

<!-- Info Content  - Principal Info-->
<div class="content_info">
    <div class="paddings">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>

                        <div class="widget-body">

                            <div class="widget-main">
                                <div id="fuelux-wizard-container">

                                    <!-- --------------------- form header --------------------- -->
                                    <div>
                                        <ul class="steps">

                                            <li data-step="1">
                                                <span class="step">১</span>
                                                <span class="title">ব্যক্তিগত পরিচিতি</span>
                                            </li>

                                            <li data-step="2">
                                                <span class="step">২</span>
                                                <span class="title">পারিবারিক পরিচিতি</span>
                                            </li>

                                            <li data-step="3">
                                                <span class="step">৩</span>
                                                <span class="title">অর্থনৈতিক অবস্থা</span>
                                            </li>

                                            <li data-step="4">
                                                <span class="step">৪</span>
                                                <span class="title">যে সেবার জন্য আগ্রহী</span>
                                            </li>

                                            <li data-step="5">
                                                <span class="step">৫</span>
                                                <span class="title">আবেদনকারীর ছবি</span>
                                            </li>

                                            <li data-step="6" class="active">
                                                <span class="step">৬</span>
                                                <span class="title">প্রত্যায়ন</span>
                                            </li>

                                            <li data-step="7">
                                                <span class="step">৭</span>
                                                <span class="title">ফলাফল</span>
                                            </li>
                                        </ul>
                                    </div>

                                    <hr />

                                    <div class="step-content pos-rel">

                                        <!-- step 01 -->
                                        <div data-step="1" class="complete"></div>

                                        <!-- step 2 -->
                                        <div data-step="2" class="complete"></div>
                                        
                                        <!-- step 3 --> 
                                        <div data-step="3" class="complete"></div>
                                        
                                        <!-- step 4 -->
                                        <div data-step="4" class="complete"></div>
                                        
                                        <!-- step 5 -->
                                        <div data-step="5" class="complete"></div>
                                        
                                        <!-- step 6 -->
                                        <div data-step="6" class="active">
                                            
                                            <form class="form-horizontal" id="validation-form" method="get">
                                                <div class="form-group">
                                                    <div class="col-xs-12 col-sm-4 col-sm-offset-3">
                                                        <label>
                                                            <input name="agree" id="agree" type="checkbox" class="ace">
                                                            <span class="lbl"> আমি সজ্ঞানে সকল তথ্য সঠিক বলিয়া নিশ্চিত করিলাম।</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </form>
                                            
                                        </div>
                                        
                                    </div>
                                </div>

                                <hr />
                                <div class="wizard-actions">
                                    <button class="btn btn-prev">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        পূর্ববর্তী
                                    </button>

                                    <button class="btn btn-success btn-next" data-last="Finish">
                                        পরবর্তী
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>


                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->

                    </div>
                </div>
            </div>
        </div>
    </div>