<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>

            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
</div>
<!-- Info Content  - Principal Info-->
<div class="content_info">
    <div class="paddings">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="fuelux-wizard-container">
                                    <!-- form header -->
                                    <div>
                                        <ul class="steps">
                                            <li data-step="1">
                                                <span class="step">১</span>
                                                <span class="title">ব্যক্তিগত পরিচিতি</span>
                                            </li>
                                            <li data-step="2">
                                                <span class="step">২</span>
                                                <span class="title">পারিবারিক পরিচিতি</span>
                                            </li>
                                            <li data-step="3" class="active">
                                                <span class="step">৩</span>
                                                <span class="title">অর্থনৈতিক অবস্থা</span>
                                            </li>
                                            <li data-step="4">
                                                <span class="step">৪</span>
                                                <span class="title">যে সেবার জন্য আগ্রহী</span>
                                            </li>
                                            <li data-step="5">
                                                <span class="step">৫</span>
                                                <span class="title">আবেদনকারীর ছবি</span>
                                            </li>
                                            <li data-step="6">
                                                <span class="step">৬</span>
                                                <span class="title">প্রত্যায়ন</span>
                                            </li>
                                            <li data-step="6">
                                                <span class="step">৭</span>
                                                <span class="title">ফলাফল</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr />
                                    <div class="step-content pos-rel">
                                        <!-- step 01 -->
                                        <div data-step="1" class="complete"></div>
                                        <!-- step 2 -->
                                        <div data-step="2" class="complete"></div>
                                        <!-- step 3 --> 
                                        <div data-step="3" class="active">
                                            <?=form_open(site_url("service/finance"), array("class" => "form-horizontal"))?>
                                                <!-- house definition -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('house_desc')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="House_desc">বাসস্থানের অবস্থার বিবরণঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <textarea class="col-xs-12 col-md-6 input-xlarge" name="house_desc" id="house_desc" rows="7" placeholder="" ></textarea>
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('house_desc')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- image -->
                                                <div class="space-2"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">বাসস্থানের ছবিঃ</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-sm-12" type="file" name="" id="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- land -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('land')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Land">জমির পরিমানঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="land" id="land"  placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('land')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- number of earning person -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('earn_person')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Earn_person">উপার্জনক্ষম সদস্য সংখাঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="earn_person" id="earn_person"  placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('earn_person')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                              <!-- earning source -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('earn_source')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Earn_source">আয়ের উৎসঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="earn_source" id="earn_source"  placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('earn_source')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- monthly income -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('monthly_income')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Monthly_income">মাসিক আয়ঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-4" type="text" name="monthly_income" id="monthly_income"  placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('monthly_income')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- cause of poorness -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('cause_poorness')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Cause_poorness">দারিদ্র্যের কারনঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <textarea class="col-xs-12 col-md-6 input-xlarge" name="cause_poorness" id="cause_poorness" rows="3" placeholder="" ></textarea>
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('cause_poorness')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>                      
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="wizard-actions">
                                    <button class="btn btn-prev">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        পূর্ববর্তী
                                    </button>
                                    <button class="btn btn-success" type="submit">
                                        পরবর্তী
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>
                            <?=form_close()?>
                        </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
                </div>
            </div>
        </div>
    </div>
</div>