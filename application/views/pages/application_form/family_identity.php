<!-- Info Content - Section Title-->
<div class="content_info">
    <!-- breadcrumbs-->
    <div class="breadcrumbs breadcrumbs-sections">
        <ul>
            <li class="breadcrumbs-home">
                <a href="#" title="Back To Home">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="#">Features</a>
            </li>
            <li>

            </li>
            <li>
                Page Full Widht
            </li>
        </ul>
    </div>
</div>
<!-- Info Content -->
<div class="content_info">
    <div class="paddings">
        <!-- Container Area - Boxes Services -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box">
                        <div class="widget-header widget-header-blue widget-header-flat">
                            <h4 class="widget-title lighter">সুবিধা প্রাপ্তির জন্য  আবেদন ফরম</h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="fuelux-wizard-container">
                                    <?=validation_errors()?>
                                    <?php
                                       if(isset($_SESSION['id'])){
                                            echo $_SESSION['id'];
                                        }
                                    ?>
                                    
                                    <!-- form header -->
                                    <div>
                                        <ul class="steps">
                                            <li data-step="1">
                                                <span class="step">১</span>
                                                <span class="title">ব্যক্তিগত পরিচিতি</span>
                                            </li>
                                            <li data-step="2" class="active">
                                                <span class="step">২</span>
                                                <span class="title">পারিবারিক পরিচিতি</span>
                                            </li>
                                            <li data-step="3">
                                                <span class="step">৩</span>
                                                <span class="title">অর্থনৈতিক অবস্থা</span>
                                            </li>
                                            <li data-step="4">
                                                <span class="step">৪</span>
                                                <span class="title">যে সেবার জন্য আগ্রহী</span>
                                            </li>
                                            <li data-step="5">
                                                <span class="step">৫</span>
                                                <span class="title">আবেদনকারীর ছবি</span>
                                            </li>
                                            <li data-step="6">
                                                <span class="step">৬</span>
                                                <span class="title">প্রত্যায়ন</span>
                                            </li>
                                            <li data-step="6">
                                                <span class="step">৭</span>
                                                <span class="title">ফলাফল</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr />
                                    <!-- form body -->
                                    <div class="step-content pos-rel">
                                        <!-- step 01 -->
                                        <div data-step="1" class="complete"></div>
                                        <!-- step 2 -->
                                        <div data-step="2" class="active">
                                            <?=form_open(site_url("service/family_identity"), array("class" => "form-horizontal"))?>
                                                <!-- father name -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('father_name')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Father_name">পিতার নামঃ</label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="father_name" id="father_name" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('father_name')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- father nid -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('father_nid')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                       echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Father_nid">জাতীয় পরিচয় পত্র নংঃ</label>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="father_nid" id="father_nid" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('father_nid')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- mother name -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('mother_name')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Mother_name">মাতার নামঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="mother_name" id="mother_name" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('mother_name')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- mother nid -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('mother_nid')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Mother_nid">জাতীয় পরিচয় পত্র নংঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="mother_nid" id="mother_nid" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('mother_nid')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- marital_status -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('marital_status')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right">বৈবাহিক অবস্থাঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="col-md-4 radio">
                                                            <label>
                                                                <input class="ace" type="radio" name="marital_status" value="yes" >
                                                                <span class="lbl"> হ্যা</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="marital_status" value="no" >
                                                                <span class="lbl"> না</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="marital_status" value="widow" >
                                                                <span class="lbl"> বিধবা</span>
                                                            </label>
                                                            <label>
                                                                <input class="ace" type="radio" name="marital_status" value="bipotnik" >
                                                                <span class="lbl"> বিপত্নীক </span>
                                                            </label>
                                                        </div>
                                                        <span class="col-md-8 help-block">
                                                            <?=form_error('marital_status')?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- husband name -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('husband_name')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>  
                                                <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Husband_name">স্বামীর নামঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="husband_name" id="husband_name" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('husband_name')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- husband nid -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('husband_nid')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Husband_nid">জাতীয় পরিচয় পত্র নংঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="husband_nid" id="husband_nid" placeholder="husband_nid_no" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('husband_nid')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- wife name -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('wife_name')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Wife_name">স্ত্রীর নামঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="wife_name" id="wife_name" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('wife_name')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- wife nid -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('wife_nid')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="wife_nid">জাতীয় পরিচয় পত্র নংঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-6" type="text" name="wife_nid" id="wife_nid" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('wife_nid')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- family member  -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('family_member')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Family_member">পরিবারের সদস্য সংখাঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <input class="col-xs-12 col-md-3" type="text" name="family_member" id="family_member" placeholder="" />
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('family_member')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- family description  -->
                                                <div class="space-2"></div>
                                                <?php 
                                                    if(form_error('family_desc')){ 
                                                        echo "<div class='form-group has-error'>";
                                                    }else{    
                                                        echo "<div class='form-group'>";
                                                    } 
                                                ?>
                                                    <label class="control-label col-xs-12 col-md-3 no-padding-right" for="Family_desc">পরিবারের সদস্যদের বর্ননাঃ</label>
                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="clearfix">
                                                            <textarea class="col-xs-12 col-md-6 input-xlarge" name="family_desc" id="family_desc" rows="7" placeholder=""></textarea>
                                                            <span class="col-md-6 help-block">
                                                                <?=form_error('family_desc')?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="wizard-actions">
                                    <button class="btn btn-prev">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        পূর্ববর্তী
                                    </button>
                                    <button class="btn btn-success" type="submit">
                                        পরবর্তী
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>
                            <?=form_close()?>
                        </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
                </div>
            </div>
        </div>
    </div>
</div>