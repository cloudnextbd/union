<div class="content_info">
    <div class="padding-top">
        
        <!-- Container Area -->
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                
                <div class="col-md-3">
                    <h3>নির্বাচন করুন</h3>
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">বিষ্ণপুর</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">আশিকাটি</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">শাহ্  মাহমুদপুর</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">কল্যাণপুর</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/rampur');?>" class="btn btn-primary">রামপুর</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">মৈশাদী</a>
                    </div>
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">তরপুচন্ডী</a>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <h3>নির্বাচন করুন</h3>
                    
                    <div class="form-group">
                        <a href="<?=base_url('home');?>" class="btn btn-primary">বাগাদী</a>
                    </div>
                    
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">লক্ষীপুর মডেল</a>
                    </div>
                    
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">হানারচর</a>
                    </div>
                    
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">চান্দ্রা</a>
                    </div>
                    
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">রাজরাজেশ্বর</a>
                    </div>
                    
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">ইব্রাহীমপুর</a>
                    </div>
                    
                    <div class="form-group">
                        <a href="<?=base_url('sub_district/coming_soon');?>" class="btn btn-primary">বালিয়া</a>
                    </div>
                    
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <!-- End Container Area -->
        
    </div>
</div>