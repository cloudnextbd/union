<!-- Container Area  -  Slide and tabs -->
                <div class="content_info">
                  <div class="container">
                      <div class="row">
                           <!-- Content Breadcumbs And Slide-->
                          <div class="col-md-12">
                              <!-- breadcrumbs-->
                              <div class="breadcrumbs">
                                  <ul>
                                      <li class="breadcrumbs-home">
                                          <a href="#" title="Back To Home">
                                            <i class="fa fa-home"></i>
                                          </a>
                                      </li>
                                      <li style="font-weight: bold">
                                          বাগাদী ইউনিয়নের কার্য্যকর উন্নয়নের লক্ষ্যে চাঁদপুর সদরের  ইউ এন ও  জনাব  উদয়ন দেওয়ান স্যার কর্তৃক উদ্ভাবিত সরকারি , বেসরকারি , সামাজিক , সাংগঠনিক ও ব্যক্তিপর্যায়ে সমন্বিত উন্নয়ন প্লাটফর্ম  এ আপনাকে স্বাগতম ...
                                      </li>
                                  </ul>
                              </div>
                              <!-- End breadcrumbs-->

                              <!-- Slide Section-->    
                              <div class="tp-banner-container">
                                  <div class="tp-banner" >
                                      <ul> 
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/1.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/2.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/3.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/4.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/5.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/6.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/7.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/8.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/9.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/10.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                          <li data-transition="zoomout" data-slotamount="7"  data-masterspeed="1500">
                                              <img src="<?php echo base_url(); ?>assets/img/rampur_slide/11.jpg"  alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                              <p class="tp-caption">রামপুর ইউনিয়ন<br><br>
                                                <a class="btn btn-success" href="#">Details</a>
                                              </p>
                                          </li>
                                      </ul> 
                                      <!-- END SLIDES  --> 
                                      <div class="tp-bannertimer"></div>  
                                  </div>
                              </div>       
                              <!-- End Slide Section-->
                          </div>
                          <!-- End Content Breadcumbs And Slide-->
                      </div>
                  </div>
                </div>
                <!-- End Container Area - Slide and tabs -->