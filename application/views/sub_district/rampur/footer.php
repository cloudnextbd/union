<!-- footer-->
            <footer id="footer">
                <!-- Services-lines Items Container -->
<!--                <div class="container">
                    <div class="row">
                         Services-lines Items 
                        <div class="col-md-12 services-lines-container">
                            <ul class="services-lines">
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-credit-card"></i>
                                        <h5>Your Payments</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                 <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-industry"></i>
                                        <h5>District Tax Payment</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-university"></i>
                                        <h5>Paying Taxes - CoopBank</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                            </ul>

                            <ul class="services-lines no-margin">
                                <li>
                                    <div class="item-service-line">
                                       <i class="fa fa-balance-scale"></i>
                                        <h5>Payment of contributions</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                 <li>
                                    <div class="item-service-line">
                                       <i class="fa fa-cc"></i>
                                        <h5>Settlement and severance pay.</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-line-chart"></i>
                                        <h5>Workers pay contributions</h5>
                                        <a href="#">Go To</a>
                                    </div>
                                </li>
                            </ul>
                        </div> 
                         End Services-lines Items  
                    </div>
                </div>-->
                <!-- Services-lines Items Container -->

                <!-- Items Footer -->
                <div class="container">
                    <div class="row paddings-mini">
                        <!-- contact Items Footer -->
                        <div class="col-sm-6 col-md-3">
                            <div class="border-right txt-right">
                                <h4>যোগাযোগ</h4>
                                <ul class="contact-footer">
                                    <li>
                                        <i class="fa fa-envelope"></i> <a href="#">unochandpursadar@gmail.com</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-headphones"></i> <a href="#">01730067060</a>
                                     </li>
                                    <li class="location">
                                        <i class="fa fa-home"></i> <a href="#">বাগাদী ইউনিয়ন, চাঁদপুর সদর উপজেলা</a>
                                    </li>                                   
                                </ul>
                                <div class="logo-footer">
                                    
                                    <img src="<?php echo base_url(); ?>assets/front/img/logo.png" alt="">
                                </div>
                           </div>
                        </div>
                        <!-- End contact items Footer -->

                        <!-- Recent Links Items Footer -->
                        <div class="col-sm-6 col-md-3">
                              <div class="border-right border-right-none">
                                  <h4>অন্যান্য </h4>
                                  <ul class="list-styles">
                                      <li><i class="fa fa-check"></i> <a href="#">চেয়ারম্যান পরিচিতি</a></li>
                                      <li><i class="fa fa-check"></i> <a href="#">সরকারী অফিস সমূহ</a></li>
                                      <li><i class="fa fa-check"></i> <a href="#">এক নজরে বাগাদী</a></li>
                                      <li><i class="fa fa-check"></i> <a href="#">বিভিন্ন তালিকা</a></li>
                                  </ul>
                             </div>
                        </div>
                        <!-- End Recent Links Items Footer -->

                        <!-- Recent Newsletter Footer -->
                        <div class="col-sm-6 col-md-4">
                            <div class="border-right txt-right">
                                <h4>সর্বশেষ অবস্থা জানুন</h4>
                                <p>সর্বশেষ অবস্থা জানুন</p>
                                <form id="newsletterForm" class="newsletterForm" action="http://html.iwthemes.com/coopbank/php/mailchip/newsletter-subscribe.php">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control" placeholder="Email Address" name="email"  type="email" required="required">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="submit" name="subscribe" >Go!</button>
                                        </span>
                                    </div>
                                </form>   
                                <div id="result-newsletter"></div>    
                           </div>
                        </div>
                        <!-- End Newsletter Footer -->

                        <!-- Follow Items Footer -->
                        <div class="col-sm-6 col-md-2">
                            <div class="border-right-none">
                              <h4>Follow To বাগাদী ইউনিয়ন</h4>
                              <ul class="social">
                                  <li class="facebook"><span><i class="fa fa-facebook"></i></span><a href="#">Facebook</a></li>
                                  <li class="twitter"><span><i class="fa fa-twitter"></i></span><a href="#">Twitter</a></li>
                                  <li class="github"><span><i class="fa fa-github"></i></span><a href="#">Github</a></li>
                                  <li class="linkedin"><span><i class="fa fa-linkedin"></i></span><a href="#">Linkedin</a></li>
                              </ul>
                            </div>
                        </div>
                        <!-- End Follow Items Footer -->
                    </div>
                </div>
                <!-- End Items Footer -->

                <!-- footer Down-->
                <div class="footer-down">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7">
                                <!-- Nav Footer-->
                                <ul class="nav-footer">
                                    <li><a href="#">HOME</a> </li>
                                    <li><a href="#">COMPANY</a></li>
                                    <li><a href="#">SERVICES</a></li> 
                                    <li><a href="#">NEWS</a></li> 
                                    <li><a href="#">PORTFOLIO</a></li>                
                                    <li><a href="#">CONTACT</a></li>
                                </ul>
                                <!-- End Nav Footer-->
                            </div>
                            <div class="col-md-5">
                                <p>&copy; 2015 CoopBank. All Rights Reserved.  2010 - 2015</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer Down-->
            </footer>      
            <!-- End footer-->
        </div>
        <!-- End layout-->

        <!-- JQuery libs for data table start -->
        <!-- jQuery local--> 
        <script src="<?php echo base_url(); ?>assets/front/js/libs/jquery.js"></script>  
        <script src="<?php echo base_url(); ?>assets/front/js/libs/jquery-ui.1.10.4.min.js"></script> 

        <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/data_table/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/data_table/css/buttons.dataTables.min.css">

        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/jquery-1.12.4.js"></script>

        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/buttons.flash.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/jszip.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/pdfmake.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/vfs_fonts.js"></script>
        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/buttons.html5.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<?=base_url()?>assets/front/data_table/js/buttons.print.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#myTable').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                } );
            } );
        </script>
        
        <!-- JQuery libs for data table end --> 
        
        
        
        <!--Totop-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/totop/jquery.ui.totop.js" ></script>   
        <!--Slide Revolution-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/rs-plugin/js/jquery.themepunch.tools.min.js" ></script>      
        <script type='text/javascript' src='<?php echo base_url(); ?>assets/front/js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>    
        <!-- Maps -->
        <script src="<?php echo base_url(); ?>assets/front/js/maps/gmap3.js"></script>            
        <!--Ligbox--> 
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/fancybox/jquery.fancybox.js"></script> 
        <!-- owl.carousel.min.js-->
        <script src="<?php echo base_url(); ?>assets/front/js/carousel/owl.carousel.min.js"></script>
        <!-- Filter -->
        <script src="<?php echo base_url(); ?>assets/front/js/filters/jquery.isotope.js" type="text/javascript"></script>
        <!-- Parallax-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/parallax/parallax.min.js"></script>  
        <!--Theme Options-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/theme-options/theme-options.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/theme-options/jquery.cookies.js"></script> 
        <!-- Bootstrap.js-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap/bootstrap.js"></script> 
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap/bootstrap-slider.js"></script> 
        <!--MAIN FUNCTIONS-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/main.js"></script>
        <!-- ======================= End JQuery libs =========================== -->

        <!--Slider Function-->
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('.tp-banner').show().revolution({
                    dottedOverlay:"none",
                    delay:5000,
                    startwidth:1170,
                    startheight:390,
                    minHeight:350,
                    navigationType:"none",
                    navigationArrows:"solo",
                    navigationStyle:"preview"
                });             
            }); //ready
        </script>
        <!--End Slider Function-->



<!-- *********************** extra form *********************** -->

        <!--[if IE]>
            <script src="assets/js/jquery-1.11.3.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url(); ?>assets/form/js/bootstrap-datepicker.min.js"></script>

        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>

        <!-- page specific plugin scripts -->
        <script src="<?php echo base_url(); ?>assets/form/js/wizard.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/form/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/form/js/jquery-additional-methods.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/form/js/bootbox.js"></script>
        <script src="<?php echo base_url(); ?>assets/form/js/jquery.maskedinput.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/form/js/select2.min.js"></script>
        
        
        <!--my multi select1-->
        <!-- <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>-->

        <!-- ace scripts -->
        <script src="<?php echo base_url(); ?>assets/form/js/ace-elements.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/form/js/ace.min.js"></script>
        
        <!-- ran js valid -->
        <script src="<?php //echo base_url(); ?>assets/front/valid.js"></script>
        
        <script type="text/javascript">
            jQuery(function($){
                $('.select2').css('width','auto').select2({allowClear:true})
                $('#select2-multiple-style .btn').on('click', function(e){
                        var target = $(this).find('input[type=radio]');
                        var which = parseInt(target.val());
                        if(which == 2) $('.select2').addClass('tag-input-style');
                         else $('.select2').removeClass('tag-input-style');
                });
            }
        </script>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">
            jQuery(function($) {


                //datepicker plugin
                //link
                $('.date-picker').datepicker({
                    autoclose: true,
                    todayHighlight: true
                })
                //show datepicker when clicking on the icon
                .next().on(ace.click_event, function(){
                    $(this).prev().focus();
                });
            
            

                $('[data-rel=tooltip]').tooltip();
            
                $('.select2').css('width','200px').select2({allowClear:true})
                .on('change', function(){
                    $(this).closest('form').validate().element($(this));
                }); 
            
            
                var $validation = false;
                $('#fuelux-wizard-container')
                .ace_wizard({
                    //step: 2 //optional argument. wizard will jump to step "2" at first
                    //buttons: '.wizard-actions:eq(0)'
                })
                .on('actionclicked.fu.wizard' , function(e, info){
                    if(info.step == 1 && $validation) {
                        if(!$('#validation-form').valid()) e.preventDefault();
                    }
                })
                //.on('changed.fu.wizard', function() {
                //})
                .on('finished.fu.wizard', function(e) {
                    bootbox.dialog({
                        message: "Thank you! Your information was successfully saved!", 
                        buttons: {
                            "success" : {
                                "label" : "OK",
                                "className" : "btn-sm btn-primary"
                            }
                        }
                    });
                }).on('stepclick.fu.wizard', function(e){
                    //e.preventDefault();//this will prevent clicking and selecting steps
                });
            
            
                //jump to a step
                /**
                var wizard = $('#fuelux-wizard-container').data('fu.wizard')
                wizard.currentStep = 3;
                wizard.setState();
                */
            
                //determine selected step
                //wizard.selectedItem().step
            
            
            
                //hide or show the other form which requires validation
                //this is for demo only, you usullay want just one form in your application
                // $('#skip-validation').removeAttr('checked').on('click', function(){
                //     $validation = this.checked;
                //     if(this.checked) {
                //         $('#sample-form').hide();
                //         $('#validation-form').removeClass('hide');
                //     }
                //     else {
                //         $('#validation-form').addClass('hide');
                //         $('#sample-form').show();
                //     }
                // })
            
            
            
                //documentation : http://docs.jquery.com/Plugins/Validation/validate
            
            
                // $.mask.definitions['~']='[+-]';
                // $('#phone').mask('(999) 999-9999');
            
                // jQuery.validator.addMethod("phone", function (value, element) {
                //     return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
                // }, "Enter a valid phone number.");
            
                // $('#validation-form').validate({
                //     errorElement: 'div',
                //     errorClass: 'help-block',
                //     focusInvalid: false,
                //     ignore: "",
                //     rules: {
                //         email: {
                //             required: true,
                //             email:true
                //         },
                //         password: {
                //             required: true,
                //             minlength: 5
                //         },
                //         password2: {
                //             required: true,
                //             minlength: 5,
                //             equalTo: "#password"
                //         },
                //         name: {
                //             required: true
                //         },
                //         phone: {
                //             required: true,
                //             phone: 'required'
                //         },
                //         url: {
                //             required: true,
                //             url: true
                //         },
                //         comment: {
                //             required: true
                //         },
                //         state: {
                //             required: true
                //         },
                //         platform: {
                //             required: true
                //         },
                //         subscription: {
                //             required: true
                //         },
                //         gender: {
                //             required: true,
                //         },
                //         agree: {
                //             required: true,
                //         }
                //     },
            
                //     messages: {
                //         email: {
                //             required: "Please provide a valid email.",
                //             email: "Please provide a valid email."
                //         },
                //         password: {
                //             required: "Please specify a password.",
                //             minlength: "Please specify a secure password."
                //         },
                //         state: "Please choose state",
                //         subscription: "Please choose at least one option",
                //         gender: "Please choose gender",
                //         agree: "Please accept our policy"
                //     },
            
            
                //     highlight: function (e) {
                //         $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                //     },
            
                //     success: function (e) {
                //         $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                //         $(e).remove();
                //     },
            
                //     errorPlacement: function (error, element) {
                //         if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                //             var controls = element.closest('div[class*="col-"]');
                //             if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                //             else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                //         }
                //         else if(element.is('.select2')) {
                //             error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                //         }
                //         else if(element.is('.chosen-select')) {
                //             error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                //         }
                //         else error.insertAfter(element.parent());
                //     },
            
                //     submitHandler: function (form) {
                //     },
                //     invalidHandler: function (form) {
                //     }
                // });
            
                
                
                
                $('#modal-wizard-container').ace_wizard();
                $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
                
                
                /**
                $('#date').datepicker({autoclose:true}).on('changeDate', function(ev) {
                    $(this).closest('form').validate().element($(this));
                });
                
                $('#mychosen').chosen().on('change', function(ev) {
                    $(this).closest('form').validate().element($(this));
                });
                */
                
                
                $(document).one('ajaxloadstart.page', function(e) {
                    //in ajax mode, remove remaining elements before leaving page
                    $('[class*=select2]').remove();
                });
            })
        </script>


    </body>
</html>