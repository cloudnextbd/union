<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title>Bagadi Union</title>
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="CoopBank - Responsive HTML5 Template">
        <meta name="author" content="iwthemes.com">  

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- donation page css  -->
        <!-- <link rel="stylesheet" href="<?= base_url() ?>assets/front/donation/custom.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/front/donation/color.css" /> -->

        
        <!-- ------------------------ member form start  -->
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/form/css/bootstrap-datepicker3.min.css" />
        
        <!--for ref. multiselect-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/form/css/bootstrap-multiselect.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/form/css/select2.min.css" />

        <!-- ace styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/form/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/form/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/form/css/ace-skins.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/form/css/ace-rtl.min.css" />

        <!--[if lte IE 9]>
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/form/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <!-- <script src="<?php echo base_url(); ?>assets/form/js/ace-extra.min.js"></script> -->


        <!-- ------------------------ member form end  -->

        <!-- Theme CSS -->
        <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet" media="screen">

        <!-- Responsive CSS -->
        <link href="<?php echo base_url(); ?>assets/front/css/theme-responsive.css" rel="stylesheet" media="screen">
        <!-- Skins Theme -->
        <link href="#" rel="stylesheet" media="screen" class="skin">
        <link href="<?php echo base_url(); ?>assets/front/css/skins/green/green.css" rel="stylesheet" media="screen">

        <!-- Favicons -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/front/img/icons/favicon.ico">
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/front/img/icons/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/front/img/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/front/img/icons/apple-touch-icon-114x114.png">  

        <!-- Head Libs -->
        <script src="<?php echo base_url(); ?>assets/front/js/libs/modernizr.js"></script>

        <!--[if IE]>
            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/ie/ie.css">
        <![endif]-->

        <!--[if lte IE 8]>
            <script src="<?php echo base_url(); ?>assets/front/js/responsive/html5shiv.js"></script>
            <script src="<?php echo base_url(); ?>assets/front/js/responsive/respond.js"></script>
        <![endif]-->


        <script type="text/javascript">
            var global_base_url = "<?php echo site_url('/') ?>";
        </script>

    </head>
    <body>

        <!--Preloader-->
        <div class="preloader">
            <div class="status">&nbsp;</div>
        </div>
        <!--End Preloader-->

        <!-- Theme-options -->
        <div id="theme-options">

            <div class="openclose"></div>
            <div class="title">
                <span>THEME OPTIONS</span>
                <p>Choose a combination of predefined skins and layouts here.</p>
            </div>
            
            <span>GENERAL SKINS</span>
            <ul>      
                <li><a class="colorbox red" href="index-bank-20e99.html?theme=red" title="Red Skin"></a></li>
                <li><a class="colorbox blue" href="index-bank-2ca00.html?theme=blue" title="Blue Skin"></a></li>                    
                <li><a class="colorbox yellow" href="index-bank-22746.html?theme=yellow" title="Yellow Skin"></a></li>
                <li><a class="colorbox green" href="index-bank-2af91.html?theme=green" title="Green Skin"></a></li>
                <li><a class="colorbox orange" href="index-bank-2ceb0.html?theme=orange" title="Orange Skin"></a></li>
                <li><a class="colorbox purple" href="index-bank-2938c.html?theme=purple" title="Purple Skin"></a></li>
                <li><a class="colorbox pink" href="index-bank-2a820.html?theme=pink" title="Pink Skin"></a></li>
                <li><a class="colorbox cocoa" href="index-bank-26788.html?theme=cocoa" title="Cocoa Skin"></a></li>
            </ul>

            <span>HEADER BG SKINS</span>
            <ul>      
                <li class="color-header-1"></li>
                <li class="color-header-2"></li>                    
                <li class="color-header-3"></li>
                <li class="color-header-4"></li>
                <li class="color-header-5"></li>
                <li class="color-header-6"></li>
                <li class="color-header-7"></li>
                <li class="color-header-8"></li>
            </ul>

            <span>HEADER BG PATTERNS</span>
            <ul class="header-pattern">      
                <li class="pattern-header-01">01</li>
                <li class="pattern-header-02">02</li>                    
                <li class="pattern-header-03">03</li>
                <li class="pattern-header-none">None</li>
            </ul>

            <span>LAYOUT STYLE</span>
            <ul class="layout-style">      
                <li class="wide">Wide</li>
                <li class="semiboxed active">Semiboxed</li> 
                <li class="boxed">Boxed</li> 
                <li class="boxed-margin">Boxed Margin</li>               
            </ul>               
            <div class="patterns">
                <span>BODY BG PATTERNS</span>
                <ul class="backgrounds">
                    <li class="bgnone" title="None - Default"></li>
                    <li class="bg1"></li>
                    <li class="bg2"></li>
                    <li class="bg3"></li>
                    <li class="bg4 "></li>
                    <li class="bg5"></li> 
                    <li class="bg6"></li>
                    <li class="bg7"></li>
                    <li class="bg8"></li>
                    <li class="bg9 "></li>
                    <li class="bg10"></li> 
                    <li class="bg11"></li> 
                    <li class="bg12"></li> 
                    <li class="bg13"></li> 
                    <li class="bg14"></li> 
                    <li class="bg15"></li> 
                    <li class="bg16"></li> 
                    <li class="bg17"></li> 
                    <li class="bg18"></li> 
                    <li class="bg19"></li> 
                    <li class="bg20"></li> 
                    <li class="bg21"></li> 
                    <li class="bg22"></li> 
                    <li class="bg23"></li> 
                    <li class="bg24"></li> 
                    <li class="bg25"></li> 
                    <li class="bg26"></li>                   
                </ul>  
            </div>
        </div>
        <!-- End Theme-options --> 

        <!-- layout-->
        <div id="layout" class="layout-boxed-margin">
            <!-- fond-header-->
            <div id="fond-header" class="fond-header"></div>
            <!-- End fond-header-->

            <!-- Header Area -->
            <header id="header">
                <div class="row">
                    <!-- Logo Area -->
                    <div class="col-md-4 col-lg-5">
                        <div class="logo">
                            <div class="icon-logo">
                                <img src="<?php echo base_url(); ?>assets/front/img/logo.png" alt="">
                            </div>
                            <a href="<?php echo base_url(); ?>">
                                বাগাদী ইউনিয়ন
                                <span>চাঁদপুর সদর </span>
                            </a>
                        </div>
                    </div>
                    <!-- End Logo Area -->

                    <!-- Login Area -->
                    <h3>আমরা কি পারিনা,<br> একজন অসহায় মানুষের পাশে দাড়াতে একটু সহায়তা করতে</h3>
                    <h3>
                        <?php if(isset($this->user->info->role)) : ?>
                        
                            <?php if($this->user->info->role ==  0){ 
                                echo "Operator";
                            } elseif($this->user->info->role ==  1) {
                                echo "Supervisor";
                            } elseif($this->user->info->role ==  2) {
                                echo "Coordinator";
                            } ?>
                        
                        <?php endif; ?>
                    </h3>
                    <!-- End Login Area -->
                </div>
            </header>
            <!-- End Header Area -->

            <!-- content-central-->
            <div class="content-central">
                <!-- Nav-->
                <nav id="menu">
                    <div class="navbar yamm navbar-default">
                        <div class="">
                            <div class="">
                                <div class="navbar-header">
                                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                                    <!-- Nav Bar Items -->
                                    <ul class="nav navbar-nav">
                                        <!-- Home Nav Items -->
                                        <li class="dropdown">
                                            <a href="<?php echo base_url(); ?>">
                                                প্রচ্ছদ
                                            </a>

                                        </li>
                                        <!-- End Home Nav Items -->

                                        <!-- Company Nav Item -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                ইউনিয়নসর্ম্পকিত<b class="caret"></b>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>ইউনিয়ন পরিচিতি</strong>  
                                                                </li>
                                                                <li><a href=""></a></li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>ইতিহাস ও ঐতিহ্য</strong>
                                                                </li>
                                                            </ul>


                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>ভৌগলিক ও অর্থনৈতিক</strong>
                                                                </li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>অন্যান্য</strong>
                                                                </li>

                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- End Company Nav Item -->

                                        <!-- Beneficiary list -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                সুবিধাভোগী<b class="caret"></b>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সরকারি </strong></li>
                                                                <?php foreach($this->gov_services->menu as $gov_serv_menu) : ?>
                                                                <li><a href="<?=base_url()?>services/mem_list/<?=$gov_serv_menu->id?>" ><?=$gov_serv_menu->ser_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সরকারি </strong></li>
                                                                
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>বেসরকারি </strong></li>
                                                                <?php foreach($this->non_gov_services->menu as $non_gov_serv_menu) : ?>
                                                                <li><a href="<?=base_url()?>services/mem_list/<?=$non_gov_serv_menu->id?>" ><?=$non_gov_serv_menu->ser_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সামাজিক / ব্যাক্তিগত উদ্যোগ </strong></li>
                                                                
                                                                <?php foreach($this->personal_services->menu as $per_serv_menu) : ?>
                                                                <li><a href="<?=base_url()?>services/mem_list/<?=$per_serv_menu->id?>" ><?=$per_serv_menu->ser_name?></a></li>
                                                                <?php endforeach; ?>
                                                                
                                                                <?php foreach($this->social_services->menu as $soc_serv_menu) : ?>
                                                                <li><a href="<?=base_url()?>services/mem_list/<?=$soc_serv_menu->id?>" ><?=$soc_serv_menu->ser_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- Beneficiary list -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                সুবিধাবঞ্চিত<b class="caret"></b>
                                            </a>

                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সরকারি </strong></li>
                                                                
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সরকারি </strong></li>
                                                                
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>বেসরকারি </strong></li>
                                                                
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সামাজিক / ব্যাক্তিগত উদ্যোগ </strong></li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>
                                        </li>

                                        <!-- Beneficiary list -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                সুবিধা প্রদানকারী<b class="caret"></b>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সরকারি </strong></li>
                                                                <?php foreach($this->gov_serv_autho->menu as $gov_serv_autho_menu) : ?>
                                                                <li><a href="<?=base_url()?>services/gov_authority/<?=$gov_serv_autho_menu->id?>" ><?=$gov_serv_autho_menu->ser_auth_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সরকারি </strong></li>
                                                                
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>বেসরকারি </strong></li>
                                                                <?php foreach($this->non_gov_serv_autho->menu as $non_gov_serv_autho_menu) : ?>
                                                                <li><a href="<?=base_url()?>services/non_gov_authority/<?=$non_gov_serv_autho_menu->id?>" ><?=$non_gov_serv_autho_menu->ser_auth_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সামাজিক / ব্যাক্তিগত উদ্যোগ </strong></li>
                                                                <?php foreach($this->personal_serv_autho->menu as $personal_serv_autho_menu) : ?>
                                                                <li><a href="<?=base_url()?>services/social_authority/<?=$personal_serv_autho_menu->id?>" ><?=$personal_serv_autho_menu->ser_auth_name?></a></li>
                                                                <?php endforeach; ?>
                                                                
                                                                <?php foreach($this->social_serv_autho->menu as $social_serv_autho_menu) : ?>
                                                                <li><a href="<?=base_url()?>services/personal_authority/<?=$social_serv_autho_menu->id?>" ><?=$social_serv_autho_menu->ser_auth_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>
                                        </li>

                                        <!-- Beneficiary list -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                আলোকিত সন্তান<b class="caret"></b>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>সফল যারা</strong>  
                                                                </li>
                                                                <li><a href="">উচ্চ পদস্থ সরকারি কর্ম কর্তা </a></li>
                                                                <li><a href="">উচ্চ পদস্থ বেসরকারি কর্ম কর্তা </a></li>
                                                                <li><a href="">সফল ব্যবসাই</a></li>
                                                                <li><a href="">সমাজ কর্মী </a></li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>বিশ্ববিদ্যালয়ে অধ্যায়নরত</strong>
                                                                </li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li> 
                                                                    <a href="<?= base_url() ?>home/data_table"><strong>উন্নয়ন প্রত্যাশী</strong></a>
                                                                </li>
                                                                <li><a href="<?php echo base_url(); ?>home/team">Team</a></li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>অন্যান্য </strong>
                                                                </li>
                                                                <li><img src="<?php echo base_url(); ?>assets/front/img/team/1.jpg" alt=""></li>

                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>
                                        </li>

                                        <!-- Beneficiary list -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                অন্যান্য তালিকা<b class="caret"></b>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">
                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>সমন্নয়কারী</strong>  
                                                                </li>
                                                                <li><a href="<?=site_url('intercessor')?>">ইউ-পি চেয়ারম্যান</a></li>
                                                                <li><a href="<?=site_url('intercessor')?>">ইউ-পি সদস্য</a></li>
                                                                <li><a href="<?=site_url('intercessor')?>">গ্রাম পুলিশ</a></li>
                                                                <li><a href="<?=site_url('intercessor')?>">সুপারিশকারী </a></li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>বিভিন্ন ব্যাক্তিবর্গের তালিকা </strong>
                                                                </li>
                                                                <li><a href="<?=site_url('intercessor')?>"> প্রবাসীদের তালিকা</a></li>
                                                                <li><a href="<?=site_url('intercessor')?>"> মুক্তিযোদ্ধাদের তালিকা</a></li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>আরো .....</strong>
                                                                </li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>আরো .....</strong>
                                                                </li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>
                                        </li>


                                        <!-- Gallery Nav Item -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                গ্যালারী<b class="caret"></b>
                                            </a>

                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>আরো .....</strong>
                                                                </li>
                                                                <li><a href="<?= site_url('home/gallery'); ?>">Gallery Page</a></li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>ফটো গ্যালারী</strong>
                                                                </li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>ভিডিও গ্যালারী</strong>
                                                                </li>
                                                            </ul>

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>আরো .....</strong>
                                                                </li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- End Gallery Nav Item -->

                                        <!-- Services Nav Item -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                সেবাসমূহ<b class="caret"></b>
                                            </a>

                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">

                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>সরকারি </strong></li>
                                                                <?php foreach($this->gov_services->menu as $gov_serv_menu) : ?>
                                                                    <li><a href="<?=base_url()?>services/gov/<?=$gov_serv_menu->id?>" ><?=$gov_serv_menu->ser_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>বেসরকারি </strong>
                                                                </li>
                                                                <?php foreach($this->non_gov_services->menu as $non_gov_serv_menu) : ?>
                                                                    <li><a href="<?=base_url()?>services/non_gov/<?=$non_gov_serv_menu->id?>" ><?=$non_gov_serv_menu->ser_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>সামাজিক / ব্যাক্তিগত উদ্যোগ </strong>
                                                                </li>
                                                                <?php foreach($this->social_services->menu as $social_serv_menu) : ?>
                                                                    <li><a href="<?=base_url()?>services/social/<?=$social_serv_menu->id?>" ><?=$social_serv_menu->ser_name?></a></li>
                                                                <?php endforeach; ?>
                                                                <?php foreach($this->personal_services->menu as $personal_serv_menu) : ?>
                                                                    <li><a href="<?=base_url()?>services/personal/<?=$personal_serv_menu->id?>" ><?=$personal_serv_menu->ser_name?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                            <ul class="col-md-3 list-unstyled">
                                                                <li><strong>Actions </strong></li>
                                                                <?php if(isset($this->user->info->role)) : ?>
                                                                
                                                                    <?php if($this->user->info->role ==  0) : ?>
                                                                    <li><a href="<?=site_url('new_form')?>">আবেদন ফরম</a></li>
                                                                    <li><a href="<?=site_url('app_list/active')?>">একটিভ তালিকা</a></li>
                                                                    <li><a href="<?=site_url('app_list/pending')?>">অপেক্ষয়মান তালিকা</a></li>
                                                                    <li><a href="<?=site_url('create_service')?>">সেবা প্রদান ফরম</a></li>
                                                                    
                                                                    
                                                                    <?php endif; ?>
                                                                    
                                                                    <?php if($this->user->info->role ==  1 || $this->user->info->role ==  2) : ?>
                                                                    <li><a href="<?=site_url('app_list/active')?>">একটিভ তালিকা</a></li>
                                                                    <li><a href="<?=site_url('app_list/pending')?>">অপেক্ষয়মান তালিকা</a></li>
                                                                    <?php endif; ?>
                                                                    
                                                                <?php endif; ?>
                                                            </ul>
                                                            
                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>

                                        </li>
                                        <!-- End Services Nav Item -->

                                        <!-- Services Nav Item -->
                                        <li class="dropdown yamm-fw">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                সামাজিক উদ্যোগ<b class="caret"></b>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Content container to add padding -->
                                                    <div class="yamm-content">
                                                        <div class="row">
                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>ব্যাক্তিগত পর্যায়</strong>  
                                                                </li>
                                                                <li><a href="<?=base_url()?>services/gov/4">অসহায় দরিদ্র মেয়ের বিয়ের ব্যবস্থা </a></li>

                                                                <li><a href="<?=base_url()?>services/gov/4">অসহায় দরিদ্রের চিকিৎসার ব্যবস্থা</a></li>

                                                                <li><a href="<?=base_url()?>services/gov/4">অসহায় দরিদ্রের গৃহ নির্মাণে সাহায্য প্রদান</a></li>

                                                                <li><a href="<?=base_url()?>services/gov/4">অসহায় দরিদ্রের ঋণ মুক্ত হতে সাহায্য প্রদান</a></li>

                                                                <li><a href="<?=base_url()?>services/gov/4">অসহায় দরিদ্রের কর্মসংস্থান এবং প্রশিক্ষণের ব্যবস্থা</a></li>

                                                                <li><a href="<?=base_url()?>services/gov/4">মেধাবীদের  শিক্ষার বায় ভার বহনের ব্যবস্থা</a></li>
                                                            </ul>
                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>সমষ্টিগত পর্যায়</strong>
                                                                </li>
                                                                <li><a href="<?=base_url()?>services/gov/4">এলাকার সমস্যার কথা জানান</a></li>
                                                                <li><a href="<?=base_url()?>services/gov/4">এলাকার সম্ভাবনার কথা জানান</a></li>
                                                                <li><a href="<?=base_url()?>services/gov/4">পাঠাগার স্থাপন</a></li>
                                                                <li><a href="<?=base_url()?>services/gov/4">বৃক্ষ রোপন</a></li>
                                                                <li><a href="<?=base_url()?>services/gov/4">জনসচেতনতা বৃদ্ধি ও সামাজিক যোগ্যতা বৃদ্ধি করা </a></li>
                                                            </ul>
                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>অবকাঠামো উন্নয়ন</strong> 
                                                                </li>

                                                            </ul>
                                                            <ul class="col-md-3 list-unstyled">
                                                                <li>
                                                                    <strong>অন্যান্য</strong> 
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <img src="<?php echo base_url(); ?>assets/front/img/img-theme/img-dropdown-bg.png" class="img-nav" alt="">
                                                </li>
                                            </ul>
                                        </li>


                                        <!-- donation -->
                                        <li class="dropdown">
                                            <a href="<?= site_url('home/donation'); ?>" class="dropdown-toggle">
                                                যোগাযোগ
                                            </a>
                                        </li>

                                        <!-- End Contact Us Nav Item -->
                                    </ul>
                                    <!-- End Nav Bar Items -->
                                    <!-- login nav -->
                                    <ul class="nav navbar-nav navbar-right">
                                        <?php  if($this->user->loggedin ==  0){ ?>
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">
                                                    Login
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <div class="yamm-content form-theme post-comment">
                                                            <?= form_open(site_url("login/check"), array("class" => "search-Form login-form")) ?>
                                                            <div class="form-group">
                                                                <label for="Phone">Phone Number</label>
                                                                <input type="text" class="form-control" id="phone" name="phone" placeholder="e.g: 01672552966" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="Password1">Password</label>
                                                                <input type="password" class="form-control" id="Password1" name="password" placeholder="**********" required>
                                                            </div>
                                                            <button type="submit" class="btn btn-default">Login</button>
                                                            <?= form_close() ?>  
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        <?php } else { ?>
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                                    Hi, 
                                                    <?php 
                                                        $a = $this->user->info->first_name;
                                                        if (strlen($a) > 7) {
                                                            $strng = substr($a, 0, 7);
                                                            echo $strng;
                                                        }else{
                                                            echo $a;
                                                        }
                                                    ?> <b class="caret"></b>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href=""><i class="fa fa-fw fa-user"></i> My Profile</a></li>
                                                    <li><a href="<?=site_url("home/logout/" . $this->security->get_csrf_hash())?>"><i class="fa fa-fw fa-sign-out"></i> Logout</a></li>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">
                                                <b class="glyphicon glyphicon-search"></b>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <div class="yamm-content">
                                                        <form class="search-Form" action="#">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-search-plus"></i>
                                                                </span>
                                                                <input class="form-control" placeholder="Search" name="search"  type="text" required="required">
                                                            </div>
                                                        </form>  
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <!-- End Search Form -->
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- End Nav-->