<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    
    public function index() {
        
        $this->load->view('common/header');
        $this->load->view('common/slider');
        $this->load->view('pages/home');
        $this->load->view('common/footer');
    }

    

    public function view_member() {

        $data = array(
            'page' => 'pages/view_member',
            'header_title' => "View Member"
        );

        $this->load->view('common/template', $data);
    }
    public function single_view() {

        $data = array(
            'page' => 'pages/single_view',
            'header_title' => "single view"
        );

        $this->load->view('common/template', $data);
    }

    public function team() {

        $data = array(
            'page' => 'pages/team',
            'header_title' => "Team"
        );

        $this->load->view('common/template', $data);
    }


    public function donation() {

        $data = array(
            'page' => 'pages/donation',
            'header_title' => "Donation"
        );

        $this->load->view('common/template', $data);
    }

    public function donation_details() {

        $data = array(
            'page' => 'pages/donation_details',
            'header_title' => "Donation Details"
        );

        $this->load->view('common/template', $data);
    }




    public function about() {
        $data = array(
            'page' => 'pages/about',
            'header_title' => "About us",
            'img' => 'assets/front/images/inner-banner.jpg'
        );
        $this->load->view('common/template', $data);
    }
    
        public function content() {
        $data = array(
            'page' => 'pages/content',
            'header_title' => "Content Page",
            'img' => 'assets/front/images/inner-banner.jpg'
        );
        $this->load->view('common/template', $data);
    }
    
    public function tab() {
        $data = array(
            'page' => 'pages/tab',
            'header_title' => "Content Page",
            'img' => 'assets/front/images/inner-banner.jpg'
        );
        $this->load->view('common/template', $data);
    }
    
    public function history() {
        $data = array(
            'page' => 'pages/history',
            'header_title' => "History Page",
            'img' => 'assets/front/images/inner-banner.jpg'
        );
        $this->load->view('common/template', $data);
    }
    


    

    public function single() {
        $data = array(
            'page' => 'pages/single'
        );
        $this->load->view('common/template', $data);
    }

    public function data_table() {
        $data = array(
            'page' => 'pages/data_table'
        );
        $this->load->view('common/template', $data);
    }



    public function gallery() {
        $data = array(
            'page' => 'pages/gallery',
            'header_title' => "gallery Page"
        );
        $this->load->view('common/template', $data);
    }

    public function institute_identity() {
        $data = array(
            'page' => 'pages/institute_identity',
            'header_title' => "Institute Identity"
        );
        $this->load->view('common/template', $data);
    }

    public function all_services() {
        $data = array(
            'page' => 'pages/all_services',
            'header_title' => "All Services"
        );
        $this->load->view('common/template', $data);
    }
    
    
    // logout ------------------------------------------------------------
    public function logout($hash) {

        $config = $this->config->item("cookieprefix");

        $this->load->helper("cookie");
        if ($hash != $this->security->get_csrf_hash()) {

//            $data['title'] = "Error";
//
//            $data['page'] = "error/admin_error";
//
//            $this->session->set_flashdata('danger', 'CSRF and Cookie problem');
//
//            $this->load->view('common/template', $data);
            
            //without hash chara ai  mthd a ashle ki hbe?
        }
        delete_cookie($config . "phone");
        delete_cookie($config . "token");
        $this->session->sess_destroy();
        redirect(site_url());
    }


}
