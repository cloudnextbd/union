<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Login extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->model("login_m");
        $this->load->model("admin_m");

        if ($this->user->loggedin) {

            redirect(site_url());

        }

    }

    // *** index ---------------------------------------------------------------
    public function index() {

        echo 'front er default login view ashbe but oi view file create kora hoini';
        exit();

        // $data['title'] = "Login Form";
        // $this->load->view('login/login', $data);

    }

    // *** login check ---------------------------------------------------------
    public function check() {

        if(!$_POST){

            redirect(site_url('login'));

        }else{

            // get info ------------
            $phone = $this->filter->nohtml($this->input->post("phone", true));
            $password = $this->input->post("password", true);
            $rem = $this->input->post("rem", true);

            // retrieve log info from database ------------
            $config = $this->config->item("cookieprefix");
            if ($this->user->loggedin) {
                redirect(site_url());
            }

            // blank check ------------
            if (empty($phone) || empty($password)) {
                $this->session->set_flashdata("danger", 'Empty Phone or Passwod!');
            }

            // is phone exist ------------
            $login = $this->login_m->get_user_by_phone($phone);
            if ($login->num_rows() == 0) {
                $this->session->set_flashdata("danger", 'Invalid Phone or Password!');
            }

            // pass matching ------------
            $r = $login->row(); 
            $id = $r->id;

            if($password !==  $r->password){
                $this->session->set_flashdata("danger", 'Invalid Phone or Password!');
            }

            // $phpass = new PasswordHash(12, false);
            // if (!$phpass->CheckPassword($password, $r->password)) {
            //     $this->session->set_flashdata("danger", 'Invalid Phone or Password!');
            // }

            // generate a token and update it ------------
            $token = rand(1, 100000) . $phone;
            $token = md5(sha1($token));
            $this->login_m->update_user_token($id, $token);


            // if remenber create cookies ------------
            if ($rem == 1) {
                $ttl = 3600 * 24;
            } else {
                $ttl = 3600 * 24;
            }
            setcookie($config . "phone", $phone, time() + $ttl, "/");
            setcookie($config . "token", $token, time() + $ttl, "/");
            redirect(site_url());

        }

        
    }
    
    // *** inactive ------------------------------------------------------------
    public function inactive() {

        echo "Your are inactive user";
        
    }

    


}