<?php defined('BASEPATH') OR exit('No direct script access allowed');

class New_form extends CI_controller {

    function __construct() {

        parent :: __construct();

        $this->load->model('data');

        $this->app_img_path = './assets/app_img/';
        $this->app_house_img_path = './assets/app_house_img/';

        $this->load->model('new_app_m');
        
        
        $this->services_table = 'services';

        if (!$this->user->info->role == 0) {
            
            redirect(site_url());
            
        }
        
    }

    public function index() {
        
        //get ref info
        $this->user_table = 'user';
        $role = 'role';
        $value = '1';
        $data['refs'] = $this->data->getall_with_specific_value($this->user_table, $role, $value);
        
        //get services for current and interested
        $data['services'] = $this->data->getall($this->services_table);
        
        $data['title'] = "Applicant form";

        $data['page'] = "pages/app_form2/form";

        $this->load->view('common/template', $data);
    }

    public function do_add() {

        if (!$_POST) {

            redirect(site_url('new_form'));
            
        } else {
            
            $this->load->library('form_validation');

            // personal
            $this->form_validation->set_rules('name', 'Name', 'trim|required');

            $this->form_validation->set_rules('gender', 'Gender', 'trim|required');

            $this->form_validation->set_rules('birth_date', 'Birth_date', 'trim|required');

            $this->form_validation->set_rules('birth_id', 'Birth_id', 'trim|required|is_unique[app_personal.birth_id]');

            $this->form_validation->set_rules('nid', 'Nid', 'trim|required|is_unique[app_personal.nid]');


            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
            
            $this->form_validation->set_rules('district', 'District', 'trim|required');

            $this->form_validation->set_rules('sub_district', 'Sub_district', 'trim|required');

            $this->form_validation->set_rules('union', 'Union', 'trim|required');

            $this->form_validation->set_rules('word_no', 'Word_no', 'trim|required');


            $this->form_validation->set_rules('village', 'Village', 'trim|required');

            $this->form_validation->set_rules('house_name', 'House_name', 'trim|required');

            $this->form_validation->set_rules('holdings_no', 'Holdings_no', 'trim|required');

            $this->form_validation->set_rules('education', 'Education', 'trim|required');

            $this->form_validation->set_rules('profession', 'Profession', 'trim|required');
            
            $this->form_validation->set_rules('capability', 'Capability', 'trim|required');

            $this->form_validation->set_rules('disability', 'Disability', 'trim|required');

            $this->form_validation->set_rules('religion', 'Religion', 'trim|required');

            $this->form_validation->set_rules('holdings_no', 'Holdings_no', 'trim|required');

            $this->form_validation->set_rules('education', 'Education', 'trim|required');

            $this->form_validation->set_rules('profession', 'Profession', 'trim|required');


            // family
            $this->form_validation->set_rules('father_name', 'Father_name', 'trim|required');

            $this->form_validation->set_rules('father_nid', 'Father_nid', 'trim|required');

            $this->form_validation->set_rules('mother_name', 'Mother_name', 'trim|required');

            $this->form_validation->set_rules('mother_nid', 'Mother_nid', 'trim|required');

            $this->form_validation->set_rules('marital_status', 'marital_status', 'trim|required');


            $this->form_validation->set_rules('husband_name', 'Husband_name', 'trim|required');

            $this->form_validation->set_rules('husband_nid', 'Husband_nid', 'trim|required');

            $this->form_validation->set_rules('wife_name', 'Wife_name', 'trim|required');

            $this->form_validation->set_rules('wife_nid', 'wife_nid', 'trim|required');

            $this->form_validation->set_rules('family_member', 'Family_member', 'trim|required');

            $this->form_validation->set_rules('family_desc', 'Family_desc', 'trim|required');



            // finance
            $this->form_validation->set_rules('house_desc', 'House_desc', 'trim|required');
            
            
            $this->form_validation->set_rules('land', 'Land', 'trim|required');

            $this->form_validation->set_rules('earn_person', 'Earn_person', 'trim|required');

            $this->form_validation->set_rules('earn_source', 'Earn_source', 'trim|required');

            $this->form_validation->set_rules('monthly_income', 'Monthly_income', 'trim|required');

            $this->form_validation->set_rules('cause_poorness', 'Cause_poorness', 'trim|required');



            // interested_service
        //  $this->form_validation->set_rules('curr_service', 'Curr_service', 'trim|required'); // multi select

        //  $this->form_validation->set_rules('inter_service', 'Interested_service', 'trim|required'); // multi select
            
            $this->form_validation->set_rules('inter_service', 'Inter_service', 'trim|required');
            
            $this->form_validation->set_rules('success_way', 'Success_way', 'trim|required');

            $this->form_validation->set_rules('inter_trainings', 'Inter_trainings', 'trim|required');
            
            $this->form_validation->set_rules('sp_info', 'Special Information', 'trim|required');
            

        //  $this->form_validation->set_rules('reference', 'Reference', 'trim|required'); // multi select



            if ($this->form_validation->run() == FALSE) {

                $data['title'] = "Error";

                $data['page'] = "pages/app_form2/error";

                $this->load->view('common/template', $data);
                
            } else {

                //app image
                if ($_FILES['image']['name'] != '') {

                    $image_upload = $this->do_upload('image', $this->app_img_path);

                    if ($image_upload == FALSE) {

                        $this->session->set_flashdata('danger', 'app Image Upload Failed');

                        $data['title'] = "Not save";

                        $data['page'] = "pages/app_form2/error";

                        $this->load->view('common/template', $data);
                        
                    } else {

                        $image = $image_upload["file_name"];
                    }
                }
                
                //house image
                if ($_FILES['house_image']['name'] != '') {

                    $image_upload = $this->do_upload('house_image', $this->app_house_img_path);

                    if ($image_upload == FALSE) {

                        $this->session->set_flashdata('danger', 'house Image Upload Failed');

                        $data['title'] = "Not save";

                        $data['page'] = "pages/app_form2/error";

                        $this->load->view('common/template', $data);
                        
                        
                    } else {

                        $house_image = $image_upload["file_name"];
                    }
                }
                

                $id = $this->new_app_m->add_form_data($image, $house_image);

                if ($id) {
                    
                    // save
                    $this->session->set_flashdata('success', 'data save successfully!!');

                    $data['title'] = "Save";

                    $data['page'] = "pages/app_form2/success";

                    $this->load->view('common/template', $data);
                    
                } else {

                    // not save
                    $this->session->set_flashdata('danger', 'data not save!');

                    $data['title'] = "Not save";

                    $data['page'] = "pages/app_form2/error";

                    $this->load->view('common/template', $data);
                    
                }
            }
        }
    }

    
    
    public function do_upload($field_name , $path) {

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '2048';
        $config['xss_clean'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field_name)) {
            return false;
        } else {
            return $this->upload->data();
        }
          
    }
    
    
    public function nid_check() {
        
        $nid = $this->input->get('nid');
        
        if(!$this->new_app_m->nid_check($nid)){
            
            $msg = "<span style='color:red'>এই এনআইডি নম্বরটি ইতিমধ্যে ব্যবহার করা হয়েছে!</span>";
            
        }else{
            
            $msg = "<span style='color:green'>এই এনআইডি নম্বরটি ব্যবহার করা যাবে</span>";
            
        }
        
        echo $msg;
        
    }
    
    public function birth_id_check() {
        
        $birth_id = $this->input->get('birth_id');
        
        if(!$this->new_app_m->birth_id_check($birth_id)){
            
            $msg = "<span style='color:red'>এই জন্ম নিবন্ধন  নম্বরটি ইতিমধ্যে ব্যবহার করা হয়েছে!</span>";
            
        }else{
            
            $msg = "<span style='color:green'>এই জন্ম নিবন্ধন নম্বরটি ব্যবহার করা যাবে</span>";
            
        }
        
        echo $msg;
        
    }
    
    
    
    
    
    
    
    
    
    
    
    

}