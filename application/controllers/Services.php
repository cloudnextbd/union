<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_controller {

    function __construct() {

        parent :: __construct();

        $this->load->model('data');
        
        $this->load->model('services_m');
        
        $this->service_autho_table = 'service_authority';
        $this->services_table = 'services';
        
        $this->goal_table = 'services_goal';
        $this->purpose_table = 'services_purpose';
        $this->qualifi_table = 'services_qualifi';
        $this->disqualifi_table = 'services_disqualifi';
        $this->selection_table = 'services_selection';
        
//        $this->app_temp_per_table = 'temp_app_personal';
//        $this->app_temp_path = './uploads/front/gallery/';
//        $this->load->model('app_temp_m');
        
        
//        if (!$this->user->info->role ==  1) {
//            redirect(site_url());
//        }

    }

    public function index() {
        
        echo 'this is service index page!';
        
    }
    
    
    public function gov($id) {
        
        $data['info'] = $this->data->getone($this->services_table, $id);
        
        $data['goals'] = $this->data->getall_by_id($this->goal_table, $id);
        
        $data['purposes'] = $this->data->getall_by_id($this->purpose_table, $id);
        
        $data['qualifis'] = $this->data->getall_by_id($this->qualifi_table, $id);
        
        $data['disqualifis'] = $this->data->getall_by_id($this->disqualifi_table, $id);
        
        $data['selections'] = $this->data->getall_by_id($this->selection_table, $id);
        
        $data['title'] = "Gov Service Description";

        $data['page'] = "pages/services/service";

        $this->load->view('common/template', $data);
        
    }
    
    public function non_gov($id) {
        
        $data['info'] = $this->data->getone($this->services_table, $id);
        
        $data['goals'] = $this->data->getall_by_id($this->goal_table, $id);
        
        $data['purposes'] = $this->data->getall_by_id($this->purpose_table, $id);
        
        $data['qualifis'] = $this->data->getall_by_id($this->qualifi_table, $id);
        
        $data['disqualifis'] = $this->data->getall_by_id($this->disqualifi_table, $id);
        
        $data['selections'] = $this->data->getall_by_id($this->selection_table, $id);
        
        $data['title'] = "Gov Service Description";

        $data['page'] = "pages/services/service";

        $this->load->view('common/template', $data);
        
    }
    
    public function social($id) {
        
        $data['info'] = $this->data->getone($this->services_table, $id);
        
        $data['goals'] = $this->data->getall_by_id($this->goal_table, $id);
        
        $data['purposes'] = $this->data->getall_by_id($this->purpose_table, $id);
        
        $data['qualifis'] = $this->data->getall_by_id($this->qualifi_table, $id);
        
        $data['disqualifis'] = $this->data->getall_by_id($this->disqualifi_table, $id);
        
        $data['selections'] = $this->data->getall_by_id($this->selection_table, $id);
        
        $data['title'] = "Gov Service Description";

        $data['page'] = "pages/services/service";

        $this->load->view('common/template', $data);
        
    }
    
    public function personal($id) {
        
        $data['info'] = $this->data->getone($this->services_table, $id);
        
        $data['goals'] = $this->data->getall_by_id($this->goal_table, $id);
        
        $data['purposes'] = $this->data->getall_by_id($this->purpose_table, $id);
        
        $data['qualifis'] = $this->data->getall_by_id($this->qualifi_table, $id);
        
        $data['disqualifis'] = $this->data->getall_by_id($this->disqualifi_table, $id);
        
        $data['selections'] = $this->data->getall_by_id($this->selection_table, $id);
        
        $data['title'] = "Gov Service Description";

        $data['page'] = "pages/services/service";

        $this->load->view('common/template', $data);
        
    }
    
    
    // authrity 
    
    public function gov_authority($id) {
        
        $data['info'] = $this->data->getone($this->service_autho_table, $id);
        
        $data['title'] = "Gov Service Description";

        $data['page'] = "pages/services/service_autho";

        $this->load->view('common/template', $data);
        
    }
    
    public function non_gov_authority($id) {
        
        $data['info'] = $this->data->getone($this->service_autho_table, $id);
        
        $data['title'] = "Gov Service Description";

        $data['page'] = "pages/services/service_autho";

        $this->load->view('common/template', $data);
        
    }
    
    public function social_authority($id) {
        
        $data['info'] = $this->data->getone($this->service_autho_table, $id);
        
        $data['title'] = "Gov Service Description";

        $data['page'] = "pages/services/service_autho";

        $this->load->view('common/template', $data);
        
    }
    
    
    public function personal_authority($id) {
        
        $data['info'] = $this->data->getone($this->service_autho_table, $id);
        
        $data['title'] = "Gov Service Description";

        $data['page'] = "pages/services/service_autho";

        $this->load->view('common/template', $data);
        
    }
    
    
    public function mem_list($serv_id) {
        
        $data['lists'] = $this->services_m->get_all_by_serv_id($serv_id);
        
        $data['title'] = "list";

        $data['page'] = "pages/services/list";

        $this->load->view('common/template', $data);
        
        
        
        
    }
    
    
    
    

}