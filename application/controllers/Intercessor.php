<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Intercessor extends CI_controller {

    function __construct() {

        parent :: __construct();
        
        $this->load->model('data');
        
        $this->user_table = 'user';

    }
    
    public function index() {
        
        $data['lists'] = $this->data->getall($this->user_table);
        
        $data['title'] = "সুপারিশকারী";
        
        $data['page'] = "pages/intercessor/team";
        
        $this->load->view('common/template', $data);
        
    }
     
}