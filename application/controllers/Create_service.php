<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Create_service extends CI_controller {

    function __construct() {

        parent :: __construct();

        $this->load->model('data');
        
        $this->load->model('create_service_m');
        
        //$this->service_autho_table = 'service_authority';

        if (!$this->user->info->role ==  0) {
            
            redirect(site_url());
            
        }

    }

    public function index() {
        
        $data['title'] = "Create Service";

        $data['page'] = "pages/create_service/form1";

        $this->load->view('common/template', $data);
        
    }
    
    public function add() {
        
        if($_POST){
            
            $id = $this->create_service_m->add();

            if ($id) {

                // save
                $this->session->set_flashdata('success', 'data save successfully!!');

                redirect(site_url('create_service'));

            } else {

                // not save
                $this->session->set_flashdata('danger', 'data not save!');

                redirect(site_url('create_service'));

            }
            
        } else {
            
            redirect(site_url('create_service'));
            
        }
        
        
        
    }
    
    // get_auth_by_cat_id (ajax) ---------------------------------
    public function get_auth_by_cat_id() {
        
        $cat_id = $this->input->get('cat_id');
        
        $r = $this->create_service_m->get_auth_by_cat_id($cat_id);
        
        $encode_data = json_encode($r);
        
        echo $encode_data;
        
    }
    
    // get_ser_name_by_ser_auth_id (ajax) ---------------------------------
    
    public function get_ser_name_by_ser_auth_id() {
        
        $ser_auth_id = $this->input->get('ser_auth_id');
        
        $re = $this->create_service_m->get_ser_name_by_ser_auth_id($ser_auth_id);
        
        $encode_data = json_encode($re);
        
        echo $encode_data;
        
    }
    
    
    public function get_mem_info_by_mem_id() {
        
        $mem_id = $this->input->get('mem_id');
        
        $mem_info = $this->create_service_m->get_mem_info_by_mem_id($mem_id);
        
        if($mem_info){
            
            $encode_data = json_encode($mem_info);
            echo $encode_data;
        
        }else{
            
            // no member found
            return false;
            
        }
        
    }
    
    
    
    
    
    
    
    

}