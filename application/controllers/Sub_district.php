<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_district extends CI_controller {

    function __construct() {
        
        parent :: __construct();

    }
    
    public function chandpur_sadar() {
        
        $this->load->view('sub_district/header');
        $this->load->view('sub_district/chand_pur_sadar');
        $this->load->view('sub_district/footer');
        
    }
    
    public function coming_soon() {
        
        $this->load->view('sub_district/header');
        $this->load->view('sub_district/coming_soon');
        $this->load->view('sub_district/footer');
        
    }
    
    public function rampur() {
        
        $this->load->view('sub_district/rampur/header');
        $this->load->view('sub_district/rampur/slider');
        $this->load->view('sub_district/rampur/home');
        $this->load->view('sub_district/rampur/footer');
        
    }
    
    
}