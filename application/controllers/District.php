<?php defined('BASEPATH') OR exit('No direct script access allowed');

class District extends CI_controller {

    function __construct() {
        
        parent :: __construct();

        
    }
    
    public function index() {
        
        $this->load->view('district/header');
        $this->load->view('district/home');
        $this->load->view('district/footer');
        
    }
    
    public function coming_soon() {
        
        $this->load->view('district/header');
        $this->load->view('district/coming_soon');
        $this->load->view('district/footer');
        
    }

}