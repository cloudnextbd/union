<?php defined('BASEPATH') OR exit('No direct script access allowed');

class App_list extends CI_controller {

    function __construct() {

        parent :: __construct();

        $this->load->model('data');
        $this->load->model('new_app_m');
        
        $this->app_per_table = 'app_personal';
        
        if (!isset($this->user->info->role)) {
            redirect(site_url());
        }
        
    }
    
    public function index() {
        
        redirect(site_url('app_list'));
        
    }
    
    
    public function pending() {
        
        // operator
        if (isset($this->user->info->role) && $this->user->info->role == 0) {
            
            
            $data['lists'] = $this->data->getall_with_inactive($this->app_per_table);
            $data['title'] = "Active List";
            $data['page'] = "Pages/list/inactive_list";
            $this->load->view('common/template', $data);
        
        // supervisor view
        } elseif(isset($this->user->info->role) && $this->user->info->role == 1){
            
            $user_id = $this->user->info->id;
            $data['lists'] = $this->new_app_m->get_pending_list_superv($user_id);
            $data['title'] = "Active List";
            $data['page'] = "Pages/list/inactive_list";
            $this->load->view('common/template', $data);
        
        // co-ordinator view
        } elseif(isset($this->user->info->role) && $this->user->info->role == 2){
            
            $data['lists'] = $this->data->getall_with_inactive($this->app_per_table);
            $data['title'] = "Active List";
            $data['page'] = "Pages/list/inactive_list";
            $this->load->view('common/template', $data);
            
        }
        
    }
    
    public function details($app_id) {
        
        // operator 
        if (isset($this->user->info->role) && $this->user->info->role == 0) {
            
            $data['info'] = $this->new_app_m->get_app_details($app_id);
            
            $data['ref_names'] = $this->new_app_m->get_ref_name($app_id);
            
            $data['curr_services'] = $this->new_app_m->get_curr_service($app_id);
            
            // get inter service name by id
            $int_ser = $this->new_app_m->get_app_details($app_id);
            $int_ser_id = $int_ser[0]->inter_ser_id;
            $data['inter_serv'] = $this->new_app_m->get_inter_service($int_ser_id);
            
            // get service history
            $data['historys'] = $this->new_app_m->get_app_serv_history($app_id);
            
            $data['title'] = "Dtails";
            $data['page'] = "pages/details/details";
            $this->load->view('common/template', $data);
        
            
        // supervisor view
        } elseif(isset($this->user->info->role) && $this->user->info->role == 1) {
            
            $data['info'] = $this->new_app_m->get_app_details($app_id);

            $data['ref_names'] = $this->new_app_m->get_ref_name($app_id);
            
            $data['curr_services'] = $this->new_app_m->get_curr_service($app_id);

            // get inter service name by id
            $int_ser = $this->new_app_m->get_app_details($app_id);
            $int_ser_id = $int_ser[0]->inter_ser_id;
            $data['inter_serv'] = $this->new_app_m->get_inter_service($int_ser_id);
            
            // get comment
            $supv_id = $this->user->info->id;
            $data['ref_comnt'] = $this->new_app_m->get_suprv_comment($app_id, $supv_id);
            
            $data['title'] = "Dtails";
            $data['page'] = "pages/details/details";
            $this->load->view('common/template', $data);
        
        // co-ordinator view
        } elseif(isset($this->user->info->role) && $this->user->info->role == 2) {
            
            $data['info'] = $this->new_app_m->get_app_details($app_id);
            
            $data['ref_names'] = $this->new_app_m->get_ref_name($app_id);
            
            $data['curr_services'] = $this->new_app_m->get_curr_service($app_id);
            
            // get inter service name by id
            $int_ser = $this->new_app_m->get_app_details($app_id);
            $int_ser_id = $int_ser[0]->inter_ser_id;
            $data['inter_serv'] = $this->new_app_m->get_inter_service($int_ser_id);
            
            // get this coord comment
            //$coor_id = $this->user->info->id;
            $data['coor_comnt'] = $this->new_app_m->get_this_coord_comment($app_id);
            
            // get all supvs comment
            $data['comnts'] = $this->new_app_m->get_all_suv_comment($app_id);
            
            $data['title'] = "Dtails";
            $data['page'] = "pages/details/details";
            $this->load->view('common/template', $data);
            
        }
        
        
    }
    
    
    public function ref_comments() {
        
        if($_POST){
            
            $app_id = $this->input->post('id');
            $supv_id = $this->user->info->id;
            
            $id = $this->new_app_m->comments_add($app_id, $supv_id);

            if ($id) {

                //$this->session->set_flashdata('success', 'Added Successfully!');
                redirect(site_url('app_list/details/' . $app_id));

            } else {

                //$this->session->set_flashdata('danger', 'Not Added!');
                redirect(site_url('app_list/details/' . $app_id));

            }

        } else {

            redirect(site_url('app_list/pending'));

        }
        
    }
    
//    public function coor_comments() {
//        
//        if($_POST){
//            
//            $app_id = $this->input->post('id');
//            $coor_id = $this->user->info->id;
//            
//            $id = $this->new_app_m->coor_action($app_id, $coor_id);
//
//            if ($id) {
//
//                //$this->session->set_flashdata('success', 'Added Successfully!');
//                redirect(site_url('app_list/details/' . $app_id));
//
//            } else {
//
//                //$this->session->set_flashdata('danger', 'Not Added!');
//                redirect(site_url('app_list/details/' . $app_id));
//
//            }
//
//        } else {
//
//            redirect(site_url('app_list/pending'));
//
//        }
//        
//    }
    
    
    public function coor_action() {
        
        if($_POST){
            
            $app_id = $this->input->post('id');
            
            $coor_id = $this->input->post('coor_id');
            
            $id = $this->new_app_m->coor_action($app_id, $coor_id);

            if ($id) {

                //$this->session->set_flashdata('success', 'Added Successfully!');
                redirect(site_url('app_list/details/' . $app_id));

            } else {

                //$this->session->set_flashdata('danger', 'Not Added!');
                redirect(site_url('app_list/details/' . $app_id));

            }

        } else {

            redirect(site_url('app_list/pending'));

        }
        
    }
    
    
    
    public function active() {
        
        $data['lists'] = $this->data->getall_with_status($this->app_per_table);
        $data['title'] = "Active List";
        $data['page'] = "pages/list/active_list";
        $this->load->view('common/template', $data);
        
    }
    
   
  
}