<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Create_service_m extends CI_Model {

    function __construct() {
        parent :: __construct();
        
        $this->service_provide_t = "service_provide_list";
        
    }
    
    
    public function add() {

        $value = array(
            
            'category' => $this->input->post('category'),
            'authority_id' => $this->input->post('authority_id'),
            'ser_id' => $this->input->post('ser_id'),
            'provider_name' => $this->input->post('provider_name'),
            'date' => $this->input->post('date'),
            'app_id' => $this->input->post('app_id'),
            'note' => $this->input->post('note')
            
        );

        $result = $this->data->save($this->service_provide_t, $value);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    
    
    
    
    public function get_auth_by_cat_id($cat_id) {
        
        $this->db->select('id, ser_auth_name');
        $this->db->where('category', "$cat_id");
        $this->db->from('service_authority');
        return $this->db->get()->result();
        
    }
    
    
    public function get_ser_name_by_ser_auth_id($ser_auth_id) {
        
        $this->db->select('id, ser_name');
        $this->db->where('authority_id', "$ser_auth_id");
        $this->db->from('services');
        return $this->db->get()->result();
        
    }
    
    public function get_mem_info_by_mem_id($id) {
        
//        $this->db->where('id', $id);
//        $this->db->limit(1);
//        $this->db->from('app_personal');
//        return $this->db->get()->row();
        
        $this->db->select('app_personal.name, app_personal.image, app_personal.village, services.id, services.ser_name');
        
        $this->db->from('app_personal');
        
        $this->db->join('app_curr_service', 'app_curr_service.id = app_personal.id', 'left');
        
        $this->db->join('services', 'services.id = app_curr_service.curr_service', 'left');
        
        $this->db->where('app_personal.id', $id);
        
        $query = $this->db->get();
        
        return $query->row();
        
    }
    
    
    
    
}
