<?php defined('BASEPATH') OR exit('No direct script access allowed');

class New_app_m extends CI_Model {

    function __construct() {
        
        parent :: __construct();
        
        $this->app_per_table = 'app_personal';
        $this->app_fam_table = 'app_family';
        $this->app_fin_table = 'app_finance';
        $this->app_int_ser_table = 'app_interested_service';
        
        $this->ref_comments_table = 'ref_comments';
        $this->coor_comments_table = 'coord_comments';
        
        
        $this->app_curr_service_table = 'app_curr_service';
        $this->app_inter_service_table = 'app_inter_service';
        
        
        
    }
    
    public function add_form_data($image, $house_image) {

        $per_value = array(
            
            'name' => $this->input->post('name'),
            'gender' => $this->input->post('gender'),
            'birth_date' => $this->input->post('birth_date'),
            'birth_id' => $this->input->post('birth_id'),
            'nid' => $this->input->post('nid'),
            'mobile' => $this->input->post('mobile'),
            'image' => $image,
            'district' => $this->input->post('district'),
            'sub_district' => $this->input->post('sub_district'),
            'union' => $this->input->post('union'),
            'word_no' => $this->input->post('word_no'),
            'village' => $this->input->post('village'),
            'house_name' => $this->input->post('house_name'),
            'holdings_no' => $this->input->post('holdings_no'),
            'education' => $this->input->post('education'),
            'profession' => $this->input->post('profession'),
            'capability' => $this->input->post('capability'),
            'disability' => $this->input->post('disability'),
            'religion' => $this->input->post('religion'),
            'app_status' => '1',
            'status' => '0'
                
        );
        $id = $this->data->save($this->app_per_table, $per_value);
       
        
        $fam_value = array(
            
            'parent_id' => $id,
            'father_name' => $this->input->post('father_name'),
            'father_nid' => $this->input->post('father_nid'),
            'mother_name' => $this->input->post('mother_name'),
            'mother_nid' => $this->input->post('mother_nid'),
            'marital_status' => $this->input->post('marital_status'),
            'husband_name' => $this->input->post('husband_name'),
            'husband_nid' => $this->input->post('husband_nid'),
            'wife_name' => $this->input->post('wife_name'),
            'wife_nid' => $this->input->post('wife_nid'),
            'family_member' => $this->input->post('family_member'),
            'family_desc' => $this->input->post('family_desc')
                
        );
        $fam_result = $this->data->save($this->app_fam_table, $fam_value);
     
        
        $fin_value = array(
            
            'parent_id' => $id,
            'house_desc' => $this->input->post('house_desc'),
            'house_image' => $house_image,
            'land' => $this->input->post('land'),
            'earn_person' => $this->input->post('earn_person'),
            'earn_source' => $this->input->post('earn_source'),
            'monthly_income' => $this->input->post('monthly_income'),
            'cause_poorness' => $this->input->post('cause_poorness')
            
        );
        $fin_result = $this->data->save($this->app_fin_table, $fin_value);
        
        $int_ser_value = array(
            
            'parent_id' => $id,
            'inter_service' => $this->input->post('inter_service'),
            'success_way' => $this->input->post('success_way'),
            'inter_trainings' => $this->input->post('inter_trainings'),
            'sp_info' => $this->input->post('sp_info')
            
        );

        $int_ser_result = $this->data->save($this->app_int_ser_table, $int_ser_value);

        if ($int_ser_result) {
            
            // save current service --------------------------------
            $currs = $this->input->post('curr_service');
            // delete exits applicant same id
            $this->data->delete($this->app_curr_service_table, $id);

            foreach ($currs as $curr) {
                $value = array(
                    'id' => $id,
                    'curr_service' => $curr
                );
                $this->data->save($this->app_curr_service_table, $value);
            }
            
            
//            // save interested service --------------------------------
//            $inters = $this->input->post('inter_service');
//            // delete exits applicant same id
//            $this->data->delete($this->app_inter_service_table, $id);
//
//            foreach ($inters as $inter) {
//                $value = array(
//                    'id' => $id,
//                    'inter_service' => $inter
//                );
//                $this->data->save($this->app_inter_service_table, $value);
//            }
            
            // save referrence service --------------------------------
            $refs = $this->input->post('reference');
            // delete exits applicant same id
            $this->data->delete($this->ref_comments_table, $id);

            // $id = applicant form id
            // $ref_id = referance id
            foreach ($refs as $ref) {
                $value = array(
                    'id' => $id,
                    'ref_id' => $ref
                );
                $this->data->save($this->ref_comments_table, $value);
            }
            
            
            // coor comments 
            $this->data->delete($this->coor_comments_table, $id);

            $coor_coments_value = array(

                'id' => $id

            );

            $this->data->save($this->coor_comments_table, $coor_coments_value);
        
        
            return TRUE;
            
        } else {
            return FALSE;
        }
        
    }
    
        
    
    // get applicant details view
    public function get_app_details($id) {
        
        $this->db->select('
                app_personal.id,
                app_personal.name,
                app_personal.gender,
                app_personal.birth_date,
                app_personal.birth_id,
                app_personal.nid,
                app_personal.mobile,
                app_personal.image,
                app_personal.district,
                app_personal.sub_district,
                app_personal.union,
                app_personal.word_no,
                app_personal.village,
                app_personal.house_name,
                app_personal.holdings_no,
                app_personal.education,
                app_personal.profession,
                app_personal.capability,
                app_personal.disability,
                app_personal.religion,
                app_personal.app_status,
                app_personal.status,
                app_personal.create_by,
                app_personal.create_date,
                app_family.father_name,
                app_family.father_nid,
                app_family.mother_name,
                app_family.mother_nid,
                app_family.marital_status,
                app_family.husband_name,
                app_family.husband_nid,
                app_family.wife_name,
                app_family.wife_nid,
                app_family.family_member,
                app_family.family_desc,
                app_finance.house_desc,
                app_finance.house_image,
                app_finance.land,
                app_finance.earn_person,
                app_finance.earn_source,
                app_finance.monthly_income,
                app_finance.cause_poorness,
                app_interested_service.inter_ser_id,
                app_interested_service.success_way,
                app_interested_service.inter_trainings,
                app_interested_service.sp_info
            ');
        
        $this->db->from($this->app_per_table);
        
        $this->db->join($this->app_fam_table, 'app_family.parent_id = app_personal.id', 'left');
        
        $this->db->join($this->app_fin_table, 'app_finance.parent_id = app_personal.id', 'left');
        
        $this->db->join($this->app_int_ser_table, 'app_interested_service.parent_id = app_personal.id', 'left');
        
        $this->db->where('app_personal.id', $id);
        
        $query = $this->db->get();
        
        $prod_results = $query->result();
        
        return $prod_results;
         
    }
    
    // get pending list supervisor
    
    
    
    
    public function get_pending_list_superv($id) {
        
        $this->db->select('*');
        
        $this->db->from($this->app_per_table);
        
        $this->db->join($this->ref_comments_table, 'ref_comments.id = app_personal.id', 'left');
        
        $this->db->where('ref_comments.ref_id', $id);
        
        $query = $this->db->get();
        
        $prod_results = $query->result();
        
        return $prod_results;
         
    }
    
    
    public function get_suprv_comment($app_id, $supv_id) {
        
        $this->db->where('id', "$app_id");
        $this->db->where('ref_id', "$supv_id");
        $this->db->from($this->ref_comments_table);
        return $this->db->get()->result();
    
    }
    
    
//    public function get_coord_comment($app_id) {
//        
//        $this->db->select('*');
//        
//        $this->db->from($this->ref_comments_table);
//        
//        $this->db->join('user', 'user.id = ref_comments.ref_id', 'left');
//        
//        $this->db->where('ref_comments.id', $app_id);
//        
//        $query = $this->db->get();
//        
//        $prod_results = $query->result();
//        
//        return $prod_results;
//    
//    }
    
    
    public function get_all_suv_comment($app_id) {
        
        $this->db->select('user.first_name, user.last_name, ref_comments.comments');
        
        $this->db->from($this->ref_comments_table);
        
        $this->db->join('user', 'user.id = ref_comments.ref_id', 'left');
        
        $this->db->where('ref_comments.id', $app_id);
        
        $query = $this->db->get();
        
        $prod_results = $query->result();
        
        return $prod_results;
    
    }
    
    
    public function get_this_coord_comment($app_id) {
        
        $this->db->select('comments');
        
        $this->db->from('coord_comments');
        
        $this->db->where('id', $app_id);
        
        $query = $this->db->get();
        
        $results = $query->result();
        
        return $results;
    
    }
    
    
    public function comments_add($app_id, $supv_id) {

        $value = array(
            
            'comments' => $this->input->post('comments')
        );

        $this->db->where('id', $app_id);
        $this->db->where('ref_id', $supv_id);
        
        $result = $this->db->update($this->ref_comments_table, $value);
        
        if($result){
            return TRUE;
        }  else {
            return FALSE;   
        }
   
    }
    
    
    // coordinator statu and comment action
    public function coor_action($app_id, $coor_id) {

        // status add
        $value = array(
            
            'app_status' => $this->input->post('app_status'),
            'status' => $this->input->post('status')
            
        );

        $this->db->where('id', $app_id);
        $result = $this->db->update($this->app_per_table, $value);
        
        
        if($result){
            
            // comments add
            $value1 = array(

                'comments' => $this->input->post('comments')

            );

            $this->db->where('id', $app_id);
            //$this->db->where('coor_id', $coor_id); // pore kaj korte hobe
            $this->db->update($this->coor_comments_table, $value1);
            
            return TRUE;
            
        }  else {
            
            return FALSE; 
            
        }
   
    }
    
    public function get_ref_name($app_id) {
        
        $this->db->select('first_name, last_name');
        
        $this->db->from('user');
        
        $this->db->join('ref_comments', 'ref_comments.ref_id = user.id', 'left');
        
        $this->db->where('ref_comments.id', $app_id);
        
        $query = $this->db->get();
        
        $prod_results = $query->result();
        
        return $prod_results;
    
    }
    
    
    public function get_curr_service($app_id) {
        
        $this->db->select('ser_name');
        
        $this->db->from('services');
        
        $this->db->join('app_curr_service', 'app_curr_service.curr_service = services.id', 'left');
        
        $this->db->where('app_curr_service.id', $app_id);
        
        $query = $this->db->get();
        
        $prod_results = $query->result();
        
        return $prod_results;
    
    }
    
    public function get_inter_service($int_ser_id) {
        
        $this->db->select('ser_name');
        
        $this->db->from('services');
        
        $this->db->join('app_inter_service', 'app_inter_service.inter_service = services.id', 'left');
        
        $this->db->where('app_inter_service.id', $int_ser_id);
        
        $query = $this->db->get();
        
        $prod_results = $query->result();
        
        return $prod_results;
    
    }
    
    // get service history by applicant id
    public function get_app_serv_history($app_id) {
        
        $this->db->select('services.ser_name, service_provide_list.date, service_authority.ser_auth_name, service_provide_list.provider_name, service_provide_list.note '); // short desc ashbe
        
        $this->db->from('service_provide_list');
        
        $this->db->join('services', 'services.id = service_provide_list.ser_id', 'left');
        
        $this->db->join('service_authority', 'service_authority.id = service_provide_list.authority_id', 'left');
        
        $this->db->where('service_provide_list.app_id', $app_id);
        
        $query = $this->db->get();
        
        $prod_results = $query->result();
        
        return $prod_results;
    
    }
    
    
    
    
    
    
    // nid check
    public function nid_check($nid) {
        
        $this->app_personal_t = 'app_personal';
        
        $this->nid_f = 'nid';

        $s = $this->db->where($this->nid_f, $nid)->get($this->app_personal_t);

        if ($s->num_rows() > 0) {
            
            return false;
            
        } else {
            
            return true;
            
        }
        
    }
    
    // birth check
    public function birth_id_check($birth_id) {
        
        $this->app_personal_t = 'app_personal';
        
        $this->birth_id_f = 'birth_id';

        $s = $this->db->where($this->birth_id_f, $birth_id)->get($this->app_personal_t);

        if ($s->num_rows() > 0) {
            
            return false;
            
        } else {
            
            return true;
            
        }
        
    }
    

    
}