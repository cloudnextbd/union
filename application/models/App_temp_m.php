<?php defined('BASEPATH') OR exit('No direct script access allowed');

class App_temp_m extends CI_Model {

    function __construct() {
        $this->personal_table = 'temp_app_personal';
        $this->family_table = 'temp_app_family';
        $this->finance_table = 'temp_app_finance';
        $this->interested_service_table = 'temp_app_interested_service';
        
        
        
        
        parent :: __construct();
    }

    public function add_temp_personal() {

        $value = array(
            'name' => $this->input->post('name'),
            'gender' => $this->input->post('gender'),
            'birth_date' => $this->input->post('birth_date'),
            'birth_id' => $this->input->post('birth_id'),
            'nid' => $this->input->post('nid'),
            'mobile' => $this->input->post('mobile'),
            'district' => $this->input->post('district'),
            'sub_district' => $this->input->post('sub_district'),
            'union' => $this->input->post('union'),
            'word_no' => $this->input->post('word_no'),
            'village' => $this->input->post('village'),
            'house_name' => $this->input->post('house_name'),
            'holdings_no' => $this->input->post('holdings_no'),
            'education' => $this->input->post('education'),
            'profession' => $this->input->post('profession'),
            'disability' => $this->input->post('disability'),
            'religion' => $this->input->post('religion'),
            'holdings_no' => $this->input->post('holdings_no'),
            'education' => $this->input->post('education'),
            'profession' => $this->input->post('profession')
        );

        $result = $this->data->save($this->personal_table, $value);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    
    
    public function add_temp_family() {

        $value = array(
            
            'father_name' => $this->input->post('father_name'),
            'father_nid' => $this->input->post('father_nid'),
            'mother_name' => $this->input->post('mother_name'),
            'mother_nid' => $this->input->post('mother_nid'),
            'marital_status' => $this->input->post('marital_status'),
            'husband_name' => $this->input->post('husband_name'),
            'husband_nid' => $this->input->post('husband_nid'),
            'wife_name' => $this->input->post('wife_name'),
            'wife_nid' => $this->input->post('wife_nid'),
            'family_member' => $this->input->post('family_member'),
            'family_desc' => $this->input->post('family_desc')
                
        );

        $result = $this->data->save($this->family_table, $value);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
    
    
    public function add_temp_finance() {

        $value = array(
            
            'house_desc' => $this->input->post('house_desc'),
            'land' => $this->input->post('land'),
            'earn_person' => $this->input->post('earn_person'),
            'earn_source' => $this->input->post('earn_source'),
            'monthly_income' => $this->input->post('monthly_income'),
            'cause_poorness' => $this->input->post('cause_poorness')
            
        );

        $result = $this->data->save($this->finance_table, $value);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
    
    
    public function add_temp_interested_service() {

        $value = array(
            
            'curr_service' => $this->input->post('curr_service'),
            'inter_service' => $this->input->post('inter_service'),
            'success_way' => $this->input->post('success_way'),
            'inter_trainings' => $this->input->post('inter_trainings'),
            'reference' => $this->input->post('reference')
            
        );

        $result = $this->data->save($this->interested_service_table, $value);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
    
    

//    public function edit($id) {
//
//        $value = array(
//            'ques' => $this->input->post('ques'),
//            'ansr' => $this->input->post('ansr')
//        );
//
//        $result = $this->data->update($this->table, $id, $value);
//
//        if ($result) {
//            return TRUE;
//        } else {
//            return FALSE;
//        }
//    }

//    // to get member last id  
//    public function get_last_mem_id($table) {
//        $r = $this->db->select('mem_id')->from("$table")->limit(1)->get();
//        return $r->row();
//    }
//
//    // check duplicate
//    public function check_duplicate($data, $field, $table) {
//
//        $s = $this->db->where("$field", $data)->get("$table");
//
//        if ($s->num_rows() > 0) {
//            return false;
//        } else {
//            return true;
//        }
//    }

}
