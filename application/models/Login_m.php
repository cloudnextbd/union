<?php defined('BASEPATH') OR exit('No direct script access allowed');

 class Login_m extends CI_Model {

    function __construct() {

        parent::__construct();

        $this->user_table = 'user';
        
    }

    // *** get user by id 
    public function get_user_by_id($id) {

        return $this->db->select('password')->where('id', $id)->get($this->user_table);
        
    }

    // *** get user by phone 
    public function get_user_by_phone($data) {

        return $this->db->select('id, password, token')->where('phone', $data)->get($this->user_table);

    }

    // *** update user token 
    public function update_user_token($id, $token) {

        $this->db->where('id', $id)->update($this->user_table, array('token' => $token));

    }


}