<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services_m extends CI_Model {

    function __construct() {
        
        parent :: __construct();
        
        $this->services_t = 'services';
        
        $this->app_per_t = 'app_personal';
        
    }

    
    public function get_all_by_serv_id($serv_id) {
        
        $this->db->select('
            
                app_personal.id,
                app_personal.name,
                app_personal.gender,
                app_personal.birth_date,
                app_personal.birth_id,
                app_personal.nid,
                app_personal.mobile,
                app_personal.image,
                app_personal.district,
                app_personal.sub_district,
                app_personal.union,
                app_personal.word_no,
                app_personal.village,
                app_personal.house_name,
                app_personal.holdings_no,
                app_personal.education,
                app_personal.profession,
                app_personal.capability,
                app_personal.disability,
                app_personal.religion,
                app_personal.app_status,
                app_personal.status,
                app_personal.create_by,
                app_personal.create_date
                
            '); 
        
        //  app_interested_service.success_way,
        //  app_interested_service.inter_trainings,
        //  app_interested_service.sp_info
        
        $this->db->from('app_personal');
        
        $this->db->join('app_curr_service', 'app_curr_service.id = app_personal.id', 'left');
        
        //$this->db->join('app_inter_service', 'app_inter_service.id = app_interested_service.inter_ser_id', 'left');
        
        $this->db->where('app_curr_service.curr_service', $serv_id); // app_inter_service.inter_service'
        
        // aproved
        $this->db->where('app_personal.app_status', 2);
        
        // active
        $this->db->where('app_personal.status', 1);
        
        $query = $this->db->get();
        
        return $query->result();
        
    }
        
    

}